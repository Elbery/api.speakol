<?php
use Phalcon\DI\FactoryDefault;
class UsersController extends BaseController {
    public function activateAction($token) {
        $user = Users::findFirst(array("confirmation_token = :confirmation_token:", "bind" => array('confirmation_token' => $token)));
        $this->response->setHeader("Content-Type", "application/json");
        $this->getDI()->set('language', $this->request->get("lang"));
        if ($user == FALSE) {
            $this->response->setStatusCode(404, $this->t->_('not-found'));
            $errors[] = 'Not Found';
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        } else if ($this->_isConfirmationTokenExpired($user->getConfirmationSentAt())) {
            $this->response->setStatusCode(400, $this->t->_('bad-request'));
            $errors[] = 'Bad Request';
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        } else if ($user->setActive(1) && $user->setConfirmationToken('')) {
            $slug = preg_replace("/[^\w]+/", "-", strtolower($user->get_first_name() . $user->get_last_name())) . time();
            $user->setSlug($slug);
            $user->save();
            $new_log = new Log();
            $new_log->log_current_action($user->getId(), $this->headers['Ip'], "/user/confirm_mail/" . $token, $user->getId());
            $this->send_email_after_activation($user);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $user->getRoleId()));
        } else {
            $this->response->setStatusCode(409, $this->t->_('conflict'));
            $errors = array();
            if (isset($user)) {
                foreach ($user->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
            } else {
                $errors[] = 'Not Found';
            }
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors['0']));
        }
        return $this->response;
    }
    public function signinAction() {
        $user = new Users();
        $json_data = json_decode(file_get_contents("php://input"));
        $params = array('email' => $json_data->email, 'password' => $json_data->password, 'role' => $json_data->role,);
        $this->getDI()->set("language", $json_data->lang);
        $return = $user->signIn($params);
        $new_log = new Log();
        $this->response->setHeader("Content-Type", "application/json");
        if ($return['status'] == 'ERROR') {
            $new_log->log_current_action("", $this->headers['Ip'], "/user/signin", "");
            $this->response->setStatusCode($return['code'], 'ERROR');
            unset($return['code']);
            $this->response->setJsonContent($return);
        } else {
            $new_log->log_current_action($return['data']['user']['id'], $this->headers['Ip'], "/user/signin", $return['data']['user']['id']);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent($return);
        }
        return $this->response;
    }
    public function signOutAction() {
        $this->response->setHeader("Content-Type", "application/json");
        $sentHeaders = $this->headers;
        if (empty($sentHeaders['Authorization'])) {
            $this->response->setStatusCode(404, $this->t->_('not-found'));
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $this->t->_('rejected')));
        } else {
            $user_id = Tokens::checkAccess();
            $return = Tokens::findFirstByToken($sentHeaders['Authorization']);
            $return->delete();
            $new_log = new Log();
            $new_log->log_current_action($user_id, $this->headers['Ip'], "/user/signout", $return->getId());
            if ($return) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => true));
            } else {
                $this->response->setStatusCode(404, $this->t->_('not-found'));
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $this->t->_('error-went-wrong')));
            }
        }
        return $this->response;
    }
    public function send_email_after_activation($user) {
        $this->assets->addCss('css/email.css');
        $view = clone $this->view;
        $this->view->start();
        $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
        $this->view->lang = $user->getLocale();
        if ($user->getRoleId() == Roles::APP) {
            $app = Apps::findFirst(array('publisher_id = :id:', 'bind' => array('id' => $user->getId())));
            $this->view->app = $app->toArray();
            $this->view->update = "http://plugins.speakol.com/apps/settings";
            $this->view->render('emails', 'app-activated');
        } else {
            $this->view->update = "http://www.speakol.com/users/update";
            $this->view->render('emails', 'activated');
        }
        $this->view->finish();
        $content = $this->view->getContent();
        if ($user->getLocale() == "ar") {
            $title = $this->t->_('welcome-to');
        } else {
            $title = $this->t->_('welcome-to') . " Speakol, " . $this->view->name;
        }
        $message = array('html' => $content, 'subject' => $title, 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
        $async = false;
        $ip_pool = 'Main Pool';
        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
    }
    public function get_email_status() {
        $this->response->setHeader("Content-Type", "application/json");
        $json_data = json_decode(file_get_contents("php://input"));
        $email = trim($json_data->email);
        if ($email == "") {
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid email'));
            return $this->response;
        }
        $user = Users::findFirst("email='" . $email . "'");
        $this->response->setStatusCode(200, 'OK');
        if ($user) {
            if ($user->getRoleId() == Roles::APP || $user->getRoleId() == Roles::ADMIN) {
                $this->response->setJsonContent(array('status' => 'OK', 'data' => "1"));
            } else if ($user->getEncryptedPassword() == NULL && $user->getActive()) {
                $this->response->setJsonContent(array('status' => 'OK', 'data' => "3"));
            } else if ($user->getActive()) {
                $this->response->setJsonContent(array('status' => 'OK', 'data' => "2"));
            } else {
                $this->response->setJsonContent(array('status' => 'OK', 'data' => "4"));
            }
        } else {
            $this->response->setJsonContent(array('status' => 'OK', 'data' => "4"));
        }
        return $this->response;
    }
    public function social_connect_action() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $user = new Users();
            $json_request = json_decode(file_get_contents('php://input'));
            $access_token = $json_request->access_token;
            $connect_type = $json_request->connect_type;
            $user_data = $user->authenticate_user($access_token, $connect_type);
            $new_log = new Log();
            $new_log->log_current_action($user_data['data']['user']['id'], $this->headers['Ip'], "/user/social/connect $connect_type", $user_data['data']['user']['id']);
            if ($user_data['code'] == "200") {
                $user_access_token = new SocialAccessToken();
                $user_access_token->set_access_token($access_token);
                $user_access_token->set_social_type($connect_type);
                $user_access_token->set_login_date(date('Y-m-d H:i:s'));
                $user_access_token->set_user_id($user_data['data']['user']['id']);
                $user_access_token->save();
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $user_data['data']));
            } else {
                $failed_token = new FailedSocialAccessToken();
                $failed_token->set_social_type($connect_type);
                $failed_token->set_access_token($access_token);
                $failed_token->set_login_date(date('Y-m-d H:i:s'));
                $failed_token->save();
                $this->response->setStatusCode($user_data['code'], "ERROR");
                $this->response->setJsonContent(array('status' => "ERROR", 'message' => $user_data['message']));
            }
        }
        catch(Exception $ex) {
            $failed_token = new FailedSocialAccessTokens();
            $failed_token->set_social_type($connect_type);
            $failed_token->set_access_token($access_token);
            $failed_token->set_login_date(date('Y-m-d H:i:s'));
            $failed_token->save();
            $this->response->setStatusCode(600, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function activate_publisher() {
        $this->response->setHeader("Content-Type", "application/json");
        $json_data = json_decode(file_get_contents("php://input"));
        $this->getDI()->set('language', $json_data->lang);
        $token = $json_data->token;
        if ($token == "") {
            $this->response->setStatusCode(602, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid token'));
            return $this->response;
        }
        $user = Users::findFirst("confirmation_token='" . $token . "'");
        if ($user) {
            $new_log = new Log();
            $new_log->log_current_action($user->getId(), $this->headers['Ip'], "/user/activatepublisher", $user->getId());
            if (!$this->_isConfirmationTokenExpired($user->getConfirmationSentAt()) && $user->getConfirmationToken() != '') {
                $user->setActive(1);
                $user->setConfirmationToken('');
                $user->setRoleId(Roles::APP);
                $app = Apps::findFirst('publisher_id=' . $user->getId());
                $app->set_subscription_date(date('Y-m-d H:i:s'));
                $app->setVerified(1);
                if ($app->get_plan() !== 1) {
                    $plan = Plan::findFirst($app->get_plan());
                    $amount = $plan->get_amount();
                    $user_card = UserCard::findFirst('app_id=' . $app->getId());
                    $charging_response = BillingController::charge_user($user_card, $amount, "Upgrade from free to " . $plan->get_name());
                    if ($charging_response['status'] == 'ERROR') {
                        $user_card->set_valid(0);
                        $user_card->save();
                        $app->set_plan(1);
                        $app->save();
                        $user->save();
                        $this->send_email_after_activation($user);
                        $this->response->setStatusCode(600, 'ERROR');
                        $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $charging_response['message']));
                        return $this->response;
                    }
                    $app->save();
                    $user->save();
                    $billing = new BillingController();
                    $billing->create_invoice_receipt($amount, $user_card->get_payment_method(), $user_card->get_number(), $user->getId(), $user->getEmail(), $charging_response['data']->id, $plan->get_name(), $user->get_first_name() . " " . $user->get_last_name(), $app->get_address(), $app->getWebsite());
                }
                $app->save();
                $user->save();
                $this->send_email_after_activation($user);
                $this->response->setStatusCode(200, 'OK');
                $this->response->setJsonContent(array('status' => 'OK'));
                return $this->response;
            }
        }
        $this->response->setStatusCode(404, 'Not found');
        $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
        return $this->response;
    }
    public function create_account() {
        $this->response->setHeader("Content-Type", "application/json");
        $json_request_data = json_decode(file_get_contents("php://input"));
        $email = trim($json_request_data->email);
        $password = $json_request_data->password;
        $first_name = trim($json_request_data->first_name);
        $last_name = trim($json_request_data->last_name);
        $redirect_url = $json_request_data->redirect_url;
        $plugin_url = $json_request_data->plugin_url;
        $image = $json_request_data->image;
        $new_log = new Log();
        $this->getDI()->set('language', $json_request_data->lang);
        $gender = NULL;
        if (isset($json_request_data->gender)) {
            $gender = intval($json_request_data->gender);
        }
        $valid_fname = preg_match("/^[\p{L}\p{N}]+$/u", $first_name);
        $valid_lname = preg_match("/^[\p{L}\p{N}]+$/u", $last_name);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $log_msg = "{/user/create/account,create speakol account with invalid email,response message:Invalid email $email,status:failed}";
            $new_log->log_current_action('', $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(400, "Bad request");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid email'));
        } else if (strlen($password) < 8 || strlen($password) > 128) {
            $log_msg = "{/user/create/account,create speakol account with invalid password length,response message:Invalid password,status:failed}";
            $new_log->log_current_action('', $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(400, "Bad request");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid password'));
        } else if (empty($first_name)) {
            $log_msg = "{/user/create/account,create speakol account with invalid first name,response message:Invalid name $first_name,status:failed}";
            $new_log->log_current_action('', $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(400, "Bad request");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-name")));
        } else if (!$valid_fname || !$valid_lname) {
            $log_msg = "{/user/create/account,create speakol account with invalid $first_name or $last_name,response message:Invalid name,status:failed}";
            $new_log->log_current_action('', $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(400, "Bad request");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-name")));
        } else if ($gender === NULL || ($gender !== 1 && $gender !== 0)) {
            $log_msg = "{/user/create/account,create speakol account with invalid gender $gender,response message:Invalid gender,status:failed}";
            $new_log->log_current_action('', $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(400, "Bad request");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid gender'));
        } else {
            $email_exist = Users::findFirstByEmail($email);
            if ($email_exist && $email_exist->getActive()) {
                $log_msg = "{/user/create/account,create speakol account with email already exists $email,response message:Email already exists,status:failed}";
                $new_log->log_current_action('', $this->headers['Ip'], $log_msg, 0);
                $this->response->setStatusCode(400, "Bad request");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_('apps-email-exist')));
            } else {
                $user = new Users();
                if ($email_exist) {
                    $app_exist = Apps::findFirst('publisher_id=' . $email_exist->getId());
                    if ($app_exist) {
                        $app_exist->delete();
                    }
                    $email_exist->delete();
                }
                if ($image) {
                    $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
                    $arrUploadedPhotos = $objImgUploader->uploadImage($objImgUploader::DIR_USERS, $image);
                    $image = $arrUploadedPhotos[0]['path'] . $arrUploadedPhotos[0]['new'];
                    $user->setProfilePicture($image);
                }
                $user->set_first_name($first_name);
                $user->set_last_name($last_name);
                $user->setEncryptedPassword($password, true);
                $user->setEmail($email);
                $user->setGender($gender);
                $user->setActive(0);
                $user->setConfirmationToken($user->generateConfirmationToken($user->getEmail()));
                $user->setConfirmationSentAt(date('y-m-d h:i:s'));
                $user->setRoleId(Roles::USER);
                $user->setLocale($json_request_data->lang);
                $user->set_newsletter(1);
                $user->set_subscribe(1);
                $user->set_init_reg("speakol");
                $user->save();
                $log_msg = "{/user/create/account,create speakol account ,response message:ok,status:ok}";
                $new_log->log_current_action($user->getId(), $this->headers['Ip'], $log_msg, $user->getId());
                $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
                $this->view->lang = $json_request_data->lang;
                $redirect_url = $redirect_url . '/' . $user->getConfirmationToken();
                $redirect_url = $redirect_url . "?redirect_url=http://www.speakol.com/newsfeed";
                $this->view->update = "http://www.speakol.com/users/update";
                $this->view->url = $redirect_url;
                $this->assets->addCss('css/email.css');
                $view = clone $this->view;
                $view->start();
                $view->render('emails', 'app-signup');
                $view->finish();
                $content = $view->getContent();
                $message = array('html' => $content, 'subject' => $this->t->_('welcome'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
                $async = false;
                $ip_pool = 'Main Pool';
                $result = $this->mandrill->messages->send($message, $async, $ip_pool);
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => "OK", 'data' => 'User registered successfully'));
            }
        }
        return $this->response;
    }
    public function users_status_action() {
        $this->response->setHeader("Content-Type", "application/json");
        $sentHeaders = $this->headers;
        $sides = $this->request->get('sides');
        for ($i = 0;$i < sizeof($sides);$i++) {
            if ($sides[$i] == '') {
                $this->response->setStatusCode(404, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => "One of sides ids is empty"));
                return $this->response;
            }
        }
        $users = Beacon::get_users($this->request->get('sides'), $this->request->get('argument_id'));
        if ($users['status'] == "ok") {
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $users));
        } else {
            $this->response->setStatusCode(404, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $users['message']));
        }
        return $this->response;
    }
    public function activeuserAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $sentHeaders = $this->headers;
            if (empty($sentHeaders['Authorization'])) {
                $this->response->setStatusCode(404, $this->t->_('not-found'));
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $this->t->_('rejected')));
            } else {
                $page = $this->request->get('page', 'int', 1);
                $perPage = $this->request->get('per_page', 'int', 10);
                $feeds = new Feeds();
                $return = $feeds->getActiveUsers(false, false, $page, $perPage);
                if ($return) {
                    $this->response->setStatusCode(200, "OK");
                    $this->response->setJsonContent(array('status' => 'OK', 'data' => $return), JSON_PRETTY_PRINT);
                } else {
                    $this->response->setStatusCode(404, $this->t->_('not-found'));
                    $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $this->t->_('error-went-wrong')));
                }
            }
        }
        catch(Exception $ex) {
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function getAppUsersAction($appId) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $this->response->setStatusCode(200, "OK");
            $appAdmin = AppAdmins::find(array('app_id = :app_id:', 'bind' => array('app_id' => $appId)));
            $users = array();
            if ($appAdmin->toArray()) {
                foreach ($appAdmin as $user) {
                    $tmp = $user->getUsers(array('columns' => array('name', 'slug', 'id', 'profile_picture', 'role_id')));
                    $tmp['super_admin'] = $user->getSuperAdmin();
                    $users[] = $tmp;
                }
            }
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $users), JSON_PRETTY_PRINT);
        }
        catch(Exception $ex) {
            $this->response->setStatusCode(404, $this->t->_('not-found'));
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function linkadminAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $sentHeaders = $this->headers;
            if (empty($sentHeaders['Authorization'])) {
                $this->response->setStatusCode(404, $this->t->_('not-found'));
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $this->t->_('rejected')));
            } else {
                $userId = Tokens::checkAccess();
                if (!$userId) {
                    throw new Exception('You must be logged in!');
                } else {
                    $appId = AppAdmins::findFirstByUserId($userId)->getAppId();
                    $newAdminEmail = $this->request->get('email');
                    $is_admin = $this->request->get('is_admin', 'int', 0);
                    if (!$newAdminEmail) {
                        throw new Exception('Email is missing!');
                    }
                    $newAdmin = Users::findFirst(array('email = :email:', 'bind' => array('email' => $newAdminEmail)));
                    if ($newAdmin) {
                        $checkIfExist = AppAdmins::findFirst(array('user_id = :user_id: and app_id = :app_id: ', 'bind' => array('user_id' => $newAdmin->getId(), 'app_id' => $appId)));
                        if ($checkIfExist) {
                            if ($checkIfExist->getSuperAdmin() == $is_admin) {
                                throw new Exception('The selected user is already a moderator!');
                            } else {
                                $checkIfExist->setSuperAdmin($is_admin ? 1 : 0);
                                $checkIfExist->update();
                                $userType = $is_admin ? 'Administrator' : 'Moderator';
                                $this->response->setStatusCode(200, "OK");
                                $this->response->setJsonContent(array('status' => 'OK', 'data' => 'User role updated to ' . $userType . ' successfully!'), JSON_PRETTY_PRINT);
                            }
                        } else {
                            $appsAdmin = new AppAdmins();
                            $appsAdmin->setAppId($appId);
                            $appsAdmin->setUserId($newAdmin->getId());
                            $appsAdmin->setSuperAdmin($is_admin ? 1 : 0);
                            $save = $appsAdmin->save();
                            $userType = $is_admin ? 'Administrator' : 'Moderator';
                            $this->response->setStatusCode(200, "OK");
                            $this->response->setJsonContent(array('status' => 'OK', 'data' => "User added as an $userType"), JSON_PRETTY_PRINT);
                        }
                    } else {
                        $this->response->setStatusCode(404, $this->t->_('not-found'));
                        $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => "Email doesn't exist"));
                    }
                }
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode(404, $this->t->_('not-found'));
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function removeadminAction($moderator_id) {
        try {
            $app_id = $this->request->get('app_id');
            $this->response->setHeader("Content-Type", "application/json");
            $userId = Tokens::checkAccess();
            if (!$userId) {
                throw new Exception('You must be logged in!');
            }
            $isAdmin = AppAdmins::findFirst(array('user_id = :user_id: and app_id = :app_id:', 'bind' => array('user_id' => $userId, 'app_id' => $app_id)));
            if (!$moderator_id) {
                throw new Exception('Admin id is missing!');
            }
            if ($isAdmin) {
                $checkIfExist = AppAdmins::findFirst(array('user_id = :user_id: and app_id = :app_id: ', 'bind' => array('user_id' => $moderator_id, 'app_id' => $app_id)));
                if ($checkIfExist) {
                    $checkIfExist->delete();
                    $this->response->setStatusCode(200, "OK");
                    $this->response->setJsonContent(array('status' => 'OK', 'data' => 'User removed successfully!'), JSON_PRETTY_PRINT);
                    return $this->response;
                }
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => "Wrong user"), JSON_PRETTY_PRINT);
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => 'User removed successfully!'), JSON_PRETTY_PRINT);
                return $this->response;
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode(404, $this->t->_('not-found'));
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function showAction() {
        $this->response->setHeader("Content-Type", "application/json");
        $signedInUserID = Tokens::checkAccess();
        $user = new Users();
        $arrUser = $user->currentUser($signedInUserID);
        if ($arrUser != FALSE) {
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrUser['data']));
        } else {
            $this->response->setStatusCode(404, $this->t->_('not-found'));
            $errors = array();
            $errors[] = 'Not Found';
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function deleteAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        $signedInUserID = Tokens::checkAccess();
        $user = new Users();
        $arrUser = $user->deleteUser($slug);
        if ($arrUser != FALSE) {
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrUser));
        } else {
            $this->response->setStatusCode(404, $this->t->_('not-found'));
            $errors = array();
            $errors[] = 'Not Found';
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function forgetPasswordAction($email) {
        $redirectUrl = $this->request->get('redirect_url');
        $user = Users::findFirst(array("email = :email:  AND active=1 AND encrypted_password IS NOT null", 'bind' => array('email' => $email,)));
        $this->response->setHeader("Content-Type", "application/json");
        if ($user != FALSE) {
            $this->getDI()->set('language', $user->getLocale());
            $reset_token = sha1(uniqid($email, true));
            $user->setResetPasswordToken($reset_token);
            $user->setResetPasswordSentAt(date("Y-m-d H:i:s"));
            if ($user->save()) {
                $this->view->redirectUrl = $redirectUrl . $reset_token;
                $this->view->user = $user->toArray();
                $this->assets->addCss('css/email.css');
                if ($user->getRoleId() == Roles::USER) {
                    $this->view->update = "http://www.speakol.com/users/update";
                } else {
                    $this->view->update = "http://plugins.speakol.com/apps/settings";
                }
                $view = clone $this->view;
                $view->start();
                $view->render('emails', 'forget-password');
                $view->finish();
                $content = $view->getContent();
                $message = array('html' => $content, 'subject' => $this->t->_('mail-title-reset-password'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
                $async = false;
                $ip_pool = 'Main Pool';
                $result = $this->mandrill->messages->send($message, $async, $ip_pool);
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $result['0']['status']));
            } else {
                $this->response->setStatusCode(409, $this->t->_('conflict'));
                $errors = array();
                foreach ($user->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
            }
        } else {
            $this->response->setStatusCode(404, $this->t->_('not-found'));
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => 'Not Found'));
        }
        return $this->response;
    }
    public function resetPasswordAction($token) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $this->getDI()->set('language', $this->request->get("lang"));
            $step = $this->request->get('step', 'int', false);
            $password = $this->request->get('password', 'string', false);
            $args = array("reset_password_token = :reset_password_token: AND active = '1'", "bind" => array('reset_password_token' => $token),);
            if ($step && $step == 1) {
                $args['columns'] = array('id', 'name', 'slug', 'email', 'profile_picture', 'reset_password_sent_at');
            }
            $user = Users::findFirst($args);
            if ($step && $step == 1) {
                $expireKey = $user->reset_password_sent_at;
            } else {
                $expireKey = $user->getResetPasswordSentAt();
            }
            if ($user == FALSE) {
                throw new Exception("User not found!", 404);
            }
            if ($this->_isResetTokenExpired($expireKey)) {
                throw new Exception("Verification key is expired!", 400);
            }
            if ($step && $step == 1) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $user->toArray()));
                return $this->response;
            }
            if (strlen($password) < 8) {
                throw new Exception("Invalid password length!", 400);
            }
            $user->setEncryptedPassword($password);
            $user->setResetPasswordToken('');
            $user->setResetPasswordSentAt('');
            if ($user->update()) {
                if ($user->getRoleId() == Roles::ADMIN || $user->getRoleId() == Roles::APP) {
                    $this->view->update = "http://plugins.speakol.com/apps/settings";
                } else {
                    $this->view->update = "http://www.speakol.com/users/update";
                }
                $this->view->user = $user->toArray();
                $view = clone $this->view;
                $view->start();
                $view->render('emails', 'password-reset');
                $view->finish();
                $content = $view->getContent();
                $message = array('html' => $content, 'subject' => $this->t->_('mail-title-password-reset'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
                $async = false;
                $ip_pool = 'Main Pool';
                $result = $this->mandrill->messages->send($message, $async, $ip_pool);
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $user->getId()));
            } else {
                $this->response->setStatusCode(409, $this->t->_('conflict'));
                $errors = array();
                foreach ($user->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function resendActivationAction() {
        $userEmail = $this->request->get('email');
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $user = Users::findFirst(array("email = :email: AND (active = '0' OR  active is NULL ) ", 'bind' => array('email' => $userEmail)));
            $confirmationToken = $user ? $user->getConfirmationToken() : false;
            if (empty($confirmationToken)) {
                $userModel = new Users();
                $user->setConfirmationToken($userModel->generateConfirmationToken($user->getEmail()));
                $user->update();
            }
            $this->view->redirectUrl = $this->request->get('redirect_url');
            if ($user == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if ($this->_isConfirmationTokenExpired($user->getConfirmationSentAt())) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->view->user = $user->toArray();
            $this->assets->addCss('css/email.css');
            $view = clone $this->view;
            $view->start();
            $view->render('emails', 'signup');
            $view->finish();
            $content = $view->getContent();
            $message = array('html' => $content, 'subject' => $this->t->_('mail-title-verifiy'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
            $async = false;
            $ip_pool = 'Main Pool';
            $result = $this->mandrill->messages->send($message, $async, $ip_pool);
            foreach ($result as $itemRes) {
                if ($itemRes['status'] != 'sent') {
                    $errors[] = $itemRes['reject_reason'];
                }
            }
            if (count($errors) > 0) {
                $this->response->setStatusCode(503, "Service Unavailable");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $user->getId()));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    private function _isResetTokenExpired($_datetime) {
        return (time() - strtotime($_datetime) > 2 * 24 * 60 * 60);
    }
    private function _isConfirmationTokenExpired($_datetime) {
        return (time() - strtotime($_datetime) > 2 * 24 * 60 * 60);
    }
    public function deactivateAction() {
        $this->response->setHeader("Content-Type", "application/json");
        $request = $this->request->getJsonRawBody();
        $user = new Users();
        if ($user->userDeactivate($request->user->slug)) {
            $this->response->setStatusCode(201, 'Created');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'Deactivated'));
        } else {
            $errors[] = $this->t->_('conflict');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
            $this->response->setStatusCode(401, 'Error');
        }
        return $this->response;
    }
    public function updateTookTourAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $signedInUserID = Tokens::checkAccess();
            if ($signedInUserID) {
                $current_user = Users::findFirst($signedInUserID);
                $current_user->setTookTour(1);
                $current_user->save();
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('data' => "success"));
            } else {
                $this->response->setStatusCode(404, "ERROR");
                $this->response->setJsonContent(array('messages' => "Unauthorized"));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(404, "ERROR");
            $this->response->setJsonContent(array('messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function check_user_activation_action() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $user_id = Tokens::checkAccess();
            if ($user_id) {
                $user_record = Users::findFirst($user_id);
                if ($user_record->getActive() != null) {
                    $user_active = 1;
                } else {
                    $user_active = 0;
                }
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => "SUCCESS", 'data' => $user_active));
            } else {
                $this->response->setStatusCode(401, "Unauthorized");
                $this->response->setJsonContent(array('messages' => "Unauthorized"));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(404, "ERROR");
            $this->response->setJsonContent(array('messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function add_favorite_category() {
        $json_data = json_decode(file_get_contents("php://input"));
        $category = intval($json_data->category_id);
        $category_exist = Categories::findFirst($category);
        if (!$category_exist) {
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "Category id doesn't exist"));
        } else {
            $user_id = Tokens::checkAccess();
            $favorite_exist = FavoriteCategories::findFirst('user_id=' . $user_id . ' AND category_id=' . $category);
            if (!$favorite_exist) {
                $favorite_cat = new FavoriteCategories();
                $favorite_cat->set_user_id($user_id);
                $favorite_cat->set_category_id($category);
                $favorite_cat->save();
                $this->response->setStatusCode(200, 'OK');
                $this->response->setJsonContent(array('status' => 'OK'));
            } else {
                $this->response->setStatusCode(302, 'Found');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Already exists'));
            }
        }
        return $this->response;
    }
    public function remove_favorite_category() {
        $category = $this->request->get('category_id');
        $user_id = Tokens::checkAccess();
        $favorite_exist = FavoriteCategories::findFirst('user_id=' . $user_id . ' AND category_id=' . $category);
        if ($favorite_exist) {
            $favorite_exist->delete();
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK'));
        } else {
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
        }
        return $this->response;
    }
    public function show_profile() {
        $user_id = Tokens::checkAccess();
        if (!$user_id) {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            return $this->response;
        }
        $user = Users::findFirst($user_id);
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => array('first_name' => $user->get_first_name(), 'last_name' => $user->get_last_name(), 'email' => $user->getEmail(), 'image' => $user->getProfilePicture(), 'subscription' => $user->get_subscribe(), 'newsletter' => $user->get_newsletter())));
        return $this->response;
    }
    public function edit_profile() {
        $user_id = Tokens::checkAccess();
        if (!$user_id) {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            return $this->response;
        }
        $user = Users::findFirst($user_id);
        $json_data = json_decode(file_get_contents("php://input"));
        $first_name = trim($json_data->first_name);
        $last_name = trim($json_data->last_name);
        $json_data->email = trim($json_data->email);
        $image = $json_data->image;
        $this->getDI()->set('language', $user->getLocale());
        if (empty($first_name)) {
            $this->response->setStatusCode(400, "Bad request");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid name'));
            return $this->response;
        }
        if ($image) {
            $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
            $arrUploadedPhotos = $objImgUploader->uploadImage($objImgUploader::DIR_USERS, $image);
            $image = $arrUploadedPhotos[0]['path'] . $arrUploadedPhotos[0]['new'];
            $user->setProfilePicture($image);
        }
        if ($json_data->new_password) {
            if ($user->getEncryptedPassword() != NULL) {
                $flag = \Phalcon\DI::getDefault()->get('security')->checkHash($json_data->old_password, $user->getEncryptedPassword());
                if (!$flag) {
                    $this->response->setStatusCode(601, 'ERROR');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Wrong password'));
                    return $this->response;
                }
            }
            if (strlen($json_data->new_password) < 8 || strlen($json_data->new_password) > 128) {
                $this->response->setStatusCode(400, "Bad request");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid password'));
                return $this->response;
            }
            $user->setEncryptedPassword($json_data->new_password);
        }
        $user->set_first_name($first_name);
        $user->set_last_name($last_name);
        $user->set_newsletter($json_data->newsletter);
        $user->set_subscribe($json_data->subscribe);
        $user->save();
        if ($json_data->email) {
            if (!filter_var($json_data->email, FILTER_VALIDATE_EMAIL)) {
                $this->response->setStatusCode(400, "Bad request");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid email'));
                return $this->response;
            } else if (Users::findFirst("email='" . $json_data->email . "'")) {
                $this->response->setStatusCode(400, "Bad request");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Email already exists'));
                return $this->response;
            }
            $pending = new PendingEmail();
            $pending->set_email($json_data->email);
            $pending->set_user_id($user->getId());
            $pending->set_confirmation_token($user->generateConfirmationToken($json_data->email));
            $pending->set_created_at(date('y-m-d h:i:s'));
            $pending->save();
            $url = $json_data->url . '/' . $pending->get_confirmation_token();
            $this->view->url = $url;
            $this->view->lang = $user->getLocale();
            $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
            $this->view->update = "http://www.speakol.com/users/update";
            $this->assets->addCss('css/email.css');
            $view = clone $this->view;
            $view->start();
            $view->render('emails', 'edit_profile');
            $view->finish();
            $content = $view->getContent();
            $message = array('html' => $content, 'subject' => $this->t->_('welcome'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $json_data->email, 'type' => 'to')),);
            $async = false;
            $ip_pool = 'Main Pool';
            $result = $this->mandrill->messages->send($message, $async, $ip_pool);
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => "Updated successfully"));
        return $this->response;
    }
    public function update_email_activation() {
        $json_data = json_decode(file_get_contents("php://input"));
        if ($json_data->token) {
            $pending = PendingEmail::findFirst("confirmation_token='" . $json_data->token . "'");
            if ($pending) {
                $user = Users::findFirst($pending->get_user_id());
                if ($user) {
                    $user->setEmail($pending->get_email());
                    $user->save();
                    $pending->delete();
                    $this->response->setStatusCode(200, 'OK');
                    $this->response->setJsonContent(array('status' => 'OK', 'message' => 'Updated successfully'));
                } else {
                    $this->response->setStatusCode(404, 'ERROR');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
                }
            } else {
                $this->response->setStatusCode(404, 'ERROR');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
            }
        } else {
            $this->response->setStatusCode(601, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid token'));
        }
        return $this->response;
    }
    public function send_user_newsletter($lang) {
        $curr_date = new DateTime();
        $email_log = new SendingEmailsLog();
        $email_log->set_email_type("/user/newsletter/optional");
        $email_log->set_date($curr_date->format('Y-m-d H:i:s'));
        $email_log->save();
        $day = intval(date('d'));
        if ($day === 1) {
            $users = Users::find("active=1 AND role_id=4 AND newsletter=2 AND locale='$lang'");
            $creation_date = new DateTime('30 days ago');
            $creation_date = $creation_date->format('Y-m-d');
        } else {
            $users = Users::find("active=1 AND role_id=4 AND newsletter=1 AND locale='$lang'");
            $creation_date = new DateTime('7 days ago');
            $creation_date = $creation_date->format('Y-m-d');
        }
        function cmp($a, $b) {
            return ($a['interactions'] < $b['interactions']);
        }
        $plugins = $this->most_interacted_plugins(0, $creation_date);
        usort($plugins, "cmp");
        $plugins = array_slice($plugins, 0, 3);
        $plugins = $this->get_plugins_data($plugins);
        for ($i = 0;$i < sizeof($plugins);$i++) {
            if ($plugins[$i]['type'] == "debate" || $plugins[$i]['type'] == "argumentbox") {
                if ($plugins[$i]['data']['lang'] == "ar") {
                    $path = $this->generate_imagick_image($plugins[$i]['sides'][0]['perc'], $plugins[$i]['sides'][0]['votes'], $plugins[$i]['sides'][1]['perc'], $plugins[$i]['sides'][1]['votes'], $plugins[$i]['lang']);
                    $temp = $plugins[$i]['sides'][1];
                    $plugins[$i]['sides'][1] = $plugins[$i]['sides'][0];
                    $plugins[$i]['sides'][0] = $temp;
                } else {
                    $path = $this->generate_imagick_image($plugins[$i]['sides'][1]['perc'], $plugins[$i]['sides'][1]['votes'], $plugins[$i]['sides'][0]['perc'], $plugins[$i]['sides'][0]['votes'], $plugins[$i]['lang']);
                }
                $plugins[$i]['img'] = $path;
            } else {
                $no_sides = sizeof($plugins[$i]['sides']);
                $has_image = ($plugins[$i]['sides'][0]['side_data']['image'] != "") ? true : false;
                for ($j = 0;$j < $no_sides;$j++) {
                    $path = $this->draw_bar($plugins[$i]['sides'][$j]['perc'], $plugins[$i]['data']['align'], $no_sides, $plugins[$i]['data']['lang'], $has_image);
                    $plugins[$i]['sides'][$j]['vote_img'] = $path;
                }
            }
        }
        foreach ($users as $user) {
            $this->getDI()->set('language', $user->getLocale());
            $this->view->lang = $user->getLocale();
            $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
            $this->view->plugins = $plugins;
            $this->view->update = "http://www.speakol.com/users/update";
            $this->view->logo = "top";
            $this->view->start();
            $this->view->render('emails', 'user_optional_newsletter');
            $this->view->finish();
            $content = $this->view->getContent();
            $message = array('html' => $content, 'subject' => $this->t->_('email14-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
            $async = false;
            $ip_pool = 'Main Pool';
            $user_email_log = UserEmailsLog::findFirst(array("user_id=" . $user->getId() . " AND email_type='/user/newsletter/optional'", "order" => "id desc"));
            if (!$user_email_log) {
                $user_email_log = new UserEmailsLog();
                $user_email_log->set_user_id($user->getId());
                $user_email_log->set_email_type("/user/newsletter/optional");
                $user_email_log->set_last_date($curr_date->format('Y-m-d'));
                $user_email_log->save();
                $result = $this->mandrill->messages->send($message, $async, $ip_pool);
            } else {
                if ($user->get_newsletter() === 1) {
                    $sending_date = new DateTime($user_email_log->get_last_date());
                    $date_after_week = $sending_date->modify('7 days');
                    if ($date_after_week <= $curr_date) {
                        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
                        $user_email_log = new UserEmailsLog();
                        $user_email_log->set_user_id($user->getId());
                        $user_email_log->set_email_type("/user/newsletter/optional");
                        $user_email_log->set_last_date($curr_date->format('Y-m-d'));
                        $user_email_log->save();
                    }
                }
            }
        }
        $email_log = SendingEmailsLog::findFirst(array("email_type='/user/newsletter/optional'", 'order' => 'id desc'));
        $email_log->set_end_date($curr_date->format('Y-m-d'));
        $email_log->save();
    }
    public function user_followup($lang) {
        function cmp($a, $b) {
            return ($a['interactions'] < $b['interactions']);
        }
        $curr_date = new DateTime();
        $creation_date = new DateTime('30 days ago');
        $creation_date = $creation_date->format('Y-m-d');
        $curr_date = $curr_date->format('Y-m-d H:i:s');
        $email_log = new SendingEmailsLog();
        $email_log->set_email_type("/user/inactive/followup");
        $email_log->set_date($curr_date);
        $email_log->save();
        $plugins = $this->most_interacted_plugins(0, $creation_date);
        usort($plugins, "cmp");
        $plugins = array_slice($plugins, 0, 3);
        $plugins = $this->get_plugins_data($plugins);
        for ($i = 0;$i < sizeof($plugins);$i++) {
            if ($plugins[$i]['type'] == "debate" || $plugins[$i]['type'] == "argumentbox") {
                if ($plugins[$i]['data']['lang'] == "ar") {
                    $path = $this->generate_imagick_image($plugins[$i]['sides'][0]['perc'], $plugins[$i]['sides'][0]['votes'], $plugins[$i]['sides'][1]['perc'], $plugins[$i]['sides'][1]['votes'], $plugins[$i]['lang']);
                    $temp = $plugins[$i]['sides'][1];
                    $plugins[$i]['sides'][1] = $plugins[$i]['sides'][0];
                    $plugins[$i]['sides'][0] = $temp;
                } else {
                    $path = $this->generate_imagick_image($plugins[$i]['sides'][1]['perc'], $plugins[$i]['sides'][1]['votes'], $plugins[$i]['sides'][0]['perc'], $plugins[$i]['sides'][0]['votes'], $plugins[$i]['lang']);
                }
                $plugins[$i]['img'] = $path;
            } else {
                $no_sides = sizeof($plugins[$i]['sides']);
                $has_image = ($plugins[$i]['sides'][0]['side_data']['image'] != "") ? true : false;
                for ($j = 0;$j < $no_sides;$j++) {
                    $path = $this->draw_bar($plugins[$i]['sides'][$j]['perc'], $plugins[$i]['data']['align'], $no_sides, $plugins[$i]['data']['lang'], $has_image);
                    $plugins[$i]['sides'][$j]['vote_img'] = $path;
                }
            }
        }
        error_log("heeelo ");
        $users = Users::find("active=1 AND role_id=4 AND locale='$lang'");
        foreach ($users as $user) {
            error_log($user->getEmail());
            $inactive = false;
            $user_email_log = UserEmailsLog::findFirst(array("user_id=" . $user->getId() . " AND email_type='/user/inactive/followup'", 'order' => "id desc"));
            $send_email = true;
            if ($user_email_log) {
                $sending_date = new DateTime($user_email_log->get_last_date());
                $diff = date_diff(new DateTime(), $sending_date);
                $days = $diff->format('%a');
                if (intval($days) >= 15) {
                    $send_email = true;
                } else {
                    $send_email = false;
                }
            }
            if (!$send_email) {
                continue;
            }
            $log = Log::findFirst(array('conditions' => "user_id=" . $user->getId() . " AND operation_type='/user/signin'", 'order' => "id desc"));
            if ($log) {
                $last_date = date($log->get_created_at());
                $date = new DateTime('15 days ago');
                $date = $date->format('Y-m-d');
                if ($last_date < $date) {
                    $inactive = true;
                }
            }
            if (!$log || $inactive) {
                $this->getDI()->set('language', $user->getLocale());
                $this->view->lang = $user->getLocale();
                $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
                $this->view->plugins = $plugins;
                $this->view->update = "http://www.speakol.com/users/update";
                $this->view->logo = "top";
                $this->view->start();
                $this->view->render('emails', 'where_have_you_been');
                $this->view->finish();
                $content = $this->view->getContent();
                $message = array('html' => $content, 'subject' => $this->t->_('email15-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
                $async = false;
                $ip_pool = 'Main Pool';
                $result = $this->mandrill->messages->send($message, $async, $ip_pool);
                $user_email_log = new UserEmailsLog();
                $user_email_log->set_user_id($user->getId());
                $user_email_log->set_email_type("/user/inactive/followup");
                $user_email_log->set_last_date($curr_date);
                $user_email_log->save();
            }
        }
        $email_log = SendingEmailsLog::findFirst(array("email_type='/user/inactive/followup'", 'order' => 'id desc'));
        $email_log->set_end_date(date('Y-m-d H:i:s'));
        $email_log->save();
    }
}
