<?php
ini_set('html_errors', false);
class StoryController extends BaseController {
    public function indexAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $storyModels = new Stories();
            $return = $storyModels->listStories();
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function showAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $storyModels = new Stories();
            $return = $storyModels->retriveStory($slug, 6);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function createAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $storyModels = new Stories();
            $data = array('title' => $this->request->get('title'), 'content' => $this->request->get('content'), 'featured' => $this->request->get('featured'), 'category_id' => $this->request->get('category_id'), 'app_id' => $this->request->get('app_id'), 'foreign_id' => $this->request->get('foreign_id'), 'tags' => $this->request->get('tags'), 'url' => $this->request->get('url'));
            $validate = $storyModels->validateEmptyFields(Stories::$createStories, $data);
            if (isset($validate['status']) && $validate['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($validate['messages'])));
                return $this->response;
            }
            $return = $storyModels->appendDataToModel($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function deleteAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $storyModels = new Stories();
            $data = array('slug' => $slug);
            $return = $storyModels->appendDataToModelDelete($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function updateAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $storyModels = new Stories();
            $data = array('slug' => $slug, 'title' => $this->request->get('title'), 'content' => $this->request->get('content'));
            $validate = $storyModels->validateEmptyFields(Stories::$updateStories, $data);
            if (isset($validate['status']) && $validate['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($validate['messages'])));
                return $this->response;
            }
            $return = $storyModels->appendDataToModel($data, true);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function followAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $storyModels = new StoryFollows();
            $data = array('slug' => $slug, 'follower_id' => $this->request->get('follower_id'));
            if (empty($data['slug']) || empty($data['follower_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $return = $storyModels->appendDatatoFollowModel($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function unfollowAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $storyModels = new StoryFollows();
            $data = array('slug' => $slug, 'follower_id' => $this->request->get('follower_id'));
            if (empty($data['slug']) || empty($data['follower_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $return = $storyModels->appendDatatoFollowModel($data, true);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function followersAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $storyModels = new StoryFollows();
            $data = array('slug' => $slug);
            if (empty($data['slug'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $return = $storyModels->getFollowers($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function voteAction($sideId) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $data = array('side_id' => $sideId, 'voter_id' => $this->request->get('voter_id'));
            if (empty($data['voter_id']) || empty($data['side_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $mutesModels = new Votes();
            $return = $mutesModels->appendDatatoVotesModel($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function unvoteAction($sideId) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $data = array('side_id' => $sideId, 'voter_id' => $this->request->get('voter_id'));
            if (empty($data['voter_id']) || empty($data['side_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $mutesModels = new Votes();
            $return = $mutesModels->appendDatatoVotesModel($data, true);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function argboxAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $data = array('app_uid' => $this->request->get('app_uid'), 'foreign_id' => $this->request->get('foreign_id'));
            if (empty($data['app_uid']) || empty($data['foreign_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $reportsModels = new Stories();
            $return = $reportsModels->getStoriesByAppIdAndForginId($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
}
