<?php
ini_set('html_errors', false);
class OpinionController extends BaseController {
    public function indexAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $opinionModels = new Opinions();
            $return = $opinionModels->listOpinions();
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function showAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $opinionModels = new Opinions();
            $return = $opinionModels->retriveOpinion($slug, 6);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function createAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $opinionModels = new Opinions();
            $data = array('title' => $this->request->get('title'), 'content' => $this->request->get('content'), 'featured' => $this->request->get('featured'), 'category_id' => $this->request->get('category_id'), 'owner_id' => $this->request->get('owner_id'));
            if (empty($data['title']) || empty($data['content']) || empty($data['featured']) || empty($data['category_id']) || empty($data['owner_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $return = $opinionModels->appendDataToModel($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function deleteAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $opinionModels = new Opinions();
            $data = array('slug' => $slug);
            $return = $opinionModels->appendDataToModelDelete($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function updateAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $opinionModels = new Opinions();
            $data = array('slug' => $slug, 'title' => $this->request->get('title'), 'content' => $this->request->get('content'));
            if (empty($data['slug']) || empty($data['title']) || empty($data['content'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $return = $opinionModels->appendDataToModel($data, true);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function followAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $opinionModels = new OpinionFollows();
            $data = array('slug' => $slug, 'follower_id' => $this->request->get('follower_id'));
            if (empty($data['slug']) || empty($data['follower_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $return = $opinionModels->appendDatatoFollowModel($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function unfollowAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $opinionModels = new OpinionFollows();
            $data = array('slug' => $slug, 'follower_id' => $this->request->get('follower_id'));
            if (empty($data['slug']) || empty($data['follower_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $return = $opinionModels->appendDatatoFollowModel($data, true);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function followersAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $opinionModels = new OpinionFollows();
            $data = array('slug' => $slug);
            if (empty($data['slug'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $return = $opinionModels->getFollowers($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function voteAction($sideId) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $data = array('side_id' => intval($sideId) ? $sideId : false, 'voter_id' => $this->request->get('voter_id'));
            if (empty($data['voter_id']) || empty($data['side_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $mutesModels = new Votes();
            $return = $mutesModels->appendDatatoVotesModel($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function unvoteAction($sideId) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $data = array('side_id' => $sideId, 'voter_id' => $this->request->get('voter_id'));
            if (empty($data['voter_id']) || empty($data['side_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $mutesModels = new Votes();
            $return = $mutesModels->appendDatatoVotesModel($data, true);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
}
