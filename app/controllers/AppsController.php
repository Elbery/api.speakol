<?php
use Phalcon\DI\FactoryDefault;
class AppsController extends BaseController {
    public function createAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelApps = new Apps();
            $json_data = json_decode(file_get_contents("php://input"));
            $params = array('card' => $json_data->card, 'lang' => $json_data->lang, 'logo' => $json_data->logo, 'name' => $json_data->name, 'password' => $json_data->password, 'email' => $json_data->email, 'category_id' => $json_data->category_id, 'website' => $json_data->website, 'country_id' => $json_data->country_id, 'address' => $json_data->address, 'network' => $json_data->network);
            $this->getDI()->set('language', $json_data->lang);
            $return = $modelApps->createApp($params);
            $new_log = new Log();
            if (strtoupper($return['status']) == 'OK') {
                $re = json_decode($return['message'], 1);
                $log_msg = "{/apps/create,create publisher account ,response message:ok,status:ok}";
                $new_log->log_current_action('', $this->headers['Ip'], $log_msg, $re['app_id']);
                $user = Users::findFirst($re['user_id']);
                $this->view->url = $json_data->redirect_url . $user->getConfirmationToken();
                $app = Apps::findFirst($re['app_id']);
                $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
                $this->view->lang = $json_data->lang;
                $this->view->app = $app->toArray();
                $this->view->update = "http://plugins.speakol.com/apps/settings";
                $view = clone $this->view;
                $view->start();
                $view->render('emails', 'app-signup');
                $view->finish();
                $content = $view->getContent();
                $message = array('html' => $content, 'subject' => $this->t->_('welcome'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
                $async = false;
                $ip_pool = 'Main Pool';
                $result = $this->mandrill->messages->send($message, $async, $ip_pool);
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'messages' => $return['message']));
            } else {
                $log_msg = "{/apps/create,create publisher account ,response message:" . $return['message'] . ",status:failed}";
                $new_log->log_current_action('', $this->headers['Ip'], $log_msg, 0);
                $this->response->setStatusCode(301, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $return['message']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function updateAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelApps = new Apps();
            $json_data = json_decode(file_get_contents("php://input"));
            $params = array('app_id' => $json_data->app_id, 'lang' => $json_data->locale, 'logo' => $json_data->logo, 'name' => $json_data->name, 'old_password' => $json_data->old_password, 'password' => $json_data->password, 'email' => $json_data->email, 'category_id' => $json_data->category_id, 'website' => $json_data->website, 'country_id' => $json_data->country_id);
            $this->getDI()->set('language', $json_data->locale);
            $return = $modelApps->updateApp($params);
            if (strtoupper($return['status']) == 'OK') {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            } else {
                $this->response->setStatusCode(301, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $return['message']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function showAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            if (Apps::getAppBySlug($slug) == FALSE) {
                throw new Exception($this->t->_("not-found"), 404);
            }
            $page = $this->request->get('page', 'int', 1);
            $perPage = $this->request->get('per_page', 'int', 10);
            $feeds = $this->request->get('feed_filter', 'string', '');
            $modelApps = new Apps();
            $arrApp = $modelApps->showActionHelper($slug, $page, $perPage, $feeds);
            if ($arrApp == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrApp));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function list_users_action() {
        $this->response->setHeader("Content-Type", "application/json");
        $logged_user_id = Tokens::getSignedInUserId();
        $logged_user = Users::findFirst($logged_user_id);
        if ($logged_user->getEmail() == "info@infralayer.com") {
            $page = ($this->request->get('page') != NULL) ? $this->request->get('page') : 1;
            $limit = 10;
            $offset = ($page == 1) ? 0 : ($page - 1) * $limit;
            $sort = "asc";
            $sort_key = "created_at";
            $users = Users::list_users($limit, $offset, $sort, $sort_key);
            $apps = Apps::list_apps($limit, $offset, $sort, $sort_key);
            $users_apps_list = array("users" => $users, "apps" => $apps);
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', "data" => $users_apps_list));
        } else {
            $this->response->setStatusCode(401, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', "message" => "Unauthorized"));
        }
        return $this->response;
    }
    public function list_sorted_apps() {
        $this->response->setHeader("Content-Type", "application/json");
        $logged_user_id = Tokens::getSignedInUserId();
        $logged_user = Users::findFirst($logged_user_id);
        if ($logged_user->getEmail() == "info@infralayer.com") {
            $page = ($this->request->get('page') != NULL) ? $this->request->get('page') : 1;
            $limit = $this->request->get('limit');
            $offset = ($page == 1) ? 0 : ($page - 1) * $limit;
            $sort = $this->request->get('sort');
            $sort_key = $this->request->get("sort_key");
            $apps = Apps::list_apps($limit, $offset, $sort, $sort_key);
            $apps_list = array("apps" => $apps);
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', "data" => $apps_list));
        } else {
            $this->response->setStatusCode(401, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', "message" => "Unauthorized"));
        }
        return $this->response;
    }
    public function list_sorted_users() {
        $this->response->setHeader("Content-Type", "application/json");
        $logged_user_id = Tokens::getSignedInUserId();
        $logged_user = Users::findFirst($logged_user_id);
        if ($logged_user->getEmail() == "info@infralayer.com") {
            $page = ($this->request->get('page') != NULL) ? $this->request->get('page') : 1;
            $limit = $this->request->get('limit');
            $offset = ($page == 1) ? 0 : ($page - 1) * $limit;
            $sort = $this->request->get('sort');
            $sort_key = $this->request->get("sort_key");
            $users = Users::list_users($limit, $offset, $sort, $sort_key);
            $users_list = array("users" => $users);
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', "data" => $users_list));
        } else {
            $this->response->setStatusCode(401, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', "message" => "Unauthorized"));
        }
        return $this->response;
    }
    public static function validate_plugins_creation() {
        $signedInUserID = Tokens::checkAccess();
        $app_admin = AppAdmins::findFirst("user_id=$signedInUserID");
        $app = Apps::findFirst($app_admin->getAppId());
        if (intval($app->get_plan()) != 4) {
            $prev_debates = Debates::count(array('conditions' => 'app_id=:app_id: AND created_at >= :subscription:', 'bind' => array('app_id' => $app->getId(), 'subscription' => $app->get_subscription_date())));
            $prev_plugins = intval($prev_debates);
            $prev_comparisons = Comparisons::count(array('conditions' => 'app_id=:app_id: AND created_at >= :subscription:', 'bind' => array('app_id' => $app->getId(), 'subscription' => $app->get_subscription_date())));
            $prev_plugins = intval($prev_plugins) + intval($prev_comparisons);
            $max_plugins = PluginsPerPlan::findFirst('plan_id=' . $app->get_plan())->get_max_plugins();
            $rem_plugins = $max_plugins - $prev_plugins;
            if ($rem_plugins < 1) {
                $rem_plugins = 0;
            }
        } else {
            $max_plugins = "Unlimited";
        }
        return array("status" => 'OK', 'data' => array('remaining_plugins' => $rem_plugins, 'max_no' => $max_plugins));
    }
    public function validate_plugins_action() {
        $plugins_creation = AppsController::validate_plugins_creation();
        $this->response->setStatusCode(200, "OK");
        $this->response->setJsonContent($plugins_creation);
        return $this->response;
    }
    public function showDataAction($appId) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            $app_admin = AppAdmins::findFirst("user_id=$signedInUserID");
            if ($appId != $app_admin->getAppId()) {
                $this->response->setStatusCode(401, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', "message" => "Unauthorized"));
                return $this->response;
            }
            if (Apps::findFirst($appId) == FALSE) {
                throw new Exception($this->t->_("not-found"), 404);
            }
            $page = $this->request->get('page');
            $perPage = $this->request->get('per_page', 'int', 10);
            $feeds = $this->request->get('type', 'string', '');
            $modelApps = new Apps();
            $arrApp = $modelApps->listAppData($appId, $page, $perPage, $feeds);
            if ($arrApp == FALSE) {
                throw new Exception($this->t->_("bad-request"), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrApp));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function listStatsAction($id) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $page = $this->request->get('page');
            $perPage = $this->request->get('per_page', 'int', 10);
            $feeds = $this->request->get('type', 'string', '');
            $modelApps = new Apps();
            $signedInUserID = Tokens::checkAccess();
            $app_admin = AppAdmins::findFirst("user_id=$signedInUserID");
            if ($id != $app_admin->getAppId()) {
                $this->response->setStatusCode(401, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', "message" => "Unauthorized"));
                return $this->response;
            }
            $arrApp = $modelApps->appStats($id, $perPage, $feeds);
            if ($arrApp == FALSE) {
                throw new Exception($this->t->_("bad-request"), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrApp));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function interactions_stats() {
        error_reporting(0);
        $this->response->setHeader("Content-Type", "application/json");
        $time = $this->request->get("time");
        $validInt = filter_var($time, FILTER_VALIDATE_INT);
        if (($time != 0 && !$validInt) || ($validInt != 1000 && ($validInt < 0 || $validInt > 30))) {
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid data'));
            return $this->response;
        }
        $signedInUserID = Tokens::checkAccess();
        if (!$signedInUserID) {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            return $this->response;
        }
        $app = Apps::findFirst('publisher_id=' . $signedInUserID);
        if (!$app) {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            return $this->response;
        }
        $app_id = $app->getId();
        $di = FactoryDefault::getDefault();
        $connection = mysql_connect($di->getShared('config')->get('database')->get('host'), $di->getShared('config')->get('database')->get('username'), $di->getShared('config')->get('database')->get('password'));
        if (!$connection) {
            die('Could not connect: ' . mysql_error());
        }
        mysql_select_db($di->getShared('config')->get('database')->get('dbname'), $connection);
        $query_result = mysql_query("SELECT id FROM sides WHERE debate_id IN(SELECT id FROM debates WHERE app_id=$app_id)");
        $sides = "";
        while ($r = mysql_fetch_array($query_result)) {
            $sides.= $r['id'] . ",";
        }
        $query_result = mysql_query("SELECT id FROM sides WHERE comparison_id IN(SELECT id FROM comparisons WHERE app_id=$app_id)");
        while ($r = mysql_fetch_array($query_result)) {
            $sides.= $r['id'] . ",";
        }
        $query_result = mysql_query("SELECT id FROM sides WHERE argumentbox_id IN(SELECT id FROM argumentboxes WHERE app_id=$app_id)");
        while ($r = mysql_fetch_array($query_result)) {
            $sides.= $r['id'] . ",";
        }
        $sides = substr($sides, 0, -1);
        if ($sides == "") {
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => array('active_users' => 0, 'votes' => 0, 'votes_stat' => 0, 'users_stat' => 0, 'arguments' => 0, 'arguments_stat' => 0)));
            return $this->response;
        }
        $from = date_create($app->getCreatedAt());
        $date = new DateTime();
        date_sub($date, date_interval_create_from_date_string($time . ' days'));
        $diff = date_diff($date, $from);
        $days = $diff->format('%a');
        $current_date = new DateTime();
        $days_from_creation_app = date_diff($current_date, $from)->format('%a');
        if ($time == 1000) {
            $time = $days_from_creation_app;
        }
        $query_result = mysql_query("SELECT count(*) as count FROM votes WHERE created_at > DATE_SUB(CURDATE(),INTERVAL $time DAY) AND side_id IN($sides)");
        $votes = mysql_fetch_array($query_result);
        $number_of_votes = $votes['count'];
        $query_result = mysql_query("SELECT count(*) as count FROM votes WHERE created_at < DATE_SUB(CURDATE(),INTERVAL $time DAY) AND side_id IN($sides)");
        $prev_votes = mysql_fetch_array($query_result);
        $number_of_prev_votes = $prev_votes['count'];
        $perc_of_old_votes = $number_of_prev_votes / $days;
        if (($days_from_creation_app <= $time && $number_of_votes) || ($number_of_votes && !$number_prev_votes)) {
            $votes_stat = 100;
        } else {
            $votes_stat = (intval($number_of_votes) / (($time) ? $time : 1)) / (($perc_of_old_votes) ? $perc_of_old_votes : 1) * 100;
        }
        if ($votes_stat < 100 && $votes_stat > 0) {
            $votes_stat = $votes_stat - 100;
        }
        $users_ids = array();
        $query_result = mysql_query("SELECT owner_id FROM arguments WHERE created_at >= DATE_SUB(CURDATE(),INTERVAL $time DAY) AND app_id=$app_id GROUP BY owner_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['owner_id']);
        }
        $query_result = mysql_query("SELECT user_id FROM replies WHERE created_at >= DATE_SUB(CURDATE(),INTERVAL $time DAY) AND app_id=$app_id GROUP BY user_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['user_id']);
        }
        $query_result = mysql_query("SELECT thumber_id FROM reply_thumbs WHERE created_at >= DATE_SUB(CURDATE(),INTERVAL $time DAY) AND reply_id IN(SELECT id FROM replies WHERE app_id=$app_id) GROUP BY thumber_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['thumber_id']);
        }
        $query_result = mysql_query("SELECT thumber_id FROM argument_thumbs WHERE created_at >= DATE_SUB(CURDATE(),INTERVAL $time DAY) AND argumnet_id IN(SELECT id FROM arguments WHERE app_id=$app_id) GROUP BY thumber_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['thumber_id']);
        }
        $query_result = mysql_query("SELECT voter_id FROM votes WHERE created_at >= DATE_SUB(CURDATE(),INTERVAL $time DAY) AND side_id IN( $sides ) GROUP BY voter_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['voter_id']);
        }
        $active_users = sizeof(array_unique($users_ids));
        $users_ids = array();
        $query_result = mysql_query("SELECT owner_id FROM arguments WHERE created_at < DATE_SUB(CURDATE(),INTERVAL $time DAY) AND app_id=$app_id GROUP BY owner_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['owner_id']);
        }
        $query_result = mysql_query("SELECT user_id FROM replies WHERE created_at < DATE_SUB(CURDATE(),INTERVAL $time DAY) AND app_id=$app_id GROUP BY user_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['user_id']);
        }
        $query_result = mysql_query("SELECT thumber_id FROM reply_thumbs WHERE created_at < DATE_SUB(CURDATE(),INTERVAL $time DAY) AND reply_id IN(SELECT id FROM replies WHERE app_id=$app_id) GROUP BY thumber_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['thumber_id']);
        }
        $query_result = mysql_query("SELECT thumber_id FROM argument_thumbs WHERE created_at < DATE_SUB(CURDATE(),INTERVAL $time DAY) AND argumnet_id IN(SELECT id FROM arguments WHERE app_id=$app_id) GROUP BY thumber_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['thumber_id']);
        }
        $query_result = mysql_query("SELECT voter_id FROM votes WHERE created_at < DATE_SUB(CURDATE(),INTERVAL $time DAY) AND side_id IN( $sides ) GROUP BY voter_id");
        while ($r = mysql_fetch_array($query_result)) {
            array_push($users_ids, $r['voter_id']);
        }
        $old_users = sizeof(array_unique($users_ids));
        $old_users_per = $old_users / $days;
        if (($days_from_creation_app <= $time && $active_users) || ($active_users && !$old_users)) {
            $users_stat = 100;
        } else {
            $users_stat = (intval($active_users) / (($time) ? $time : 1)) / (($old_users_per) ? $old_users_per : 1) * 100;
        }
        if ($users_stat < 100 && $users_stat > 0) {
            $users_stat = $users_stat - 100;
        }
        $query_result = mysql_query("SELECT count(*) as count FROM arguments WHERE created_at > DATE_SUB(CURDATE(),INTERVAL $time DAY) AND app_id=$app_id");
        $number_of_new_arguments = mysql_fetch_array($query_result);
        $number_of_new_arguments = intVal($number_of_new_arguments['count']);
        $query_result = mysql_query("SELECT count(*) as count FROM arguments WHERE created_at < DATE_SUB(CURDATE(),INTERVAL $time DAY) AND app_id=$app_id");
        $number_of_old_arguments = mysql_fetch_array($query_result);
        $number_of_old_arguments = intVal($number_of_old_arguments['count']);
        if (($days_from_creation_app <= $time && $number_of_new_arguments) || ($number_of_new_arguments && !$number_of_old_arguments)) {
            $arguments_stat = 100;
        } else {
            $old_arguments_per = $number_of_old_arguments / $days;
            $arguments_stat = ($number_of_new_arguments / (($time) ? $time : 1)) / (($old_arguments_per) ? $old_arguments_per : 1) * 100;
        }
        if ($arguments_stat < 100 && $arguments_stat > 0) {
            $arguments_stat = $arguments_stat - 100;
        }
        mysql_close($connection);
        $this->response->setStatusCode(200, "OK");
        $this->response->setJsonContent(array('status' => 'OK', 'data' => array('active_users' => $active_users, 'votes' => $number_of_votes, 'votes_stat' => $votes_stat, 'users_stat' => $users_stat, 'arguments' => $number_of_new_arguments, 'arguments_stat' => $arguments_stat)));
        return $this->response;
    }
    public function showSiteAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $page = $this->request->get('page');
            $perPage = $this->request->get('per_page', 'int', 10);
            $feeds = $this->request->get('type', 'string', '');
            $modelApps = new Apps();
            $arrApp = $modelApps->listSiteData($page, $perPage, $feeds);
            if ($arrApp == FALSE) {
                throw new Exception($this->t->_("bad-user"), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrApp));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function getFollowersAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t_('unauthorized'), 401);
            }
            $apps = new Apps();
            $page = $this->request->get('page', 'int', 0);
            $perPage = $this->request->get('per_page', 'int', 4);
            $data = $apps->getFollowers($slug, $page, $perPage);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $data));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function deleteAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            if (Apps::getAppBySlug($slug) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            $modelApps = new Apps();
            $arrApp = $modelApps->deleteActionHelper($slug);
            if ($arrApp == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrApp));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function followAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            $modelAppFollows = new AppFollows();
            $app = Apps::getAppBySlug($slug);
            if ($app == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if ($modelAppFollows->isFollowing($app->getId(), $signedInUserID)) {
                throw new Exception($this->t->_('found'), 302);
            }
            if (FALSE == $modelAppFollows->followActionHelper($app->getId(), $signedInUserID)) {
                throw new Exception($this->t->_('conflict'), 409);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function unfollowAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            $modelAppFollows = new AppFollows();
            $app = Apps::getAppBySlug($slug);
            if ($app == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if ($modelAppFollows->isFollowing($app->getId(), $signedInUserID) == FALSE) {
                throw new Exception($this->t->_("not-following"), 302);
            }
            if (FALSE == $modelAppFollows->unfollowActionHelper($app->getId(), $signedInUserID)) {
                throw new Exception($this->t->_("conflict"), 409);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $this->t->_('ok')));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function followersAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $app = Apps::getAppBySlug($slug);
            if ($app == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            $modelApps = new Apps();
            $arrApp = $modelApps->followersActionHelper($app->getId());
            if ($arrApp == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrApp));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function viewComponentAction($appSlug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $appsModels = new Apps();
            $filters = $this->request->get('filters');
            $return = $appsModels->retrivecomponents($appSlug, $filters);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function listAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $appsModels = new Apps();
            $page = $this->request->get('page', 'int', 1);
            $perPage = $this->request->get('per_page', 'int', 10);
            $return = $appsModels->listApps($page, $perPage);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function latestRepliesAction($appSlug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $appsModels = new Apps();
            $data['type'] = $this->request->get('content_type');
            $data['slug'] = $appSlug;
            if (empty($data['type'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array('Some data are missing!')));
                return $this->response;
            }
            $return = $appsModels->retriveLatestRepliesByContentType($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function deleteComponentsAction($appSlug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $appsModels = new Apps();
            $data['type'] = $this->request->get('content_type');
            $data['slug'] = $appSlug;
            $data['item_id'] = $this->request->get('item_id');
            if (empty($data['type']) || empty($data['item_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array('Some data are missing!')));
                return $this->response;
            }
            $return = $appsModels->deleteComponent($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function latestArgumentAction($appSlug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $appsModels = new Apps();
            $data['type'] = $this->request->get('content_type');
            $data['slug'] = $appSlug;
            if (empty($data['type'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array('Some data are missing!')));
                return $this->response;
            }
            $return = $appsModels->retriveLatestArgumentsByContentType($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function createComponentAction($appSlug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $data['type'] = $this->request->get('content_type');
            $data['url'] = $this->request->get('url');
            $data['description'] = $this->request->get('description', null, '');
            $data['slug'] = $appSlug;
            $app = Apps::findFirst('slug="' . $appSlug . '"');
            $data['app_id'] = !empty($app) ? $app->getId() : false;
            if (empty($data['type']) || empty($data['app_id']) || empty($data['url'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array('Some data are missing!')));
                return $this->response;
            }
            $appsModels = new Argumentboxes();
            $return = $appsModels->appendDataToModel($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function statsAction($app_id, $page = 1, $limit = 2) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $module = $this->request->get('module', 'string', FALSE);
            $orderby = $this->request->get('order');
            $arrStats = Apps::getAppStats($app_id, $page, $limit, $orderby, $module);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrStats));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function follow_inactive_app($lang) {
        $curr_date = new DateTime();
        $current_date = new DateTime();
        $email_log = new SendingEmailsLog();
        $email_log->set_email_type("/apps/inactive/follow");
        $email_log->set_date($curr_date->format('Y-m-d H:i:s'));
        $email_log->save();
        $current_date->setTime(0, 0, 0);
        $apps = Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT Apps.* FROM Apps JOIN Users ON Apps.publisher_id=Users.id WHERE Users.locale='$lang' AND Apps.verified=1");
        foreach ($apps as $app) {
            $creation_date = new DateTime($app->getCreatedAt());
            $diff = date_diff($current_date, $creation_date);
            $diff_days = intval($diff->format('%a'));
            if ($diff_days > 3) {
                $argumentboxes = Argumentboxes::count("app_id=" . $app->getId());
                $debates = Debates::count("app_id=" . $app->getId());
                $comparisons = Comparisons::count("app_id=" . $app->getId());
                if (!$argumentboxes && !$debates && !$comparisons) {
                    $user_email_log = UserEmailsLog::findFirst(array("user_id=" . $app->getPublisherId() . " AND email_type='/apps/inactive/follow'", 'order' => "id desc"));
                    if ($user_email_log) {
                        $sending_date = new DateTime($user_email_log->get_last_date());
                        $date_after_day = $sending_date->modify('3 days');
                        if ($date_after_day > $curr_date) {
                            continue;
                        }
                    }
                    $this->send_follow_email($app->getPublisherId());
                }
            }
        }
        $email_log = SendingEmailsLog::findFirst(array("email_type='/apps/inactive/follow'", 'order' => 'id desc'));
        $email_log->set_end_date(date('Y-m-d H:i:s'));
        $email_log->save();
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK'));
        return $this->response;
    }
    function send_follow_email($publisher_id) {
        $publisher = Users::findFirst($publisher_id);
        $this->getDI()->set('language', $publisher->getLocale());
        $this->view->name = $publisher->get_first_name() . " " . $publisher->get_last_name();
        $this->view->lang = $publisher->getLocale();
        $this->view->update = "http://plugins.speakol.com/apps/settings";
        $this->view->start();
        $this->view->render('emails', 'follow-inactivity');
        $this->view->finish();
        $content = $this->view->getContent();
        $message = array('html' => $content, 'subject' => $this->t->_('follow-inactivity'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $publisher->getEmail(), 'type' => 'to')),);
        $async = false;
        $ip_pool = 'Main Pool';
        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
        $user_email_log = new UserEmailsLog();
        $user_email_log->set_user_id($publisher_id);
        $user_email_log->set_email_type("/apps/inactive/follow");
        $user_email_log->set_last_date(date('Y-m-d H:i:s'));
        $user_email_log->save();
    }
    public function long_inactivity($lang) {
        function cmp($a, $b) {
            return ($a['interactions'] < $b['interactions']);
        }
        $curr_date = new DateTime();
        $current_date = new DateTime();
        $email_log = new SendingEmailsLog();
        $email_log->set_email_type("/apps/email/longinactivity");
        $email_log->set_date($curr_date->format('Y-m-d H:i:s'));
        $email_log->save();
        $current_date->setTime(0, 0, 0);
        $apps = Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT Apps.* FROM Apps JOIN Users ON Apps.publisher_id=Users.id WHERE Users.locale='$lang' AND Apps.verified=1 AND Apps.newsletter!=0");
        $creation_date = new DateTime('90 days ago');
        $creation_date = $creation_date->format('Y-m-d');
        foreach ($apps as $app) {
            $send_email = true;
            $date = new DateTime('15 days ago');
            $date = $date->format('Y-m-d');
            $argumentboxes = Argumentboxes::count("app_id=" . $app->getId() . " AND created_at>$date");
            $debates = Debates::count("app_id=" . $app->getId() . " AND created_at>$date");
            $comparisons = Comparisons::count("app_id=" . $app->getId() . " AND created_at>$date");
            if (!$argumentboxes && !$debates && !$comparisons) {
                $user_email_log = UserEmailsLog::findFirst(array("user_id=" . $app->getPublisherId() . " AND email_type='/apps/email/longinactivity'", 'order' => "id desc"));
                if ($user_email_log) {
                    $sending_date = new DateTime($user_email_log->get_last_date());
                    $diff = date_diff(new DateTime(), $sending_date);
                    $days = $diff->format('%a');
                    if (intval($days) >= 15) {
                        $send_email = true;
                    } else {
                        $send_email = false;
                    }
                }
                if (!$send_email) {
                    continue;
                }
                $user = Users::findFirst($app->getPublisherId());
                $plugins = $this->most_interacted_plugins($app->getId(), $creation_date);
                usort($plugins, "cmp");
                $plugins = array_slice($plugins, 0, 3);
                $plugins = $this->get_plugins_data($plugins);
                for ($i = 0;$i < sizeof($plugins);$i++) {
                    if ($plugins[$i]['type'] == "debate" || $plugins[$i]['type'] == "argumentbox") {
                        if ($plugins[$i]['data']['lang'] == "ar") {
                            $path = $this->generate_imagick_image($plugins[$i]['sides'][0]['perc'], $plugins[$i]['sides'][0]['votes'], $plugins[$i]['sides'][1]['perc'], $plugins[$i]['sides'][1]['votes'], $plugins[$i]['lang']);
                            $temp = $plugins[$i]['sides'][1];
                            $plugins[$i]['sides'][1] = $plugins[$i]['sides'][0];
                            $plugins[$i]['sides'][0] = $temp;
                        } else {
                            $path = $this->generate_imagick_image($plugins[$i]['sides'][1]['perc'], $plugins[$i]['sides'][1]['votes'], $plugins[$i]['sides'][0]['perc'], $plugins[$i]['sides'][0]['votes'], $plugins[$i]['lang']);
                        }
                        $plugins[$i]['img'] = $path;
                    } else {
                        $no_sides = sizeof($plugins[$i]['sides']);
                        $has_image = ($plugins[$i]['sides'][0]['side_data']['image'] != "") ? true : false;
                        for ($j = 0;$j < $no_sides;$j++) {
                            $path = $this->draw_bar($plugins[$i]['sides'][$j]['perc'], $plugins[$i]['data']['align'], $no_sides, $plugins[$i]['data']['lang'], $has_image);
                            $plugins[$i]['sides'][$j]['vote_img'] = $path;
                        }
                    }
                }
                $other_plugins = array();
                $same_apps = Apps::find('verified=1 AND category_id=' . $app->getCategoryId() . ' AND id!=' . $app->getId());
                foreach ($same_apps as $same_app) {
                    $app_plugins = $this->most_interacted_plugins($same_app->getId(), date('Y-m-1'));
                    $other_plugins = array_merge($other_plugins, $app_plugins);
                }
                usort($other_plugins, "cmp");
                $other_plugins = array_slice($other_plugins, 0, 3);
                $other_plugins = $this->get_plugins_data($other_plugins);
                for ($i = 0;$i < sizeof($other_plugins);$i++) {
                    if ($other_plugins[$i]['type'] == "debate" || $other_plugins[$i]['type'] == "argumentbox") {
                        if ($other_plugins[$i]['data']['lang'] == "ar") {
                            $path = $this->generate_imagick_image($other_plugins[$i]['sides'][0]['perc'], $other_plugins[$i]['sides'][0]['votes'], $other_plugins[$i]['sides'][1]['perc'], $other_plugins[$i]['sides'][1]['votes'], $other_plugins[$i]['lang']);
                        } else {
                            $path = $this->generate_imagick_image($other_plugins[$i]['sides'][1]['perc'], $other_plugins[$i]['sides'][1]['votes'], $other_plugins[$i]['sides'][0]['perc'], $other_plugins[$i]['sides'][0]['votes'], $other_plugins[$i]['lang']);
                        }
                        $other_plugins[$i]['img'] = $path;
                    } else {
                        $no_sides = sizeof($other_plugins[$i]['sides']);
                        $has_image = ($other_plugins[$i]['sides'][0]['side_data']['image'] != "") ? true : false;
                        for ($j = 0;$j < $no_sides;$j++) {
                            $path = $this->draw_bar($other_plugins[$i]['sides'][$j]['perc'], $other_plugins[$i]['data']['align'], $no_sides, $other_plugins[$i]['data']['lang'], $has_image);
                            $other_plugins[$i]['sides'][$j]['vote_img'] = $path;
                        }
                    }
                }
                $hot_topics = $this->hot_topics($user->getLocale());
                $hot_topics = array_slice($hot_topics, 0, 3);
                $this->getDI()->set('language', $user->getLocale());
                $this->view->lang = $user->getLocale();
                $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
                $this->view->logo = "top";
                $this->view->plugins = $plugins;
                $this->view->high_communities = $other_plugins;
                $this->view->hot_topics = $hot_topics;
                $this->view->update = "http://plugins.speakol.com/apps/settings";
                $this->view->start();
                $this->view->render('emails', 'long_inactivity_followup');
                $this->view->finish();
                $content = $this->view->getContent();
                $content = mb_convert_encoding($content, 'UTF-8', 'auto');
                $message = array('html' => $content, 'subject' => $this->t->_('email9-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
                $async = false;
                $ip_pool = 'Main Pool';
                $result = $this->mandrill->messages->send($message, $async, $ip_pool);
                $user_email_log = new UserEmailsLog();
                $user_email_log->set_user_id($user->getId());
                $user_email_log->set_email_type("/apps/email/longinactivity");
                $user_email_log->set_last_date(date('Y-m-d H:i:s'));
                $user_email_log->save();
            }
        }
        $email_log = SendingEmailsLog::findFirst(array("email_type='/apps/email/longinactivity'", 'order' => 'id desc'));
        $email_log->set_end_date(date('Y-m-d H:i:s'));
        $email_log->save();
    }
    public function send_optional_newsletter($lang) {
        $curr_date = new DateTime();
        $email_log = new SendingEmailsLog();
        $email_log->set_email_type("/apps/email/newsletter/optional");
        $email_log->set_date($curr_date->format('Y-m-d H:i:s'));
        $email_log->save();
        $day = intval(date('d'));
        if ($day === 1) {
            $newsletter = 2;
        } else {
            $newsletter = 1;
        }
        $apps = Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT Apps.* FROM Apps JOIN Users ON Apps.publisher_id=Users.id WHERE Users.locale='$lang' AND Apps.verified=1 AND Apps.newsletter=$newsletter");
        foreach ($apps as $app) {
            $user = Users::findFirst($app->getPublisherId());
            $hot_topics = $this->hot_topics($user->getLocale());
            $hot_topics = array_slice($hot_topics, 0, 3);
            $this->getDI()->set('language', $user->getLocale());
            $this->view->lang = $user->getLocale();
            $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
            $this->view->hot_topics = $hot_topics;
            $this->view->logo = "top";
            $this->view->update = "http://plugins.speakol.com/apps/settings";
            $this->view->start();
            $this->view->render('emails', 'optional_newsletter');
            $this->view->finish();
            $content = $this->view->getContent();
            $content = mb_convert_encoding($content, 'UTF-8', 'auto');
            $message = array('html' => $content, 'subject' => $this->t->_('optional-newsletter-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
            $async = false;
            $ip_pool = 'Main Pool';
            $user_email_log = UserEmailsLog::findFirst(array("user_id=" . $user->getId() . " AND email_type='/apps/email/newsletter/optional'", 'order' => 'id desc'));
            if (!$user_email_log) {
                $user_email_log = new UserEmailsLog();
                $user_email_log->set_user_id($user->getId());
                $user_email_log->set_email_type("/apps/email/newsletter/optional");
                $user_email_log->set_last_date($curr_date->format('Y-m-d H:i:s'));
                $user_email_log->save();
                $result = $this->mandrill->messages->send($message, $async, $ip_pool);
            } else {
                if ($app->get_newsletter() === 1) {
                    $sending_date = new DateTime($user_email_log->get_last_date());
                    $date_after_week = $sending_date->modify('7 days');
                    if ($date_after_week <= $curr_date) {
                        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
                        $user_email_log = new UserEmailsLog();
                        $user_email_log->set_user_id($user->getId());
                        $user_email_log->set_email_type("/apps/email/newsletter/optional");
                        $user_email_log->set_last_date($curr_date->format('Y-m-d H:i:s'));
                        $user_email_log->save();
                    }
                }
            }
        }
        $email_log = SendingEmailsLog::findFirst(array("email_type='/apps/email/newsletter/optional'", 'order' => 'id desc'));
        $email_log->set_end_date(date('Y-m-d H:i:s'));
        $email_log->save();
    }
    public function send_monthly_newsletter($lang) {
        function cmp($a, $b) {
            return ($a['interactions'] < $b['interactions']);
        }
        $curr_date = new DateTime();
        $email_log = new SendingEmailsLog();
        $email_log->set_email_type("/apps/email/newsletter/monthly");
        $email_log->set_date($curr_date->format('Y-m-d H:i:s'));
        $email_log->save();
        $apps = Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT Apps.* FROM Apps JOIN Users ON Apps.publisher_id=Users.id WHERE Users.locale='$lang' AND Apps.verified=1");
        $creation_date = new DateTime('30 days ago');
        $creation_date = $creation_date->format('Y-m-d');
        foreach ($apps as $app) {
            $user = Users::findFirst($app->getPublisherId());
            $plugins = $this->most_interacted_plugins($app->getId(), $creation_date);
            usort($plugins, "cmp");
            $plugins = array_slice($plugins, 0, 3);
            $plugins = $this->get_plugins_data($plugins);
            for ($i = 0;$i < sizeof($plugins);$i++) {
                if ($plugins[$i]['type'] == "debate" || $plugins[$i]['type'] == "argumentbox") {
                    if ($plugins[$i]['data']['lang'] == "ar") {
                        $path = $this->generate_imagick_image($plugins[$i]['sides'][0]['perc'], $plugins[$i]['sides'][0]['votes'], $plugins[$i]['sides'][1]['perc'], $plugins[$i]['sides'][1]['votes'], $plugins[$i]['lang']);
                        $temp = $plugins[$i]['sides'][1];
                        $plugins[$i]['sides'][1] = $plugins[$i]['sides'][0];
                        $plugins[$i]['sides'][0] = $temp;
                    } else {
                        $path = $this->generate_imagick_image($plugins[$i]['sides'][1]['perc'], $plugins[$i]['sides'][1]['votes'], $plugins[$i]['sides'][0]['perc'], $plugins[$i]['sides'][0]['votes'], $plugins[$i]['lang']);
                    }
                    $plugins[$i]['img'] = $path;
                } else {
                    $no_sides = sizeof($plugins[$i]['sides']);
                    $has_image = ($plugins[$i]['sides'][0]['side_data']['image'] != "") ? true : false;
                    for ($j = 0;$j < $no_sides;$j++) {
                        $path = $this->draw_bar($plugins[$i]['sides'][$j]['perc'], $plugins[$i]['data']['align'], $no_sides, $plugins[$i]['data']['lang'], $has_image);
                        $plugins[$i]['sides'][$j]['vote_img'] = $path;
                    }
                }
            }
            $other_plugins = array();
            $same_apps = Apps::find('verified=1 AND category_id=' . $app->getCategoryId() . ' AND id!=' . $app->getId());
            foreach ($same_apps as $same_app) {
                $app_plugins = $this->most_interacted_plugins($same_app->getId(), $creation_date);
                $other_plugins = array_merge($other_plugins, $app_plugins);
            }
            usort($other_plugins, "cmp");
            $other_plugins = array_slice($other_plugins, 0, 3);
            $other_plugins = $this->get_plugins_data($other_plugins);
            for ($i = 0;$i < sizeof($other_plugins);$i++) {
                if ($other_plugins[$i]['type'] == "debate" || $other_plugins[$i]['type'] == "argumentbox") {
                    if ($other_plugins[$i]['data']['lang'] == "ar") {
                        $path = $this->generate_imagick_image($other_plugins[$i]['sides'][0]['perc'], $other_plugins[$i]['sides'][0]['votes'], $other_plugins[$i]['sides'][1]['perc'], $other_plugins[$i]['sides'][1]['votes'], $other_plugins[$i]['lang']);
                    } else {
                        $path = $this->generate_imagick_image($other_plugins[$i]['sides'][1]['perc'], $other_plugins[$i]['sides'][1]['votes'], $other_plugins[$i]['sides'][0]['perc'], $other_plugins[$i]['sides'][0]['votes'], $other_plugins[$i]['lang']);
                    }
                    $other_plugins[$i]['img'] = $path;
                } else {
                    $no_sides = sizeof($other_plugins[$i]['sides']);
                    $has_image = ($plugins[$i]['sides'][0]['side_data']['image'] != "") ? true : false;
                    for ($j = 0;$j < $no_sides;$j++) {
                        $path = $this->draw_bar($other_plugins[$i]['sides'][$j]['perc'], $other_plugins[$i]['data']['align'], $no_sides, $other_plugins[$i]['data']['lang'], $has_image);
                        $other_plugins[$i]['sides'][$j]['vote_img'] = $path;
                    }
                }
            }
            $hot_topics = $this->hot_topics($user->getLocale());
            $hot_topics = array_slice($hot_topics, 0, 3);
            $this->getDI()->set('language', $user->getLocale());
            $this->view->lang = $user->getLocale();
            $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
            $this->view->plugins = $plugins;
            $this->view->high_communities = $other_plugins;
            $this->view->hot_topics = $hot_topics;
            $this->view->logo = "top";
            $this->view->update = "http://plugins.speakol.com/apps/settings";
            $this->view->start();
            $this->view->render('emails', 'monthly_digests');
            $this->view->finish();
            $content = $this->view->getContent();
            $content = mb_convert_encoding($content, 'UTF-8', 'auto');
            $message = array('html' => $content, 'subject' => $this->t->_('monthly-digest-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
            $async = false;
            $ip_pool = 'Main Pool';
            $user_email_log = UserEmailsLog::findFirst(array("user_id=" . $user->getId() . " AND email_type='/apps/email/newsletter/monthly'", "order" => "id desc"));
            if (!$user_email_log) {
                $user_email_log = new UserEmailsLog();
                $user_email_log->set_user_id($user->getId());
                $user_email_log->set_email_type("/apps/email/newsletter/monthly");
                $user_email_log->set_last_date($curr_date->format('Y-m-d H:i:s'));
                $user_email_log->save();
                $result = $this->mandrill->messages->send($message, $async, $ip_pool);
            } else {
                if ($user->get_newsletter() === 1) {
                    $sending_date = new DateTime($user_email_log->get_last_date());
                    $date_after_week = $sending_date->modify('7 days');
                    if ($date_after_week <= $curr_date) {
                        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
                        $user_email_log = new UserEmailsLog();
                        $user_email_log->set_user_id($user->getId());
                        $user_email_log->set_email_type("/apps/email/newsletter/monthly");
                        $user_email_log->set_last_date($curr_date->format('Y-m-d H:i:s'));
                        $user_email_log->save();
                    }
                }
            }
        }
        $email_log = SendingEmailsLog::findFirst(array("email_type='/apps/email/newsletter/monthly'", 'order' => 'id desc'));
        $email_log->set_end_date(date('Y-m-d H:i:s'));
        $email_log->save();
    }
    public function hashingAction($hash) {
        try {
            $url = null;
            $interaction = Interaction::findFirst(array("hash = :hash: ", 'bind' => array('hash' => $hash)));
            if ($interaction) {
                $count = $interaction->getCount();
                $count++;
                $interaction->setCount($count);
                $interaction->save();
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $interaction->getUrl()));
                return $this->response;
            }
            $this->response->setStatusCode(404, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $url));
            return $this->response;
        }
        catch(Exception $ex) {
            $this->response->setStatusCode(502, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $e->getMessage()));
            return $this->response;
        }
    }
}
