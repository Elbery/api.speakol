<?php
use Speakol\concreteSocialPluginFactory;
class VotesController extends BaseController {
    public function voteAction() {
        try {
            $container_type = $this->request->get('type');
            $container_id = $this->request->get('id');
            $side_id = $this->request->get('side_id');
            $url = trim($this->request->get('url'));
            $lang = $this->request->get('lang');
            $link = trim($this->request->get("link"));
            $factory = new concreteSocialPluginFactory();
            $plugin = $factory->make($container_type);
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::getSignedInUserId();
            $return = array();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            $plugin_exist = $plugin->findFirst($container_id);
            if ($plugin_exist == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if (Sides::findFirst($side_id) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if (Votes::hasVoted($side_id, $signedInUserID)) {
                throw new Exception($this->t->_('found'), 302);
            }
            if ($url != "" && ($plugin_exist->getUrl() == '' || $plugin_exist->getUrl() == null)) {
                $url = urldecode($url);
                $plugin_exist->setUrl($url);
                $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $url)));
                if (!$interaction) {
                    $uuid = $this->generate_uuid();
                    $hash = new Interaction;
                    $hash->setHash($uuid);
                    $hash->setCreatedAt(date("Y-m-d H:i:s"));
                    $hash->setUpdatedAt(date("Y-m-d H:i:s"));
                    $hash->setUrl($url);
                    $hash->save();
                }
            }
            if ($lang != "") {
                $plugin_exist->set_lang($lang);
            }
            $plugin_exist->save();
            $return = $plugin->voteDevoteActionHelper($container_id, $side_id, $signedInUserID, $link);
            if (FALSE == $return) {
                throw new Exception($this->t->_('conflict'), 409);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $return));
        }
        catch(Exception $ex) {
            $errors[] = $ex->getMessage();
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage())->sendHeaders();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function unvoteAction() {
        $container_type = $this->request->get('type');
        $container_id = $this->request->get('id');
        $side_id = $this->request->get('side_id');
        $this->response->setHeader("Content-Type", "application/json");
        $signedInUserID = Tokens::getSignedInUserId();
        $vote = Votes::findFirst("side_id=$side_id AND voter_id=$signedInUserID");
        if ($vote) {
            $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
            $new_log = new Log();
            $new_log->log_current_action($signedInUserID, $sentHeaders['Ip'], "/unvote $container_type", $vote->getId());
            $vote->delete();
            $sidesTotalVotes = array();
            $sides = array();
            if ($container_type == "comparison") {
                $arrSides = Sides::getSideIdByCompId($container_id);
            } else if ($container_type == "argumentbox") {
                $arrSides = Sides::getSideIdByArgumentBoxId($container_id);
            } else {
                $arrSides = Sides::getSideIdByDebateId($container_id);
            }
            foreach ($arrSides as $side) {
                $sidesInfo = array();
                $sidesInfo['side_id'] = $side['id'];
                $sidesInfo['votes'] = Votes::getSideVotes($side['id']);
                $sidesTotalVotes[] = $sidesInfo;
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $sidesTotalVotes));
        } else {
            $this->response->setStatusCode(404, "Not found");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
        }
        return $this->response;
    }
}
