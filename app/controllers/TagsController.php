<?php
class TagsController extends BaseController {
    public function showAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelTags = new Tags();
            $arrTag = $modelTags->showTagActionHelper($slug);
            if ($arrTag == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrTag));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function updateAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelTags = new Tags();
            $data = array('slug' => $slug, 'name' => $this->request->get('name'), 'promoted' => $this->request->get('promoted', 'string', false), 'remove_image' => $this->request->get('remove_image'),);
            $arrTag = $modelTags->updateTagActionHelper($data);
            if ($arrTag == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrTag));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function listAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelTags = new Tags();
            $page = $this->request->get('page', 'int', 1);
            $perPage = $this->request->get('per_page', 'int', 10);
            $arrTag = $modelTags->listTagActionHelper($page, $perPage);
            if ($arrTag == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrTag));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function trendsAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelTags = new Tags();
            $limit = $this->request->get('limit', 'int', 4);
            $arrTag = $modelTags->listTrends($limit);
            if ($arrTag == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrTag));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function followAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            $modelTags = new Tags();
            if (Tags::findFirstBySlug($slug) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if ($modelTags->isFollowing($slug, $signedInUserID)) {
                throw new Exception($this->t->_('found'), 302);
            }
            if (FALSE == $modelTags->followTagActionHelper($slug, $signedInUserID)) {
                throw new Exception($this->t->_('conflict'), 409);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function unfollowAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            $modelTags = new Tags();
            if (Tags::findFirstBySlug($slug) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if ($modelTags->isFollowing($slug, $signedInUserID) == FALSE) {
                throw new Exception("Not Following", 302);
            }
            if (FALSE == $modelTags->unfollowTagActionHelper($slug, $signedInUserID)) {
                throw new Exception($this->t->_('conflict'), 409);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function followersAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelTags = new Tags();
            $arrFollowers = $modelTags->showFollowersActionHelper($slug);
            if ($arrFollowers == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrFollowers));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function reportAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            $modelTags = new Tags();
            if (Tags::findFirstBySlug($slug) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if ($modelTags->isReported($slug, $signedInUserID)) {
                throw new Exception($this->t->_('found'), 302);
            }
            if (FALSE == $modelTags->reportTagActionHelper($slug, $signedInUserID)) {
                throw new Exception($this->t->_('conflict'), 409);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function unreportAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            $modelTags = new Tags();
            if (Tags::findFirstBySlug($slug) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if ($modelTags->isReported($slug, $signedInUserID) == FALSE) {
                throw new Exception("Not Reported", 302);
            }
            if (FALSE == $modelTags->unreportTagActionHelper($slug, $signedInUserID)) {
                throw new Exception($this->t->_('conflict'), 409);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function reportsAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelTags = new Tags();
            $arrReports = $modelTags->allTagReportsActionHelper();
            if ($arrReports == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrReports));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
}
