<?php
class CountriesController extends BaseController {
    public function indexAction() {
        try {
            $data = Country::find(array('columns' => array('id', 'name'), 'order' => "name asc"))->toArray();
            if ($data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setContentType('application/json', 'UTF-8');
                $this->response->setJsonContent(array('status' => 'OK', 'messages' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
}
