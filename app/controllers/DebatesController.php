<?php
class DebatesController extends BaseController {
    public function indexAction() {
        try {
            $appId = $this->request->get('app_id');
            if ($appId == 'null') {
                $conditions = array("conditions" => "app_id is null", 'order' => 'id desc');
                $data = Debates::listSiteDebates($conditions);
            } else {
                $conditions = array("conditions" => "app_id=:app_id:", "bind" => array('app_id' => $appId), 'order' => 'id desc');
                $data = Debates::listSiteDebates($conditions);
            }
            if (is_array($data)) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $this->t->_('some-thing-went-wrong')));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function openAction() {
        try {
            $appId = $this->request->get('app_id');
            $conditions = array('conditions' => 'status=:status:', 'bind' => array('status' => Debates::STATUS_PENDING), 'order' => 'status desc');
            if ($appId == 'null') {
                $conditions = array("conditions" => "app_id is null AND status=:status:", "bind" => array('status' => Debates::STATUS_PENDING), 'order' => 'id desc');
            }
            $data = Debates::listSiteDebates($conditions);
            if (is_array($data)) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $this->t->_('some-thing-went-wrong')));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function createAction() {
        try {
            $params = $this->request->getPost();
            $this->getDI()->set('language', $params["lang"]);
            $debate = new Debates();
            $validate_plugins_creation = AppsController::validate_plugins_creation();
            if ($validate_plugins_creation['data']['remaining_plugins'] === 0) {
                $this->response->setStatusCode(605, 'ERROR');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_("max_plugins")));
                return $this->response;
            }
            $data = $debate->createDebate($params);
            if (strtoupper($data['status']) == 'OK') {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data['messages']));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data['message']));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function updateAction($slug) {
        try {
            $params = $this->request->getPut();
            $debate = new Debates();
            $data = $debate->updateDebate($slug, $params);
            if ($data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function show_debate_data() {
        $slug = $this->request->get('slug');
        $debate = Debates::findFirst(array('slug="' . $slug . '"'));
        if ($debate) {
            $sides = Sides::find('debate_id=' . $debate->getId())->toArray();
            $debate_data = array('id' => $debate->getId(), 'slug' => $debate->getSlug(), 'image' => $debate->getImage(), 'url' => $debate->getUrl(), 'title' => $debate->getTitle(), 'category' => $debate->getCategoryId(), 'max_days' => $debate->getMaxDays(), 'created_at' => $debate->getCreatedAt(), 'updated_at' => $debate->getUpdatedAt(), 'sides' => Sides::debate_sides($sides),);
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $debate_data));
        } else {
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('debate-slug-not-exist')));
        }
        return $this->response;
    }
    function update_sides_meta($debate_id, $sidesData) {
        $sides = Sides::find('debate_id=' . $debate_id)->toArray();
        for ($i = 0;$i < 2;$i++) {
            $debateSideMeta = DebateSidesMeta::findFirst('side_id=' . $sides[$i]['id']);
            $debateSideMeta->setTitle(trim($sidesData[$i]['title']));
            $debateSideMeta->setJobTitle(trim($sidesData[$i]['job_title']));
            if ($sidesData[$i]['image']) {
                $debateSideMeta->setImage($sidesData[$i]['image'][0]['path'] . '' . $sidesData[$i]['image'][0]['new']);
            }
            $debateSideMeta->setOpinion(trim($sidesData[$i]['opinion']));
            $debateSideMeta->save();
        }
    }
    public function update_debate() {
        $post_data = $this->request->getPost();
        $debate = Debates::findFirst(array('slug="' . $post_data['slug'] . '"'));
        if ($debate) {
            $signedInUserID = Tokens::checkAccess();
            $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
            $new_log = new Log();
            $new_log->log_current_action($signedInUserID, $sentHeaders['Ip'], "/debates/update", $debate->getId());
            $app = Apps::findFirst('publisher_id=' . $signedInUserID);
            $validate_feature = PluginsFeature::validate_feature_using_plan_id($app->get_plan(), "edit_plugin");
            if ($app && ($app->getId() == $debate->getAppId()) && $validate_feature) {
                $debate->setTitle(trim($post_data['title']));
                $debate->setMaxDays($post_data['max_days']);
                $debate->setCategoryId($post_data['category_id']);
                $debate->setUpdatedAt(date('y-m-d H:i:s'));
                $di = $this->getDi()->getShared('request');
                $arrPhotos = $di->getUploadedFiles();
                $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
                $sidesData = json_decode($post_data['sides'], true);
                foreach ($arrPhotos as $arrPhoto) {
                    switch ($arrPhoto->getKey()) {
                        case 'debater_1_pic':
                            $debater_1_pic = $objImgUploader->upload($objImgUploader::DIR_DEBATES, array($arrPhoto));
                            if ($debater_1_pic[0]['status'] != 1) {
                                $this->response->setStatusCode(400, 'ERROR');
                                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $debater_1_pic[0]['error']));
                                return $this->response;
                            }
                            $sidesData[0]['image'] = $debater_1_pic;
                        break;
                        case 'debater_2_pic':
                            $debater_2_pic = $objImgUploader->upload($objImgUploader::DIR_DEBATES, array($arrPhoto));
                            if ($debater_2_pic[0]['status'] != 1) {
                                $this->response->setStatusCode(400, 'ERROR');
                                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $debater_2_pic[0]['error']));
                                return $this->response;
                            }
                            $sidesData[1]['image'] = $debater_2_pic;
                        break;
                    }
                }
                $debate->save();
                $this->update_sides_meta($debate->getId(), $sidesData);
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $debate->getSlug()));
            } else {
                $this->response->setStatusCode(401, 'Unauthorized');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            }
        } else {
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('debate-slug-not-exist')));
        }
        return $this->response;
    }
    public function showAction($slug) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $order_by = $this->request->get("order_by");
            if (empty($order_by)) {
                $order_by = "most_voted";
            }
            $app_id = $this->request->get("app_id");
            if (!is_numeric($app_id)) {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1));
                return $this->response;
            }
            $url = $this->request->get("url");
            $domains = array($this->getDI()->getShared('config')->get("application")->get("default-url"));
            $domains[] = $this->getDI()->getShared('config')->get("application")->get("plugins_speakol");
            $app_domains = AppDomains::find('app_id=' . $app_id);
            foreach ($app_domains as $app_domain) {
                $domains[] = $app_domain->get_domain();
            }
            $valid_url = $this->valid_domain($domains, $url);
            if (!$valid_url) {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1, 'free_plan' => $free_plan, 'domains' => $domains));
                return $this->response;
            }
            $data = Debates::showHelper($slug, $order_by);
            if ($data != false) {
                if ($data['app_id'] != $app_id) {
                    $this->response->setStatusCode(201, "ERROR");
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1));
                    return $this->response;
                }
                if ($data['plugin_slider'] == 1) {
                    $next_plugin = $this->get_next($app_id, $data['created_at'], $url);
                    $previous_plugin = $this->get_previous($app_id, $data['created_at'], $url);
                    if (empty($next_plugin)) {
                        $random_plugins = $this->get_random_plugins($app_id, $url);
                        $i = 0;
                        while ($i < 20) {
                            if (count($random_plugins) == 1) {
                                break;
                            }
                            if (count($random_plugins) == 2) {
                                if ($random_plugins[0]['url'] == $url) {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[1]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($previous_plugin['url'] != $random_plugins[1]['url']) {
                                            $next_plugin['type'] = $random_plugins[1]['debates'];
                                            $next_plugin['url'] = $random_plugins[1]['url'];
                                            $next_plugin['title'] = $random_plugins[1]['title'];
                                            $next_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                } else {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[0]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($previous_plugin['url'] != $random_plugins[0]['url']) {
                                            $next_plugin['type'] = $random_plugins[0]['debates'];
                                            $next_plugin['url'] = $random_plugins[0]['url'];
                                            $next_plugin['title'] = $random_plugins[0]['title'];
                                            $next_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                }
                            }
                            $key = array_rand($random_plugins);
                            if ($previous_plugin['url'] != $random_plugins[$key]['url']) {
                                $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[$key]['url'])));
                                if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                    $next_plugin['url'] = $random_plugins[$key]['url'];
                                    $next_plugin['title'] = $random_plugins[$key]['title'];
                                    $next_plugin['type'] = $random_plugins[$key]['debates'];
                                    $next_plugin['hash'] = $interaction->getHash();
                                }
                                break;
                            }
                            $i++;
                        }
                    }
                    if (empty($previous_plugin)) {
                        $random_plugins = $this->get_random_plugins($app_id, $url);
                        $i = 0;
                        while ($i < 20) {
                            if (count($random_plugins) == 1) {
                                break;
                            }
                            if (count($random_plugins) == 2) {
                                if ($random_plugins[0]['url'] == $url) {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[1]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($next_plugin['url'] != $random_plugins[1]['url']) {
                                            $previous_plugin['type'] = $random_plugins[1]['debates'];
                                            $previous_plugin['url'] = $random_plugins[1]['url'];
                                            $previous_plugin['title'] = $random_plugins[1]['title'];
                                            $previous_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                } else {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[0]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($next_plugin['url'] != $random_plugins[0]['url']) {
                                            $previous_plugin['type'] = $random_plugins[0]['debates'];
                                            $previous_plugin['url'] = $random_plugins[0]['url'];
                                            $previous_plugin['title'] = $random_plugins[0]['title'];
                                            $previous_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                }
                            }
                            $key = array_rand($random_plugins);
                            if ($next_plugin['url'] != $random_plugins[$key]['url']) {
                                $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[$key]['url'])));
                                if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                    $previous_plugin['title'] = $random_plugins[$key]['title'];
                                    $previous_plugin['url'] = $random_plugins[$key]['url'];
                                    $previous_plugin['type'] = $random_plugins[$key]['debates'];
                                    $previous_plugin['hash'] = $interaction->getHash();
                                }
                                break;
                            }
                            $i++;
                        }
                    }
                    $hash['next'] = $next_plugin;
                    $hash['previous'] = $previous_plugin;
                }
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data, 'links' => $hash));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function deleteAction($slug) {
        try {
            $signedInUserID = Tokens::checkAccess();
            $data = Debates::deleteDebate($slug, $signedInUserID);
            if ($data == true) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function viewAction($slug) {
        try {
            $data = Debates::viewOneDebate(array('conditions' => "slug='$slug'"));
            if ($data != false && is_array($data)) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function unvoteAction($debateId, $side_id) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            if (Debates::findFirst($debateId) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if (Sides::findFirst($side_id) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if (!Votes::hasVoted($side_id, $signedInUserID)) {
                throw new Exception($this->t->_('not-found'), 302);
            }
            $return = Debates::unvoteActionHelper($debateId, $side_id, $signedInUserID);
            if (FALSE == $return) {
                throw new Exception($this->t->_('conflict'), 409);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $return));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function argumentsAction($slug) {
        try {
            $data = Debates::getArguments($slug, 1, 3);
            if (is_array($data)) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function repliesAction($argument_id) {
        try {
            $data = Replies::getReplies($argument_id, 2, 0);
            if (is_array($data) && $data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
}
