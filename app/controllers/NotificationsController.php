<?php
use Speakol\concreteSocialPluginFactory;
class NotificationsController extends BaseController {
    public function indexAction() {
        try {
            $data = Notifications::find()->toArray();
            if ($data) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function sendmailAction() {
        try {
            $htmlContent = $this->request->getPost('html_content');
            $text = $this->request->getPost('text');
            $subject = $this->request->getPost('subject');
            $fromEmail = $this->request->getPost('from_email');
            $fromName = $this->request->getPost('from_name');
            $toEmail = $this->request->getPost('to_email');
            $message = array('html' => $htmlContent, 'subject' => $text, 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $toEmail, 'type' => 'to')),);
            $async = false;
            $ip_pool = 'Main Pool';
            $data = $this->mandrill->messages->send($message, $async, $ip_pool);
            if ($data) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function createAction() {
        try {
            $params = $this->request->get();
            $notifications = new Notifications();
            $data = $notifications->insertNotification($params);
            if ($data) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function listbyrecipientAction() {
        try {
            $id = Tokens::checkAccess();
            $beacon = new Beacon();
            $current_date_time = new DateTime();
            $beacon->addBeacon($id, $current_date_time->format('Y-m-d H:i:s'));
            $params = $this->request->get();
            $newItemsCount = NotificationRecipients::count(array('conditions' => 'user_id=:user_id: AND seen!=:seen:', 'bind' => array('user_id' => $id, 'seen' => 1),));
            $options = array('conditions' => 'user_id=:user_id:', 'bind' => array('user_id' => $id), 'order' => 'id desc',);
            if (isset($params['limit'])) {
                $offset = ($params['page'] == 1) ? 0 : ($params['page'] - 1) * $params['limit'];
                $options['limit'] = array('number' => $params['limit'], 'offset' => $offset);
            }
            $recipients = NotificationRecipients::find($options);
            $data = array();
            foreach ($recipients as $recipient) {
                $recipientData = $recipient->getNotifications()->toArray();
                $user = Users::findFirst($recipientData['user_id'])->toArray();
                if ($recipientData['admin_at'] != 0) {
                    $app = Apps::findFirst($recipientData['admin_at']);
                    $recipientData['name'] = $app->getName();
                    $recipientData['user_slug'] = $app->getName();
                    $recipientData['image'] = $app->getImage();
                } else {
                    $recipientData['name'] = $user['first_name'] . " " . $user['last_name'];
                    $recipientData['user_slug'] = $user['slug'];
                    $recipientData['image'] = $user['profile_picture'];
                }
                $recipientData['user_id'] = $user['id'];
                $recipientData['created_at'] = Phalcon\DI::getDefault()->getShared('Utility')->formatDateTime($recipientData['created_at']);
                if ($recipientData['notification_type'] == "argumentsbox") {
                    $plugin = Argumentboxes::findFirst($recipientData['notifiable_id']);
                } else if ($recipientData['notification_type'] == "debate") {
                    $plugin = Debates::findFirst($recipientData['notifiable_id']);
                } else {
                    $plugin = Comparisons::findFirst($recipientData['notifiable_id']);
                }
                if ($plugin) {
                    $data[] = $recipientData;
                }
            }
            $data['new_items'] = $newItemsCount;
            if ($data) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function updatebyrecipientAction() {
        try {
            $id = Tokens::checkAccess();
            $query = "UPDATE NotificationRecipients SET seen=1 WHERE user_id=$id";
            $nr = new NotificationRecipients();
            $data = $nr->updateRows($query);
            if ($data) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => true));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function thumbMyArgument($data) {
        $notifications = new Notifications();
        $params = array();
        $params['notification_type'] = $data['module_type'];
        $params['notifiable_id'] = $data['module_id'];
        $factory = new concreteSocialPluginFactory();
        $module = $factory->make($data['module_type']);
        $moduleData = $module->findFirst($data['module_id']);
        $params['url'] = '';
        $params['extra_info'] = array();
        if ($moduleData) {
            $moduleData = $moduleData->toArray();
            $params['url'] = $data['link'];
            if (!empty($data['app_id'])) {
                $app = Apps::findFirst($data['app_id']);
                if ($app != false) {
                    $app = $app->toArray();
                    $params['extra_info']['app_id'] = $app['id'];
                    $params['extra_info']['app_name'] = $app['name'];
                    $params['extra_info']['referrer'] = $data['referrer'];
                    $params['extra_info']['referrer'] = $data['link'];
                    $params['title'] = $moduleData['title'];
                }
            }
        }
        $params['extra_info'] = json_encode($params['extra_info']);
        if ($data['action'] == "up") {
            $params['notifiable_type'] = 'arg-thumbup';
        } else {
            $params['notifiable_type'] = 'arg-thumbdown';
        }
        $params['user_id'] = Tokens::checkAccess();
        $params['recipient_id'] = $data['argument_owner'];
        $params['admin_at'] = $data['admin_at'];
        $notifications->insertNotification($params);
        $user = Users::findFirst($data['argument_owner']);
        if ($user->get_subscribe() == 1) {
            $this->getDI()->set('language', $user->getLocale());
            $user2 = Users::findFirst($params['user_id']);
            if ($params['admin_at'] != 0) {
                $app = Apps::findFirst($params['admin_at']);
                $user2->set_first_name($app->getName());
                $user2->set_last_name("");
                $user2->setProfilePicture($app->getImage());
            }
            if ($user->getRoleId() == Roles::USER) {
                $this->view->update = "http://www.speakol.com/users/update";
            } else {
                $this->view->update = "http://plugins.speakol.com/apps/settings";
            }
            $this->view->user = $user->toArray();
            $this->view->user2 = $user2->toArray();
            $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
            $this->view->name2 = $user2->get_first_name() . " " . $user2->get_last_name();
            if ($data['action'] == "up") {
                $this->view->action = $this->t->_('support-your-comment');
            } else {
                $this->view->action = $this->t->_('down-vote-your-comment');
            }
            $this->view->message = $this->t->_('notification-content');
            $this->view->message2 = $this->t->_('notification-content-2');
            $this->view->title = $params['title'];
            $this->view->link = $params['url'];
            $view = clone $this->view;
            $view->start();
            $view->render('emails', 'notification');
            $view->finish();
            $content = $view->getContent();
            $message = array('html' => $content, 'subject' => $this->t->_('mail-notification'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
            $async = false;
            $ip_pool = 'Main Pool';
            $result = $this->mandrill->messages->send($message, $async, $ip_pool);
        }
    }
    public function thumbMyReply($data) {
        $notifications = new Notifications();
        $params = array();
        $params['notification_type'] = $data['module_type'];
        $params['notifiable_id'] = $data['module_id'];
        $factory = new concreteSocialPluginFactory();
        $module = $factory->make($data['module_type']);
        $moduleData = $module->findFirst($data['module_id']);
        $params['url'] = '';
        if (!empty($data['app_id'])) {
            $app = Apps::findFirst($data['app_id']);
            if ($app != false) {
                $app = $app->toArray();
                $params['extra_info']['app_id'] = $app['id'];
                $params['extra_info']['app_name'] = $app['name'];
                $params['extra_info']['referrer'] = $data['referrer'];
                $params['url'] = $data['link'];
                $params['extra_info']['referrer'] = $data['link'];
                $params['title'] = $moduleData->getTitle();
            }
        }
        $params['extra_info'] = json_encode($params['extra_info']);
        if ($data['action'] == "up") {
            $params['notifiable_type'] = 'reply-thumbup';
        } else {
            $params['notifiable_type'] = 'reply-thumbdown';
        }
        $params['user_id'] = Tokens::checkAccess();
        $params['recipient_id'] = $data['reply_owner'];
        $params['admin_at'] = $data['admin_at'];
        $notifications->insertNotification($params);
        $user = Users::findFirst($data['reply_owner']);
        $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
        if ($user->get_subscribe() == 1) {
            $this->getDI()->set('language', $user->getLocale());
            $user2 = Users::findFirst($params['user_id']);
            $this->view->name2 = $user2->get_first_name() . " " . $user2->get_last_name();
            if ($params['admin_at'] != 0) {
                $app = Apps::findFirst($params['admin_at']);
                $this->view->name2 = $app->getName();
                $user2->setProfilePicture($app->getImage());
            }
            if ($user->getRoleId() == Roles::USER) {
                $this->view->update = "http://www.speakol.com/users/update";
            } else {
                $this->view->update = "http://plugins.speakol.com/apps/settings";
            }
            $this->view->user = $user->toArray();
            $this->view->user2 = $user2->toArray();
            if ($data['action'] == "up") {
                $this->view->action = $this->t->_('support-your-reply');
            } else {
                $this->view->action = $this->t->_('down-vote-your-reply');
            }
            $this->view->message = $this->t->_('notification-content');
            $this->view->message2 = $this->t->_('notification-content-2');
            $this->view->title = $params['title'];
            $this->view->link = $params['url'];
            $view = clone $this->view;
            $view->start();
            $view->render('emails', 'notification');
            $view->finish();
            $content = $view->getContent();
            $message = array('html' => $content, 'subject' => $this->t->_('mail-notification'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
            $async = false;
            $ip_pool = 'Main Pool';
            $result = $this->mandrill->messages->send($message, $async, $ip_pool);
        }
    }
    public function replyMyArgument($data) {
        $notifications = new Notifications();
        $params = array();
        $params['notification_type'] = $data['module_type'];
        $params['notifiable_id'] = $data['module_id'];
        $factory = new concreteSocialPluginFactory();
        $module = $factory->make($data['module_type']);
        $moduleData = $module->findFirst($data['module_id']);
        $params['url'] = '';
        if ($moduleData) {
            $moduleData = $moduleData->toArray();
            $params['url'] = $data['link'];
            if (!empty($data['app_id'])) {
                $app = Apps::findFirst($data['app_id']);
                if ($app != false) {
                    $app = $app->toArray();
                    $params['extra_info']['app_id'] = $app['id'];
                    $params['extra_info']['app_name'] = $app['name'];
                    $params['extra_info']['referrer'] = $data['referrer'];
                    $params['title'] = $moduleData['title'];
                }
            }
        }
        $params['extra_info'] = json_encode($params['extra_info']);
        $params['notifiable_type'] = 'arg-reply';
        $params['user_id'] = Tokens::checkAccess();
        $params['recipient_id'] = $data['argument_owner'];
        $params['admin_at'] = $data['admin_at'];
        $notifications->insertNotification($params);
        $user = Users::findFirst($data['argument_owner']);
        $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
        if ($user->get_subscribe() == 1) {
            $this->getDI()->set('language', $user->getLocale());
            $user2 = Users::findFirst($params['user_id']);
            $this->view->name2 = $user2->get_first_name() . " " . $user2->get_last_name();
            if ($params['admin_at'] != 0) {
                $app = Apps::findFirst($params['admin_at']);
                $this->view->name2 = $app->getName();
                $user2->setProfilePicture($app->getImage());
            }
            $this->view->user = $user->toArray();
            $this->view->user2 = $user2->toArray();
            if ($user->getRoleId() == Roles::USER) {
                $this->view->update = "http://www.speakol.com/users/update";
            } else {
                $this->view->update = "http://plugins.speakol.com/apps/settings";
            }
            $this->view->action = $this->t->_('reply-on-comment');
            $this->view->link = $params['url'];
            $this->view->message = $this->t->_('notification-content');
            $this->view->message2 = $this->t->_('notification-content-2');
            $this->view->title = $params['title'];
            $view = clone $this->view;
            $view->start();
            $view->render('emails', 'notification');
            $view->finish();
            $content = $view->getContent();
            $message = array('html' => $content, 'subject' => $this->t->_('mail-notification'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
            $async = false;
            $ip_pool = 'Main Pool';
            $result = $this->mandrill->messages->send($message, $async, $ip_pool);
        }
    }
}
