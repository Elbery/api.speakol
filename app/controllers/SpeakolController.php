<?php
class SpeakolController extends BaseController {
    public function show_intro() {
        $speakol_intro = SpeakolIntro::findFirst("lang='" . $this->request->get("lang") . "'");
        if (!$speakol_intro) {
            $speakol_intro = SpeakolIntro::findFirst("lang='en'");
        }
        if ($speakol_intro) {
            $data = array('feeds' => $speakol_intro->get_description(), 'dashboard' => $speakol_intro->get_dashboard());
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => $data));
        return $this->response;
    }
    public function update_intro() {
        $json_data = json_decode(file_get_contents("php://input"));
        $user_id = Tokens::checkAccess();
        $new_log = new Log();
        if (!$user_id) {
            $log_msg = "{/speakol/intro/update,update speakol introducion without token,response message :Unauthorized,status:ERROR}";
            $new_log->log_current_action(0, $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            return $this->response;
        }
        $speakol_intro = SpeakolIntro::findFirst("lang='" . $json_data->speakol_lang . "'");
        $user = Users::findFirst($user_id);
        $this->getDI()->set('language', $user->getLocale());
        if ($json_data->description == "") {
            $log_msg = "{/speakol/intro/update,update speakol introducion with invalid description" . $json_data->description . ",response message :Invalid description,status:ERROR}";
            $new_log->log_current_action($user_id, $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(601, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('invalid-description')));
            return $this->response;
        }
        if (!$speakol_intro) {
            $speakol_intro = new SpeakolIntro();
            $speakol_intro->set_lang($json_data->speakol_lang);
        }
        $speakol_intro->set_description($json_data->description);
        if ($json_data->dashboard != "") {
            $speakol_intro->set_dashboard($json_data->dashboard);
        }
        $speakol_intro->save();
        $new_log = new Log();
        $new_log->log_current_action($user_id, $this->headers['Ip'], "/speakol/intro/update", $speakol_intro->get_id());
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'message' => $this->t->_('updated-success')));
        return $this->response;
    }
    public function highlight_plugin() {
        $json_data = json_decode(file_get_contents("php://input"));
        $plugin_id = $json_data->plugin_id;
        $plugin_type = $json_data->plugin_type;
        $user_id = Tokens::checkAccess();
        if ($plugin_type == "debates") {
            $plugin = Debates::findFirst($plugin_id);
        } elseif ($plugin_type == "comparisons") {
            $plugin = Comparisons::findFirst($plugin_id);
        } else {
            $plugin = Argumentboxes::findFirst($plugin_id);
        }
        $new_log = new Log();
        if (!$plugin) {
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not Found'));
            return $this->response;
        }
        $lang = $plugin->get_lang();
        $highlighted_plugin = HighlightedPlugins::findFirst("plugin_id=$plugin_id AND plugin_type='$plugin_type'");
        if ($highlighted_plugin) {
            $new_log->log_current_action($user_id, $this->headers['Ip'], "/speakol/highlight/add Plugin id already exists", $highlighted_plugin->get_id());
            $this->response->setStatusCode(302, 'Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Found'));
        } else {
            $highlighted_plugin = new HighlightedPlugins();
            $highlighted_plugin->set_plugin_id($plugin_id);
            $highlighted_plugin->set_plugin_type($plugin_type);
            $highlighted_plugin->set_lang($lang);
            $highlighted_plugin->save();
            $new_log->log_current_action($user_id, $this->headers['Ip'], "/speakol/highlight/add", $highlighted_plugin->get_id());
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'message' => $this->t->_('added-success')));
        }
        return $this->response;
    }
    public function unhighlight_plugin() {
        $plugin_id = $this->request->get("plugin_id");
        $plugin_type = $this->request->get("plugin_type");
        $user_id = Tokens::checkAccess();
        $new_log = new Log();
        $highlighted_plugin = HighlightedPlugins::findFirst("plugin_id=$plugin_id AND plugin_type='$plugin_type'");
        if ($highlighted_plugin) {
            $new_log->log_current_action($user_id, $this->headers['Ip'], "/speakol/highlight/remove", $highlighted_plugin->get_id());
            $highlighted_plugin->delete();
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'message' => $this->t->_('removed-success')));
        } else {
            $new_log->log_current_action($user_id, $this->headers['Ip'], "/speakol/highlight/remove Id doesn't exist", 0);
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not Found'));
        }
        return $this->response;
    }
}
