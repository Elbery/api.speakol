<?php
use Phalcon\DI\FactoryDefault;
class BillingController extends BaseController {
    public static function add_user_card_data($app_id, $card_data) {
        $user_card = UserCard::findFirst('app_id=' . $app_id);
        $new_card = new UserCard();
        if (!$user_card) {
            $new_card->set_default_card(1);
        }
        $new_card->set_app_id($app_id);
        $new_card->set_payment_method($card_data->method);
        $new_card->set_code($card_data->code);
        $new_card->set_name($card_data->name);
        $new_card->set_number($card_data->number);
        $new_card->set_expiry_year($card_data->year);
        $new_card->set_expiry_month($card_data->month);
        $new_card->set_postal_code($card_data->postal_code);
        $new_card->set_city($card_data->city);
        $new_card->set_state($card_data->state);
        $new_card->set_address($card_data->address);
        $new_card->set_address2(($card_data->address2) ? $card_data->address2 : "");
        $new_card->set_country_id($card_data->country_id);
        $new_card->save();
        return $new_card->get_id();
    }
    public function send_invoice($invoice, $plan) {
        $file = BillingController::generate_pdf("en", "Invoice", $plan, $invoice);
        $user = Users::findFirst($invoice->get_user_id());
        $this->view->plan = $plan;
        $this->view->name = $invoice->get_user_name();
        $this->getDI()->set('language', $user->getLocale());
        $this->view->lang = $user->getLocale();
        $this->view->amount = $invoice->get_amount();
        $date = date_create($invoice->get_paying_date());
        $this->view->date = date_format($date, 'd/m/Y');
        $this->assets->addCss('css/email.css');
        $this->view->start();
        $this->view->render('emails', 'invoice');
        $this->view->finish();
        $content = $this->view->getContent();
        $message = array('html' => $content, 'subject' => $this->t->_('invoice-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $invoice->get_user_email(), 'type' => 'to')), 'attachments' => array(array('type' => 'application/pdf', 'name' => 'Invoice.pdf', 'content' => base64_encode($file))),);
        $async = false;
        $ip_pool = 'Main Pool';
        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
    }
    function validate_card_data($card_data) {
        try {
            $customer = \Stripe\Customer::create(array('source' => array('name' => $card_data->name, 'object' => 'card', 'number' => $card_data->number, 'exp_month' => $card_data->month, 'exp_year' => $card_data->year, 'cvc' => $card_data->code,)));
            $customer->delete();
            return array("status" => "OK");
        }
        catch(\Stripe\Error\Card $e) {
            $error = $e->getJsonBody();
            if ($error['error']['message'] == "Your card number is incorrect.") {
                $message = FactoryDefault::getDefault()->get('t')->_("wrong-card-number");
            } else if ($error['error']['message'] == "Your card's expiration year is invalid.") {
                $message = FactoryDefault::getDefault()->get('t')->_("wrong-card-year");
            } else if ($error['error']['message'] == "Your card's expiration month is invalid.") {
                $message = FactoryDefault::getDefault()->get('t')->_("wrong-card-month");
            } else if ($error['error']['message'] == "Your card's security code is invalid.") {
                $message = FactoryDefault::getDefault()->get('t')->_("wrong-card-code");
            } else if ($error['error']['message'] == "This card number looks invalid.") {
                $message = FactoryDefault::getDefault()->get('t')->_("wrong-card");
            } else {
                $message = FactoryDefault::getDefault()->get('t')->_("card-connection-error");
            }
        }
        catch(Exception $ex) {
            $message = FactoryDefault::getDefault()->get('t')->_("card-connection-error");
        }
        return array("status" => "ERROR", "message" => $message);
    }
    public function validate_add_card($card_data, $app_id) {
        $valid_name = preg_match("/^[\p{L}\p{N} -]+$/u", $card_data->name);
        if (!$valid_name) {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-name"));
        }
        if (!ctype_digit($card_data->code)) {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("wrong-card-code"));
        }
        if (!ctype_digit($card_data->year)) {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("wrong-card-year"));
        }
        if (!ctype_digit($card_data->month)) {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("wrong-card-month"));
        }
        if (strlen($card_data->postal_code) > 16 || strlen($card_data->postal_code) < 3) {
            $log_msg = "{/billing/add/card,Add credit card with invalid postal code,response message:Invalid postal code,user_card postal code :" . $card_data->postal_code . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-postal")));
            return $this->response;
        }
        if (strlen($card_data->city) > 64 || strlen($card_data->city) < 2) {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-city"));
        }
        if (strlen($card_data->state) > 64) {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-state"));
        }
        if (strlen($card_data->address) > 128 || strlen($card_data->address) < 2) {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-address"));
        }
        if (!is_numeric($card_data->country_id)) {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-country"));
        }
        $valid_card = $this->validate_card_data($card_data);
        if ($valid_card['status'] == "OK") {
            $card_id = BillingController::add_user_card_data($app_id, $card_data);
            return array('status' => 'OK', 'data' => $card_id);
        }
        return array('status' => 'ERROR', 'message' => $valid_card['message']);
    }
    public function show_card($card_id) {
        $signedInUserID = Tokens::checkAccess();
        $app_admin = AppAdmins::findFirst('user_id=' . $signedInUserID);
        $app = Apps::findFirst($app_admin->getAppId());
        $new_log = new Log();
        $card = UserCard::findFirst(array('conditions' => "id=$card_id AND app_id=" . $app->getId(), 'columns' => array('id', 'name', 'number', 'payment_method', 'expiry_year', 'expiry_month', 'default_card', 'postal_code', 'city', 'state', 'address', 'country_id', 'address2')));
        if ($card) {
            $card['number'] = substr_replace($card['number'], "**** **** **** ", 0, strlen($card['number']) - 4);
            $log_msg = "{/billing/card/show/{card_id:[0-9]+},show card id $card_id,response message:ok with card data:" . $card['number'] . "}";
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $card));
        } else {
            $log_msg = "{/billing/card/show/{card_id:[0-9]+},show card id $card_id,response message:Not found}";
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
        }
        $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
        return $this->response;
    }
    public function add_card_for_app() {
        $signedInUserID = Tokens::checkAccess();
        $card_data = json_decode(file_get_contents("php://input"));
        $new_log = new Log();
        $card_data->postal_code = filter_var($card_data->postal_code, FILTER_SANITIZE_STRING);
        $card_data->state = filter_var($card_data->state, FILTER_SANITIZE_STRING);
        $card_data->city = filter_var($card_data->city, FILTER_SANITIZE_STRING);
        $card_data->address = filter_var($card_data->address, FILTER_SANITIZE_STRING);
        $card_data->address2 = filter_var($card_data->address2, FILTER_SANITIZE_STRING);
        if ($card_data->code == "") {
            $log_msg = "{/billing/add/card,Add credit card with invalid code,response message:Invalid code,user_card code :" . $card_data->code . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid code'));
            return $this->response;
        }
        if ($card_data->number == "") {
            $log_msg = "{/billing/add/card,Add credit card with invalid Number,response message:Invalid number,user_card number :" . $card_data->number . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid number'));
            return $this->response;
        }
        $this->getDI()->set('language', $card_data->lang);
        $app_admin = AppAdmins::findFirst('user_id=' . $signedInUserID);
        $app = Apps::findFirst($app_admin->getAppId());
        $add_card = $this->validate_add_card($card_data, $app->getId());
        if ($add_card['status'] == "OK") {
            $log_msg = "{/billing/add/card,Add credit card,response message:OK,user_card number:" . $card_data->number . ",status:OK}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $add_card['data']);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => array('card_id' => $add_card['data'])));
        } else {
            $log_msg = "{/billing/add/card,Add credit card,response message:" . $add_card['message'] . ",user_card number:" . $card_data->number . " card code:" . $card_data->code . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(600, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $add_card['message']));
        }
        return $this->response;
    }
    public function send_receipt($user_email, $receipt, $invoice, $plan) {
        $file = BillingController::generate_pdf("en", "Receipt", $plan, $invoice, $receipt);
        $user = Users::findFirst("email='$user_email'");
        $this->view->user = $user->toArray();
        $this->view->plan = $plan;
        $this->view->name = $receipt->get_user_name();
        $this->getDI()->set('language', $user->getLocale());
        $this->view->amount = $invoice->get_amount();
        $this->assets->addCss('css/email.css');
        $this->view->start();
        $this->view->render('emails', 'payment');
        $this->view->finish();
        $content = $this->view->getContent();
        $message = array('html' => $content, 'subject' => $this->t->_('success_payment'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user_email, 'type' => 'to')), 'attachments' => array(array('type' => 'application/pdf', 'name' => 'Receipt.pdf', 'content' => base64_encode($file))),);
        $async = false;
        $ip_pool = 'Main Pool';
        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
    }
    public function delete_card_action($card_id) {
        $signedInUserID = Tokens::checkAccess();
        $json_data = json_decode(file_get_contents("php://input"));
        $app_admin = AppAdmins::findFirst('user_id=' . $signedInUserID);
        $app = Apps::findFirst($app_admin->getAppId());
        $user_card = UserCard::findFirst("id=$card_id AND app_id=" . $app->getId());
        $new_log = new Log();
        if ($user_card) {
            $log_msg = "{/billing/card/delete,Delete credit card,response message:OK,user_card number :" . $user_card->get_number() . ", card_id:" . $user_card->get_id() . ",status:OK}";
            $user_card->delete();
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'Removed successfully'));
        } else {
            $log_msg = "{/billing/card/delete,Delete credit card with card id $card_id does not exist,response message:Not found,status:failed}";
            $this->response->setStatusCode(404, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
        }
        $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $card_id);
        return $this->response;
    }
    public function update_card_action() {
        $signedInUserID = Tokens::checkAccess();
        $card_data = json_decode(file_get_contents("php://input"));
        $new_log = new Log();
        if (!$card_data->id) {
            $log_msg = "{/billing/card/update,Update credit card without card id,response message:Not found,status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, "");
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
            return $this->response;
        }
        $user_card = UserCard::findFirst($card_data->id);
        if (!$user_card) {
            $log_msg = "{/billing/card/update,Update credit card with card id does not exist,response message:Not found,user_card id :" . $card_data->id . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $card_data->id);
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
            return $this->response;
        }
        $this->getDI()->set('language', $card_data->lang);
        $valid_name = preg_match("/^[\p{L}\p{N} -]+$/u", $card_data->name);
        if (!$valid_name) {
            $log_msg = "{/billing/card/update,Update credit card with Invalid name,response message:" . FactoryDefault::getDefault()->get('t')->_("invalid-name") . ",user_card name:" . $card_data->name . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $card_data->id);
            $this->response->setStatusCode(601, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-name")));
            return $this->response;
        }
        if (!ctype_digit($card_data->year)) {
            $log_msg = "{/billing/card/update,Update credit card with Invalid expiry year,response message:" . FactoryDefault::getDefault()->get('t')->_("wrong-card-year") . ",user_card year:" . $card_data->year . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $card_data->id);
            $this->response->setStatusCode(601, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("wrong-card-year")));
            return $this->response;
        }
        if (!ctype_digit($card_data->month)) {
            $log_msg = "{/billing/card/update,Update credit card with Invalid expiry month,response message:" . FactoryDefault::getDefault()->get('t')->_("wrong-card-month") . ",user_card month:" . $card_data->month . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $card_data->id);
            $this->response->setStatusCode(601, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("wrong-card-month")));
            return $this->response;
        }
        if ($card_data->number) {
            $number = $card_data->number;
        } else {
            $number = $user_card->get_number();
        }
        if ($card_data->code) {
            if (!ctype_digit($card_data->code)) {
                $log_msg = "{/billing/card/update,Update credit card with Invalid code,response message:" . FactoryDefault::getDefault()->get('t')->_("wrong-card-code") . ",user_card code:" . $card_data->code . ",status:failed}";
                $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $card_data->id);
                $this->response->setStatusCode(601, 'ERROR');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("wrong-card-code")));
                return $this->response;
            }
            $code = $card_data->code;
        } else {
            $code = $user_card->get_code();
        }
        $card_data->number = $number;
        $card_data->code = $code;
        $card_data->postal_code = filter_var($card_data->postal_code, FILTER_SANITIZE_STRING);
        $card_data->state = filter_var($card_data->state, FILTER_SANITIZE_STRING);
        $card_data->city = filter_var($card_data->city, FILTER_SANITIZE_STRING);
        $card_data->address = filter_var($card_data->address, FILTER_SANITIZE_STRING);
        $card_data->address2 = filter_var($card_data->address2, FILTER_SANITIZE_STRING);
        if (strlen($card_data->postal_code) > 16) {
            $log_msg = "{/billing/add/card,Add credit card with invalid postal code,response message:Invalid postal code,user_card postal code :" . $card_data->postal_code . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $add_card['data']);
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-postal")));
            return $this->response;
        }
        if (strlen($card_data->city) > 64 || strlen($card_data->city) < 2) {
            $log_msg = "{/billing/add/card,Add credit card with invalid city,response message:Invalid city,user_card city :" . $card_data->city . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $add_card['data']);
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-city")));
            return $this->response;
        }
        if (strlen($card_data->state) > 64 || strlen($card_data->state) < 2) {
            $log_msg = "{/billing/add/card,Add credit card with invalid state,response message:Invalid state,user_card state :" . $card_data->state . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $add_card['data']);
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-state")));
            return $this->response;
        }
        if (strlen($card_data->address) > 128 || strlen($card_data->address) < 2) {
            $log_msg = "{/billing/add/card,Add credit card with invalid address,response message:Invalid address,user_card address :" . $card_data->address . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $add_card['data']);
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-address")));
            return $this->response;
        }
        if (!is_numeric($card_data->country_id)) {
            $log_msg = "{/billing/add/card,Add credit card with invalid country id,response message:Invalid country id,user_card country id :" . $card_data->country_id . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $add_card['data']);
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-country")));
            return $this->response;
        }
        $valid_card = $this->validate_card_data($card_data);
        if ($valid_card['status'] == "OK") {
            $user_card->set_name($card_data->name);
            $user_card->set_payment_method($card_data->method);
            $user_card->set_code($code);
            $user_card->set_number($number);
            $user_card->set_expiry_year($card_data->year);
            $user_card->set_expiry_month($card_data->month);
            $user_card->set_postal_code($card_data->postal_code);
            $user_card->set_city($card_data->city);
            $user_card->set_state($card_data->state);
            $user_card->set_address($card_data->address);
            $user_card->set_address2($card_data->address2);
            $user_card->set_country_id($card_data->country_id);
            $user_card->save();
            $log_msg = "{/billing/card/update,Update credit card with valid data,response message:OK,user_card number:" . $card_data->number . ",status:ok}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $card_data->id);
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'message' => 'Updated successfully'));
        } else {
            $log_msg = "{/billing/card/update,Update credit card with Invalid data,response message:" . $valid_card['message'] . ",user_card:" . $card_data->number . ",status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $card_data->id);
            $this->response->setStatusCode(600, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $valid_card['message']));
        }
        return $this->response;
    }
    public function user_card_exist() {
        $signedInUserID = Tokens::checkAccess();
        $new_log = new Log();
        if (!$signedInUserID) {
            $log_msg = "{/billing/user/card/exist,status:unauthorized}";
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
        } else {
            $app_admin = AppAdmins::findFirst('user_id=' . $signedInUserID);
            $app = Apps::findFirst($app_admin->getAppId());
            $card_exist = UserCard::findFirst(array('conditions' => 'app_id=:app_id:', 'bind' => array('app_id' => $app->getId())));
            if ($card_exist) {
                $log_msg = "{/billing/user/card/exist,card exists,status:OK}";
                $this->response->setStatusCode(200, 'OK');
                $this->response->setJsonContent(array('status' => 'OK', 'card_exists' => True));
            } else {
                $log_msg = "{/billing/user/card/exist,card doesn't exist,status:OK}";
                $this->response->setStatusCode(200, 'OK');
                $this->response->setJsonContent(array('status' => 'OK', 'card_exists' => False));
            }
        }
        $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
        return $this->response;
    }
    public static function charge_user($card_data, $amount, $description) {
        try {
            $charge = \Stripe\Charge::create(array('source' => array('name' => $card_data->get_name(), 'object' => 'card', 'number' => $card_data->get_number(), 'exp_month' => $card_data->get_expiry_month(), 'exp_year' => $card_data->get_expiry_year(), 'cvc' => intval($card_data->get_code()),), 'amount' => $amount * 100, 'currency' => 'usd', 'description' => $description));
            return array('status' => 'OK', 'data' => $charge);
        }
        catch(\Stripe\Error\Card $e) {
            $error = $e->getJsonBody();
            if ($error['error']['message'] == "Your card number is incorrect.") {
                $message = FactoryDefault::getDefault()->get('t')->_("wrong-card-number");
            } else if ($error['error']['message'] == "Your card's expiration year is invalid.") {
                $message = FactoryDefault::getDefault()->get('t')->_("wrong-card-year");
            } else if ($error['error']['message'] == "Your card's expiration month is invalid.") {
                $message = FactoryDefault::getDefault()->get('t')->_("wrong-card-month");
            } else if ($error['error']['message'] == "Your card's security code is invalid.") {
                $message = FactoryDefault::getDefault()->get('t')->_("wrong-card-code");
            } else {
                $message = FactoryDefault::getDefault()->get('t')->_("connection-error");
            }
            return array('status' => 'ERROR', 'message' => $message);
        }
        catch(Exception $ex) {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("connection-error"));
        }
    }
    public function upgrade_action() {
        $new_log = new Log();
        $signedInUserID = Tokens::checkAccess();
        $json_data = json_decode(file_get_contents("php://input"));
        $plan_id = intval($json_data->plan_id);
        $current_date = new DateTime();
        $app_admin = AppAdmins::findFirst('user_id=' . $signedInUserID);
        $app = Apps::findFirst($app_admin->getAppId());
        $user = Users::findFirst($app->getPublisherId());
        $this->getDI()->set("language", $user->getLocale());
        if ($plan_id < 1 || $plan_id > 4 || ($plan_id < $app->get_plan())) {
            $log_msg = "{/billing/upgrade,try to upgrade to plan id=$plan_id,response message : Invalid plan id,status:failed}";
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "Invalid plan id"));
        } else {
            if ($app->get_plan() !== $plan_id) {
                $plan = Plan::findFirst($json_data->plan_id);
                $user_card = UserCard::findFirst('app_id=' . $app->getId());
                $old_plan = Plan::findFirst($app->get_plan());
                if ($app->get_plan() === 1) {
                    $amount = $plan->get_amount();
                    $charging_response = $this->charge_user($user_card, $amount, "Upgrade from free to " . $plan->get_name());
                    if ($charging_response['status'] == 'ERROR') {
                        $log_msg = "{/billing/upgrade,try to upgrade from free plan to " . $plan->get_name() . ",response message :" . $charging_response['message'] . ",status:failed}";
                        $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
                        $this->response->setStatusCode(600, 'ERROR');
                        $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('charging-error')));
                        return $this->response;
                    }
                    $log_msg = "{/billing/upgrade,upgrade from free plan to " . $plan->get_name() . ",response message :OK,status:OK,amount:$amount,stripe charge_id : " . $charging_response['data']->id . "}";
                    $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $charging_response['data']->id);
                    $app->set_subscription_date(date('Y-m-d H:i:s'));
                    $downgraded_log = DowngradedAppsLog::findFirst('app_id=' . $app->getId());
                    if ($downgraded_log) {
                        $downgraded_log->delete();
                    }
                } else {
                    $subscription_date = new DateTime($app->get_subscription_date());
                    $subscription_date->setTime(0, 0, 0);
                    $month_later = clone $subscription_date;
                    $month_later->add(new DateInterval("P1M"));
                    $diff = date_diff($subscription_date, $month_later);
                    $total_days = $diff->format('%a');
                    $diff2 = date_diff($subscription_date, $current_date);
                    $used_days = $diff2->format('%a');
                    $new_days = $total_days - $used_days;
                    $cost_per_day_old_plan = $old_plan->get_amount() / $total_days;
                    $used_days_cost = $cost_per_day_old_plan * $used_days;
                    $cost_per_day_new_plan = $plan->get_amount() / $total_days;
                    $new_days_cost = $cost_per_day_new_plan * $new_days;
                    $remaining_cost = $old_plan->get_amount() - $used_days_cost;
                    $amount = round(($new_days_cost - $remaining_cost), 2);
                    $charging_response = $this->charge_user($user_card, $amount, "Upgrade to " . $plan->get_name());
                    if ($charging_response['status'] == 'ERROR') {
                        $log_msg = "{/billing/upgrade,try to upgrade from " . $old_plan->get_name() . " plan to " . $plan->get_name() . ",response message :" . $charging_response['message'] . ",status:failed}";
                        $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
                        $this->response->setStatusCode(600, 'ERROR');
                        $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('charging-error')));
                        return $this->response;
                    }
                    $log_msg = "{/billing/upgrade,upgrade from " . $old_plan->get_name() . " plan to " . $plan->get_name() . ",response message :OK,status:OK,amount:$amount,stripe charge_id : " . $charging_response['data']->id . "}";
                    $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $charging_response['data']->id);
                    if ($app->get_pending_plan() !== 0) {
                        $app->set_pending_plan(0);
                    }
                }
                $app->set_plan($json_data->plan_id);
                $app->save();
                $this->create_invoice_receipt($amount, $user_card->get_payment_method(), $user_card->get_number(), $user->getId(), $user->getEmail(), $charging_response['data']->id, $plan->get_name(), $user->get_first_name() . " " . $user->get_last_name(), $app->get_address(), $app->getWebsite());
                $this->send_upgrade_email($user, $old_plan->get_id(), $plan->get_id());
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => array('plan_id' => $plan->get_id(), 'amount' => $amount)));
            } else {
                $this->response->setStatusCode(600, 'ERROR');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "You are already in this plan"));
            }
        }
        return $this->response;
    }
    public function send_upgrade_email($user, $plan_1, $plan_2) {
        $old_plan = Plan::findFirst($plan_1);
        $new_plan = Plan::findFirst($plan_2);
        $old_plan_features = PluginsFeature::find('plan_id=' . $plan_1);
        $plugins_features = PluginsFeature::find('plan_id=' . $plan_2);
        $features = array();
        $old_features = array();
        foreach ($plugins_features as $plugin_feature) {
            $feature = Feature::findFirst($plugin_feature->get_feature_id());
            $feature_name = $feature->get_name();
            array_push($features, $this->t->_($feature_name));
        }
        foreach ($old_plan_features as $plugin_feature) {
            $feature = Feature::findFirst($plugin_feature->get_feature_id());
            $feature_name = $feature->get_name();
            array_push($old_features, $this->t->_($feature_name));
        }
        $features = array_diff($features, $old_features);
        $features = array_slice($features, 0, 3);
        $this->getDI()->set("language", $user->getLocale());
        $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
        $this->view->old_plan = $old_plan->get_name();
        $this->view->new_plan = $new_plan->get_name();
        $this->view->features = $features;
        $this->view->lang = $user->getLocale();
        $this->assets->addCss('css/email.css');
        $this->view->start();
        $this->view->render('emails', 'upgrade');
        $this->view->finish();
        $message = array('html' => $this->view->getContent(), 'subject' => $this->t->_('upgrade-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
        $async = false;
        $ip_pool = 'Main Pool';
        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
    }
    public function send_downgrade_email($user, $plan_1, $plan_2, $date) {
        $old_plan = Plan::findFirst($plan_1);
        $new_plan = Plan::findFirst($plan_2);
        $old_plan_features = PluginsFeature::find('plan_id=' . $plan_1);
        $plugins_features = PluginsFeature::find('plan_id=' . $plan_2);
        $features = array();
        $old_features = array();
        foreach ($plugins_features as $plugin_feature) {
            $feature = Feature::findFirst($plugin_feature->get_feature_id());
            $feature_name = $feature->get_name();
            array_push($features, $this->t->_($feature_name));
        }
        foreach ($old_plan_features as $plugin_feature) {
            $feature = Feature::findFirst($plugin_feature->get_feature_id());
            $feature_name = $feature->get_name();
            array_push($old_features, $this->t->_($feature_name));
        }
        $features = array_diff($old_features, $features);
        $features = array_slice($features, 0, 3);
        $this->view->name = $user->get_first_name() . " " . $user->get_last_name();
        $this->view->old_plan = $old_plan->get_name();
        $this->view->new_plan = $new_plan->get_name();
        $this->view->features = $features;
        $this->view->date = $date;
        $this->view->lang = $user->getLocale();
        $this->assets->addCss('css/email.css');
        $this->view->start();
        $this->view->render('emails', 'downgrade');
        $this->view->finish();
        $body = $this->view->getContent();
        $message = array('html' => $body, 'subject' => $this->t->_('downgrade-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user->getEmail(), 'type' => 'to')),);
        $async = false;
        $ip_pool = 'Main Pool';
        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
    }
    public function show_receipt($id) {
        $receipt = Receipt::findFirst($id);
        $signedInUserID = Tokens::checkAccess();
        $new_log = new Log();
        if ($receipt) {
            $log_msg = "{/billing/receipt/show/{id:[0-9]+},show receipt id $id,response message:OK with receipt data}";
            $invoice = Invoice::findFirst($receipt->get_invoice_id());
            $app_admin = AppAdmins::findFirst('user_id=' . $signedInUserID);
            $app = Apps::findFirst($app_admin->getAppId());
            if ($app->getPublisherId() != $invoice->get_user_id()) {
                $this->response->setStatusCode(404, 'Not found');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('receipt-not-found')));
                return $this->response;
            }
            $plan = Plan::findFirst($invoice->get_plan());
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => array('invoice_no' => $invoice->get_id(), 'amount' => $invoice->get_amount(), 'due_date' => $invoice->get_date(), 'name' => $receipt->get_user_name(), 'plan' => $plan->get_name(), 'paying_date' => $invoice->get_paying_date(), 'payment_method' => $invoice->get_payment_method(), 'card_no' => $invoice->get_card_no(), 'address' => $receipt->get_address(), 'website' => $receipt->get_website())));
        } else {
            $log_msg = "{/billing/receipt/show/{id:[0-9]+},show receipt id $id,response message:Not found}";
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('receipt-not-found')));
        }
        $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $id);
        return $this->response;
    }
    public function create_invoice_receipt($amount, $payment_method, $card_no, $user_id, $email, $charge_id, $plan, $name, $address, $website) {
        $receipt = new Receipt();
        $invoice = new Invoice();
        $plan_obj = Plan::findFirst("name='" . $plan . "'");
        $invoice->set_amount($amount);
        $invoice->set_payment_method($payment_method);
        $invoice->set_card_no($card_no);
        $invoice->set_date(date('Y-m-d'));
        $invoice->set_paying_date(date('Y-m-d'));
        $invoice->set_user_id($user_id);
        $invoice->set_user_email($email);
        $invoice->set_plan($plan_obj->get_id());
        $invoice->set_status("Paid");
        $invoice->set_address($address);
        $invoice->set_website($website);
        $invoice->set_user_name($name);
        $invoice->save();
        $receipt->set_invoice_id($invoice->get_id());
        $receipt->set_gateway_charge_id($charge_id);
        $receipt->set_created_at(date('Y-m-d H:i:s'));
        $receipt->set_address($address);
        $receipt->set_website($website);
        $receipt->set_user_name($name);
        $receipt->save();
        $this->send_receipt($email, $receipt, $invoice, $plan);
    }
    public function downgrade_action() {
        $signedInUserID = Tokens::checkAccess();
        $json_data = json_decode(file_get_contents("php://input"));
        $app = Apps::findFirst(array('conditions' => 'publisher_id=:user_id:', 'bind' => array('user_id' => $signedInUserID)));
        $plan = Plan::findFirst($json_data->plan_id);
        $curr_plan = Plan::findFirst($app->get_plan());
        $new_log = new Log();
        if (!$plan) {
            $log_msg = "{/billing/downgrade,downgrade from " . $curr_plan->get_name() . " plan to " . $plan->get_name() . " from the next month,response message :Invalid plan id,status:failed}";
            $this->response->setStatusCode(600, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid plan id'));
        } else {
            $log_msg = "{/billing/downgrade,downgrade from " . $curr_plan->get_name() . " plan to " . $plan->get_name() . " from the next month,response message :OK,status:OK}";
            $app->set_pending_plan($json_data->plan_id);
            $app->save();
            $user = Users::findFirst($app->getPublisherId());
            $subscription_date = new DateTime($app->get_subscription_date());
            $subscription_date->setTime(0, 0, 0);
            $month_later = clone $subscription_date;
            $month_later->add(new DateInterval("P1M"));
            $date = $month_later->format('Y-m-d');
            $this->getDI()->set("language", $user->getLocale());
            $this->send_downgrade_email($user, $app->get_plan(), $app->get_pending_plan(), $date);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => array('plan_id' => $app->get_plan())));
        }
        $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, $json_data->plan_id);
        return $this->response;
    }
    function user_invoices($sort, $user_id) {
        $invoices = Invoice::find(array('conditions' => 'user_id=' . $user_id . ' AND status IS NOT NULL', 'columns' => array('id', 'status', 'payment_method', 'card_no', 'date'), "order" => $sort, 'limit' => 10))->toArray();
        for ($i = 0;$i < sizeof($invoices);$i++) {
            $invoices[$i]['card_no'] = substr_replace($invoices[$i]['card_no'], "**** **** **** ", 0, strlen($invoices[$i]['card_no']) - 4);
            $receipt = Receipt::findFirst("invoice_id=" . $invoices[$i]['id']);
            if ($receipt) {
                $invoices[$i]['receipt_id'] = $receipt->get_id();
            }
        }
        return $invoices;
    }
    public function list_invoices($sort) {
        $signedInUserID = Tokens::checkAccess();
        $app_admin = AppAdmins::findFirst('user_id=' . $signedInUserID);
        $app = Apps::findFirst($app_admin->getAppId());
        $sort = "id " . $sort;
        $invoices = $this->user_invoices($sort, $app->getPublisherId());
        $new_log = new Log();
        $log_msg = "{/billing/invoices/{sort},list user's invoices sorted by date asc or desc,status:ok}";
        $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => $invoices));
        return $this->response;
    }
    public function billing_history() {
        $new_log = new Log();
        $signedInUserID = Tokens::checkAccess();
        $app_admin = AppAdmins::findFirst('user_id=' . $signedInUserID);
        $app = Apps::findFirst($app_admin->getAppId());
        $user_cards = UserCard::find(array('conditions' => 'app_id=' . $app->getId(), 'columns' => array('id', 'payment_method', 'name', 'expiry_year', 'expiry_month', 'number', 'default_card', 'postal_code', 'city', 'state', 'address', 'country_id', 'address2')))->toArray();
        for ($i = 0;$i < sizeof($user_cards);$i++) {
            $user_cards[$i]['number'] = substr_replace($user_cards[$i]['number'], "**** **** **** ", 0, strlen($user_cards[$i]['number']) - 4);
        }
        $invoices = $this->user_invoices("id desc", $app->getPublisherId());
        $current_plan = Plan::findFirst($app->get_plan());
        $date = new DateTime($app->get_subscription_date());
        $interval = new DateInterval('P1M');
        $date->add($interval);
        $next_billing_time = $date->format('Y-m-d');
        if ($user_cards) {
            $default = $user_cards[0];
        }
        $plan_data = array('name' => $current_plan->get_name(), 'billing_time' => $next_billing_time);
        if ($app->get_pending_plan() > 0) {
            $plan = Plan::findFirst($app->get_pending_plan());
            $plan_data['pending_plan'] = $plan->get_name();
        } else {
            $plan_data['pending_plan'] = 0;
            $plan_data['amount'] = $current_plan->get_amount();
        }
        $log_msg = "{/billing/history,response with credit cards data , current plan , pending plan and invoices,status:OK}";
        $new_log->log_current_action($signedInUserID, $this->headers['Ip'], $log_msg, 0);
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => array('default_card' => $default, 'card' => $user_cards, 'invoices' => $invoices, 'plan' => $plan_data)));
        return $this->response;
    }
    public function charging($lang) {
        $curr_date = new DateTime();
        $email_log = new SendingEmailsLog();
        $email_log->set_email_type("/billing/monthly/charge");
        $email_log->set_date($curr_date->format('Y-m-d H:i:s'));
        $email_log->save();
        $apps = Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT Apps.* FROM Apps JOIN Users ON Apps.publisher_id=Users.id WHERE Users.locale='$lang'");
        foreach ($apps as $app) {
            $user_email_log = UserEmailsLog::findFirst("user_id=" . $app->getPublisherId() . " AND email_type='/billing/monthly/charge'");
            $current_date = new DateTime();
            $current_date->setTime(0, 0, 0);
            $subscription_date = new DateTime($app->get_subscription_date());
            $subscription_date->setTime(0, 0, 0);
            $month_later = clone $subscription_date;
            $month_later->add(new DateInterval("P1M"));
            $billing_date = $month_later->format('Y-m-d');
            $diff = date_diff($current_date, $month_later);
            $diff_days = intval($diff->format('%a'));
            if ($diff_days === 0 || ($current_date > $month_later)) {
                if ($user_email_log) {
                    $sending_date = new DateTime($user_email_log->get_last_date());
                    $date_after_month = $sending_date->modify('1 month');
                    if ($date_after_month > $curr_date) {
                        continue;
                    }
                }
                $current_date = $current_date->format('Y-m-d');
                if ($app->get_pending_plan() !== 0) {
                    $user = Users::findFirst($app->getPublisherId());
                    $app->set_plan($app->get_pending_plan());
                    $app->set_pending_plan(0);
                    if ($app->get_plan === 1) {
                        $app_domains = AppDomains::find(array('app_id=' . $app->getId(), 'limit' => array('number' => 5, 'offset' => 2), 'order' => 'id asc'));
                        if ($app_domains) {
                            foreach ($app_domains as $domain) {
                                $domain->delete();
                            }
                        }
                    } else if ($app->get_plan === 2) {
                        $app_domains = AppDomains::find(array('app_id=' . $app->getId(), 'limit' => array('number' => 5, 'offset' => 3), 'order' => 'id asc'));
                        if ($app_domains) {
                            foreach ($app_domains as $domain) {
                                $domain->delete();
                            }
                        }
                    } else if ($app->get_plan === 3) {
                        $app_domains = AppDomains::find(array('app_id=' . $app->getId(), 'limit' => array('number' => 5, 'offset' => 4), 'order' => 'id asc'));
                        if ($app_domains) {
                            foreach ($app_domains as $domain) {
                                $domain->delete();
                            }
                        }
                    }
                }
                $new_log = new Log();
                if ($app->get_plan() !== 1) {
                    $user_card = UserCard::findFirst('app_id=' . $app->getId());
                    $user = Users::findFirst($app->getPublisherId());
                    $this->getDI()->set("language", $user->getLocale());
                    $plan = Plan::findFirst($app->get_plan());
                    if ($user_card) {
                        $charging_response = $this->charge_user($user_card, $plan->get_amount(), "Charging for " . $plan->get_name());
                        if ($charging_response['status'] == 'ERROR') {
                            $app->set_plan(1);
                            $app_domains = AppDomains::find(array('app_id=' . $app->getId(), 'limit' => array('number' => 5, 'offset' => 2), 'order' => 'id asc'));
                            if ($app_domains) {
                                foreach ($app_domains as $domain) {
                                    $domain->delete();
                                }
                            }
                            $downgrade_log = DowngradedAppsLog::findFirst("app_id=" . $app->getId());
                            if (!$downgraded_log) {
                                $downgrade_log = new DowngradedAppsLog();
                                $downgrade_log->set_app_id($app->getId());
                            }
                            $downgrade_log->set_payment_method($user_card->get_payment_method());
                            $downgrade_log->set_card_number(substr($user_card->get_number(), strlen($user_card->get_number()) - 4, strlen($user_card->get_number())));
                            $downgrade_log->save();
                            $log_msg = "{/billing/monthly/charge,charging for " . $plan->get_name() . " plan,response message:" . $charging_response['message'] . ",user_card:" . $user_card->get_id() . ",status:fialed and downgrade to free plan}";
                            $new_log->log_current_action($signedInUserID, "", $log_msg, $user_card->get_id());
                            $this->send_error_email($user->getEmail(), $user->getLocale(), $user->get_first_name() . " " . $user->get_last_name(), $plan->get_name());
                        } else {
                            $log_msg = "{/billing/monthly/charge,charging for " . $plan->get_name() . " plan,amount" . $plan->get_amount() . ",response message:OK,stripe charge_id : " . $charging_response['data']->id . ",user_card:" . $user_card->get_id() . ",status:ok}";
                            $new_log->log_current_action($signedInUserID, "", $log_msg, $user_card->get_id());
                            $invoice = Invoice::findFirst('user_id=' . $app->getPublisherId() . " AND status IS NULL AND paying_date='$billing_date'");
                            if ($invoice) {
                                $receipt = new Receipt();
                                $invoice->set_payment_method($user_card->get_payment_method());
                                $invoice->set_card_no($user_card->get_number());
                                $invoice->set_status("Paid");
                                $invoice->set_amount($plan->get_amount());
                                $invoice->save();
                                $receipt->set_invoice_id($invoice->get_id());
                                $receipt->set_gateway_charge_id($charging_response['data']->id);
                                $receipt->set_created_at(date('Y-m-d H:i:s'));
                                $receipt->set_address($app->get_address());
                                $receipt->set_website($app->getWebsite());
                                $receipt->set_user_name($user->get_first_name() . " " . $user->get_last_name());
                                $receipt->save();
                                $this->send_receipt($user->getEmail(), $receipt, $invoice, $plan->get_name());
                            }
                        }
                    } else {
                        $downgrade_log = DowngradedAppsLog::findFirst("app_id=" . $app->getId());
                        if (!$downgrade_log) {
                            $downgrade_log = new DowngradedAppsLog();
                            $downgrade_log->set_app_id($app->getId());
                        } else {
                            $downgrade_log->set_payment_method("");
                            $downgrade_log->set_card_number("");
                        }
                        $app->set_plan(1);
                        $app_domains = AppDomains::find(array('app_id=' . $app->getId(), 'limit' => array('number' => 5, 'offset' => 2), 'order' => 'id asc'));
                        if ($app_domains) {
                            foreach ($app_domains as $domain) {
                                $domain->delete();
                            }
                        }
                        $log_msg = "{/billing/monthly/charge,charging for " . $plan->get_name() . " plan,response message:send drop plan email as there isn't any credit card,status:fialed and downgrade to free plan}";
                        $new_log->log_current_action($signedInUserID, "", $log_msg, 0);
                        $this->send_error_email($user->getEmail(), $user->getLocale(), $user->get_first_name() . " " . $user->get_last_name(), $plan->get_name());
                    }
                }
                $app->set_subscription_date($billing_date);
                $app->save();
                if (!$user_email_log) {
                    $user_email_log = new UserEmailsLog();
                    $user_email_log->set_user_id($app->getPublisherId());
                    $user_email_log->set_email_type("/billing/monthly/charge");
                }
                $user_email_log->set_last_date($curr_date->format('Y-m-d H:i:s'));
                $user_email_log->save();
            }
        }
        $email_log = SendingEmailsLog::findFirst(array("email_type='/billing/monthly/charge'", 'order' => 'id desc'));
        $email_log->set_end_date(date('Y-m-d H:i:s'));
        $email_log->save();
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK'));
        return $this->response;
    }
    function send_error_email($u_email, $lang, $name, $plan_name) {
        $this->view->email = $u_email;
        $this->view->lang = $lang;
        $this->view->plan = $plan_name;
        $this->view->name = $name;
        $this->getDI()->set("language", $lang);
        $this->assets->addCss('css/email.css');
        $this->view->start();
        $this->view->render('emails', 'drop-plan');
        $this->view->finish();
        $title = $this->t->_('drop-plan-title');
        if ($lang == "ar") {
            $title = $title . " Speakol ";
        }
        $content = $this->view->getContent();
        $message = array('html' => $content, 'subject' => $title, 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $u_email, 'type' => 'to')),);
        $async = false;
        $ip_pool = 'Main Pool';
        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
    }
    public function send_invoices($lang) {
        $curr_date = new DateTime();
        $email_log = new SendingEmailsLog();
        $email_log->set_email_type("/billing/send/invoice");
        $email_log->set_date($curr_date->format('Y-m-d H:i:s'));
        $email_log->save();
        $apps = Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT Apps.* FROM Apps JOIN Users ON Apps.publisher_id=Users.id WHERE Users.locale='$lang' AND Apps.plan_id != 1");
        foreach ($apps as $app) {
            $current_date = new DateTime();
            $current_date->setTime(0, 0, 0);
            $subscription_date = new DateTime($app->get_subscription_date());
            $subscription_date->setTime(0, 0, 0);
            $month_later = clone $subscription_date;
            $month_later->add(new DateInterval("P1M"));
            $diff = date_diff($current_date, $month_later);
            $diff_days = intval($diff->format('%a'));
            if ($diff_days <= 7) {
                $user_email_log = UserEmailsLog::findFirst("user_id=" . $app->getPublisherId() . " AND email_type='/billing/send/invoice'");
                if ($user_email_log) {
                    $sending_date = new DateTime($user_email_log->get_last_date());
                    $date_after_days = $sending_date->modify('7 days'); {
                        if ($date_after_days >= $curr_date) {
                            continue;
                        }
                    }
                }
                $user = Users::findFirst($app->getPublisherId());
                if ($app->get_pending_plan() !== 0) {
                    $plan_id = $app->get_pending_plan();
                } else {
                    $plan_id = $app->get_plan();
                }
                if ($plan_id != 1) {
                    $plan = Plan::findFirst($plan_id);
                    $invoice = new Invoice();
                    $invoice->set_plan($plan_id);
                    $invoice->set_amount($plan->get_amount());
                    $invoice->set_date(date('Y-m-d'));
                    $invoice->set_paying_date($month_later->format('Y-m-d'));
                    $invoice->set_user_id($user->getId());
                    $invoice->set_user_email($user->getEmail());
                    $invoice->set_address($app->get_address());
                    $invoice->set_website($app->getWebsite());
                    $invoice->set_user_name($user->get_first_name() . " " . $user->get_last_name());
                    $invoice->save();
                    $this->send_invoice($invoice, $plan->get_name());
                    if (!$user_email_log) {
                        $user_email_log = new UserEmailsLog();
                        $user_email_log->set_user_id($app->getPublisherId());
                        $user_email_log->set_email_type("/billing/send/invoice");
                    }
                    $user_email_log->set_last_date($curr_date->format('Y-m-d H:i:s'));
                    $user_email_log->save();
                }
            }
        }
        $email_log = SendingEmailsLog::findFirst(array("email_type='/billing/send/invoice'", 'order' => 'id desc'));
        $email_log->set_end_date(date('Y-m-d H:i:s'));
        $email_log->save();
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK'));
        return $this->response;
    }
    public function generate_pdf($lang, $type, $plan, $object, $object2 = null) {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetPrintHeader(false);
        $fontname = TCPDF_FONTS::addTTFfont(dirname(__DIR__) . '/libs/tcpdf/fonts/inv2.ttf', 'TrueTypeUnicode', '', 96);
        $bold_font = TCPDF_FONTS::addTTFfont(dirname(__DIR__) . '/libs/tcpdf/fonts/bold.ttf', 'TrueTypeUnicode', '', 96);
        $normal_font = TCPDF_FONTS::addTTFfont(dirname(__DIR__) . '/libs/tcpdf/fonts/inv.ttf', 'TrueTypeUnicode', '', 96);
        $pdf->SetFont($fontname, '', 14, '', false);
        $pdf->AddPage('P', 'A4', false, false);
        $amount = $object->get_amount();
        $id = $object->get_id();
        if ($type == "Receipt") {
            $paying_date = strtotime($object2->get_created_at());
            $paying_date = date('F d, Y', $paying_date);
        } else {
            $paying_date = strtotime($object->get_paying_date());
            $paying_date = date('F d, Y', $paying_date);
        }
        $date = strtotime($object->get_date());
        $date = date('F d, Y', $date);
        $src = __DIR__ . '/../../public/img/logo2.png';
        $img = __DIR__ . '/../../public/img/paid.png';
        if ($type == "Receipt") {
            $address = $object2->get_address();
            $name = $object2->get_user_name();
            $website = $object2->get_website();
            $html = <<<EOD
<table style="width:100%;">
    <tr>
        <td width="40%;"><img src="$src"/></td>
        <td width="40%;"></td>
        <td width="20%;" style="font-family:$bold_font;" ><p><font size="8">SPEAKOL UK LIMITED</font><br><font size="8">99 CANTERBURY ROAD<br>WHITSTABLE<br>KENT<br>ENGLAND<br>info@speakol.com</font></p></td>
    </tr>
    <tr>
        <td style="font-family:$normal_font;font-size:11;"><font face="$bold_font" size="13">Bill To</font><br>$website<br/>Attention: $name<br/>$address
</td>
    </tr>
<tr>
<td>
</td>
<td>
<img src="$img" height="50px" width="120px"/>
</td>
</tr>
    </table>
<table style="width:35%;font-family:$bold_font;font-size:11;">
    <tr><td style="width:40%;">Invoice #:</td><td style="text-align:right;width:60%;">$id</td></tr>
    <tr><td style="width:40%;">Due Date</td><td style="text-align:right;width:60%;">$date</td></tr>
    <tr><td style="width:40%;">Paid Date</td><td style="text-align:right;width:60%;">$paying_date</td></tr>
</table>
<br>
<br>
<br>
<table  style="width:100%;"  cellpadding="4">
    <tr style="font-family:$normal_font;">
        <td width="55%;">&nbsp;&nbsp;Description</td>
        <td width="15%" style="text-align:right;" >Unit Price</td>
        <td width="15%" style="text-align:right;">Quantity</td>
        <td width="15%" style="text-align:right;">Amount</td>
    </tr>
</table>
<table bgcolor="#e0e0e0" cellpadding="1">
<tr>
<td>
<table style="font-size:11;width:100%;" cellpadding="5">
    <tr bgcolor="#efefef">
        <td width="55%;">&nbsp;&nbsp;$plan Subscription</td>
        <td width="15%" style="text-align:right;">$$amount</td>
        <td width="15%" style="text-align:right;">1.0</td>
        <td width="15%" style="text-align:right;">$$amount</td>
    </tr>
</table>
</td>
</tr>
</table>
<br>
<table style="width:100%;" cellpadding="5">
<tr style="text-align:right;font-size:9;"><td width="74%"></td><td style="font-family:$bold_font;text-align:left;" width="14%">Subtotal:</td><td width="12%;">&nbsp; $$amount</td></tr>
<tr style="text-align:right;font-size:15;"><td width="74%"></td><td style="font-family:$bold_font;text-align:left;" width="14%">Total:</td><td width="12%;">$$amount</td></tr>
</table>

EOD;
            
        } else {
            $address = $object->get_address();
            $name = $object->get_user_name();
            $website = $object->get_website();
            $html = <<<EOD
<table style="width:100%;">
    <tr>
        <td width="40%;"><img src="$src"/></td>
        <td width="40%;"></td>
        <td width="20%;" style="font-family:$bold_font;" ><p><font size="8">SPEAKOL UK LIMITED</font><br><font size="8">99 CANTERBURY ROAD<br>WHITSTABLE<br>KENT<br>ENGLAND<br>info@speakol.com</font></p></td>
    </tr>
    <tr>
        <td style="font-family:$normal_font;font-size:11;"><font face="$bold_font" size="13">Bill To</font><br>$website<br/>Attention: $name<br/>$address
</td>
    </tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
    </table>
<table style="width:35%;font-family:$bold_font;font-size:11;">
    <tr><td style="width:40%;">Invoice #:</td><td style="text-align:right;width:60%;">$id</td></tr>
    <tr><td style="width:40%;">Due Date</td><td style="text-align:right;width:60%;">$date</td></tr>
    <tr><td style="width:40%;">Paid Date</td><td style="text-align:right;width:60%;">$paying_date</td></tr>
</table>
<br>
<br>
<br>
<table  style="width:100%;"  cellpadding="4">
    <tr style="font-family:$normal_font;">
        <td width="55%;">&nbsp;&nbsp;Description</td>
        <td width="15%" style="text-align:right;" >Unit Price</td>
        <td width="15%" style="text-align:right;">Quantity</td>
        <td width="15%" style="text-align:right;">Amount</td>
    </tr>
</table>
<table bgcolor="#e0e0e0" cellpadding="1">
<tr>
<td>
<table style="font-size:11;width:100%;" cellpadding="5">
    <tr bgcolor="#efefef">
        <td width="55%;">&nbsp;&nbsp;$plan Subscription</td>
        <td width="15%" style="text-align:right;">$$amount</td>
        <td width="15%" style="text-align:right;">1.0</td>
        <td width="15%" style="text-align:right;">$$amount</td>
    </tr>
</table>
</td>
</tr>
</table>
<br>
<table style="width:100%;" cellpadding="5">
<tr style="text-align:right;font-size:9;"><td width="74%"></td><td style="font-family:$bold_font;text-align:left;" width="14%">Subtotal:</td><td width="12%;">&nbsp; $$amount</td></tr>
<tr style="text-align:right;font-size:15;"><td width="74%"></td><td style="font-family:$bold_font;text-align:left;" width="14%">Total:</td><td width="12%;">$$amount</td></tr>
</table>

EOD;
            
        }
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        return $pdf->Output('Speakol.pdf', 'S');
    }
}
