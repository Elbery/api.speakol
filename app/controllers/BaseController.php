<?php
use \Phalcon\Mvc\Controller, Phalcon\Translate\Adapter\NativeArray;
class BaseController extends \Phalcon\DI\Injectable {
    public function __construct() {
        $this->getDI()->set("language", $this->request->get("lang"));
    }
    public function loadCustomTrans($transFile) {
        $translationPath = $this->_getTransPath();
        require $translationPath . '/' . $transFile . '.php';
        $this->_messages = !empty($this->_messages) ? array_merge($this->_messages, $messages) : $messages;
        $controllerTranslate = new NativeArray(array("content" => $this->_messages));
        $this->t = $controllerTranslate;
        $this->view->setVar("t", $controllerTranslate);
    }
    protected function _getTransPath() {
        $translationPath = '../app/messages/';
        $language = $this->session->get("lang");
        if (!$language) {
            $this->session->set("lang", "en");
        }
        if ($language === 'sp' || $language === 'en' || $language === 'ar') {
            return $translationPath . $language;
        } else {
            return $translationPath . 'en';
        }
    }
    protected function get_plugins_data($plugins) {
        $all_feeds = array();
        foreach ($plugins as $feed) {
            if ($feed['type'] == "argumentbox") {
                $plugin = Argumentboxes::findFirst($feed['id'])->toArray();
                $sides = Sides::find(array("columns" => "id", "conditions" => "argumentbox_id=" . $feed['id']));
            } else if ($feed['type'] == "debate") {
                $plugin = Debates::findFirst(array("columns" => array("id", "title", "slug", "created_at", "url", "lang", "app_id"), "conditions" => "id=" . $feed['id']))->toArray();
                $sides = Sides::find(array("columns" => "id", "conditions" => "debate_id=" . $feed['id']));
            } else {
                $plugin = Comparisons::findFirst(array("columns" => array("id", "title", "image", "slug", "created_at", "url", "align", "lang", "app_id"), "conditions" => "id=" . $feed['id']))->toArray();
                $sides = Sides::find(array("columns" => "id", "conditions" => "comparison_id=" . $feed['id']));
            }
            $plugin['title'] = htmlspecialchars($plugin['title']);
            $plugin_sides = array();
            $sides = $sides->toArray();
            $totalSum = Votes::sumVotes($sides);
            foreach ($sides as $side) {
                $votes = Votes::count("side_id=" . $side['id']);
                if ($feed['type'] == "comparison") {
                    $side_meta = ComparisonSidesMeta::findFirst("side_id=" . $side['id'])->toArray();
                    $side_meta['title'] = htmlspecialchars($side_meta['title']);
                    $side_meta['description'] = htmlspecialchars($side_meta['description']);
                    if ($side_meta['image'] != "") {
                        $side_meta['image'] = $this->getDI()->getShared('config')->get("application")->get("img-domain") . $side_meta['image'];
                    }
                } else if ($feed['type'] == "debate") {
                    $side_meta = DebateSidesMeta::findFirst("side_id=" . $side['id'])->toArray();
                    $side_meta['title'] = htmlspecialchars($side_meta['title']);
                    $side_meta['job_title'] = htmlspecialchars($side_meta['job_title']);
                    $side_meta['opinion'] = htmlspecialchars($side_meta['opinion']);
                    if ($side_meta['image'] != "") {
                        $side_meta['image'] = $this->getDI()->getShared('config')->get("application")->get("img-domain") . $side_meta['image'];
                    }
                    if (strlen($side_meta['job_title']) > 60) {
                        $side_meta['job_title'] = substr($side_meta['job_title'], 0, 60);
                        $side_meta['job_title'].= "...";
                    }
                }
                array_push($plugin_sides, array('id' => $side['id'], 'votes' => $votes, 'perc' => (!$votes) ? 0 : round($votes / ($totalSum ? $totalSum : 1) * 100), 'side_data' => $side_meta));
            }
            array_push($all_feeds, array("data" => $plugin, "type" => $feed['type'], 'sides' => $plugin_sides));
        }
        return $all_feeds;
    }
    protected function generate_imagick_image($right, $right_votes, $left, $left_votes, $lang) {
        if ($lang == "ar") {
            $left_color = "#d11b1f";
            $right_color = "#4dac54";
        } else {
            $left_color = "#4dac54";
            $right_color = "#d11b1f";
        }
        $draw = new \ImagickDraw();
        $right_perc = intval($right * 2.2);
        $left_perc = intval($left * 2.2);
        $draw->setStrokeWidth(18);
        $draw->setFillColor("#3d3d3d");
        $draw->circle(220, 220, 252, 220);
        $draw->setStrokeColor("#ececec");
        $draw->setStrokeDashArray(array(0, 220));
        $draw->circle(220, 220, 252, 220);
        $draw->setStrokeColor($left_color);
        if ($left_perc === 0) {
            $draw->setStrokeOpacity(0.0);
        }
        $draw->setStrokeDashArray(array(220, 220));
        $draw->circle(220, 220, 252, 220);
        $draw->setStrokeColor("#ececec");
        $draw->setStrokeDashArray(array($right_perc + 1, 220));
        $draw->circle(220, 220, 252, 220);
        $draw->setStrokeColor($right_color);
        if ($right_perc === 0) {
            $draw->setStrokeOpacity(0.0);
        }
        $draw->setStrokeDashArray(array($right_perc, 220));
        $draw->circle(220, 220, 252, 220);
        $draw->setStrokeWidth(10);
        $draw->setStrokeOpacity(0.0);
        $draw->setStrokeDashArray(array(0, 220));
        $draw->circle(220, 220, 252, 220);
        $imagick = new \Imagick();
        $imagick->newImage(440, 440, "none");
        $imagick->setImageFormat("png");
        $imagick->drawImage($draw);
        if ($right <= 50) {
            $rotation_angle = - (($right_perc + 10) / 2);
        } else {
            $rotation_angle = - ($right_perc - $left);
        }
        $imagick->rotateimage(new ImagickPixel('none'), $rotation_angle);
        $d = $imagick->getImageGeometry();
        $img_width = $d['width'];
        $img_height = $d['height'];
        $start_x = $img_height / 2 - 32 - 195;
        $start_y = $img_width / 2 - 3;
        $end_x = $img_height / 2 - 32 - 2;
        $end_y = $start_y + 10;
        $width = $end_x - $start_x;
        $draw2 = new \ImagickDraw();
        $draw2->setFillColor("#DFDCDC");
        $draw2->roundRectangle($start_x, $start_y, $end_x, $end_y, 8, 8);
        $left_x = $end_x - ($left * 1.95);
        $draw2->setFillColor($left_color);
        $draw2->roundRectangle($left_x, $start_y, $end_x, $end_y, 8, 8);
        $start_x2 = $img_height / 2 + 32 + 2;
        $draw2->setFillColor("#DFDCDC");
        $draw2->roundRectangle($start_x2, $start_y, $start_x2 + 195, $end_y, 8, 8);
        $right_x = ($right * 1.95) + $start_x2;
        $draw2->setFillColor($right_color);
        $draw2->roundRectangle($start_x2, $start_y, $right_x, $end_y, 8, 8);
        $draw2->setFontSize(14);
        $draw2->setFillColor('#d11b1f');
        $imagick->annotateImage($draw2, $start_x2 + 15, $start_y - 10, 0, $right . "%");
        $draw2->setFillColor('#4dac54');
        $imagick->annotateImage($draw2, $end_x - 50, $start_y - 10, 0, $left . "%");
        $draw2->setFillColor('white');
        $draw2->setTextAlignment(\Imagick::ALIGN_CENTER);
        $draw2->annotation($img_height / 2, $img_width / 2 - 10, ($right_votes + $left_votes));
        $draw2->annotation($img_height / 2, $img_width / 2 + 10, "votes");
        $draw2->setFillColor('#9fa9af');
        $imagick->annotateImage($draw2, $start_x, $start_y - 8, 0, $left_votes . " votes");
        $imagick->annotateImage($draw2, $start_x2 + 135, $start_y - 8, 0, $right_votes . " votes");
        $imagick->drawImage($draw2);
        $margin = ($img_width / 2) - 32 - 195;
        $imagick->cropImage(464, 90, $margin, (($img_width / 2) - 45));
        $name = rand() . "_cirlce.png";
        $path = __DIR__ . "/../../public/emails/" . $name;
        $imagick->writeImage($path);
        $imagick->destroy();
        return $this->getDI()->getShared('config')->get("application")->get("img-domain") . "/emails/" . $name;
    }
    protected function draw_bar($votes, $align, $sides, $lang, $has_image) {
        $perc = 3;
        if ($align) {
            if ($sides > 2 && $sides < 5) {
                $perc = 1;
            } else {
                $perc = 2;
            }
        }
        if ((!$align || $sides > 5) && !$has_image) {
            $perc = 4;
        }
        $votes_perc = $votes * $perc;
        $imagick = new \Imagick();
        $imagick->newImage(450, 450, "none");
        $imagick->setImageFormat("png");
        $start_x = 0;
        $start_y = 10;
        $end_x = 100 * $perc;
        $end_y = $start_y + 8;
        $draw2 = new \ImagickDraw();
        $draw2->setFillColor("#DFDCDC");
        $draw2->roundRectangle($start_x, $start_y, $end_x, $end_y, 8, 8);
        if ($lang == "ar") {
            $start_x = $end_x - $votes_perc + 1;
            $end_votes = $end_x;
            $start_square = $start_x - 1;
            if ($start_square == $end_x) {
                $start_square = $start_square - 18;
            }
        } else {
            $end_votes = $start_x + $votes_perc;
            $start_square = $end_votes - 27;
            if ($start_square < 0) {
                $start_square = 1;
            }
        }
        $draw2->setFillColor("#4dac54");
        $draw2->roundRectangle($start_x, $start_y, $end_votes, $end_y, 8, 8);
        $draw2->setFillColor("#4dac54");
        $draw2->roundRectangle($start_square, $start_y - 5, $start_square + 25, $end_y + 5, 2, 2);
        $imagick->drawImage($draw2);
        $draw = new \ImagickDraw();
        $draw->setFontSize(10);
        $draw->setFillColor('white');
        $draw->setTextAlignment(\Imagick::ALIGN_CENTER);
        $draw->annotation(($start_square + 25 / 2), $start_y + 6, $votes . "%");
        $imagick->drawImage($draw);
        $imagick->cropImage($perc * 100 + 2, 30, 0, 0);
        $name = rand() . "_bar.png";
        $path = __DIR__ . "/../../public/emails/" . $name;
        $imagick->writeImage($path);
        $imagick->destroy();
        return $this->getDI()->getShared('config')->get("application")->get("img-domain") . "/emails/" . $name;
    }
    protected function most_interacted_plugins($app_id, $creation_date) {
        $plugins = array();
        if ($app_id != 0) {
            $condition = "app_id=$app_id AND created_at>'$creation_date'";
        } else {
            $condition = "created_at>'$creation_date'";
        }
        $debates = Debates::find($condition);
        $comparisons = Comparisons::find($condition);
        $argumentboxes = Argumentboxes::find($condition);
        foreach ($debates as $debate) {
            $sides = Sides::find("debate_id=" . $debate->getId());
            $votes = 0;
            $comments = 0;
            foreach ($sides as $side) {
                $votes+= Votes::count('side_id=' . $side->getId());
                $comments+= Arguments::count('side_id=' . $side->getId());
            }
            $interactions = $votes + $comments;
            array_push($plugins, array('type' => 'debate', 'id' => $debate->getId(), 'interactions' => $interactions, 'votes' => $votes, 'comments' => $comments));
        }
        foreach ($comparisons as $comparison) {
            $sides = Sides::find("comparison_id=" . $comparison->getId());
            $votes = 0;
            $comments = 0;
            foreach ($sides as $side) {
                $votes+= Votes::count('side_id=' . $side->getId());
                $comments+= Arguments::count('side_id=' . $side->getId());
            }
            $interactions = $votes + $comments;
            array_push($plugins, array('type' => 'comparison', 'id' => $comparison->getId(), 'interactions' => $interactions, 'votes' => $votes, 'comments' => $comments));
        }
        foreach ($argumentboxes as $argumentbox) {
            $sides = Sides::find("argumentbox_id=" . $argumentbox->getId());
            $votes = 0;
            $comments = 0;
            foreach ($sides as $side) {
                $votes+= Votes::count('side_id=' . $side->getId());
                $comments+= Arguments::count('side_id=' . $side->getId());
            }
            $interactions = $votes + $comments;
            array_push($plugins, array('type' => 'argumentbox', 'id' => $argumentbox->getId(), 'interactions' => $interactions, 'votes' => $votes, 'comments' => $comments));
        }
        return $plugins;
    }
    protected function hot_topics($lang) {
        $curl_obj = curl_init();
        if ($lang == "ar") {
            $url = "http://www.google.com/trends/hottrends/atom/feed?pn=p29";
        } else {
            $url = "http://www.google.com/trends/hottrends/atom/feed?pn=p1";
        }
        curl_setopt($curl_obj, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_obj, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_obj, CURLOPT_URL, $url);
        $res = curl_exec($curl_obj);
        $feed = simplexml_load_string($res);
        $max_chars = 240;
        if ($lang == "ar") {
            $max_chars = 480;
        }
        $topics = array();
        foreach ($feed->channel->item as $entry) {
            $description = (string)$entry->children('http://www.google.com/trends/hottrends')->news_item[0]->news_item_snippet;
            if (strlen($description) > $max_chars) {
                $description = substr($description, 0, $max_chars - 4);
                $description.= "...";
            }
            $description = str_replace("<b>", "", $description);
            $description = str_replace("</b>", "", $description);
            array_push($topics, array('title' => (string)$entry->title, 'description' => $description, 'image' => (string)$entry->children('http://www.google.com/trends/hottrends')->picture[0], 'link' => (string)$entry->children('http://www.google.com/trends/hottrends')->news_item[0]->news_item_url));
        }
        curl_close($curl_obj);
        return $topics;
    }
    public function valid_domain($domains, $url) {
        $domain_url = str_replace("http://", "", $url);
        $domain_url = str_replace("https://", "", $domain_url);
        $del_pos = strpos($domain_url, "/") ? strpos($domain_url, "/") : strlen($domain_url);
        $domain_url = substr($domain_url, 0, $del_pos);
        $del_pos = strpos($domain_url, ":") ? strpos($domain_url, ":") : strlen($domain_url);
        $domain_url = substr($domain_url, 0, $del_pos);
        $domain_url = strtolower($domain_url);
        $valid = in_array($domain_url, $domains);
        return $valid;
    }
    public function get_next($app_id, $date, $url) {
        $result = array();
        $domain = $this->extract_domain($url);
        $plugins = $this->db->fetchAll("(SELECT id, url, title, created_at, 'debates' FROM debates WHERE app_id = $app_id AND created_at>'$date' AND url LIKE '%$domain%' AND title IS NOT NULL AND title !='') UNION( SELECT id, url, title, created_at, 'comparison' FROM comparisons WHERE app_id = $app_id AND created_at>'$date' AND url LIKE '%$domain%' AND title IS NOT NULL AND title !='') UNION (SELECT id, CONCAT(protocol, domain, uri) AS url, title, created_at, 'argumentboxes' FROM argumentboxes WHERE app_id = $app_id AND created_at>'$date' AND domain ='$domain' AND title IS NOT NULL AND title !='') ORDER BY created_at  LIMIT 10", Phalcon\Db::FETCH_ASSOC);
        if (empty($plugins)) {
            return $result;
        }
        foreach ($plugins as $next_plugin) {
            $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $next_plugin['url'])));
            if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                $result['title'] = $next_plugin['title'];
                $result['type'] = $next_plugin['debates'];
                $result['url'] = $next_plugin['url'];
                $result['hash'] = $interaction->getHash();
                break;
            }
        }
        return $result;
    }
    public function get_previous($app_id, $date, $url) {
        $result = array();
        $domain = $this->extract_domain($url);
        $plugins = $this->db->fetchAll("(SELECT id, url, title, created_at, 'debates' FROM debates WHERE app_id = $app_id AND created_at<'$date' AND url LIKE '%$domain%' AND title IS NOT NULL AND title !='') UNION( SELECT id, url, title, created_at, 'comparison' FROM comparisons WHERE app_id = $app_id AND created_at<'$date' AND url LIKE '%$domain' AND title IS NOT NULL AND title !='') UNION (SELECT id, CONCAT(protocol, domain, uri) AS url, title, created_at, 'argumentboxes' FROM argumentboxes WHERE app_id = $app_id AND created_at<'$date' AND domain ='$domain' AND title IS NOT NULL AND title !='') ORDER BY created_at DESC LIMIT 10", Phalcon\Db::FETCH_ASSOC);
        if (empty($plugins)) {
            return $result;
        }
        foreach ($plugins as $previous_plugin) {
            $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $previous_plugin['url'])));
            if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                $result['title'] = $previous_plugin['title'];
                $result['type'] = $previous_plugin['debates'];
                $result['url'] = $previous_plugin['url'];
                $result['hash'] = $interaction->getHash();
                break;
            }
        }
        return $result;
    }
    public function get_random_plugins($app_id, $url) {
        $result = array();
        $domain = $this->extract_domain($url);
        $random_plugins = $this->db->fetchAll("(SELECT id, url, title, created_at, 'debates' FROM debates WHERE app_id = $app_id AND url LIKE '%$domain%' AND title IS NOT NULL AND title !='') UNION( SELECT id, url, title, created_at, 'comparison' FROM comparisons WHERE app_id = $app_id AND url LIKE '%$domain%' AND title IS NOT NULL AND title !='') UNION (SELECT id, CONCAT(protocol, domain, uri) AS url, title, created_at, 'argumentboxes' FROM argumentboxes WHERE app_id = $app_id AND domain ='$domain' AND title IS NOT NULL AND title !='') ORDER BY created_at DESC LIMIT 20", Phalcon\Db::FETCH_ASSOC);
        if (empty($random_plugins)) {
            return $result;
        }
        foreach ($random_plugins as $plugin) {
            $result[] = $plugin;
        }
        return $result;
    }
    public function extract_domain($url) {
        $url = urldecode($url);
        $domain_url = str_replace("http://", "", $url);
        $domain_url = str_replace("https://", "", $domain_url);
        $del_pos = strpos($domain_url, "/") ? strpos($domain_url, "/") : strlen($domain_url);
        $domain_url = substr($domain_url, 0, $del_pos);
        $del_pos = strpos($domain_url, ":") ? strpos($domain_url, ":") : strlen($domain_url);
        $domain_url = substr($domain_url, 0, $del_pos);
        $domain_url = strtolower($domain_url);
        return $domain_url;
    }
    public function generate_uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }
}
