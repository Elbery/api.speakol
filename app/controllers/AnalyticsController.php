<?php
class AnalyticsController extends BaseController {
    public function debatescountAction() {
        try {
            $type = $this->request->getQuery('type');
            if ($type == 'publisher') {
                $conditions = array("conditions" => "app_id is not null",);
            } else if ($type == 'website') {
                $conditions = array("conditions" => "app_id is null",);
            } else {
                return false;
            }
            $data = Debates::count($conditions);
            if (is_array($data) && $data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function comparisonscountAction() {
        try {
            $type = $this->request->getQuery('type');
            if ($type == 'publisher') {
                $conditions = array("conditions" => "app_id is not null",);
            } else if ($type == 'website') {
                $conditions = array("conditions" => "app_id is null",);
            } else {
                return false;
            }
            $data = Comparisons::count($conditions);
            if (is_array($data) && $data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function argumentscountAction() {
        try {
            $type = $this->request->getQuery('type');
            if ($type == 'publisher') {
                $conditions = array("conditions" => "app_id is not null",);
            } else if ($type == 'website') {
                $conditions = array("conditions" => "app_id is null",);
            } else {
                return false;
            }
            $data = Arguments::count($conditions);
            if (is_array($data) && $data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function userscountAction() {
        try {
            $type = $this->request->getQuery('type');
            if ($type == 'publisher') {
                $conditions = array("conditions" => "role_id=3",);
            } else if ($type == 'website') {
                $conditions = array("conditions" => "role_id!=3",);
            } else {
                return false;
            }
            $data = Users::count($conditions);
            if (is_array($data) && $data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function countsummaryAction() {
        try {
            $type = $this->request->get('type');
            $appId = $this->request->get('app_id');
            if ($type == 'publisher') {
                $conditions = array("conditions" => "app_id is not null",);
                $userConditions = array("conditions" => "role_id= " . $appId,);
            } else if ($type == 'website') {
                $conditions = array("conditions" => "app_id is null",);
                $userConditions = array("conditions" => "role_id!=3",);
            } else {
                return false;
            }
            $debates = Debates::count();
            $comparisons = Comparisons::count();
            $users = Users::count();
            $arguments = Arguments::count();
            $data = array('debates' => $debates, 'comparisons' => $comparisons, 'arguments' => $arguments, 'users' => $users,);
            if (is_array($data) && $data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function debatescategoryAction() {
        try {
            $debates = Debates::count(array('group' => 'category_id'))->toArray();
            $data = $debates;
            foreach ($debates as $key => $debate) {
                $data[$key]['category_name'] = Categories::findFirstById($debate['category_id'])->getName();
            }
            if (is_array($data) && $data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function comparisonscategoryAction() {
        try {
            $comparisons = Comparisons::count(array('group' => 'category_id'))->toArray();
            $data = $comparisons;
            foreach ($comparisons as $key => $comparison) {
                $data[$key]['category_name'] = Categories::findFirstById($comparison['category_id'])->getName();
            }
            if (is_array($data) && $data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
}
