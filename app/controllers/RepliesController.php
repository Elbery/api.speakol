<?php
class RepliesController extends BaseController {
    public function createAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            $modelReplies = new Replies();
            $this->getDI()->set('language', $this->request->get("lang"));
            $boolReply = $modelReplies->createActionHelper($signedInUserID);
            if ($boolReply['status'] == 'ERROR') {
                $this->response->setStatusCode(400, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $boolReply['message']));
                return $this->response;
            }
            $new_log = new Log();
            $new_log->log_current_action($signedInUserID, $this->headers['Ip'], "/replies/create", $boolReply);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $boolReply));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function showAction($argument_id, $page = 1, $limit = 2) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelReplies = new Replies();
            $limit = $this->config->pagination->debate->replies;
            $arrArguments = $modelReplies->showActionHelper($argument_id, $page, $limit);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrArguments));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function showNestedAction($parent_reply, $page = 1, $limit = 2) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $limit = $this->config->pagination->debate->replies;
            $modelReplies = new Replies();
            $arrArguments = $modelReplies->getNestedReplies($parent_reply, $page, $limit);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrArguments), JSON_PRETTY_PRINT);
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function thumbUpAction($reply_id) {
        $link = $this->request->getPost("link");
        return $this->voteAction($reply_id, $link, TRUE);
    }
    public function thumbDownAction($reply_id) {
        $link = $this->request->getPost("link");
        return $this->voteAction($reply_id, $link, FALSE);
    }
    private function voteAction($reply_id, $link, $vote) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::getSignedInUserId();
            $modelReplies = new Replies();
            if (Replies::findFirst($reply_id) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if ($modelReplies->hasVoted($reply_id, $signedInUserID, $vote)) {
                throw new Exception($this->t->_('found'), 302);
            }
            $thumbup = $modelReplies->voteActionHelper($reply_id, $signedInUserID, $vote, $link);
            if (FALSE == $thumbup) {
                throw new Exception($this->t->_('conflict'), 409);
            } else {
                $new_log = new Log();
                $new_log->log_current_action($signedInUserID, $this->headers['Ip'], "/replies/" . $reply_id . "/" . $action, $thumbup);
                $reply = Replies::findFirstById($reply_id);
                if ($reply != false) {
                    if ($vote == True) {
                        $action = "up";
                    } else {
                        $action = "down";
                    }
                    $reply = $reply->toArray();
                    $argumentId = $reply['argument_id'];
                    $argument = Arguments::findFirstById($argumentId);
                    if ($argument != false) {
                        $argument = $argument->toArray();
                        $current_user = Tokens::checkAccess();
                        if ($reply['user_id'] != $current_user) {
                            $app = AppAdmins::findFirst(array('user_id = :user_id: AND super_admin=1', 'bind' => array('user_id' => $current_user)));
                            if ($app) {
                                $admin_at = $app->getAppId();
                            } else {
                                $admin_at = 0;
                            }
                            $notifications_controller = new NotificationsController();
                            $notifications_controller->thumbMyReply(array('app_id' => $argument['app_id'], 'referrer' => $this->request->getPost('referrer'), 'reply_owner' => $reply['user_id'], 'module_type' => $argument['type'], 'module_id' => $argument['module_id'], 'action' => $action, 'admin_at' => $admin_at, 'link' => $link,));
                        }
                    }
                }
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function showSingleReplyAction($reply_id) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => Replies::fetchReply($reply_id)));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function deleteAction($reply_id) {
        try {
            $replies = new Replies();
            $data = $replies->deleteReply($reply_id);
            $new_log = new Log();
            $new_log->log_current_action(Tokens::checkAccess(), $this->headers['Ip'], "/replies/" . $reply_id . "/delete", $reply_id);
            if ($data === true) {
                $this->response->setStatusCode(200, 'OK');
                $this->response->setJsonContent(array('status' => 'OK', 'data' => true));
            } else {
                $this->response->setStatusCode(404, 'Not Found');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $data));
            }
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
}
