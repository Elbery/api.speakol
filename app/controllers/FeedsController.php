<?php
use Phalcon\DI\FactoryDefault;
class FeedsController extends BaseController {
    public function list_new_feeds() {
        $data_valid = true;
        $category = $this->request->get("category");
        $page = $this->request->get("page") ? $this->request->get("page") : 1;
        $limit = $this->request->get("limit") ? $this->request->get("limit") : 10;
        $lang = $this->request->get("lang") ? $this->request->get("lang") : "en";
        $network = $this->request->get("network") ? $this->request->get("network") : "en";
        $type = $this->request->get("type");
        $last_date = $this->request->get("last_date");
        if ($category != "") {
            if (!ctype_digit($category)) {
                $data_valid = false;
            }
        }
        if (!ctype_digit($page) && $page != 1) {
            $data_valid = false;
        }
        if (!ctype_digit($limit) && $limit != 10) {
            $data_valid = false;
        }
        if ($lang != "en" && $lang != "ar") {
            $data_valid = false;
        }
        if ($type != "all" && $type != "debates" && $type != "comparisons" && $type != "argumentboxes") {
            $data_valid = false;
        }
        if ($last_date != "") {
            if (!strtotime($last_date)) {
                $data_valid = false;
            }
        }
        if (!$data_valid) {
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
            return $this->response;
        }
        if (intval($page) === 1) {
            $last_date = date('Y-m-d H:i:s');
        } else {
            $date = strtotime($last_date);
            $last_date = date('Y-m-d H:i:s', $date);
        }
        $all_network_apps = Apps::find(array('conditions' => "network='$network'", 'columns' => 'id'));
        foreach ($all_network_apps as $network_app) {
            $network_ids.= $network_app['id'] . ",";
        }
        $network_ids = substr($network_ids, 0, -1);
        if (!$network_ids) {
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => array()));
            return $this->response;
        }
        if ($type == "all" && $category == "") {
            $query = "(select id,created_at,title,app_id,'debates' as module from debates where created_at < '$last_date' AND app_id IN($network_ids)) UNION (select id,created_at,title,app_id," . "'comparisons' as module from comparisons where created_at < '$last_date' AND app_id IN($network_ids)) UNION (select id,created_at,title,app_id,'argumentboxes' as module" . " from argumentboxes where created_at < '$last_time' AND app_id IN($network_ids)) order by created_at desc limit $limit";
        } elseif ($type != "all" && $category == "") {
            $query = "select id,created_at,title,app_id,'$type' as module from $type where created_at < '$last_date' AND app_id IN($network_ids) order by created_at desc limit " . $limit;
        } elseif ($type == "all" && $category != "") {
            $user_id = Tokens::checkAccess();
            if ($user_id) {
                $last_seen_categories = LastSeenCategories::findFirst("user_id=$user_id");
                if (!$last_seen_categories) {
                    $data = array();
                    $plugins = array();
                    $categories = Categories::find();
                    $current_date = date('Y-m-d H:i:s');
                    foreach ($categories as $cat) {
                        $data[$cat->getId() ] = $current_date;
                    }
                    $last_seen_categories = new LastSeenCategories();
                    $last_seen_categories->set_user_id($user_id);
                    $last_seen_categories->set_last_seen_dates(json_encode($data));
                    $last_seen_categories->save();
                } else {
                    $dates = json_decode($last_seen_categories->get_last_seen_dates());
                    $dates->$category = date('Y-m-d H:i:s');
                    $last_seen_categories->set_last_seen_dates(json_encode($dates));
                    $last_seen_categories->save();
                }
            }
            $query = "(select id,created_at,category_id,title,app_id,'debates' as module from debates where category_id=$category AND app_id IN($network_ids) AND created_at < '$last_date') UNION" . " (select id,created_at,category_id,title,app_id,'comparisons' as module from comparisons where app_id IN($network_ids) AND category_id=$category AND created_at" . " < '$last_date')UNION(select a.id,a.created_at,$category,title,app_id,'argumentboxes' as module from argumentboxes a join apps p ON app_id=p.id" . " where p.category_id=$category AND a.created_at < '$last_date' AND a.app_id IN($network_ids)) order by created_at desc limit $limit";
        } elseif ($type != "all" && $category != "") {
            if ($type == "argumentboxes") {
                $query = "select a.id,a.created_at,$category,title,a.app_id,'argumentboxes' as module from argumentboxes a join apps p ON app_id=p.id where " . "p.category_id=$category AND a.app_id IN($network_ids) AND a.created_at <'$last_date' order by created_at desc limit $limit";
            } else {
                $query = "select id,created_at,title,app_id,'$type' as module from $type where category_id=$category AND app_id IN($network_ids) AND created_at < '$last_date' order by created_at" . " desc limit $limit";
            }
        }
        $all_feeds = $this->plugins_content($query);
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => $all_feeds['plugins']));
        return $this->response;
    }
    public function search() {
        $category = $this->request->get("category");
        $page = $this->request->get("page") ? $this->request->get("page") : 1;
        $limit = $this->request->get("limit") ? $this->request->get("limit") : 10;
        $type = $this->request->get("type");
        $search_key = $this->request->get("search_key");
        $last_date = $this->request->get("last_date");
        if (intval($page) === 1) {
            $last_date = date('Y-m-d H:i:s');
        } else {
            $date = strtotime($last_date);
            $last_date = date('Y-m-d H:i:s', $date);
        }
        $search_key = '[[:<:]]' . $search_key . '[[:>:]]';
        if ($type == "all" && $category == "") {
            $query = "(select distinct d.id,d.created_at,d.app_id,'debates' as module from debates as d inner join sides as s on d.id=s.debate_id inner join" . " debate_sides_meta as dm on dm.side_id=s.id where d.created_at<'$last_date' AND( dm.title REGEXP '$search_key' or" . " dm.opinion REGEXP '$search_key' or d.title REGEXP '$search_key')) UNION (select distinct c.id,c.created_at,c.app_id," . "'comparisons' as module from comparisons as c inner join sides as s on c.id=s.comparison_id inner join comparison_sides_meta as " . "cm on cm.side_id=s.id where c.created_at<'$last_date' AND (cm.title REGEXP '$search_key' or cm.description REGEXP '$search_key' or " . "c.title REGEXP '$search_key')) order by created_at desc limit $limit";
        } elseif ($type != "all" && $category == "") {
            if ($type == "debates") {
                $query = "select distinct d.id,d.created_at,d.app_id,'debates' as module from debates as d inner join sides as s on d.id=s.debate_id" . " inner join debate_sides_meta as dm on dm.side_id=s.id where d.created_at<'$last_date' AND (dm.title REGEXP '$search_key' or dm.opinion" . " REGEXP '$search_key' or d.title REGEXP '$search_key') order by d.created_at desc limit $limit";
            } else if ($type == "comparisons") {
                $query = "select distinct c.id,c.created_at,c.app_id,'comparisons' as module from comparisons as c inner join sides as s on c.id=s.comparison_id" . " inner join comparison_sides_meta as cm on cm.side_id=s.id where c.created_at<'$last_date' AND (cm.title REGEXP '$search_key' or" . " cm.description REGEXP '$search_key' or c.title REGEXP '$search_key') order by c.created_at desc limit $limit";
            }
        } elseif ($type == "all" && $category != "") {
            $query = "(select distinct d.id,d.created_at,d.app_id,'debates' as module from debates as d inner join sides as s on d.id=s.debate_id inner join" . " debate_sides_meta as dm on dm.side_id=s.id where d.created_at<'$last_date' AND d.category_id=$category AND(dm.title REGEXP '$search_key' " . "or dm.opinion REGEXP '$search_key' or d.title REGEXP '$search_key')) UNION (select distinct c.id,c.created_at,c.app_id,'comparisons' as" . " module from comparisons as c inner join sides as s on c.id=s.comparison_id inner join comparison_sides_meta as cm on cm.side_id=s.id " . "where c.created_at<'$last_date' AND c.category_id=$category AND (cm.title REGEXP '$search_key' or cm.description REGEXP '$search_key' or" . " c.title REGEXP '$search_key')) order by created_at desc limit $limit";
        } elseif ($type != "all" && $category != "") {
            if ($type == "debates") {
                $query = "select distinct d.id,d.created_at,d.app_id,'debates' as module from debates as d inner join sides as s on d.id=s.debate_id inner " . "join debate_sides_meta as dm on dm.side_id=s.id where d.created_at<'$last_date' AND d.category_id=$category AND( dm.title REGEXP '$search_key' or dm.opinion REGEXP '$search_key' or d.title REGEXP '$search_key') order by created_at desc limit $limit";
            } elseif ($type == "comparisons") {
                $query = "select distinct c.id,c.created_at,c.app_id,'comparisons' as module from comparisons as c inner join sides as s on c.id=s.comparison_id" . " inner join comparison_sides_meta as cm on cm.side_id=s.id where c.created_at<'$last_date' AND c.category_id=$category AND(cm.title " . "REGEXP '$search_key' or cm.description REGEXP '$search_key' or c.title REGEXP '$search_key') order by created_at desc limit $limit";
            }
        }
        $all_feeds = $this->plugins_content($query);
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => $all_feeds['plugins']));
        return $this->response;
    }
    function plugins_content($query, $app_id = 0, $page = 1) {
        $connection = $this->connection_initialization();
        $response_data = array();
        $user_id = Tokens::checkAccess();
        $super_admin = 0;
        if ($user_id) {
            $user = Users::findFirst($user_id);
            if ($user->getRoleId() == Roles::ADMIN) {
                $super_admin = 1;
            }
        }
        if ($app_id !== 0 && intval($page) === 1) {
            $query_result = mysql_query("select id from sides where argumentbox_id IN(select id from argumentboxes where app_id=$app_id) OR debate_id " . "IN(select id from debates where app_id=$app_id) OR comparison_id IN(select id from comparisons where app_id=$app_id)");
            while ($r = mysql_fetch_array($query_result)) {
                $sides.= $r['id'] . ",";
            }
            $sides = substr($sides, 0, -1);
            if ($sides) {
                $query_result = mysql_query("select count(id) as count from votes where side_id IN($sides)");
                $r = mysql_fetch_array($query_result);
                $votes_count = intval($r['count']);
                $query_result = mysql_query("select count(id) as count from arguments where side_id IN($sides)");
                $r = mysql_fetch_array($query_result);
                $arguments_count = intval($r['count']);
            } else {
                $votes_count = 0;
                $arguments_count = 0;
            }
            $response_data['interactions'] = $votes_count + $arguments_count;
        }
        $query_result = mysql_query($query);
        $feeds = array();
        while ($r = mysql_fetch_array($query_result)) {
            array_push($feeds, $r);
        }
        $all_feeds = array();
        foreach ($feeds as $feed) {
            $app = Apps::findFirst($feed['app_id']);
            if ($feed['module'] == "argumentboxes") {
                $plugin = Argumentboxes::findFirst($feed['id'])->toArray();
                $plugin['url'] = $plugin['protocol'] . $plugin['domain'] . $plugin['uri'];
                $sides = Sides::find(array("columns" => "id", "conditions" => "argumentbox_id=" . $feed['id']));
            } else if ($feed['module'] == "debates") {
                $plugin = Debates::findFirst(array("columns" => array("id", "title", "slug", "created_at", "url"), "conditions" => "id=" . $feed['id']))->toArray();
                $sides = Sides::find(array("columns" => "id", "conditions" => "debate_id=" . $feed['id']));
            } else {
                $plugin = Comparisons::findFirst(array("columns" => array("id", "title", "image", "slug", "created_at", "url", "align"), "conditions" => "id=" . $feed['id']))->toArray();
                $sides = Sides::find(array("columns" => "id", "conditions" => "comparison_id=" . $feed['id']));
            }
            $plugin['title'] = htmlspecialchars($plugin['title']);
            $plugin_sides = array();
            $sides = $sides->toArray();
            $totalSum = Votes::sumVotes($sides);
            foreach ($sides as $side) {
                $votes = Votes::count("side_id=" . $side['id']);
                if ($feed['module'] == "comparisons") {
                    $side_meta = ComparisonSidesMeta::findFirst("side_id=" . $side['id'])->toArray();
                    $side_meta['title'] = htmlspecialchars($side_meta['title']);
                    $side_meta['description'] = htmlspecialchars($side_meta['description']);
                } else if ($feed['module'] == "debates") {
                    $side_meta = DebateSidesMeta::findFirst("side_id=" . $side['id'])->toArray();
                    $side_meta['title'] = htmlspecialchars($side_meta['title']);
                    $side_meta['job_title'] = htmlspecialchars($side_meta['job_title']);
                    $side_meta['opinion'] = htmlspecialchars($side_meta['opinion']);
                }
                array_push($plugin_sides, array('id' => $side['id'], 'votes' => $votes, 'perc' => (!$votes) ? 0 : round($votes / ($totalSum ? $totalSum : 1) * 100), 'side_data' => $side_meta));
            }
            if ($super_admin) {
                $highlight_status = 0;
                $highlighted = HighlightedPlugins::findFirst("plugin_id=" . $plugin['id'] . " AND plugin_type='" . $feed['module'] . "'");
                if ($highlighted) {
                    $highlight_status = 1;
                }
            }
            array_push($all_feeds, array('action' => 'create', 'created_at' => $plugin['created_at'], 'app' => array('id' => $app->getId(), 'name' => $app->getName(), "image" => $app->getImage()), "data" => $plugin, 'highlight' => $highlight_status, "type" => $feed['module'], 'sides' => $plugin_sides, 'total_votes' => $totalSum));
        }
        $response_data['plugins'] = $all_feeds;
        return $response_data;
    }
    function connection_initialization() {
        $di = FactoryDefault::getDefault();
        $connection = mysql_connect($di->getShared('config')->get('database')->get('host'), $di->getShared('config')->get('database')->get('username'), $di->getShared('config')->get('database')->get('password'));
        if (!$connection) {
            die('Could not connect: ' . mysql_error());
        }
        mysql_select_db($di->getShared('config')->get('database')->get('dbname'), $connection);
        mysql_query("SET time_zone = 'Africa/Cairo'");
        return $connection;
    }
    function plugins_with_action($app, $id, $limit, $last_date, $type) {
        $connection = $this->connection_initialization();
        $user_id = Tokens::checkAccess();
        $super_admin = 0;
        if ($user_id) {
            $user = Users::findFirst($user_id);
            if ($user->getRoleId() == Roles::ADMIN) {
                $super_admin = 1;
            }
        }
        if ($app == 'app') {
            $condition = "v.admin_at=$id";
            $condition2 = "a.admin_at=$id";
        } else {
            $condition = "v.voter_id=$id";
            $condition2 = "a.owner_id=$id";
        }
        if ($type == "all") {
            $query_result = mysql_query("(select side_id,v.created_at,'vote' as action from votes v join sides s on v.side_id = s.id and $condition and" . " v.created_at<'$last_date' and(case when (s.argumentbox_id IS NOT NULL) THEN (select id from argumentboxes where" . " s.argumentbox_id=id) ELSE (case when(s.debate_id IS NOT NULL) THEN (select id from debates where" . " id=s.debate_id) ELSE (select id from comparisons where id=s.comparison_id) END)END))" . "UNION ALL(select a.side_id,a.created_at,'comment' as action from arguments a join sides s on a.side_id = s.id and" . " $condition2 and a.created_at<'$last_date' and(case when (s.argumentbox_id IS NOT NULL) THEN (select id from argumentboxes" . " where s.argumentbox_id=id) ELSE (case when(s.debate_id IS NOT NULL) THEN (select id from debates where" . " id=s.debate_id) ELSE (select id from comparisons where id=s.comparison_id)" . " END)END))order by created_at desc limit $limit");
        } elseif ($type == "comparisons") {
            $query_result = mysql_query("(select side_id,created_at,'vote' as action from votes v join sides s ON v.side_id=s.id AND s.comparison_id IS NOT NULL AND $condition AND created_at<'$last_date' and (case when (s.comparison_id IS NOT NULL) THEN (select id from comparisons where s.comparison_id=id)END)) UNION ALL (select side_id ,created_at,'comment' as action from arguments a join sides s ON a.side_id=s.id AND s.comparison_id IS NOT NULL AND $condition2 AND created_at<'$last_date' and (case when (s.comparison_id IS NOT NULL) THEN (select id from comparisons where s.comparison_id=id)END)) order by created_at desc limit $limit");
        } elseif ($type == "debates") {
            $query_result = mysql_query("(select side_id,created_at,'vote' as action from votes v join sides s ON v.side_id=s.id AND s.debate_id IS NOT NULL AND" . " $condition AND created_at<'$last_date' and (case when (s.debate_id IS NOT NULL) THEN (select id from debates where s.debate_id=id)END)) UNION ALL (select side_id ,created_at,'comment' as action from arguments a join" . " sides s ON a.side_id=s.id AND s.debate_id IS NOT NULL AND $condition2 AND created_at<'$last_date' and (case when (s.debate_id IS NOT NULL) THEN (select id from debates where s.debate_id=id)END)) order by" . " created_at desc limit $limit");
        } else {
            $query_result = mysql_query("(select side_id,created_at,'vote' as action from votes v join sides s ON v.side_id=s.id AND s.argumentbox_id IS NOT NULL AND" . " $condition AND created_at<'$last_date' and (case when (s.argumentbox_id IS NOT NULL) THEN (select id from argumentboxes where s.argumentbox_id=id)END)) UNION ALL (select side_id ,created_at,'comment' as action from arguments a join" . " sides s ON a.side_id=s.id AND s.argumentbox_id IS NOT NULL AND $condition2 AND created_at<'$last_date' and (case when (s.argumentbox_id IS NOT NULL) THEN (select id from argumentboxes where s.argumentbox_id=id)END)) order by" . " created_at desc limit $limit");
        }
        $plugins = array();
        $all_feeds = array();
        while ($r = mysql_fetch_array($query_result)) {
            $side_id = $r['side_id'];
            $action = $r['action'];
            $side = Sides::findFirst($side_id);
            if ($side->getArgumentboxId()) {
                $plugin_type = "argumentboxes";
                $plugin = Argumentboxes::findFirst($side->getArgumentboxId())->toArray();
                $sides = Sides::find(array("columns" => "id", "conditions" => "argumentbox_id=" . $plugin['id']));
            } else if ($side->getDebateId()) {
                $plugin_type = "debates";
                $plugin = Debates::findFirst(array("columns" => array("id", "title", "slug", "created_at", "url"), "conditions" => "id=" . $side->getDebateId()))->toArray();
                $sides = Sides::find(array("columns" => array("id"), "conditions" => "debate_id=" . $plugin['id']));
            } else {
                $plugin_type = "comparisons";
                $plugin = Comparisons::findFirst(array("columns" => array("id", "title", "image", "slug", "created_at", "url", "align"), "conditions" => "id=" . $side->getComparisonId()))->toArray();
                $sides = Sides::find(array("columns" => "id", "conditions" => "comparison_id=" . $plugin['id']));
            }
            $plugin['title'] = htmlspecialchars($plugin['title']);
            $app = Apps::findFirst($plugin['app_id']);
            $plugin_sides = array();
            $sides = $sides->toArray();
            $totalSum = Votes::sumVotes($sides);
            foreach ($sides as $side) {
                $votes = Votes::count("side_id=" . $side['id']);
                if ($plugin_type == "comparisons") {
                    $side_meta = ComparisonSidesMeta::findFirst("side_id=" . $side['id']);
                    if ($side_meta) {
                        $side_meta = $side_meta->toArray();
                        $side_meta['title'] = htmlspecialchars($side_meta['title']);
                        $side_meta['description'] = htmlspecialchars($side_meta['description']);
                    }
                } else if ($plugin_type == "debates") {
                    $side_meta = DebateSidesMeta::findFirst("side_id=" . $side['id']);
                    if ($side_meta) {
                        $side_meta = $side_meta->toArray();
                        $side_meta['title'] = htmlspecialchars($side_meta['title']);
                        $side_meta['job_title'] = htmlspecialchars($side_meta['job_title']);
                        $side_meta['opinion'] = htmlspecialchars($side_meta['opinion']);
                    }
                }
                array_push($plugin_sides, array('id' => $side['id'], 'votes' => $votes, 'perc' => (!$votes) ? 0 : round($votes / ($totalSum ? $totalSum : 1) * 100), 'side_data' => $side_meta));
            }
            if ($super_admin) {
                $highlight_status = 0;
                $highlighted = HighlightedPlugins::findFirst("plugin_id=" . $plugin['id'] . " AND plugin_type='" . $feed['module'] . "'");
                if ($highlighted) {
                    $highlight_status = 1;
                }
            }
            array_push($all_feeds, array('action' => $r['action'], 'created_at' => $r['created_at'], 'app' => array('name' => $app->getName(), "image" => $app->getImage()), "data" => $plugin, 'highlight' => $highlight_status, "type" => $plugin_type, 'sides' => $plugin_sides));
        }
        return $all_feeds;
    }
    public function community_profile($app_id) {
        $app = Apps::findFirst($app_id);
        if (!$app) {
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
            return $this->response;
        }
        $category = $this->request->get("category");
        $page = $this->request->get("page") ? $this->request->get("page") : 1;
        $limit = $this->request->get("limit") ? $this->request->get("limit") : 10;
        $type = $this->request->get("type");
        $last_date = $this->request->get("last_date");
        if (intval($page) === 1) {
            $last_date = date('Y-m-d H:i:s');
        } else {
            $date = strtotime($last_date);
            $last_date = date('Y-m-d H:i:s', $date);
        }
        $info = array();
        $interactions = $this->plugins_with_action('app', $app_id, $limit, $last_date, $type);
        if ($type == "all" && $category == "") {
            $query = "(select id,created_at,title,app_id,'debates' as module from debates where app_id=$app_id AND created_at<'$last_date') UNION (select id" . ",created_at,title,app_id,'comparisons' as module from comparisons where app_id=$app_id AND created_at<'$last_date') UNION (select id" . ",created_at,title,app_id,'argumentboxes' as module from argumentboxes where app_id=$app_id AND created_at<'$last_date') order by " . "created_at desc limit $limit";
        } elseif ($type != "all" && $category == "") {
            $query = "select id,created_at,title,app_id,'$type' as module from $type where app_id=$app_id AND created_at<'$last_date' order by created_at" . " desc limit $limit";
        } elseif ($type == "all" && $category != "") {
            $query = "(select id,created_at,category_id,title,app_id,'debates' as module from debates where created_at<'$last_date' AND app_id=$app_id AND " . "category_id=$category) UNION (select id,created_at,category_id,title,app_id,'comparisons' as module from comparisons where " . "created_at<'$last_date' AND app_id=$app_id AND category_id=$category)UNION(select id,created_at,$category,title,app_id,'argumentboxes' as" . " module from argumentboxes where app_id=$app_id AND created_at<'$last_date') order by created_at desc limit $limit";
        } elseif ($type != "all" && $category != "") {
            if ($type == "argumentboxes") {
                $query = "select id,created_at,$category,title,app_id,'argumentboxes' as module from argumentboxes where app_id=$app_id AND created_at" . "<'$last_date' order by created_at desc limit $limit";
            } else {
                $query = "select id,created_at,title,app_id,'$type' as module from $type where app_id=$app_id AND category_id=$category AND created_at<'$last_Date' order by created_at desc limit $limit";
            }
        }
        $plugins = $this->plugins_content($query, $app_id, $page);
        $plugins_actions = array_merge($plugins['plugins'], $interactions);
        function sort_plugins($a, $b) {
            $t1 = $a['created_at'];
            $t2 = $b['created_at'];
            return $t2 > $t1;
        }
        usort($plugins_actions, 'sort_plugins');
        $info['plugins'] = array_slice($plugins_actions, 0, $limit);
        if (intval($page) === 1) {
            $debates_count = Debates::count('app_id=' . $app_id);
            $comparisons_count = Comparisons::count('app_id=' . $app_id);
            $argumentboxes_count = Argumentboxes::count('app_id=' . $app_id);
            $info['argumentboxes_count'] = intval($argumentboxes_count);
            $info['debates_count'] = intval($debates_count);
            $info['comparisons_count'] = intval($comparisons_count);
            $info['interactions_count'] = $plugins['interactions'];
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => array('name' => $app->getName(), 'image' => $app->getImage(), 'info' => $info)));
        return $this->response;
    }
    public function community_user_profile($user_id) {
        $user = Users::findFirst($user_id);
        if (!$user) {
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
            return $this->response;
        }
        $category = $this->request->get("category");
        $page = $this->request->get("page") ? $this->request->get("page") : 1;
        $limit = $this->request->get("limit") ? $this->request->get("limit") : 10;
        $type = $this->request->get("type");
        $last_date = $this->request->get("last_date");
        if (intval($page) === 1) {
            $last_date = date('Y-m-d H:i:s');
        } else {
            $date = strtotime($last_date);
            $last_date = date('Y-m-d H:i:s', $date);
        }
        $info = array();
        $interactions = $this->plugins_with_action('user', $user->getId(), $limit, $last_date, $type);
        $info['plugins'] = $interactions;
        if (intval($page) === 1) {
            $votes_count = Votes::count('voter_id=' . $user->getId());
            $comments_count = Arguments::count('owner_id=' . $user->getId());
            $replies_count = Replies::count('user_id=' . $user->getId());
            $info['votes_count'] = intval($votes_count);
            $info['comments_count'] = intval($comments_count) + intval($replies_count);
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => array('name' => $user->get_first_name() . " " . $user->get_last_name(), 'image' => $user->getProfilePicture(), 'info' => $info)));
        return $this->response;
    }
    public function highlighted_plugins() {
        $highlighted_count = intval(HighlightedPlugins::count());
        if ($highlighted_count > 0) {
            $connection = $this->connection_initialization();
            $lang = $this->request->get("lang") ? $this->request->get("lang") : "en";
            $network = $this->request->get("network") ? $this->request->get("network") : "en";
            $type = $this->request->get("type");
            if ($type == "all") {
                $query_result = mysql_query("select * from (select * from highlighted_plugins where network='$network' ORDER BY id desc limit 15)as a order by rand() limit 2");
            } else {
                $query_result = mysql_query("select * from (select * from highlighted_plugins where network='$network' AND plugin_type='$type' ORDER BY id desc limit 15)as a order by rand() limit 2");
            }
            $plugins = array();
            while ($r = mysql_fetch_array($query_result)) {
                array_push($plugins, $r);
            }
            if ($plugins) {
                foreach ($plugins as $plugin) {
                    if ($query != "") {
                        $query.= "UNION";
                    }
                    $query = $query . "(select id,created_at,title,app_id,'" . $plugin['plugin_type'] . "' as module from " . $plugin['plugin_type'] . " where id=" . $plugin['plugin_id'] . ")";
                }
                $all_feeds = $this->plugins_content($query);
                $all_feeds = $all_feeds['plugins'];
            }
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => $all_feeds));
        return $this->response;
    }
    public function number_of_new_plugins() {
        $user_id = Tokens::checkAccess();
        $last_seen_categories = LastSeenCategories::findFirst("user_id=$user_id");
        $data = array();
        $plugins = array();
        if (!$last_seen_categories) {
            $categories = Categories::find();
            $current_date = date('Y-m-d H:i:s');
            foreach ($categories as $cat) {
                $data[$cat->getId() ] = $current_date;
            }
            $last_seen_categories = new LastSeenCategories();
            $last_seen_categories->set_user_id($user_id);
            $last_seen_categories->set_last_seen_dates(json_encode($data));
            $last_seen_categories->save();
        } else {
            $connection = $this->connection_initialization();
            $last_dates = json_decode($last_seen_categories->get_last_seen_dates());
            $i = 1;
            foreach ($last_dates as $d) {
                $query_result = mysql_query("select count(id) as count from debates where category_id=$i AND created_at> '$d'");
                $result = mysql_fetch_array($query_result);
                $debates_count = $result['count'];
                $query_result = mysql_query("select count(id) as count from comparisons where category_id=$i AND created_at> '$d'");
                $result = mysql_fetch_array($query_result);
                $comparisons_count = $result['count'];
                $query_result = mysql_query("select count(a.id) as count from argumentboxes a join apps p ON app_id=p.id where p.category_id=$i AND a.created_at>'$d'");
                $result = mysql_fetch_array($query_result);
                $argumentboxes_count = $result['count'];
                $count = $debates_count + $comparisons_count + $argumentboxes_count;
                array_push($plugins, array("id" => $i, "count" => $count));
                $i++;
            }
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => $plugins));
        return $this->response;
    }
}
