<?php
use Phalcon\DI\FactoryDefault;
class AdsController extends BaseController {
    public function create_action() {
        $post_data = $this->request->getPost();
        $ad = new Ads();
        if ($post_data['owner_name'] == "" || $post_data['owner_email'] == "") {
            $this->response->setStatusCode(601, 'Invalid data');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "Invalid data"));
            return $this->response;
        }
        $valid_name = filter_var($post_data['owner_name'], FILTER_SANITIZE_STRING);
        if ($valid_name == false) {
            $this->response->setStatusCode(601, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-name")));
            return $this->response;
        }
        if ($post_data['url'] == "") {
            $this->response->setStatusCode(601, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-website")));
            return $this->response;
        }
        $code = $post_data['code'];
        $valid_email = filter_var($post_data['owner_email'], FILTER_VALIDATE_EMAIL);
        if ($valid_email == false) {
            $this->response->setStatusCode(601, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("user-invalid-email")));
            return $this->response;
        }
        $plugin_type = $post_data['plugin_type'];
        $identification = $post_data['plugin'];
        if ($plugin_type == "debate") {
            $plugin = Debates::findFirst("slug='" . $identification . "'");
        } else if ($plugin_type == "comparison") {
            $plugin = Comparisons::findFirst("slug='" . $identification . "'");
        } else {
            if ($code != "") {
                $plugin = Argumentboxes::findFirst("code='$code'");
            } else {
                $identification = str_replace("http://", "", $identification);
                $identification = str_replace("https://", "", $identification);
                $plugin = Argumentboxes::findFirst("CONCAT(domain,uri)='" . $identification . "'");
            }
        }
        if (!$plugin) {
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "Plugin doesn't exist"));
        } else {
            if ($this->request->getUploadedFiles()) {
                $files = $this->request->getUploadedFiles();
                $file_1 = $files[0];
                $file_2 = $files[1];
                $files_name = array($file_1->getName() => $file_1->getKey());
                if ($file_2) {
                    $files_name[$file_2->getName() ] = $file_2->getKey();
                }
                $objImgUploader = \Phalcon\DI::getDefault()->getShared('Utility')->getImageUploader();
                $photos = $objImgUploader->upload($objImgUploader::DIR_ARGUMENTS, $this->request->getUploadedFiles());
                for ($i = 0;$i < sizeof($photos);$i++) {
                    $file = $files_name[$photos[$i]['file']];
                    if ($file == "image") {
                        $ad->set_attached_photos($photos[$i]['path'] . $photos[$i]['new']);
                    } else {
                        $ad->set_owner_photo($photos[$i]['path'] . $photos[$i]['new']);
                    }
                }
            }
            $ad->set_url($post_data['url']);
            $ad->set_owner_name($post_data['owner_name']);
            $ad->set_owner_email($post_data['owner_email']);
            $ad->set_owner_lang($post_data['owner_lang']);
            $ad->set_context($post_data['text']);
            $ad->set_created_at(date('Y-m-d H:i:s'));
            $ad->save();
            $plugins_ads = new PluginsAds();
            $plugins_ads->set_ad_id($ad->get_id());
            $plugins_ads->set_plugin_type($plugin_type);
            $plugins_ads->set_plugin_id($plugin->getId());
            $plugins_ads->save();
            $new_log = new Log();
            $new_log->log_current_action(Tokens::checkAccess(), $this->headers['Ip'], "/ads/create", $plugins_ads->get_id());
            if ($plugin_type == "argumentbox") {
                $plugin_url = $plugin->get_protocol() . $plugin->get_domain() . $plugin->get_uri();
            } else {
                $plugin_url = $plugin->getUrl();
            }
            $this->send_sponsorship_email($post_data['owner_name'], $post_data['owner_email'], $plugin_type, $plugin->getTitle(), $plugin_url, $post_data['owner_lang']);
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK'));
        }
        return $this->response;
    }
    function send_sponsorship_email($owner_name, $owner_email, $plugin_type, $plugin_title, $plugin_url, $lang) {
        $this->getDI()->set('language', $lang);
        $plugin_type = $this->t->_($plugin_type);
        $this->view->lang = $lang;
        $this->view->name = $owner_name;
        $this->view->email = $owner_email;
        $this->view->type = $plugin_type;
        $this->view->title = $plugin_title;
        $this->view->url = urldecode(urldecode(urldecode($plugin_url)));
        $this->assets->addCss('css/email.css');
        $this->view->start();
        $this->view->update = "apps/settings";
        $this->view->render('emails', 'sponsorship-alert');
        $this->view->finish();
        $content = $this->view->getContent();
        $message = array('html' => $content, 'subject' => $this->t->_('sponsorship-alert-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $owner_email, 'type' => 'to')),);
        $async = false;
        $ip_pool = 'Main Pool';
    }
    function send_money_started_email($name, $email, $plugin_title, $lang) {
        $this->getDI()->set('language', $lang);
        $this->view->lang = $lang;
        $this->view->name = $name;
        $this->view->update = "apps/settings";
        $this->view->title = $plugin_title;
        $this->assets->addCss('css/email.css');
        $this->view->start();
        $this->view->render('emails', 'money-started');
        $this->view->finish();
        $content = $this->view->getContent();
        $message = array('html' => $content, 'subject' => $this->t->_('money-started-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $email, 'type' => 'to')),);
        $async = false;
        $ip_pool = 'Main Pool';
    }
    public function thumbUpAction($ad_id) {
        $userId = Tokens::checkAccess();
        if ($userId) {
            $thumb_exist = AdsThumbs::findFirst(array('conditions' => 'thumber_id=' . $userId . ' AND ad_id=' . $ad_id));
            if ($thumb_exist) {
                if (intval($thumb_exist->get_thumb_up()) === 1) {
                    $thumb_exist->delete();
                    $this->response->setStatusCode(302, 'Found');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Found'));
                    return $this->response;
                } else {
                    $thumb_exist->set_thumb_up(1);
                    $thumb_exist->save();
                }
            } else {
                $ad = Ads::findFirst($ad_id);
                if ($ad->get_publisher_email() === 0) {
                    $plugin_ad = PluginsAds::findFirst('ad_id=' . $ad_id);
                    if ($plugin_ad->get_plugin_type() == "comparison") {
                        $plugin = Comparisons::findFirst($plugin_ad->get_plugin_id());
                    } else if ($plugin_ad->get_plugin_type() == "debate") {
                        $plugin = Debates::findFirst($plugin_ad->get_plugin_id());
                    } else {
                        $plugin = Argumentboxes::findFirst($plugin_ad->get_plugin_id());
                    }
                    $app = Apps::findFirst($plugin->getAppId());
                    $user = Users::findFirst($app->getPublisherId());
                    $this->send_money_started_email($user->get_first_name() . " " . $user->get_last_name(), $user->getEmail(), $plugin->getTitle(), $user->getLocale());
                    $ad->set_publisher_email(1);
                    $ad->save();
                }
                $ad_thumb = new AdsThumbs();
                $ad_thumb->set_thumber_id($userId);
                $ad_thumb->set_thumb_up(1);
                $ad_thumb->set_ad_id($ad_id);
                $ad_thumb->save();
            }
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        } else {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => '401 Unauthorized'));
        }
        return $this->response;
    }
    public function thumbDownAction($ad_id) {
        $userId = Tokens::checkAccess();
        if ($userId) {
            $thumb_exist = AdsThumbs::findFirst(array('conditions' => 'thumber_id=' . $userId . ' AND ad_id=' . $ad_id));
            if ($thumb_exist) {
                if (intval($thumb_exist->get_thumb_up()) === 0) {
                    $thumb_exist->delete();
                    $this->response->setStatusCode(302, 'Found');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Found'));
                    return $this->response;
                } else {
                    $thumb_exist->set_thumb_up(0);
                    $thumb_exist->save();
                }
            } else {
                $ad = Ads::findFirst($ad_id);
                if ($ad->get_publisher_email() === 0) {
                    $plugin_ad = PluginsAds::findFirst('ad_id=' . $ad_id);
                    if ($plugin_ad->get_plugin_type() == "comparison") {
                        $plugin = Comparisons::findFirst($plugin_ad->get_plugin_id());
                    } else if ($plugin_ad->get_plugin_type() == "debate") {
                        $plugin = Debates::findFirst($plugin_ad->get_plugin_id());
                    } else {
                        $plugin = Argumentboxes::findFirst($plugin_ad->get_plugin_id());
                    }
                    $app = Apps::findFirst($plugin->getAppId());
                    $user = Users::findFirst($app->getPublisherId());
                    $this->send_money_started_email($user->get_first_name() . " " . $user->get_last_name(), $user->getEmail(), $plugin->getTitle(), $user->getLocale());
                    $ad->set_publisher_email(1);
                    $ad->save();
                }
                $ad_thumb = new AdsThumbs();
                $ad_thumb->set_thumber_id($userId);
                $ad_thumb->set_thumb_up(0);
                $ad_thumb->set_ad_id($ad_id);
                $ad_thumb->save();
            }
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        } else {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => '401 Unauthorized'));
        }
        return $this->response;
    }
    public function add_reply_action() {
        $userId = Tokens::checkAccess();
        if (!$userId) {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => '401 Unauthorized'));
            return $this->response;
        }
        $intAdId = $this->getDi()->getShared('request')->getPost('ad_id');
        $intParentReply = $this->getDi()->getShared('request')->getPost('parent_reply');
        $txtContext = $this->getDi()->getShared('request')->getPost('context');
        $txtContext = trim($txtContext);
        $arrPhotos = $this->getDi()->getShared('request')->getUploadedFiles();
        $audio = $this->getDi()->getShared('request')->getPost('audio');
        if (empty($txtContext) && empty($arrPhotos)) {
            $this->response->setStatusCode(400, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("reply-emty-comments")));
            return $this->response;
        }
        if (!empty($arrPhotos)) {
            $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
            if (intval($audio) === 1) {
                $arrUploadedPhotos = $objImgUploader->upload($objImgUploader::DIR_REPLIES, $arrPhotos, null, array(), true);
            } else {
                $arrUploadedPhotos = $objImgUploader->upload($objImgUploader::DIR_REPLIES, $arrPhotos);
            }
            if ($arrUploadedPhotos[0]['status'] != 1) {
                $this->response->setStatusCode(400, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $arrUploadedPhotos[0]['error']));
                return $this->response;
            }
        }
        $ad = Ads::findFirst($intAdId);
        if (!$ad) {
            $this->response->setStatusCode(404, "Not found");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
            return $this->response;
        }
        $plugin_ad = PluginsAds::findFirst('ad_id=' . $ad->get_id());
        $ads_reply = new AdsReplies();
        $ads_reply->set_user_id($userId);
        $ads_reply->set_ad_id($intAdId);
        $ads_reply->set_parent_reply($intParentReply);
        $ads_reply->set_context($txtContext);
        if (!empty($arrUploadedPhotos)) {
            foreach ($arrUploadedPhotos as $photo) {
                if ($photo['status'] == FALSE) {
                    continue;
                }
                $ads_reply->set_attached_photos($photo['path'] . $photo['new']);
            }
        }
        $ads_reply->save();
        if ($ad->get_publisher_email() === 0) {
            $plugin_ad = PluginsAds::findFirst('ad_id=' . $ad->get_id());
            if ($plugin_ad->get_plugin_type() == "comparison") {
                $plugin = Comparisons::findFirst($plugin_ad->get_plugin_id());
            } else if ($plugin_ad->get_plugin_type() == "debate") {
                $plugin = Debates::findFirst($plugin_ad->get_plugin_id());
            } else {
                $plugin = Argumentboxes::findFirst($plugin_ad->get_plugin_id());
            }
            $app = Apps::findFirst($plugin->getAppId());
            $user = Users::findFirst($app->getPublisherId());
            $this->send_money_started_email($user->get_first_name() . " " . $user->get_last_name(), $user->getEmail(), $plugin->getTitle(), $user->getLocale());
            $ad->set_publisher_email(1);
            $ad->save();
        }
        $new_log = new Log();
        $new_log->log_current_action(Tokens::checkAccess(), $this->headers['Ip'], "/ads/replies/create", $ads_reply->get_id());
        $this->response->setStatusCode(200, "OK");
        $this->response->setJsonContent(array('status' => 'OK', 'data' => array('id' => $ads_reply->get_id())));
        return $this->response;
    }
    public function show_replies_action($ad_id, $_page = 1, $_limit = 2) {
        $_offset = ($_page == 1) ? 0 : ($_page - 1) * $_limit;
        $page = $_page < 1 ? 1 : $_page;
        $startValue = ($page - 1) * $_limit;
        $count = AdsReplies::count(array('ad_id = :ad_id: AND parent_reply is NULL', 'bind' => array('ad_id' => $ad_id),));
        $hasmore = (($count >= (($startValue + 1) + $_limit)) ? true : FALSE);
        $ads = array('has_more' => $hasmore, 'data' => AdsReplies::find(array('ad_id = :ad_id: AND parent_reply is NULL', 'bind' => array('ad_id' => $ad_id), "order" => "id desc", "limit" => array("number" => $_limit, "offset" => $_offset))));
        foreach ($ads['data'] as $ad) {
            $arrAds[] = self::formatReply($ad);
        }
        if ($arrAds === FALSE) {
            throw new Exception(FactoryDefault::getDefault()->get('t')->_("bad-request"), 400);
        }
        $replies = array('data' => $arrAds, 'hasmore' => $ads['has_more']);
        $this->response->setStatusCode(200, "OK");
        $this->response->setJsonContent(array('status' => 'OK', 'data' => $replies));
        return $this->response;
    }
    public static function formatReply($reply) {
        if ($reply->get_attached_photos()) {
            $arrAttachments[] = array('url' => ((empty($_SERVER['HTTPS'])) ? 'http' : 'https') . '://' . $_SERVER['SERVER_NAME'] . $reply->get_attached_photos());
        }
        if ($reply->get_parent_reply()) {
            $return = AdsReplies::findFirst($reply->get_parent_reply());
            $user = Users::findFirst($return->get_user_id());
            $replyUser = $user->get_first_name() . " " . $user->get_last_name();
        } else {
            $replyUser = '';
        }
        $perPage = Phalcon\DI::getDefault()->getShared('config')->application->commentsPagination;
        $repliesCount = AdsReplies::count(array('conditions' => 'parent_reply=' . $reply->get_id()));
        $user = Users::findFirst($reply->get_user_id());
        $owner_id = $user->getId();
        $owner_name = $user->get_first_name() . " " . $user->get_last_name();
        $owner_slug = $user->getSlug();
        $owner_photo = $user->getProfilePicture();
        $current_user = Tokens::getSignedInUserId();
        if ($current_user) {
            $current_user_voted = AdsRepliesThumbs::findFirst('thumber_id=' . $current_user . ' AND ad_reply_id=' . $reply->get_id());
        } else {
            $current_user_voted = FALSE;
        }
        $date = strtotime($reply->get_created_at());
        $date = date('F jS', $date);
        $reply_data = array('id' => $reply->get_id(), 'context' => $reply->get_context(), 'attached_photos' => $arrAttachments, 'owner_id' => $owner_id, 'owner_name' => $owner_name, 'owner_slug' => $owner_slug, 'owner_photo' => $owner_photo, 'owner_status' => Beacon::user_status($owner_id), 'created_at' => $date, 'count_thumbs_up' => AdsRepliesThumbs::count(array('conditions' => 'ad_reply_id=' . $reply->get_id() . ' AND thumb_up=1')), 'count_thumbs_dn' => AdsRepliesThumbs::count(array('conditions' => 'ad_reply_id=' . $reply->get_id() . ' AND thumb_up=0')), 'replies_count' => $repliesCount, 'reply_level' => static ::getReplyLevel($reply->get_id()), 'reply_to_user' => $replyUser, 'currentUserhasVoted' => (FALSE != $current_user_voted), 'currentUserVote' => (FALSE != $current_user_voted ? $current_user_voted->get_thumb_up() : FALSE), 'total_replies_pages' => intval($perPage ? $repliesCount / $perPage : $repliesCount / 2));
        return $reply_data;
    }
    public static function getReplyLevel($_replyID, $level = 1) {
        $result = AdsReplies::findFirst($_replyID);
        if ($result) {
            $parentReply = $result->get_parent_reply();
            if (is_null($parentReply)) {
                return $level;
            } else {
                $level++;
                return static ::getReplyLevel($parentReply, $level);
            }
        }
        return false;
    }
    public function thumb_down_reply($reply_id) {
        $userId = Tokens::checkAccess();
        if ($userId) {
            $new_log = new Log();
            $new_log->log_current_action(Tokens::checkAccess(), $this->headers['Ip'], "/ads/replies/" . $reply_id . "/thumb_down", $reply_id);
            $thumb_exist = AdsRepliesThumbs::findFirst(array('conditions' => 'thumber_id=' . $userId . ' AND ad_reply_id=' . $reply_id));
            if ($thumb_exist) {
                if (intval($thumb_exist->get_thumb_up()) === 0) {
                    $thumb_exist->delete();
                    $this->response->setStatusCode(302, 'Found');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Found'));
                    return $this->response;
                } else {
                    $thumb_exist->set_thumb_up(0);
                    $thumb_exist->save();
                }
            } else {
                $ad_thumb = new AdsRepliesThumbs();
                $ad_thumb->set_thumber_id($userId);
                $ad_thumb->set_thumb_up(0);
                $ad_thumb->set_ad_reply_id($reply_id);
                $ad_thumb->save();
            }
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        } else {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => '401 Unauthorized'));
        }
        return $this->response;
    }
    public function thumb_up_reply($reply_id) {
        $userId = Tokens::checkAccess();
        if ($userId) {
            $new_log = new Log();
            $new_log->log_current_action(Tokens::checkAccess(), $this->headers['Ip'], "/ads/replies/" . $reply_id . "/thumb_up", $reply_id);
            $thumb_exist = AdsRepliesThumbs::findFirst(array('conditions' => 'thumber_id=' . $userId . ' AND ad_reply_id=' . $reply_id));
            if ($thumb_exist) {
                if (intval($thumb_exist->get_thumb_up()) === 1) {
                    $thumb_exist->delete();
                    $this->response->setStatusCode(302, 'Found');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Found'));
                    return $this->response;
                } else {
                    $thumb_exist->set_thumb_up(1);
                    $thumb_exist->save();
                }
            } else {
                $ad_thumb = new AdsRepliesThumbs();
                $ad_thumb->set_thumber_id($userId);
                $ad_thumb->set_thumb_up(1);
                $ad_thumb->set_ad_reply_id($reply_id);
                $ad_thumb->save();
            }
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        } else {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => '401 Unauthorized'));
        }
        return $this->response;
    }
    public function showNestedAction($parent_reply, $page = 1, $limit = 2) {
        $offset = ($page == 1) ? 0 : ($page - 1) * $limit;
        $page = $page < 1 ? 1 : $page;
        $startValue = ($page - 1) * $limit;
        $count = AdsReplies::count(array('parent_reply = :parent_reply:', 'bind' => array('parent_reply' => $parent_reply), "order" => "id desc",));
        $hasmore = (($count >= (($startValue + 1) + $limit)) ? true : FALSE);
        $replies = array('has_more' => $hasmore, 'data' => AdsReplies::find(array('parent_reply = :parent_reply:', 'bind' => array('parent_reply' => $parent_reply), "order" => "id desc", "limit" => array("number" => $limit, "offset" => $offset))));
        $arrReplies = array();
        foreach ($replies['data'] as $reply) {
            $arrReplies[] = self::formatReply($reply);
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => array('hasmore' => $replies['has_more'], 'data' => $arrReplies)));
        return $this->response;
    }
    public function showSingleReplyAction($reply_id) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => self::formatReply(AdsReplies::findFirst($reply_id))));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function deleteAction($reply_id) {
        try {
            $user_id = Tokens::checkAccess();
            if (!$user_id) {
                $this->response->setStatusCode(401, 'Unauthorized');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => '401 Unauthorized'));
                return $this->response;
            }
            $reply_data = AdsReplies::findFirst("id=$reply_id AND user_id=$user_id");
            $new_log = new Log();
            $new_log->log_current_action($user_id, $this->headers['Ip'], "/ads/replies/" . $reply_id . "/delete", $reply_id);
            if ($reply_data) {
                $nested_replies = AdsReplies::find('parent_reply=' . $reply_data->get_id());
                foreach ($nested_replies as $reply) {
                    $replies_thumbs = AdsRepliesThumbs::find('ad_reply_id=' . $reply->get_id());
                    if ($replies_thumbs) {
                        $replies_thumbs->delete();
                    }
                    $reply->delete();
                }
                $parent_reply_thumbs = AdsRepliesThumbs::find('ad_reply_id=' . $reply_data->get_id());
                if ($parent_reply_thumbs) {
                    $parent_reply_thumbs->delete();
                }
                $reply_data->delete();
                $this->response->setStatusCode(200, 'OK');
                $this->response->setJsonContent(array('status' => 'OK', 'data' => true));
            } else {
                $this->response->setStatusCode(404, 'Not Found');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "Reply id doesn't exist"));
            }
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
}
