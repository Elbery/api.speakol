<?php
use Phalcon\DI\FactoryDefault;
class ComparisonsController extends BaseController {
    public function indexAction() {
        try {
            $comparisons = new Comparisons();
            $appId = $this->request->get('app_id');
            $conditions = array("conditions" => "app_id=:app_id:", "bind" => array('app_id' => $appId), "order" => "id desc");
            if ($appId == 'null') {
                $conditions = array("conditions" => "app_id is null", "order" => "id desc");
            }
            $data = $comparisons->listComparison($conditions);
            if (is_array($data)) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function createAction() {
        try {
            $this->getDI()->set('language', $this->request->get("lang"));
            $validate_plugins_creation = AppsController::validate_plugins_creation();
            if ($validate_plugins_creation['data']['remaining_plugins'] === 0) {
                $this->response->setStatusCode(605, 'ERROR');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_("max_plugins")));
                return $this->response;
            }
            $comparisonObj = new Comparisons();
            $return = $comparisonObj->createComparison($this->request);
            if ($return['status'] == 'ok') {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['messages']));
            } else {
                $this->response->setStatusCode(500, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $return['data']));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(500, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function isfollowedAction($slug) {
        try {
            $signedInUserID = Tokens::checkAccess();
            $comparisonFollows = new ComparisonFollows();
            $data = $comparisonFollows->isFollowed($slug, $signedInUserID);
            if (is_int($data)) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function followAction($slug) {
        try {
            $signedInUserID = Tokens::checkAccess();
            $comparisonFollows = new ComparisonFollows();
            $data = $comparisonFollows->insertFollow($slug, $signedInUserID);
            if ($data === true) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function unfollowAction($slug) {
        try {
            $signedInUserID = Tokens::checkAccess();
            $comparisonFollows = new ComparisonFollows();
            $data = $comparisonFollows->deleteFollow($slug, $signedInUserID);
            if ($data === true) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function listFollowersAction($slug) {
        try {
            $data = ComparisonFollows::listFollowers($slug);
            if ($data != false) {
                $this->setResponse($data);
            } else {
                $this->setResponse($this->t->_('no-com-found'), false);
            }
        }
        catch(Exception $e) {
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function showAction($slug) {
        try {
            $order_by = $this->request->get("order_by");
            if (empty($oder_by)) {
                $order_by = "most_voted";
            }
            $url = $this->request->get("url");
            $app_id = $this->request->get("app_id");
            if (!is_numeric($app_id)) {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1));
                return $this->response;
            }
            $domains = array($this->getDI()->getShared('config')->get("application")->get("default-url"));
            $domains[] = $this->getDI()->getShared('config')->get("application")->get("plugins_speakol");
            $app_domains = AppDomains::find('app_id=' . $app_id);
            foreach ($app_domains as $app_domain) {
                $domains[] = $app_domain->get_domain();
            }
            $valid_url = $this->valid_domain($domains, $url);
            if (!$valid_url) {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1, 'domains' => $domains));
                return $this->response;
            }
            $data = Comparisons::showHelper($slug, $order_by);
            if ($data != false) {
                if ($data['app_id'] != $app_id) {
                    $this->response->setStatusCode(201, "ERROR");
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1));
                    return $this->response;
                }
                if ($data['plugin_slider'] == 1) {
                    $next_plugin = $this->get_next($app_id, $data['created_at'], $url);
                    $previous_plugin = $this->get_previous($app_id, $data['created_at'], $url);
                    if (empty($next_plugin)) {
                        $random_plugins = $this->get_random_plugins($app_id, $url);
                        $i = 0;
                        while ($i < 20) {
                            if (count($random_plugins) == 1) {
                                break;
                            }
                            if (count($random_plugins) == 2) {
                                if ($random_plugins[0]['url'] == $url) {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[1]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($previous_plugin['url'] != $random_plugins[1]['url']) {
                                            $next_plugin['type'] = $random_plugins[1]['debates'];
                                            $next_plugin['url'] = $random_plugins[1]['url'];
                                            $next_plugin['title'] = $random_plugins[1]['title'];
                                            $next_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                } else {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[0]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($previous_plugin['url'] != $random_plugins[0]['url']) {
                                            $next_plugin['type'] = $random_plugins[0]['debates'];
                                            $next_plugin['url'] = $random_plugins[0]['url'];
                                            $next_plugin['title'] = $random_plugins[0]['title'];
                                            $next_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                }
                            }
                            $key = array_rand($random_plugins);
                            if ($previous_plugin['url'] != $random_plugins[$key]['url']) {
                                $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[$key]['url'])));
                                if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                    $next_plugin['url'] = $random_plugins[$key]['url'];
                                    $next_plugin['title'] = $random_plugins[$key]['title'];
                                    $next_plugin['type'] = $random_plugins[$key]['debates'];
                                    $next_plugin['hash'] = $interaction->getHash();
                                    break;
                                }
                            }
                            $i++;
                        }
                    }
                    if (empty($previous_plugin)) {
                        $random_plugins = $this->get_random_plugins($app_id, $url);
                        $i = 0;
                        while ($i < 20) {
                            if (count($random_plugins) == 1) {
                                break;
                            }
                            if (count($random_plugins) == 2) {
                                if ($random_plugins[0]['url'] == $url) {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[1]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($next_plugin['url'] != $random_plugins[1]['url']) {
                                            $previous_plugin['type'] = $random_plugins[1]['debates'];
                                            $previous_plugin['url'] = $random_plugins[1]['url'];
                                            $previous_plugin['title'] = $random_plugins[1]['title'];
                                            $previous_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                } else {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[0]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($next_plugin['url'] != $random_plugins[0]['url']) {
                                            $previous_plugin['type'] = $random_plugins[0]['debates'];
                                            $previous_plugin['url'] = $random_plugins[0]['url'];
                                            $previous_plugin['title'] = $random_plugins[0]['title'];
                                            $previous_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                }
                            }
                            $key = array_rand($random_plugins);
                            if ($next_plugin['url'] != $random_plugins[$key]['url']) {
                                $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[$key]['url'])));
                                if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                    $previous_plugin['url'] = $random_plugins[$key]['url'];
                                    $previous_plugin['title'] = $random_plugins[$key]['title'];
                                    $previous_plugin['type'] = $random_plugins[$key]['debates'];
                                    $previous_plugin['hash'] = $interaction->getHash();
                                    break;
                                }
                            }
                            $i++;
                        }
                    }
                    $hash['next'] = $next_plugin;
                    $hash['previous'] = $previous_plugin;
                }
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $data, 'links' => $hash));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => 'No result found'));
            }
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function deleteAction($slug) {
        try {
            $signedInUserID = Tokens::checkAccess();
            $data = Comparisons::deleteComparison($slug, $signedInUserID);
            if ($data == true) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $e) {
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    public function show_comparison_data() {
        $slug = $this->request->get('slug');
        $comparison = Comparisons::findFirst(array('slug="' . $slug . '"'));
        if ($comparison) {
            $sides = Sides::find('comparison_id=' . $comparison->getId())->toArray();
            $comparison_data = array('id' => $comparison->getId(), 'slug' => $comparison->getSlug(), 'image' => $comparison->getImage(), 'title' => $comparison->getTitle(), 'category_id' => $comparison->getCategoryId(), 'align' => $comparison->get_align(), 'hide_comment' => $comparison->get_hide_comment(), 'created_at' => $comparison->getCreatedAt(), 'updated_at' => $comparison->getUpdatedAt(), 'sides' => Sides::comparison_sides($sides),);
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $comparison_data));
        } else {
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('comparison-slug-not-exist')));
        }
        return $this->response;
    }
    function update_sides_meta($comparison_id, $sidesData, $sides_length, $no_images) {
        $sides = Sides::find(array('columns' => 'id', 'conditions' => 'comparison_id=' . $comparison_id))->toArray();
        $all_sides_ids = array();
        $edited_ids = array();
        foreach ($sides as $side_data) {
            array_push($all_sides_ids, $side_data['id']);
        }
        for ($i = 0;$i < $sides_length;$i++) {
            if ($sidesData[$i]['id'] == 0) {
                $side = new Sides();
                $side->setComparisonId($comparison_id);
                if ($side->save() == false) {
                    $messages = $side->getMessages();
                    $message = $messages[0];
                    $transaction->rollback($message->getMessage());
                }
                $comparisonSideMeta = new ComparisonSidesMeta();
                $comparisonSideMeta->setSideId($side->getId());
            } else {
                array_push($edited_ids, $sidesData[$i]['id']);
                $comparisonSideMeta = ComparisonSidesMeta::findFirst('side_id=' . $sidesData[$i]['id']);
            }
            $comparisonSideMeta->setTitle(trim($sidesData[$i]['title']));
            $comparisonSideMeta->setDescription(trim($sidesData[$i]['description']));
            if ($sidesData['image']['logo_' . $i]) {
                $comparisonSideMeta->setImage($sidesData['image']['logo_' . $i][0]['path'] . '' . $sidesData['image']['logo_' . $i][0]['new']);
            } else if ($no_images) {
                $comparisonSideMeta->setImage("");
            }
            $comparisonSideMeta->save();
        }
        $removed_sides = array_diff($all_sides_ids, $edited_ids);
        foreach ($removed_sides as $id) {
            $side_meta = ComparisonSidesMeta::findFirst('side_id=' . $id);
            if ($side_meta) {
                $side_meta->delete();
            }
            $side = Sides::findFirst($id);
            if ($side) {
                $side->delete();
            }
        }
    }
    public function update_comparison() {
        $post_data = $this->request->getPost();
        $comparison = Comparisons::findFirst(array('slug="' . $post_data['slug'] . '"'));
        if ($comparison) {
            $signedInUserID = Tokens::checkAccess();
            $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
            $new_log = new Log();
            $new_log->log_current_action($signedInUserID, $sentHeaders['Ip'], "/comparisons/update", $comparison->getId());
            $app = Apps::findFirst('publisher_id=' . $signedInUserID);
            $validate_feature = PluginsFeature::validate_feature_using_plan_id($app->get_plan(), "edit_plugin");
            if ($app && ($app->getId() == $comparison->getAppId()) && $validate_feature) {
                $comparison->setTitle(trim($post_data['title']));
                $comparison->setCategoryId($post_data['category_id']);
                $comparison->setUpdatedAt(date('y-m-d H:i:s'));
                $comparison->set_align($post_data['align']);
                $comparison->set_hide_comment($post_data['hide_comment']);
                $di = $this->getDi()->getShared('request');
                $arrPhotos = $di->getUploadedFiles();
                $sidesData = json_decode($post_data['sides'], true);
                $sidesLength = count($sidesData);
                if ($sidesLength < 2) {
                    $this->response->setStatusCode(600, 'ERROR');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("comparison-two-items")));
                    return $this->response;
                } else if ($sidesLength > 8) {
                    return $returns->response->setStatusCode(600, 'ERROR');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("comparison-eight-items")));
                    return $this->response;
                }
                $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
                if (!$post_data['no_images']) {
                    for ($i = 0;$i < 8;$i++) {
                        if ($this->request->getPost('side_' . ($i + 1) . '_logo')) {
                            $sidesData['image']['logo_' . $i] = $objImgUploader->uploadImage($objImgUploader::DIR_COMPARISONS, $this->request->getPost('side_' . ($i + 1) . '_logo'));
                        }
                    }
                }
                $comparison->save();
                $this->update_sides_meta($comparison->getId(), $sidesData, $sidesLength, $post_data['no_images']);
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $comparison->getSlug()));
            } else {
                $this->response->setStatusCode(401, 'Unauthorized');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            }
        } else {
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('comparison-slug-not-exist')));
        }
        return $this->response;
    }
    public function repliesAction($argument_id) {
        try {
            $data = Replies::getReplies($argument_id, 2, 0);
            if (is_array($data) && $data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
}
