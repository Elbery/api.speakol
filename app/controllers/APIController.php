<?php
use \Phalcon\Tag as Tag;
class APIController extends BaseController {
    public function mailviewerAction() {
        $templates = scandir($this->config->application->viewsDir . '/emails');
        unset($templates[0]);
        unset($templates[1]);
        $templates = array_values($templates);
        $this->view->langs = array('en' => 'English', 'ar' => 'العربية');
        $mailTemplate = $this->request->get('template', 'int', 0);
        $email = $this->request->get('email', 'email', false);
        $lang = $this->request->get('lang', 'string', false);
        if ($email) {
            $user = Users::findFirstByEmail($email);
        } else {
            $user = false;
        }
        if (!$user) {
            $user = Users::findFirst(array('conditions' => 'role_id = 4'));
        }
        $app = Apps::findFirst();
        $this->view->app = $app->toArray();
        $this->view->redirectUrl = '';
        Tag::setDefault("template", $mailTemplate);
        Tag::setDefault("email", $email);
        Tag::setDefault("lang", $lang);
        $this->view->templates = array_values($templates);
        $this->view->user = $user->toArray();
        $this->view->templateName = str_replace('.volt', '', $templates[$mailTemplate]);
        $this->view->title = $templates[$mailTemplate] . ' | Mail Viewer';
        if (!empty($email)) {
            $view = clone $this->view;
            $view->start();
            $view->render('emails', str_replace('.volt', '', $templates[$mailTemplate]));
            $view->finish();
            $content = $view->getContent();
            $mailData = array('html' => $content, 'text' => $this->t->_('Demo Mail'), 'subject' => $this->t->_('Demo Mail'), 'from_email' => 'users@speakol.com', 'from_name' => 'Speakol', 'to' => $email);
            $this->mail->send($mailData);
            echo 'Mail Sent successfully!';
            echo '<script>
                    window.location.href = "http://api.speakol.com/mail-viewer";
                </script>';
        }
        $view = clone $this->view;
        $view->start();
        $view->render('api', 'email-viewer');
        $view->finish();
        $content = $view->getContent();
        echo $content;
        die;
    }
}
