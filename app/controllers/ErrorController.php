<?php
class ErrorController {
    private $_message = array();
    const OKEY = 'okey';
    const CREATED = 'created';
    const ACCEPTED = 'accepted';
    const RESET_CONTENT = 'reset_content';
    const UNAUTHORIZED = 'unauthorized';
    const FORBIDDEN = 'forbidden';
    const NOT_FOUND = 'not_found';
    const EMPTY_RESPONSE = 'empty';
    const FOUND = 'found';
    const METHOD_NOT_ALLOWED = 'method_not_allowed';
    const NOT_ACCEPTABLE = 'not_acceptable';
    const CONFLICT = 'conflict';
    const ERROR = 'error';
    private $_codes = array(static ::OKEY => 200, static ::CREATED => 201, static ::ACCEPTED => 202, static ::RESET_CONTENT => 205, static ::UNAUTHORIZED => 401, static ::FORBIDDEN => 403, static ::NOT_FOUND => 404, static ::METHOD_NOT_ALLOWED => 405, static ::NOT_ACCEPTABLE => 406, static ::CONFLICT => 409, static ::ERROR => 500,);
    private $_status = array(static ::OKEY => 'Success', static ::CREATED => 'Created', static ::ACCEPTED => 'Accepted', static ::RESET_CONTENT => 'Reset Content', static ::UNAUTHORIZED => 'Unauthorized', static ::FORBIDDEN => 'Forbidden', static ::NOT_FOUND => 'Not Found', static ::FOUND => 'Found', static ::METHOD_NOT_ALLOWED => 'Method Not Allowed', static ::NOT_ACCEPTABLE => 'Not Acceptable', static ::CONFLICT => 'Conflict', static ::ERROR => 'Error',);
    public function setMessage($key, $message) {
        $this->_message[$key] = $message;
    }
    public function getMessage() {
        return $this->_message;
    }
    public function getCodes($code) {
        return $this->_codes[$code];
    }
    public function getStatus($status) {
        return $this->_status[$status];
    }
}
