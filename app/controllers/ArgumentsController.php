<?php
use Phalcon\Filter;
class ArgumentsController extends BaseController {
    public function createByArgumentBoxIDAction() {
        try {
            $objData = new stdClass();
            $objData->argumentsBoxID = $this->request->get('argument_id');
            $objData->context = trim($this->request->get('context'));
            $objData->link = $this->request->get('link');
            $objData->moduleId = $this->request->get('argument_id');
            $objData->type = BaseModel::ARGUMENTSBOX;
            $objData->files = $this->request->getUploadedFiles();
            $objData->audio = $this->request->get("audio");
            $filter = new Filter();
            $this->response->setHeader("Content-Type", "application/json");
            $objData->signedInUserID = Tokens::getSignedInUserId();
            $argumentbox = Argumentboxes::doesExist($objData->argumentsBoxID);
            if ($argumentbox->get_high_interaction() < 50) {
                if ($argumentbox->get_high_interaction() === 49) {
                    $app = Apps::findFirst($argumentbox->getAppId());
                    $user = Users::findFirst($app->getPublisherId());
                    $this->send_high_interaction_email($user->getEmail(), $user->get_first_name() . " " . $user->get_last_name(), $argumentbox->getTitle(), $user->getLocale(), "argument");
                }
                $argumentbox->set_high_interaction($argumentbox->get_high_interaction() + 1);
                $argumentbox->save();
            }
            $voted_side = Argumentboxes::getFavoredSideOfArgumentsBox($objData);
            $objData->sideID = $objData->sideID = ($this->request->get("side_id")) ? $this->request->get("side_id") : $voted_side;
            $this->getDI()->set('language', $this->request->get("lang"));
            $intArgumentID = Arguments::appendToModel($objData);
            $new_log = new Log();
            if ($intArgumentID['status'] == "ERROR") {
                $new_log->log_current_action($objData->signedInUserID, $this->headers['Ip'], "/arguments/insert image with wrong extension", $intArgumentID['id']);
                $this->response->setStatusCode(400, 'ERROR');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $intArgumentID['message']));
                return $this->response;
            }
            $new_log->log_current_action($objData->signedInUserID, $this->headers['Ip'], "/arguments/insert", $intArgumentID['id']);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => array('argument_id' => $intArgumentID['id'], 'side_id' => $objData->sideID)));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function showAction($side, $page = 1, $limit = 2) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $modelArguments = new Arguments();
            $order_by = $this->request->get('order_by');
            if (empty($order_by)) {
                $order_by = "most_voted";
            }
            $arrArguments = $modelArguments->showActionHelper($side, $order_by, $page, $limit);
            if ($arrArguments == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrArguments));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
    public function thumbUpAction($argument_id) {
        $link = $this->request->getPost("link");
        return $this->voteAction($argument_id, $link, TRUE);
    }
    public function thumbDownAction($argument_id) {
        $link = $this->request->getPost("link");
        return $this->voteAction($argument_id, $link, FALSE);
    }
    private function voteAction($argument_id, $link, $vote) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::getSignedInUserId();
            $modelArguments = new Arguments();
            if (Arguments::findFirst($argument_id) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if ($modelArguments->hasVoted($argument_id, $signedInUserID, $vote)) {
                throw new Exception($this->t->_('found'), 302);
            }
            $thummbUp = $modelArguments->voteActionHelper($argument_id, $signedInUserID, $vote, $link);
            if (FALSE == $thummbUp) {
                throw new Exception($this->t->_($this->t->_('conflict')), 409);
            } else {
                $new_log = new Log();
                $new_log->log_current_action($signedInUserID, $this->headers['Ip'], "/arguments/" . $argument_id . "/" . $action, $thumb_id);
                $argument = Arguments::findFirstById($argument_id);
                if ($vote == True) {
                    $action = "up";
                } else {
                    $action = "down";
                }
                if ($argument != false) {
                    $argument = $argument->toArray();
                    $current_user = Tokens::checkAccess();
                    if ($argument['owner_id'] != $current_user) {
                        $app = AppAdmins::findFirst(array('user_id = :user_id: AND super_admin=1', 'bind' => array('user_id' => $current_user)));
                        if ($app) {
                            $admin_at = $app->getAppId();
                        } else {
                            $admin_at = 0;
                        }
                        $notification_controller = new NotificationsController();
                        $notification_controller->thumbMyArgument(array('app_id' => $argument['app_id'], 'referrer' => $this->request->getPost('referrer'), 'argument_owner' => $argument['owner_id'], 'module_type' => $argument['type'], 'module_id' => $argument['module_id'], 'action' => $action, 'admin_at' => $admin_at, 'link' => $link,));
                    }
                }
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => 'OK'));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function showByArgIDAction($argument_id) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $arrArgument = Arguments::showSingleByArgID($argument_id);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrArgument));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function createByDebateIDAction() {
        try {
            $objData = new stdClass();
            $objData->debateID = $this->request->get('debate_id');
            $objData->context = trim($this->request->get('context'));
            $objData->moduleId = $this->request->get('debate_id');
            $objData->type = BaseModel::DEBATE;
            $objData->files = $this->request->getUploadedFiles();
            $objData->link = $this->request->get('link');
            $objData->audio = $this->request->get("audio");
            $this->response->setHeader("Content-Type", "application/json");
            $objData->signedInUserID = Tokens::getSignedInUserId();
            $debate = Debates::doesExist($objData->debateID);
            if ($debate->get_high_interaction() < 50) {
                if ($debate->get_high_interaction() === 49) {
                    $app = Apps::findFirst($debate->getAppId());
                    $user = Users::findFirst($app->getPublisherId());
                    $this->send_high_interaction_email($user->getEmail(), $user->get_first_name() . " " . $user->get_last_name(), $debate->getTitle(), $user->getLocale(), "debate");
                }
                $debate->set_high_interaction($debate->get_high_interaction() + 1);
                $debate->save();
            }
            $voted_side = Debates::getFavoredSideOfDebate($objData);
            $objData->sideID = ($this->request->get("side_id")) ? $this->request->get("side_id") : $voted_side;
            $this->getDI()->set('language', $this->request->get("lang"));
            $argument = Arguments::appendToModel($objData);
            $new_log = new Log();
            if ($argument['status'] == "ERROR") {
                $new_log->log_current_action($objData->signedInUserID, $this->headers['Ip'], "/arguments/debates/insert image with wrong extension", $argument['id']);
                $this->response->setStatusCode(400, 'ERROR');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $argument['message']));
                return $this->response;
            }
            $new_log->log_current_action($objData->signedInUserID, $this->headers['Ip'], "/arguments/debates/insert", $argument['id']);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => array('argument_id' => $argument['id'], 'side_id' => $objData->sideID,)));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function makeAction() {
        try {
            $objData = new stdClass();
            $objData->context = trim($this->request->get('context'));
            $objData->link = $this->request->get('link');
            $objData->container_type = $this->request->get('type');
            $objData->container_id = $this->request->get('id');
            $objData->files = $this->request->getUploadedFiles();
            $objData->audio = $this->request->get("audio");
            $this->response->setHeader("Content-Type", "application/json");
            $objData->signedInUserID = Tokens::getSignedInUserId();
            $factory = new \Speakol\concreteSocialPluginFactory();
            $plugin = $factory->make($objData->container_type);
            $plugin_obj = $plugin->doesExist($objData->container_id);
            if ($plugin_obj->get_high_interaction() < 50) {
                if ($plugin_obj->get_high_interaction() === 49) {
                    $app = Apps::findFirst($plugin_obj->getAppId());
                    $user = Users::findFirst($app->getPublisherId());
                    $this->send_high_interaction_email($user->getEmail(), $user->get_first_name() . " " . $user->get_last_name(), $plugin_obj->getTitle(), $user->getLocale(), "comparison");
                }
                $plugin_obj->set_high_interaction($plugin_obj->get_high_interaction() + 1);
                $plugin_obj->save();
            }
            $voted_side = $plugin->getFavoredSideOfComp($objData);
            $objData->sideID = $objData->sideID = ($this->request->get("side_id")) ? $this->request->get("side_id") : $voted_side;
            $this->getDI()->set('language', $this->request->get("lang"));
            $intArgumentID = Arguments::appendToModel($objData);
            $new_log = new Log();
            if ($intArgumentID['status'] == "ERROR") {
                $new_log->log_current_action($objData->signedInUserID, $this->headers['Ip'], "/arguments/factory/make image with wrong extension", $intArgumentID['id']);
                $this->response->setStatusCode(400, 'ERROR');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $intArgumentID['message']));
                return $this->response;
            }
            $new_log->log_current_action($objData->signedInUserID, $this->headers['Ip'], "/arguments/factory/make", $intArgumentID['id']);
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => array('argument_id' => $intArgumentID, 'side_id' => $objData->sideID)));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
    public function deleteAction($argument_id) {
        try {
            $arguments = new Arguments();
            $data = $arguments->deleteArgument($argument_id);
            $new_log = new Log();
            $new_log->log_current_action(Tokens::checkAccess(), $this->headers['Ip'], "/arguments/" . $argument_id . "/delete", $argument_id);
            if ($data) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'SUCCESS', 'data' => true));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => false));
            }
        }
        catch(Exception $e) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $e->getMessage()));
        }
        return $this->response;
    }
    function send_high_interaction_email($user_email, $name, $title, $lang, $type) {
        $this->view->name = $name;
        $this->view->title = $title;
        $this->getDI()->set('language', $lang);
        if ($type == "argument") {
            $this->view->type = $this->t->_('argument-plugin');
        } elseif ($type == "debate") {
            $this->view->type = $this->t->_('debate-plugin');
        } else {
            $this->view->type = $this->t->_('comparison-plugin');
        }
        $this->view->lang = $lang;
        $this->view->update = "apps/settings";
        $this->assets->addCss('css/email.css');
        $this->view->start();
        $this->view->render('emails', 'high-interaction');
        $this->view->finish();
        $content = $this->view->getContent();
        $message = array('html' => $content, 'subject' => $this->t->_('high-interaction-title'), 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to' => array(array('email' => $user_email, 'type' => 'to')),);
        $async = false;
        $ip_pool = 'Main Pool';
        $result = $this->mandrill->messages->send($message, $async, $ip_pool);
    }
}
