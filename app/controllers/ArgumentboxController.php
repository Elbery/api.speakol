<?php
class ArgumentboxController extends BaseController {
    public function indexAction() {
        try {
            $app_id = $this->request->getQuery('app_id');
            $argumentboxModels = new Argumentboxes();
            $return = $argumentboxModels->listArgumentboxes($app_id);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function getCommunityAction() {
        try {
            $recomendedModel = new ArgumentboxRecomended();
            $data = array('id' => $this->request->get('app_id'));
            $return = $recomendedModel->getCommunity($data['id'], $this->request->get('url'));
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function showAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $argumentboxModels = new Argumentboxes();
            $code = $this->request->get('code');
            if (!$code) {
                $code = "";
            }
            $url = $this->request->get('url');
            if (empty($url)) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-arguments'))));
                return $this->response;
            }
            $url = str_replace("http://", "", $url);
            $url = str_replace("https://", "", $url);
            if ($this->request->get("order_by") == "") {
                $order_by = "most_voted";
            } else {
                $order_by = $this->request->get("order_by");
            }
            $app_id = $this->request->get("app_id");
            if (!is_numeric($app_id)) {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1));
                return $this->response;
            }
            $free_plan = 1;
            $domains = array($this->getDI()->getShared('config')->get("application")->get("default-url"));
            $domains[] = $this->getDI()->getShared('config')->get("application")->get("plugins_speakol");
            $app_domains = AppDomains::find('app_id=' . $app_id);
            foreach ($app_domains as $app_domain) {
                $domains[] = $app_domain->get_domain();
            }
            $valid_url = $this->valid_domain($domains, $url);
            if (!$valid_url) {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1, 'domains' => $domains));
                return $this->response;
            }
            $return = $argumentboxModels->retriveArgumentbox($url, $code, Tokens::getSignedInUserId(), $domains, $order_by);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $app_plan = Apps::findFirst($app_id);
                if ($app_plan) {
                    if ($app_plan->get_plan() != 1) {
                        $free_plan = 0;
                    }
                }
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages']), 'free_plan' => $free_plan, 'domains' => $domains));
            } else {
                if (sizeof($return['data']) != 0 && $return['data'][0]['app_id'] != $app_id) {
                    $this->response->setStatusCode(201, "ERROR");
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1));
                    return $this->response;
                }
                if ($return['data'][0]['show_title'] === 1) {
                    $free_plan = 0;
                } else {
                    $app_plan = Apps::findFirst($app_id);
                    if ($app_plan) {
                        if ($app_plan->get_plan() != 1) {
                            $free_plan = 0;
                        }
                    }
                }
                $url = $this->request->get('url');
                if (empty($return['data'][0])) {
                    $random_plugins = $this->get_random_plugins($app_id, $url);
                    $i = 0;
                    while ($i < 20) {
                        $i++;
                        if (count($random_plugins) == 1) {
                            break;
                        }
                        if (count($random_plugins) == 2) {
                            if ($random_plugins[0]['url'] == $url) {
                                $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[1]['url'])));
                                if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                    if ($next_plugin['url'] != $random_plugins[1]['url']) {
                                        $previous_plugin['type'] = $random_plugins[1]['debates'];
                                        $previous_plugin['url'] = $random_plugins[1]['url'];
                                        $previous_plugin['title'] = $random_plugins[1]['title'];
                                        $previous_plugin['hash'] = $interaction->getHash();
                                    }
                                }
                                break;
                            } else {
                                $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[0]['url'])));
                                if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                    if ($next_plugin['url'] != $random_plugins[0]['url']) {
                                        $previous_plugin['type'] = $random_plugins[0]['debates'];
                                        $previous_plugin['url'] = $random_plugins[0]['url'];
                                        $previous_plugin['title'] = $random_plugins[0]['title'];
                                        $previous_plugin['hash'] = $interaction->getHash();
                                    }
                                }
                                break;
                            }
                        }
                        $key_1 = array_rand($random_plugins);
                        $key_2 = array_rand($random_plugins);
                        if ($key_1 == $key_2) {
                            continue;
                        }
                        $interaction_1 = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[$key_1]['url'])));
                        $interaction_2 = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[$key_2]['url'])));
                        if ($interaction_1 && ($interaction_1->getHash() != '' || $interaction_1->getHash() != null)) {
                            $next_plugin['type'] = $random_plugins[$key_1]['debates'];
                            $next_plugin['url'] = $random_plugins[$key_1]['url'];
                            $next_plugin['title'] = $random_plugins[$key_1]['title'];
                            $next_plugin['hash'] = $interaction_1->getHash();
                        } else {
                            continue;
                        }
                        if ($interaction_2 && ($interaction_2->getHash() != '' || $interaction_2->getHash() != null)) {
                            $previous_plugin['type'] = $random_plugins[$key_2]['debates'];
                            $previous_plugin['url'] = $random_plugins[$key_2]['url'];
                            $previous_plugin['title'] = $random_plugins[$key_2]['title'];
                            $previous_plugin['hash'] = $interaction_2->getHash();
                        } else {
                            continue;
                        }
                        $hash['next'] = $next_plugin;
                        $hash['previous'] = $previous_plugin;
                        break;
                    }
                } elseif ($return['data'][0]['plugin_slider'] == 1) {
                    $next_plugin = $this->get_next($app_id, $return['data'][0]['created_at'], $url);
                    $previous_plugin = $this->get_previous($app_id, $return['data'][0]['created_at'], $url);
                    if (empty($next_plugin)) {
                        $random_plugins = $this->get_random_plugins($app_id, $url);
                        $i = 0;
                        while ($i < 20) {
                            if (count($random_plugins) == 1) {
                                break;
                            }
                            if (count($random_plugins) == 2) {
                                if ($random_plugins[0]['url'] == $url) {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[1]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($previous_plugin['url'] != $random_plugins[1]['url']) {
                                            $next_plugin['type'] = $random_plugins[1]['debates'];
                                            $next_plugin['url'] = $random_plugins[1]['url'];
                                            $next_plugin['title'] = $random_plugins[1]['title'];
                                            $next_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                } else {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[0]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($previous_plugin['url'] != $random_plugins[0]['url']) {
                                            $next_plugin['type'] = $random_plugins[0]['debates'];
                                            $next_plugin['url'] = $random_plugins[0]['url'];
                                            $next_plugin['title'] = $random_plugins[0]['title'];
                                            $next_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                }
                            }
                            $key = array_rand($random_plugins);
                            if ($previous_plugin['url'] != $random_plugins[$key]['url']) {
                                $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[$key]['url'])));
                                if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                    $next_plugin['url'] = $random_plugins[$key]['url'];
                                    $next_plugin['title'] = $random_plugins[$key]['title'];
                                    $next_plugin['type'] = $random_plugins[$key]['debates'];
                                    $next_plugin['hash'] = $interaction->getHash();
                                    break;
                                }
                            }
                            $i++;
                        }
                    }
                    if (empty($previous_plugin)) {
                        $random_plugins = $this->get_random_plugins($app_id, $url);
                        $i = 0;
                        while ($i < 20) {
                            if (count($random_plugins) == 1) {
                                break;
                            }
                            if (count($random_plugins) == 2) {
                                if ($random_plugins[0]['url'] == $url) {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[1]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($next_plugin['url'] != $random_plugins[1]['url']) {
                                            $previous_plugin['type'] = $random_plugins[1]['debates'];
                                            $previous_plugin['url'] = $random_plugins[1]['url'];
                                            $previous_plugin['title'] = $random_plugins[1]['title'];
                                            $previous_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                } else {
                                    $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[0]['url'])));
                                    if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                        if ($previous_plugin['url'] != $random_plugins[0]['url']) {
                                            $previous_plugin['type'] = $random_plugins[0]['debates'];
                                            $previous_plugin['url'] = $random_plugins[0]['url'];
                                            $previous_plugin['title'] = $random_plugins[0]['title'];
                                            $previous_plugin['hash'] = $interaction->getHash();
                                        }
                                    }
                                    break;
                                }
                            }
                            $key = array_rand($random_plugins);
                            if ($next_plugin['url'] != $random_plugins[$key]['url']) {
                                $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $random_plugins[$key]['url'])));
                                if ($interaction && ($interaction->getHash() != '' || $interaction->getHash() != null)) {
                                    $previous_plugin['url'] = $random_plugins[$key]['url'];
                                    $previous_plugin['title'] = $random_plugins[$key]['title'];
                                    $previous_plugin['type'] = $random_plugins[$key]['debates'];
                                    $previous_plugin['hash'] = $interaction->getHash();
                                    break;
                                }
                            }
                            $i++;
                        }
                    }
                    $hash['next'] = $next_plugin;
                    $hash['previous'] = $previous_plugin;
                }
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data'], 'links' => $hash, 'free_plan' => $free_plan, 'domains' => $domains));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage(), 'free_plan' => $free_plan, 'domains' => $domains));
        }
        return $this->response;
    }
    public function createAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $code = $this->request->get('code') ? $this->request->get('code') : "";
            $argumentboxModels = new Argumentboxes();
            $data = array('app_id' => $this->request->get('app_id'), 'url' => trim($this->request->get('url')), 'description' => $this->request->get('description'), 'title' => trim($this->request->get('title')), 'image' => $this->request->get('image'), 'lang' => $this->request->get('lang'), 'option' => $this->request->get('option'), 'code' => $code);
            if (intval($data['option']) > 1) {
                $app = Apps::findFirst($data['app_id']);
                if (!$app) {
                    $this->response->setStatusCode(401, 'Unauthorized');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "Unauthorized"));
                    return $this->response;
                }
                $available_feature = PluginsFeature::validate_feature_using_plan_id($app->get_plan(), "argument_option");
                if (!$available_feature) {
                    $data['option'] = 1;
                }
            }
            if ($data['title'] != "") {
                $app = Apps::findFirst($data['app_id']);
                if (!$app) {
                    $this->response->setStatusCode(401, 'Unauthorized');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "Unauthorized"));
                    return $this->response;
                }
                $available_feature = PluginsFeature::validate_feature_using_plan_id($app->get_plan(), "argumentbox_title");
                if (!$available_feature) {
                    $data['title'] = "";
                }
            }
            if ($data['code'] != "") {
                $app = Apps::findFirst($data['app_id']);
                if (!$app) {
                    $this->response->setStatusCode(401, 'Unauthorized');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "Unauthorized"));
                    return $this->response;
                }
                $available_feature = PluginsFeature::validate_feature_using_plan_id($app->get_plan(), "argumentbox_multiple_use");
                if (!$available_feature) {
                    $this->response->setStatusCode(610, 'ERROR');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => "You don't have access to Premium features"));
                    return $this->response;
                }
            }
            if (empty($data['app_id']) || empty($data['url'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-data'))));
                return $this->response;
            }
            $domains = array();
            $app_domains = AppDomains::find('app_id=' . $data['app_id']);
            foreach ($app_domains as $app_domain) {
                $domains[] = $app_domain->get_domain();
            }
            $valid_url = $this->valid_domain($domains, $data['url']);
            if (!$valid_url) {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'ERROR', 'unauthorized_domain' => 1, 'free_plan' => $free_plan, 'domains' => $domains));
                return $this->response;
            }
            $argumentBoxID = Argumentboxes::getArgumentsBoxByURL($data['url'], $data['code']);
            if ($argumentBoxID != FALSE) {
                $this->response->setStatusCode(302, "FOUND");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $argumentBoxID));
            } else {
                $return = $argumentboxModels->appendDataToModel($data);
                if (isset($return['status']) && $return['status'] == 'ERROR') {
                    $this->response->setStatusCode(201, "ERROR");
                    $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
                } else {
                    $data['argumentbox_id'] = $return['data']['0'];
                    $recomended = new ArgumentboxRecomended();
                    $recomended = $recomended->appCreate($data);
                    if (isset($recomended['status']) && $recomended['status'] == 'ERROR') {
                        $this->response->setStatusCode(201, "ERROR");
                        $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($recomended['messages'])));
                        return $this->response;
                    }
                    $this->response->setStatusCode(200, "OK");
                    $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
                }
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function delete_argumentbox() {
        $this->response->setHeader("Content-Type", "application/json");
        $post_data = json_decode(file_get_contents("php://input"));
        $post_data->url = filter_var($post_data->url, FILTER_SANITIZE_URL);
        if (trim($post_data->url) == "") {
            $this->response->setStatusCode(601, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid url'));
        } else {
            $argumentboxModels = new Argumentboxes();
            $userId = Tokens::checkAccess();
            $app = AppAdmins::findFirst(array('user_id = :user_id:', 'bind' => array('user_id' => $userId)));
            if ($app) {
                if (!$post_data->code) {
                    $post_data->code = "";
                }
                $data = array('url' => $post_data->url, 'code' => $post_data->code, 'app_id' => $app->getAppId());
                $result = $argumentboxModels->delete_argumentbox_data($data);
                if (isset($result['status']) && $result['status'] == 'ERROR') {
                    $this->response->setStatusCode(404, "Not Found");
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $result['messages']));
                } else {
                    $this->response->setStatusCode(200, "OK");
                    $this->response->setJsonContent(array('status' => 'OK', 'message' => $result['data']));
                }
            } else {
                $this->response->setStatusCode(401, "Unauthorized");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            }
        }
        return $this->response;
    }
    public function deleteAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $argumentboxModels = new Argumentboxes();
            $userId = Tokens::checkAccess();
            $app = AppAdmins::findFirst(array('user_id = :user_id: AND super_admin = 1', 'bind' => array('user_id' => $userId, '')));
            $data = array('slug' => $slug, 'app_id' => $app->getAppId());
            $return = $argumentboxModels->appendDataToModelDelete($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function getRandomRecomendAction() {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $recomendedModel = new ArgumentboxRecomended();
            $data = array('id' => $this->request->get('app_id'));
            $return = $recomendedModel->getRandomRecomendation($data['id']);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function show_argumentbox_data() {
        $url = $this->request->get('url');
        $code = $this->request->get('code') ? $this->request->get('code') : "";
        $argumentbox = Argumentboxes::findFirst(array("CONCAT(domain,uri)='$url' AND code='$code'"));
        if ($argumentbox) {
            $signedInUserID = Tokens::getSignedInUserId();
            $app = AppAdmins::findFirst('user_id=' . $signedInUserID);
            if ($app->getAppId() != $argumentbox->getAppId()) {
                $this->response->setStatusCode(401, 'Unauthorized');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
                return $this->response;
            }
            $sides = Sides::find('argumentbox_id=' . $argumentbox->getId())->toArray();
            $argumentbox_data = array('id' => $argumentbox->getId(), 'url' => $argumentbox->get_domain() . $argumentbox->get_uri(), 'code' => $argumentbox->get_code(), 'title' => $argumentbox->getTitle(), 'created_at' => $argumentbox->getCreatedAt(), 'updated_at' => $argumentbox->getUpdatedAt(), 'option' => $argumentbox->get_option_id());
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $argumentbox_data));
        } else {
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('argbox-slug-not-exist')));
        }
        return $this->response;
    }
    public function update_argumentbox() {
        $post_data = json_decode(file_get_contents("php://input"));
        if (!$post_data->code) {
            $post_data->code = "";
        }
        $argumentbox = Argumentboxes::findFirst(array('CONCAT(domain,uri)="' . $post_data->url . '" AND code="' . $post_data->code . '"'));
        if ($argumentbox) {
            $signedInUserID = Tokens::getSignedInUserId();
            $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
            $app = Apps::findFirst('publisher_id=' . $signedInUserID);
            $validate_feature = PluginsFeature::validate_feature_using_plan_id($app->get_plan(), "edit_plugin");
            $post_data->title = trim($post_data->title);
            if ($app && ($app->getId() == $argumentbox->getAppId()) && $validate_feature) {
                if ($post_data->title != "" || $post_data->title != NULL) {
                    $title = $post_data->title;
                } else {
                    $title = NULL;
                }
                $argumentbox->set_title($title);
                $argumentbox->set_show_title(1);
                $recomended = ArgumentboxRecomended::findFirst('argumentbox_id=' . $argumentbox->getId());
                if (!$recomended) {
                    $recomended = new ArgumentboxRecomended();
                    $recomended->setArgumentboxId($argumentbox->getId());
                }
                $recomended->setTitle($title);
                $recomended->save();
                $argumentbox->set_option_id($post_data->option);
                $argumentbox->setUpdatedAt(date('y-m-d H:i:s'));
                $argumentbox->save();
                $new_log = new Log();
                $new_log->log_current_action($signedInUserID, $sentHeaders['Ip'], "/argument-box/update", $argumentbox->getId());
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $argumentbox->getId()));
            } else {
                $this->response->setStatusCode(401, 'Unauthorized');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            }
        } else {
            $this->response->setStatusCode(404, 'Not found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Not found'));
        }
        return $this->response;
    }
    public function show_argumentbox_options() {
        $argumentbox_options = ArgumentboxOptions::find()->toArray();
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => $argumentbox_options));
        return $this->response;
    }
    public function updateAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $argumentboxModels = new Argumentboxes();
            $data = array('slug' => $slug, 'title' => $this->request->get('title'), 'content' => $this->request->get('content'));
            if (empty($data['slug']) || empty($data['title']) || empty($data['content'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-arguments'))));
                return $this->response;
            }
            $return = $argumentboxModels->appendDataToModel($data, true);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function followAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $argumentboxModels = new OpinionFollows();
            $data = array('slug' => $slug, 'follower_id' => $this->request->get('follower_id'));
            if (empty($data['slug']) || empty($data['follower_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-arguments'))));
                return $this->response;
            }
            $return = $argumentboxModels->appendDatatoFollowModel($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function unfollowAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $argumentboxModels = new OpinionFollows();
            $data = array('slug' => $slug, 'follower_id' => $this->request->get('follower_id'));
            if (empty($data['slug']) || empty($data['follower_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-arguments'))));
                return $this->response;
            }
            $return = $argumentboxModels->appendDatatoFollowModel($data, true);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function followersAction($slug) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $argumentboxModels = new OpinionFollows();
            $data = array('slug' => $slug);
            if (empty($data['slug'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-arguments'))));
                return $this->response;
            }
            $return = $argumentboxModels->getFollowers($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function voteAction($sideId) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $data = array('side_id' => intval($sideId) ? $sideId : false, 'voter_id' => $this->request->get('voter_id'));
            if (empty($data['voter_id']) || empty($data['side_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-arguments'))));
                return $this->response;
            }
            $mutesModels = new Votes();
            $return = $mutesModels->appendDatatoVotesModel($data);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function unvoteAction($sideId) {
        $this->response->setHeader("Content-Type", "application/json");
        try {
            $data = array('side_id' => $sideId, 'voter_id' => $this->request->get('voter_id'));
            if (empty($data['voter_id']) || empty($data['side_id'])) {
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($this->t->_('missing-arguments'))));
                return $this->response;
            }
            $mutesModels = new Votes();
            $return = $mutesModels->appendDatatoVotesModel($data, true);
            if (isset($return['status']) && $return['status'] == 'ERROR') {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array($return['messages'])));
            } else {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $return['data']));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function votedevoteAction($argumentbox_id, $side_id) {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $signedInUserID = Tokens::getSignedInUserId();
            if (FALSE == $signedInUserID) {
                throw new Exception($this->t->_('unauthorized'), 401);
            }
            if (Argumentboxes::findFirst($argumentbox_id) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if (Sides::findFirst($side_id) == FALSE) {
                throw new Exception($this->t->_('not-found'), 404);
            }
            if (Votes::hasVoted($side_id, $signedInUserID)) {
                throw new Exception($this->t->_('found'), 302);
            }
            $link = $this->request->get("link");
            $data = Argumentboxes::voteDevoteActionHelper($argumentbox_id, $side_id, $signedInUserID, $link);
            if (FALSE == $data) {
                throw new Exception($this->t->_('conflict'), 409);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $data));
        }
        catch(Exception $ex) {
            $this->Utility->handleException($this->response, $ex);
        }
        return $this->response;
    }
}
