<?php
class IndexController extends BaseController {
    public function indexAction() {
    }
    public function homeFeedsAction() {
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $feeds = new Feeds();
            $page = $this->request->get('page', 'int', 1);
            $perPage = $this->request->get('per_page', 'int', 10);
            $module = $this->request->get('module', 'string', '');
            $arrTag = $feeds->getHomeActivity($page, $perPage, 'created_at DESC', $module);
            if ($arrTag == FALSE) {
                throw new Exception($this->t->_('bad-request'), 400);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 'OK', 'data' => $arrTag));
        }
        catch(Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        }
        return $this->response;
    }
}
