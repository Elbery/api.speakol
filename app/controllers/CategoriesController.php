<?php
class CategoriesController extends BaseController {
    public function indexAction() {
        try {
            $data = Categories::find(array('columns' => array('id', 'name')))->toArray();
            if ($data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'messages' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
    public function viewAction($cat) {
        try {
            $cats = new Categories();
            $page = $this->request->get('page', 'int', 1);
            $per_page = $this->request->get('per_page', 'int', 1);
            $data = $cats->viewCats($cat, $page, $per_page);
            if ($data != false) {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $data));
            } else {
                $this->response->setStatusCode(201, "ERROR");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $data));
            }
        }
        catch(Exception $ex) {
            $this->response->setStatusCode(201, "ERROR");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        return $this->response;
    }
}
