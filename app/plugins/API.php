<?php
use Phalcon\Mvc\Micro\Collection as MicroCollection;
class API {
    private $_instanceName;
    protected $_application;
    private $_config;
    protected function __construct() {
        $this->_config = array();
    }
    public static function instance($name = NULL, $force = FALSE) {
        static $instances = array(), $instanceIndex = - 1;
        $name = (is_null($name) ? ($instanceIndex < 0 || $force ? ++$instanceIndex : $instanceIndex) : $name);
        if (!array_key_exists($name, $instances) || $force) {
            $instance = new static;
            $instances[$name] = $instance->setInstanceName($name);
        }
        return $instances[$name];
    }
    public function setInstanceName($name) {
        $this->_instanceName = $name;
        return $this;
    }
    public function getInstanceName() {
        return $this->_instanceName;
    }
    public function setConfig($key, $config) {
        $this->_config[$key] = $config;
        return $this;
    }
    public function getConfig($key = NULL) {
        if (!is_null($key)) {
            return $this->_config[$key];
        }
        return $this->_config;
    }
    public function setApp($oApplication) {
        $this->_application = $oApplication;
        return $this;
    }
    public function getApp() {
        return $this->_application;
    }
    public function run($oApplication = NULL, $di = null) {
        if (!is_null($oApplication)) {
            $this->setApp($oApplication)->getApp()->notFound(function () use ($oApplication) {
                $oApplication->response->setStatusCode(404, 'Not Found')->sendHeaders();
                echo 'This is crazy, but this page was not found!';
            });
            $app = $this->getApp();
            $this->setApp($oApplication)->getApp()->before(function () use ($oApplication, $di, $app) {
                $oApplication->response->setStatusCode(404, 'Not Found')->sendHeaders();
                $acl = new Speakol\ACL();
                return $acl->call($app, $di);
            });
            $this->setApp($oApplication)->getApp()->finish(function () use ($oApplication, $di) {
                if (defined('PHALCONDEBUG') && PHALCONDEBUG === true) {
                    $return = json_decode($this->setApp($oApplication)->getApp()->getReturnedValue()->getContent());
                    if ($return) {
                        $return->debug = $di->get('debug')->getAsArray();
                        $this->setApp($oApplication)->getApp()->getReturnedValue()->setContent(json_encode($return));
                    }
                }
            });
            if (!empty($this->_config)) {
                if (!empty($this->_config['routesConfiguration'])) {
                    foreach ($this->_config['routesConfiguration'] as $handler => $routes) {
                        $apiMicroCollection = new MicroCollection();
                        $apiMicroCollection->setLazy(TRUE)->setHandler($handler, TRUE)->setPrefix($this->_config['mainConfiguration']->application->baseUri);
                        foreach ($routes as $path => $strAction) {
                            $apiMicroCollection->{$strAction->method}($path, $strAction->action);
                        }
                        $this->getApp()->mount($apiMicroCollection);
                    }
                }
                ob_end_clean();
                $re = $this->getApp();
                return $re;
            }
        }
    }
    protected function getEventsManager() {
        $eventManager = new Phalcon\Events\Manager();
        $eventManager->attach('micro', function ($event, $app) {
            if ($event->getType() == 'beforeExecuteRoute') {
                exit(0);
            }
        });
        return $eventManager;
    }
    public function handleOptions($oApplication) {
        return true;
    }
}
