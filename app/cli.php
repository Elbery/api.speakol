<?php
use Phalcon\DI\FactoryDefault\CLI as CliDI, Phalcon\CLI\Console as ConsoleApp, Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;
define('VERSION', '1.0.0');
$di2 = new CliDI();
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__)));
$loader = new \Phalcon\Loader();
$loader->registerDirs(array(APPLICATION_PATH . '/tasks', APPLICATION_PATH . '/models', APPLICATION_PATH . '/controllers', APPLICATION_PATH . '/libs', APPLICATION_PATH . '/plugins', APPLICATION_PATH . '/views', APPLICATION_PATH . '/libs/mailer', APPLICATION_PATH . '/libs/stripe/stripe-php', APPLICATION_PATH . '/libs/tcpdf/', APPLICATION_PATH . '/libs/Mandrill',))->registerClasses(array("Stripe" => APPLICATION_PATH . '/libs/stripe/stripe-php/init.php', "Tcpdf" => APPLICATION_PATH . '/libs/tcpdf/tcpdf.php', "Mandrill" => APPLICATION_PATH . '/libs/Mandrill/Mandrill.php',));
$loader->register();
if (is_readable(APPLICATION_PATH . '/config/config.php')) {
    $config = include APPLICATION_PATH . '/config/config.php';
    $di2->set('config', $config);
    $di2->setShared('db', function () use ($config) {
        return new MysqlAdapter(array('host' => '127.0.0.1:17462', 'username' => '2jhd82bs1', 'password' => 'FUmz_#kf(#F_', 'dbname' => '3jfhwsx_api'));
    });
}
$console = new ConsoleApp();
$console->setDI($di2);
$di2['view'] = function () use ($config) {
    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    $view->registerEngines(array(".volt" => 'voltService'));
    return $view;
};
$di2->set('voltService', function ($view, $di2) use ($config) {
    $volt = new View\Engine\Volt($view, $di2);
    $volt->setOptions(array("compiledPath" => $config->application->compieldDir, "compiledExtension" => ".compiled", "compileAlways" => false,));
    return $volt;
});
$di2->set("language", function () {
    return "";
});
$di2->set("t", function () use ($di2) {
    $translationPath = '../app/messages/';
    $language = $di2->getService('language')->getDefinition();
    if ($language == "") {
        $language = $di2->get("request")->get("lang");
    }
    if ($language === 'sp' || $language === 'en' || $language === 'ar') {
        $translationPath = $translationPath . $language;
    } else {
        $translationPath = $translationPath . 'en';
    }
    require $translationPath . "/main.php";
    $mainTranslate = new \Phalcon\Translate\Adapter\NativeArray(array("content" => $messages));
    return $mainTranslate;
});
$di2->set('mandrill', function () use ($config) {
    return new \Mandrill($config->services->mandrillApiKey);
});
$di2->set('mail', function () {
    return new \Mail();
});
$arguments = array();
foreach ($argv as $k => $arg) {
    if ($k == 1) {
        $arguments['task'] = $arg;
    } elseif ($k == 2) {
        $arguments['action'] = $arg;
    } elseif ($k >= 3) {
        $arguments['params'][] = $arg;
    }
}
define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));
try {
    $console->handle($arguments);
}
catch(\Phalcon\Exception $e) {
    echo $e->getMessage();
    exit(255);
}
