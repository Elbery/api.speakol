<?php
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;
$di = new FactoryDefault();
$di['view'] = function () use ($config) {
    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    $view->registerEngines(array(".volt" => 'voltService'));
    return $view;
};
$di->set('voltService', function ($view, $di) use ($config) {
    $volt = new View\Engine\Volt($view, $di);
    $volt->setOptions(array("compiledPath" => $config->application->compieldDir, "compiledExtension" => ".compiled", "compileAlways" => false,));
    return $volt;
});
$di['url'] = function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
};
$di->setShared('db', function () use ($config) {
    return new MysqlAdapter(array('host' => $config->database->host, 'username' => $config->database->username, 'password' => $config->database->password, 'dbname' => $config->database->dbname));
});
$di->set('mandrill', function () use ($config) {
    return new \Mandrill($config->services->mandrillApiKey);
});
$di->set('mail', function () {
    return new \Mail();
});
$di['facebook'] = function () use ($config) {
    $facebook = new Facebook(array('appId' => '1405353299758190', 'secret' => '3b59d237d528d97fcc287f4e34fcd06e', 'allowSignedRequest' => false));
    $scope = 'user_status,email,publish_stream,publish_actions,offline_access,read_friendlists,status_update,user_birthday,user_hometown,user_location';
    $fb = new FacebookLibrary($facebook, $scope);
    return $fb;
};
$di['google_client'] = function () use ($config) {
    $client = new apiClient();
    $client->setClientId('5420708974-g2geolk9m1m5dkuk3nspe863i9rslnke.apps.googleusercontent.com');
    $client->setClientSecret('zXCQ-OILrno7gvuFlL2Bao5J');
    $client->setDeveloperKey('AIzaSyDXwfGrCwGFkVA7ZR47UCVOsgO7MWQp5Hc');
    $client->setApprovalPrompt('auto');
    return $client;
};
$di['google'] = function () use ($di) {
    $gooleLib = new GoogleLibrary();
    return $gooleLib;
};
$di['google_oauth'] = function () use ($di) {
    $oauth2 = new apiOauth2Service($di->get('google_client'));
    return $oauth2;
};
$di->setShared('twitter', function () use ($config) {
    $tmhOAuth = new tmhOAuth(array('consumer_key' => 'ReAXuWDQfayYowNmbpK3ceSsU', 'consumer_secret' => 'OX18oMXrB7STERHBrP9shSuLSpU7y1OAeeVDgm9TFjQ3qZxNVg'));
    return $tmhOAuth;
});
$di['twitterLib'] = function () use ($config) {
    $tmhOAuth = new twitterLibrary();
    return $tmhOAuth;
};
$di['modelsManager'] = function () use ($config) {
    return new Phalcon\Mvc\Model\Manager();
};
$di['url'] = function () use ($config) {
    $url = new Phalcon\Mvc\Url();
    $url->setBaseUri('http://' . $_SERVER['HTTP_HOST'] . '/');
    return $url;
};
$di->set("request", function () {
    return new Phalcon\Http\Request();
});
$di->set("config", function () use ($config) {
    return $config;
});
$di->set("Utility", function () use ($di) {
    return new Speakol\Utility($di);
});
$di->set("language", function () {
    return "";
});
$di->set("t", function () use ($di) {
    $translationPath = '../app/messages/';
    $language = $di->getService('language')->getDefinition();
    if ($language == "") {
        $language = $di->get("request")->get("lang");
    }
    if ($language === 'sp' || $language === 'en' || $language === 'ar') {
        $translationPath = $translationPath . $language;
    } else {
        $translationPath = $translationPath . 'en';
    }
    require $translationPath . "/main.php";
    $mainTranslate = new \Phalcon\Translate\Adapter\NativeArray(array("content" => $messages));
    return $mainTranslate;
});
$di->set("headers", function () use ($di) {
    if (function_exists('apache_request_headers')) {
        return apache_request_headers();
    }
    $headers = '';
    foreach ($_SERVER as $name => $value) {
        if (substr($name, 0, 5) == 'HTTP_') {
            $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))) ] = $value;
        }
    }
    return $headers;
});
