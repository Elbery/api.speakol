<?php
use Phalcon\DI\FactoryDefault;
class DowngradedAppsLog extends BaseModel {
    protected $id;
    protected $app_id;
    protected $payment_method;
    protected $card_number;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_app_id($app_id) {
        $this->app_id = $app_id;
    }
    public function set_payment_method($method) {
        $this->payment_method = $method;
    }
    public function set_card_number($card) {
        $this->card_number = $card;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_app_id() {
        return $this->app_id;
    }
    public function get_payment_method() {
        return $this->payment_method;
    }
    public function get_card_number() {
        return $this->card_number;
    }
}
