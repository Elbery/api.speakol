<?php
class AppFollows extends BaseModel {
    protected $id;
    protected $app_id;
    protected $follower_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setFollowerId($follower_id) {
        $this->follower_id = $follower_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getFollowerId() {
        return $this->follower_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('follower_id', 'Users', 'id', NULL);
        $this->belongsTo('app_id', 'Apps', 'id', NULL);
    }
    public function isFollowing($_appId, $_userId) {
        return (AppFollows::query()->where('app_id = :app_id:')->AndWhere('follower_id = :follower_id:')->bind(array('app_id' => $_appId, 'follower_id' => $_userId))->execute()->count());
    }
    public function followActionHelper($_appId, $_userId) {
        $objectAppFollows = new AppFollows();
        $objectAppFollows->setAppId($_appId);
        $objectAppFollows->setFollowerId($_userId);
        $objectAppFollows->setCreatedAt(strftime('%Y-%m-%d', time()));
        $objectAppFollows->setUpdatedAt(strftime('%Y-%m-%d', time()));
        return ($objectAppFollows->save());
    }
    public function unfollowActionHelper($_appId, $_userId) {
        return ($this->getDI()->getShared('modelsManager')->executeQuery("DELETE FROM AppFollows WHERE app_id = :app_id: AND follower_id = :follower_id:", array('app_id' => $_appId, 'follower_id' => $_userId))->success());
    }
}
