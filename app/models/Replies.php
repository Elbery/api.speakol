<?php
use Phalcon\DI\FactoryDefault;
class Replies extends BaseModel {
    protected $id;
    protected $argument_id;
    protected $parent_reply;
    protected $user_id;
    protected $side_id;
    protected $content;
    protected $app_id;
    protected $created_at;
    protected $updated_at;
    protected $admin_at;
    protected $link;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setArgumentId($argument_id) {
        $this->argument_id = $argument_id;
        return $this;
    }
    public function setParentReply($parent_reply) {
        $this->parent_reply = $parent_reply;
        return $this;
    }
    public function setSideId($side_id) {
        $this->side_id = $side_id;
        return $this;
    }
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    public function setContent($content) {
        $this->content = $content;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function set_admin_at($app_id) {
        $this->admin_at = $app_id;
    }
    public function setLink($link) {
        $this->link = $link;
    }
    public function getId() {
        return $this->id;
    }
    public function getArgumentId() {
        return $this->argument_id;
    }
    public function getUserId() {
        return $this->user_id;
    }
    public function getContent() {
        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        preg_match('#((?:http(s)?://)?(?:www\.)?(?:youtube\.com/(?:v/|watch\?v=)|youtu\.be/)([\w-]+)(?:\S+)?)#', $this->content, $match);
        if (isset($match[3])) {
            $embed = <<<YOUTUBE
        <div align="center">
            <iframe title="YouTube video player" width="100%" height="200" src="#ytzs#$match[3]?autoplay=0" frameborder="0" allowfullscreen></iframe>
        </div> 
YOUTUBE;
            $this->content = str_replace($match[0], $embed, $this->content);
        }
        $this->content = preg_replace("/(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?/iu", '<a href="\0" target="blank">\0</a>', $this->content);
        $this->content = str_replace('"#ytzs#', 'http://www.youtube.com/embed/', $this->content);
        return htmlspecialchars($this->content);
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getParentReply() {
        return $this->parent_reply;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function get_admin_at() {
        return $this->admin_at;
    }
    public function getLink() {
        return $this->link;
    }
    public function initialize() {
        $this->hasMany('id', 'ReplyThumbs', 'reply_id', NULL);
        $this->hasMany('id', 'ReplyPhotos', 'reply_id', NULL);
        $this->hasMany('id', 'Replies', 'parent_reply', array('alias' => 'NestedReply'));
        $this->belongsTo('argument_id', 'Arguments', 'id');
        $this->belongsTo('user_id', 'Users', 'id', NULL);
    }
    public function createActionHelper($_userId) {
        $intArgumentId = $this->getDi()->getShared('request')->getPost('argument_id');
        $intParentReply = $this->getDi()->getShared('request')->getPost('parent_reply');
        $txtContext = $this->getDi()->getShared('request')->getPost('context');
        $txtContext = trim($txtContext);
        $arrPhotos = $this->getDi()->getShared('request')->getUploadedFiles();
        $audio = $this->getDi()->getShared('request')->getPost('audio');
        $link = $this->getDi()->getShared('request')->getPost('link');
        if (empty($txtContext) && empty($arrPhotos)) {
            throw new \Exception(FactoryDefault::getDefault()->get('t')->_("reply-emty-comments"), 409);
        }
        if (!empty($arrPhotos)) {
            $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
            if (intval($audio) === 1) {
                $arrUploadedPhotos = $objImgUploader->upload($objImgUploader::DIR_REPLIES, $arrPhotos, null, array(), true);
            } else {
                $arrUploadedPhotos = $objImgUploader->upload($objImgUploader::DIR_REPLIES, $arrPhotos);
            }
            if ($arrUploadedPhotos[0]['status'] != 1) {
                return array('status' => 'ERROR', 'message' => $arrUploadedPhotos[0]['error']);
            }
        }
        $argumentSide = Arguments::findFirst($intArgumentId);
        if (!$argumentSide) {
            throw new \Exception("Argument id doesn't exist", 409);
        }
        $sideId = $argumentSide->getSideId();
        $modelReplies = new Replies();
        $modelReplies->setUserId($_userId);
        $modelReplies->setArgumentId($intArgumentId);
        $modelReplies->setParentReply($intParentReply);
        $modelReplies->setSideId($sideId);
        $modelReplies->setContent($txtContext);
        $modelReplies->setLink($link);
        $admin = AppAdmins::findFirst("user_id=$_userId");
        if ($admin) {
            $modelReplies->set_admin_at($admin->getAppId());
        }
        if ($modelReplies->save() == FALSE) {
            $strErrors = "";
            foreach ($modelReplies->getMessages() as $message) {
                $strErrors.= $message->getMessage();
                $strErrors.= ". ";
            }
            throw new \Exception(FactoryDefault::getDefault()->get('t')->_("conflict") . ":{$strErrors}", 409);
        }
        if (!empty($arrUploadedPhotos)) {
            foreach ($arrUploadedPhotos as $photo) {
                if ($photo['status'] == FALSE) {
                    continue;
                }
                $modelReplyPhotos = new ReplyPhotos();
                $modelReplyPhotos->setReplyId($modelReplies->getId());
                $modelReplyPhotos->setUrl($photo['path']);
                $modelReplyPhotos->setImage($photo['new']);
                if ($modelReplyPhotos->save() == FALSE) {
                    $strErrors = "";
                    foreach ($modelReplyPhotos->getMessages() as $message) {
                        $strErrors.= $message->getMessage();
                        $strErrors.= ". ";
                    }
                    throw new \Exception(FactoryDefault::getDefault()->get('t')->_("conflict") . ":{$strErrors}", 409);
                }
            }
        }
        if ($argumentSide) {
            $argument = $argumentSide->toArray();
            $current_user = Tokens::checkAccess();
            if ($argument['owner_id'] != $current_user) {
                $app = AppAdmins::findFirst(array('user_id = :user_id:', 'bind' => array('user_id' => $current_user)));
                if ($app) {
                    $admin_at = $app->getAppId();
                } else {
                    $admin_at = 0;
                }
                $reply_notification = new NotificationsController();
                $reply_notification->replyMyArgument(array('app_id' => $argument['app_id'], 'referrer' => $this->getDi()->getShared('request')->getPost('referrer'), 'argument_owner' => $argument['owner_id'], 'module_type' => $argument['type'], 'module_id' => $argument['module_id'], 'admin_at' => $admin_at, 'link' => $link,));
            }
        }
        return array('status' => 'OK', 'id' => $modelReplies->getId());
    }
    public function deleteReply($replye_id) {
        $signedInUserID = Tokens::getSignedInUserId();
        $reply = self::findFirst(array('conditions' => 'id=:id: AND user_id=:user_id:', 'bind' => array('id' => $replye_id, 'user_id' => $signedInUserID),));
        if (!$reply) {
            $app = AppAdmins::findFirst(array('user_id = :user_id: AND super_admin=1', 'bind' => array('user_id' => $signedInUserID)));
            $isAppAdmin = $app ? $app->getAppId() : false;
            if ($isAppAdmin) {
                $reply = self::findFirst(array('conditions' => 'id=:id: AND app_id=:app_id:', 'bind' => array('id' => $replye_id, 'app_id' => $isAppAdmin)));
            }
        }
        if (!$reply) {
            return "Not found";
        }
        return $reply->delete();
    }
    public static function showActionHelper($_argumentId, $_pageNum, $_perPage) {
        $arrReplies = array();
        $replies = self::_getRepliesByArgumentId($_argumentId, $_pageNum, $_perPage);
        foreach ($replies['data'] as $reply) {
            $arrReplies[] = self::formatReply($reply);
        }
        if ($arrReplies === FALSE) {
            throw new Exception(FactoryDefault::getDefault()->get('t')->_("bad-request"), 400);
        }
        return array('data' => $arrReplies, 'hasmore' => $replies['has_more']);
    }
    public static function getNestedReplies($_parentReply, $_pageNum, $_perPage) {
        $arrReplies = array();
        if (static ::getReplyLevel($_parentReply) > 2) {
            throw new Exception(FactoryDefault::getDefault()->get('t')->_("bad-request"), 400);
            return false;
        }
        $replies = self::_getRepliesByParentReply($_parentReply, $_pageNum, $_perPage);
        foreach ($replies['data'] as $reply) {
            $arrReplies[] = self::formatReply($reply);
        }
        if ($arrReplies == FALSE) {
            throw new Exception(FactoryDefault::getDefault()->get('t')->_("bad-request"), 400);
        }
        return array('hasmore' => $replies['has_more'], 'data' => $arrReplies);
    }
    public function voteActionHelper($_replyId, $_userId, $vote, $link) {
        if (FALSE == ReplyThumbs::findThumbsByReplyIDUserID($_replyId, $_userId)) {
            return ReplyThumbs::createVote($_replyId, $_userId, $vote, $link);
        } else {
            return ReplyThumbs::updateVote($_replyId, $_userId, $vote, $link);
        }
    }
    private static function _getRepliesByArgumentId($_argumentId, $_page = 1, $_limit = 2) {
        $_offset = ($_page == 1) ? 0 : ($_page - 1) * $_limit;
        $page = $_page < 1 ? 1 : $_page;
        $startValue = ($page - 1) * $_limit;
        $count = static ::count(array('argument_id = :argument_id: AND parent_reply is NULL', 'bind' => array('argument_id' => $_argumentId),));
        $hasmore = (($count >= (($startValue + 1) + $_limit)) ? true : FALSE);
        return array('has_more' => $hasmore, 'data' => Replies::find(array('argument_id = :argument_id: AND parent_reply is NULL', 'bind' => array('argument_id' => $_argumentId), "order" => "id desc", "limit" => array("number" => $_limit, "offset" => $_offset))));
    }
    public static function getReplies($argument_id, $pageNum, $perPage) {
        $replies = Replies::find(array("conditions" => "argument_id=:argument_id:", "bind" => array("argument_id" => $argument_id), "limit" => array("number" => $pageNum, "offset" => $perPage), "order" => 'id desc'));
        $data = array();
        foreach ($replies as $reply) {
            $data[] = Replies::formatReply($reply);
        }
        return $data;
    }
    private static function _getRepliesByParentReply($_parentReply, $_page = 1, $_limit = 2) {
        $_offset = ($_page == 1) ? 0 : ($_page - 1) * $_limit;
        $page = $_page < 1 ? 1 : $_page;
        $startValue = ($page - 1) * $_limit;
        $count = static ::count(array('parent_reply = :parent_reply:', 'bind' => array('parent_reply' => $_parentReply), "order" => "id desc",));
        $hasmore = (($count >= (($startValue + 1) + $_limit)) ? true : FALSE);
        return array('has_more' => $hasmore, 'data' => Replies::find(array('parent_reply = :parent_reply:', 'bind' => array('parent_reply' => $_parentReply), "order" => "id desc", "limit" => array("number" => $_limit, "offset" => $_offset))));
    }
    private static function _getRepliesById($_replyID) {
        return Replies::find(array('id = :id:', 'bind' => array('id' => $_replyID)));
    }
    public static function hasVoted($_replyId, $_userId, $_vote) {
        if (0 < ReplyThumbs::findThumbsByTrinary($_replyId, $_userId, $_vote)) {
            throw new Exception(FactoryDefault::getDefault()->get('t')->_("found"), 302);
        }
    }
    public static function countRepliesPerArgument($_argumentID) {
        return Replies::count(array('argument_id = :argument_id: AND parent_reply IS NULL', 'bind' => array('argument_id' => $_argumentID)));
    }
    public static function fetchReply($_replyID) {
        $arrReplies = array();
        foreach (self::_getRepliesById($_replyID) as $reply) {
            return self::formatReply($reply);
        }
        throw new Exception(FactoryDefault::getDefault()->get('t')->_("bad-request"), 400);
    }
    public static function formatReply($_reply) {
        $arrAttachments = array();
        foreach ($_reply->getReplyPhotos() as $photo) {
            $arrAttachments[] = array('id' => $photo->getId(), 'image' => $photo->getImage(), 'url' => Phalcon\DI::getDefault()->getShared('Utility')->generateURLFromDirPath($photo->getUrl(), $photo->getImage()));
        }
        if ($_reply->getParentReply()) {
            $return = static ::getReplyToUser($_reply->getParentReply());
            $replyUser = $return->get_first_name() . " " . $return->get_last_name();
        } else {
            $replyUser = '';
        }
        $perPage = Phalcon\DI::getDefault()->getShared('config')->application->commentsPagination;
        $repliesCount = count($_reply->NestedReply);
        $user = Users::findFirst($_reply->getUserId());
        $owner_id = $_reply->getUserId();
        $owner_name = $user->get_first_name() . " " . $user->get_last_name();
        $owner_photo = $user->getProfilePicture();
        if ($_reply->get_admin_at() != 0) {
            $app = Apps::findFirst($_reply->get_admin_at());
            if ($app) {
                $owner_name = $app->getName();
                $owner_photo = $app->getImage();
            }
        }
        $owner_slug = $user->getSlug();
        $date = strtotime($_reply->getCreatedAt());
        $date = date('F jS', $date);
        $reply_data = array('id' => $_reply->getId(), 'context' => $_reply->getContent(), 'attached_photos' => $arrAttachments, 'owner_id' => $owner_id, 'owner_name' => $owner_name, 'owner_slug' => $owner_slug, 'owner_photo' => $owner_photo, 'owner_status' => Beacon::user_status($owner_id), 'created_at' => $date, 'count_thumbs_up' => ReplyThumbs::findThumbsUPByReplyID($_reply->getId()), 'count_thumbs_dn' => ReplyThumbs::findThumbsDNByReplyID($_reply->getId()), 'replies_count' => $repliesCount, 'reply_level' => static ::getReplyLevel($_reply->getId()), 'reply_to_user' => $replyUser, 'admin_at' => $_reply->get_admin_at(), 'currentUserhasVoted' => (FALSE != ReplyThumbs::findThumbsByReplyIDUserID($_reply->getId(), Tokens::getSignedInUserId())), 'currentUserVote' => (FALSE != ReplyThumbs::findThumbByReplyIDUserID($_reply->getId(), Tokens::getSignedInUserId())), 'total_replies_pages' => intval($perPage ? $repliesCount / $perPage : $repliesCount / 2));
        if ($user->getRoleId() == Roles::APP || $user->getRoleId() == Roles::ADMIN) {
            $app = Apps::findFirst(array('columns' => 'website', 'publisher_id=' . $user->getId()));
            $reply_data['url'] = $app['website'];
        }
        return $reply_data;
    }
    public static function getReplyLevel($_replyID, $level = 1) {
        $result = Replies::findFirst($_replyID);
        if ($result) {
            $parentReply = $result->getParentReply();
            if (is_null($parentReply)) {
                return $level;
            } else {
                $level++;
                return static ::getReplyLevel($parentReply, $level);
            }
        }
        return false;
    }
    public static function getReplyToUser($_replyID) {
        $result = Replies::findFirst($_replyID);
        $user = $result->Users;
        if ($result) {
            if ($result->get_admin_at() != 0) {
                $app = Apps::findFirst($result->get_admin_at());
                if ($app) {
                    $user->first_name = $app->getName();
                    $user->last_name = "";
                }
            }
            return $user;
        }
        return false;
    }
    public static function doesExist($_replyID) {
        if (self::count($_replyID) == FALSE) {
            throw new Exception(FactoryDefault::getDefault()->get('t')->_("not-found"), 404);
        }
        return true;
    }
    private static function canDelete($user_id, $reply_id) {
        $replies = self::findFirst('id=' . $reply_id . ' AND user_id=' . $user_id);
        return $replies;
    }
    public function beforeCreate() {
        parent::beforeCreate();
        $argId = $this->getArgumentId();
        $modelArgument = Arguments::findFirst($argId);
        $moduleName = $modelArgument->getType();
        $con = new Speakol\concreteSocialPluginFactory();
        $obj = $con->make($moduleName);
        if ($obj) {
            $parentId = $modelArgument->getModuleId();
            $obj = $obj->findFirst($parentId);
            $this->setAppId($obj->getAppId());
        }
    }
}
