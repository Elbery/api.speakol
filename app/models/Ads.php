<?php
class Ads extends BaseModel {
    protected $id;
    protected $context;
    protected $attached_photos;
    protected $owner_name;
    protected $owner_photo;
    protected $owner_email;
    protected $owner_lang;
    protected $created_at;
    protected $url;
    protected $publisher_email;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_context($text) {
        $this->context = $text;
    }
    public function set_attached_photos($image) {
        $this->attached_photos = $image;
    }
    public function set_owner_name($name) {
        $this->owner_name = $name;
    }
    public function set_owner_photo($photo) {
        $this->owner_photo = $photo;
    }
    public function set_owner_email($email) {
        $this->owner_email = $email;
    }
    public function set_owner_lang($lang) {
        $this->owner_lang = $lang;
    }
    public function set_created_at($time) {
        $this->created_at = $time;
    }
    public function set_url($url) {
        $this->url = $url;
    }
    public function set_publisher_email($flag) {
        $this->publisher_email = $flag;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_context() {
        return htmlspecialchars($this->context);
    }
    public function get_attached_photos() {
        return $this->attached_photos;
    }
    public function get_owner_name() {
        return $this->owner_name;
    }
    public function get_owner_photo() {
        return $this->owner_photo;
    }
    public function get_owner_email() {
        return $this->owner_email;
    }
    public function get_owner_lang() {
        return $this->owner_lang;
    }
    public function get_created_at() {
        return $this->created_at;
    }
    public function get_url() {
        return $this->url;
    }
    public function get_publisher_email() {
        return intval($this->publisher_email);
    }
}
