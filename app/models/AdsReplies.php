<?php
class AdsReplies extends BaseModel {
    protected $id;
    protected $ad_id;
    protected $parent_reply;
    protected $context;
    protected $created_at;
    protected $user_id;
    protected $attached_photos;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_ad_id($ad_id) {
        $this->ad_id = $ad_id;
    }
    public function set_parent_reply($parent_id) {
        $this->parent_reply = $parent_id;
    }
    public function set_context($context) {
        $this->context = $context;
    }
    public function set_created_at($time) {
        $this->created_at = $time;
    }
    public function set_user_id($u_id) {
        $this->user_id = $u_id;
    }
    public function set_attached_photos($photos) {
        $this->attached_photos = $photos;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_ad_id() {
        return $this->ad_id;
    }
    public function get_parent_reply() {
        return $this->parent_reply;
    }
    public function get_context() {
        return htmlspecialchars($this->context);
    }
    public function get_created_at() {
        return $this->created_at;
    }
    public function get_user_id() {
        return $this->user_id;
    }
    public function get_attached_photos() {
        return $this->attached_photos;
    }
}
