<?php
class ReportReporters extends BaseModel {
    protected $id;
    protected $report_id;
    protected $reporter_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setReportId($report_id) {
        $this->report_id = $report_id;
        return $this;
    }
    public function setReporterId($reporter_id) {
        $this->reporter_id = $reporter_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getReportId() {
        return $this->report_id;
    }
    public function getReporterId() {
        return $this->reporter_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('reporter_id', 'Users', 'id', NULL);
    }
}
