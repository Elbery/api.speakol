<?php
use Phalcon\Mvc\Model\Validator\PresenceOf, Phalcon\Mvc\Model\Validator\Uniqueness, Phalcon\Mvc\Model\Relation, Phalcon\Mvc\Model\Transaction\Manager as TrManager, Phalcon\DI\FactoryDefault;
class Comparisons extends BaseModel {
    protected $id;
    protected $title;
    protected $hash;
    protected $image;
    protected $category_id;
    protected $plugin_slider;
    protected $slug;
    protected $created_at;
    protected $updated_at;
    protected $app_id;
    protected $user_id;
    protected $url;
    protected $align;
    protected $hide_comment;
    protected $high_interaction;
    protected $lang;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setHash($hash) {
        $this->hash = $hash;
    }
    public function set_plugin_slider($plugin_slider) {
        $this->plugin_slider = $plugin_slider;
    }
    public function setCategoryId($categoryId) {
        $this->category_id = $categoryId;
        return $this;
    }
    public function setSlug($slug) {
        $countSlug = self::find(array('conditions' => 'title=:title:', 'bind' => array('title' => $slug)))->count();
        if ($countSlug > 0) {
            $this->slug = parent::uniqueSlug($slug . '-' . $countSlug);
        } else {
            $this->slug = parent::uniqueSlug($slug);
        }
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    public function set_align($align) {
        $this->align = $align;
    }
    public function set_hide_comment($hide) {
        $this->hide_comment = $hide;
    }
    public function set_high_interaction($no_comments) {
        $this->high_interaction = $no_comments;
    }
    public function set_lang($lang) {
        $this->lang = $lang;
    }
    public function getId() {
        return $this->id;
    }
    public function getTitle() {
        return htmlspecialchars($this->title);
    }
    public function getImage() {
        return $this->image;
    }
    public function getHash($hash) {
        return $this->hash;
    }
    public function getCategoryId() {
        return $this->category_id;
    }
    public function get_plugin_slider() {
        return $this->plugin_slider;
    }
    public function getSlug() {
        return $this->slug;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getUserId() {
        return $this->user_id;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function getUrl() {
        return $this->url;
    }
    public function get_align() {
        return $this->align;
    }
    public function get_hide_comment() {
        return $this->hide_comment;
    }
    public function get_high_interaction() {
        return intval($this->high_interaction);
    }
    public function get_lang() {
        return $this->lang;
    }
    public function initialize() {
        $this->hasMany('id', 'ComparisonFollows', 'comparison_id', array('foreignKey' => array('action' => Relation::ACTION_CASCADE)));
        $this->hasMany('id', 'SphereComparisons', 'comparison_id', NULL);
        $this->hasMany('id', 'Sides', 'comparison_id', array('foreignKey' => array('action' => Relation::ACTION_CASCADE)));
        $this->belongsTo('app_id', 'Apps', 'id', NULL);
    }
    private function _validate() {
        $this->validate(new PresenceOf(array('field' => 'title', 'message' => 'Title is required',)));
        $this->validate(new PresenceOf(array('field' => 'slug', 'message' => 'Title is required',)));
        $this->validate(new Uniqueness(array('field' => 'slug', 'message' => 'Slug should be unique',)));
        if ($this->validationHasFailed()) {
            return false;
        }
        return true;
    }
    public function createComparison($request) {
        $return = array();
        $sides = json_decode($request->getPost('sides'), true);
        $sidesLength = count($sides);
        if ($sidesLength < 2) {
            $return['status'] = 401;
            $return['data'] = FactoryDefault::getDefault()->get('t')->_("comparison-two-items");
            return $return;
        } else if ($sidesLength > 8) {
            $return['status'] = 401;
            $return['data'] = FactoryDefault::getDefault()->get('t')->_("comparison-eight-items");
            return $return;
        }
        $isPublisher = false;
        $app_id = $request->getPost('app_id');
        if (!empty($app_id)) {
            $isPublisher = true;
        }
        $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
        for ($i = 1;$i <= $sidesLength;$i++) {
            if ($request->getPost("side_" . $i . "_logo")) {
                $sides['image'][] = $objImgUploader->uploadImage($objImgUploader::DIR_COMPARISONS, $request->getPost('side_' . $i . '_logo'));
            }
        }
        $transactionManager = new TrManager();
        $transaction = $transactionManager->get();
        $this->setTransaction($transaction);
        $curr_date = date('Y-m-d H:i:s');
        $title = trim($request->getPost('title'));
        $this->setTitle($title);
        $this->setSlug($title);
        $this->setCategoryId(intval($request->getPost('category_id')));
        $this->setUserId(intval($request->getPost('user_id')));
        $this->set_align($request->getPost('align'));
        $this->set_hide_comment($request->getPost('hide_comment'));
        $this->set_high_interaction(0);
        $this->setCreatedAt($curr_date);
        $this->setUpdatedAt($curr_date);
        $this->set_plugin_slider(1);
        $app = Apps::findFirst($app_id);
        $user = Users::findFirst($app->getPublisherId());
        $this->set_lang($user->getLocale());
        if ($isPublisher) {
            $this->setAppId(intval($request->getPost('app_id')));
        }
        if ($isPublisher == false) {
            $this->setImage($comparisonCover[0]['path'] . $comparisonCover[0]['new']);
        }
        if (!$this->save()) {
            $messages = $this->getMessages();
            $message = $messages[0];
            $transaction->rollback($message->getMessage());
        }
        $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
        $new_log = new Log();
        $new_log->log_current_action($this->getUserId(), $sentHeaders['Ip'], "/comparisons/create", $this->getId());
        for ($i = 0;$i < $sidesLength;$i++) {
            $side = new Sides();
            $side->setComparisonId($this->getId());
            if ($side->save() == false) {
                $messages = $side->getMessages();
                $message = $messages[0];
                $transaction->rollback($message->getMessage());
            }
            $comparisonSideMeta = new ComparisonSidesMeta();
            $comparisonSideMeta->setSideId($side->getId());
            $title = trim($sides[$i]['title']);
            if ($title == "") {
                $transaction->rollback(FactoryDefault::getDefault()->get('t')->_("invalid-title"));
                $transaction->commit();
            }
            $comparisonSideMeta->setTitle($title);
            $description = trim($sides[$i]['description']);
            $comparisonSideMeta->setDescription($description);
            $comparisonSideMeta->setImage($sides['image'][$i][0]['path'] . $sides['image'][$i][0]['new']);
            if ($comparisonSideMeta->save() == false) {
                $messages = $comparisonSideMeta->getMessages();
                $message = $messages[0];
                $transaction->rollback($message->getMessage());
            }
        }
        $title = Tags::filterTitle(trim($request->getPost('title')));
        $tags = new Tags();
        $tags->createTag($title['tags']);
        $transaction->commit();
        return array('status' => 'ok', 'messages' => $this->getSlug());
    }
    public function listComparison($conditions = null) {
        $comparisonData = array();
        $comparisons = self::find($conditions);
        $main = array();
        $counter = 0;
        foreach ($comparisons as $comparison) {
            $counter++;
            $comparisonInfo = $comparison->toArray();
            $comparisonInfo['comparison_id'] = $comparisonInfo['id'];
            unset($comparisonInfo['id']);
            $userId = $comparisonInfo['user_id'];
            $usr = Users::findFirst(array('conditions' => "id=$userId", 'columns' => array('id', 'slug', 'first_name', 'last_name', 'profile_picture')))->toArray();
            $usr['name'] = $usr['first_name'] . " " . $usr['last_name'];
            $usr['user_slug'] = $usr['slug'];
            $usr['user_id'] = $usr['id'];
            unset($usr['id']);
            unset($usr['slug']);
            $main = array_merge($comparisonInfo, $usr);
            $sides = $comparison->getSides();
            $related = array();
            $main['total_votes'] = 0;
            $main['total_comments'] = 0;
            foreach ($sides as $key => $side) {
                $sidesMeta = $side->getComparisonSidesMeta();
                $queryCondition = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $main['total_votes']+= Votes::count($queryCondition);
                $main['total_comments']+= Arguments::count($queryCondition);
                $related[] = $sidesMeta->toArray();
                $related[$key]['total_votes'] = Votes::count($queryCondition);
            }
            $comparisonData[$counter]['main'] = $main;
            $comparisonData[$counter]['related'] = $related;
        }
        return $comparisonData;
    }
    public function listMiniComparison($conditions = null, $option = null, $sort = array()) {
        $profileUserId = Tokens::checkAccess();
        $comparisonData = array();
        $comparisons = self::find(array_merge($conditions, $option));
        $main = array();
        $utility = new Speakol\Utility(FactoryDefault::getDefault());
        foreach ($comparisons as $comparison) {
            $sides = $comparison->getSides();
            $userInfo = $comparison->toArray();
            $appId = $userInfo['app_id'];
            $userId = $userInfo['user_id'];
            $app = $usr = array();
            if (!is_null($appId)) {
                $app = Apps::findFirst($userInfo['app_id'])->toArray();
            } else {
                $usr = Users::findFirst(array('conditions' => "id=$userId", 'columns' => array('id', 'slug', 'first_name', 'last_name', 'profile_picture')))->toArray();
                $usr['name'] = $usr['first_name'] . " " . $usr['last_name'];
            }
            $comparison = $comparison->toArray();
            $comparison['title'] = htmlspecialchars($comparison['title']);
            $comparison['module'] = self::COMPARISON;
            $comparison['sort_id'] = isset($sort[self::COMPARISON . '_' . $comparison['id']]) ? $sort[self::COMPARISON . '_' . $comparison['id']] : 0;
            $comparison['created_at'] = $utility->time_elapsed_string(strtotime($comparison['created_at']));
            $totalVotes = $totalComments = 0;
            $sidesArr = array();
            foreach ($sides as $side) {
                $voteQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $sid = $side->getComparisonSidesMeta(array('columns' => array('title', 'image')))->toArray();
                $sid['title'] = htmlspecialchars($sid['title']);
                $sid['description'] = htmlspecialchars($sid['description']);
                $sid['votes_count'] = Votes::count($voteQuery);
                $sid['comments_count'] = Arguments::count($voteQuery);
                $sid['side_id'] = $side->getId();
                $totalVotes+= $sid['votes_count'];
                $totalComments+= $sid['comments_count'];
                if ($profileUserId) {
                    $sid['voted'] = Votes::hasVoted($side->getId(), $profileUserId);
                } else {
                    $sid['voted'] = false;
                }
                $sidesArr[] = $sid;
            }
            foreach ($sidesArr as $k => $sid) {
                $sidesArr[$k]['percentage'] = !$totalVotes ? 0 : number_format($sid['votes_count'] * 100 / $totalVotes, '0', '.', '');
            }
            $main[$comparison['id']] = array('user' => $usr, 'app' => $app, 'owner_is_app' => $appId ? true : false, 'data' => $comparison, 'sides' => $sidesArr, 'counts' => array('total_votes' => $totalVotes, 'total_comments' => $totalComments,));
        }
        return $main;
    }
    static public function findComparisonBySlug($slug) {
        return self::find(array("conditions" => "slug= :slug:", "bind" => array('slug' => $slug)));
    }
    public static function showHelper($_slug, $order_by = "most_voted") {
        $_cmp = self::findComparisonBySlug($_slug);
        return self::formatComp($_cmp->getFirst(), $order_by);
    }
    public static function formatComp($_cmp, $order_by) {
        if ($_cmp == false) {
            return false;
        }
        $app = Apps::findFirst($_cmp->getAppId());
        $audio = PluginsFeature::validate_feature_using_plan_id($app->get_plan(), "audio_comment");
        $date = strtotime($_cmp->getUpdatedAt());
        $date = date('F j \a\t h:i A', $date);
        $comparison_data = array('id' => $_cmp->getId(), 'owner_id' => $_cmp->getUserId(), 'app_id' => $_cmp->getAppId(), 'app_name' => $app->getName(), 'app_image' => $app->getImage(), 'slug' => $_cmp->getSlug(), 'url' => $_cmp->getUrl(), 'image' => $_cmp->getImage(), 'title' => $_cmp->getTitle(), 'align' => $_cmp->get_align(), 'hide_comment' => $_cmp->get_hide_comment(), 'audio' => $audio, 'category' => $_cmp->getCategoryId(), 'created_at' => $_cmp->getCreatedAt(), 'updated_at' => $_cmp->getUpdatedAt(), 'plugin_slider' => $_cmp->get_plugin_slider(), 'updated_date' => $date, 'sides' => Sides::formatSides($_cmp->getId(), self::COMPARISON, $order_by), 'type' => self::COMPARISON,);
        $ads = PluginsAds::plugin_ads("comparison", $_cmp->getId(), sizeof($comparison_data['sides']));
        $comparison_data['ads'] = $ads;
        return $comparison_data;
    }
    static public function deleteComparison($slug, $userId) {
        $isAdmin = Tokens::isCurrentUserIs(array(1, 2));
        $app = AppAdmins::findFirst(array('user_id = :user_id: AND super_admin = 1', 'bind' => array('user_id' => $userId)));
        $isAppAdmin = false;
        if ($app) {
            $isAppAdmin = $app->getAppId();
        }
        if ($isAdmin) {
            $comparison = self::findFirst(array('conditions' => "slug='$slug'"));
        } elseif ($isAppAdmin) {
            $comparison = self::findFirst(array('conditions' => "slug='$slug' AND app_id = $isAppAdmin "));
        } else {
            $comparison = self::findFirst(array('conditions' => "slug='$slug' AND user_id=$userId"));
        }
        if ($comparison != false) {
            $images = array();
            $images[] = $comparison->getImage();
            $notifications = Notifications::find("notifiable_id=" . $comparison->getId() . " AND notification_type='comparison'");
            if ($notifications) {
                foreach ($notifications as $notification) {
                    $notification_recipients = NotificationRecipients::find('notification_id=' . $notification->getId());
                    if ($notification_recipients) {
                        $notification_recipients->delete();
                    }
                    $notification->delete();
                }
            }
            foreach ($comparison->getSides() as $side) {
                $sideData = $side->getComparisonSidesMeta()->toArray();
                $images[] = $sideData['image'];
            }
            $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
            $new_log = new Log();
            $new_log->log_current_action($userId, $sentHeaders['Ip'], "/comparisons/" . $slug . "/delete", $comparison->getId());
            if ($comparison->delete()) {
                $feed = Feeds::find(array('conditions' => 'module=:module: AND parent_id=:parent_id:', 'bind' => array('module' => BaseModel::COMPARISON, 'parent_id' => $comparison->getId())));
                if ($feed) {
                    $feed->delete();
                }
                foreach ($images as $image) {
                    if (is_file($_SERVER['DOCUMENT_ROOT'] . $image)) {
                        unlink($_SERVER['DOCUMENT_ROOT'] . $image);
                    }
                }
                return true;
            }
            return false;
        }
        return false;
    }
    public static function voteDevoteActionHelper($_comparisonID, $_sideId, $_userId, $link) {
        $arrSides = Sides::getSideIdByCompId($_comparisonID);
        if (is_array($arrSides)) {
            foreach ($arrSides as $key => $value) {
                if (Votes::unvote($value["id"], $_userId) == False) {
                    return FALSE;
                }
            }
        }
        $vote_id = votes::vote($_sideId, $_userId, $link);
        if ($vote_id) {
            $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
            $new_log = new Log();
            $new_log->log_current_action($_userId, $sentHeaders['Ip'], "/vote on comparison", $vote_id);
            if (is_array($arrSides)) {
                $votes = array();
                foreach ($arrSides as $key => $side) {
                    $votes[$side['id']] = Votes::getSideVotes($side['id']);
                }
                return $votes;
            }
        }
        return false;
    }
    public static function getFavoredSideOfComp($objData) {
        $arrSides = Sides::getSideIdByCompId($objData->container_id);
        if (is_array($arrSides)) {
            foreach ($arrSides as $key => $value) {
                if (Votes::hasVoted($value["id"], $objData->signedInUserID) == TRUE) {
                    return $value["id"];
                }
            }
        }
        return 0;
    }
}
