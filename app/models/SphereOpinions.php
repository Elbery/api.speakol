<?php
class SphereOpinions extends BaseModel {
    protected $id;
    protected $opinion_id;
    protected $sphere_id;
    protected $featured;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setOpinionId($opinion_id) {
        $this->opinion_id = $opinion_id;
        return $this;
    }
    public function setSphereId($sphere_id) {
        $this->sphere_id = $sphere_id;
        return $this;
    }
    public function setFeatured($featured) {
        $this->featured = $featured;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getOpinionId() {
        return $this->opinion_id;
    }
    public function getSphereId() {
        return $this->sphere_id;
    }
    public function getFeatured() {
        return $this->featured;
    }
    public function initialize() {
        $this->belongsTo('opinion_id', 'Opinions', 'id', NULL);
        $this->belongsTo('sphere_id', 'Spheres', 'id', NULL);
    }
}
