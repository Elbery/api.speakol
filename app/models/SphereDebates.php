<?php
class SphereDebates extends BaseModel {
    protected $id;
    protected $debate_id;
    protected $sphere_id;
    protected $featured;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setDebateId($debate_id) {
        $this->debate_id = $debate_id;
        return $this;
    }
    public function setSphereId($sphere_id) {
        $this->sphere_id = $sphere_id;
        return $this;
    }
    public function setFeatured($featured) {
        $this->featured = $featured;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getDebateId() {
        return $this->debate_id;
    }
    public function getSphereId() {
        return $this->sphere_id;
    }
    public function getFeatured() {
        return $this->featured;
    }
    public function initialize() {
        $this->belongsTo('debate_id', 'Debates', 'id', NULL);
        $this->belongsTo('sphere_id', 'Spheres', 'id', NULL);
    }
}
