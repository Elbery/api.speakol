<?php
use Phalcon\DI\FactoryDefault;
class AttachedPhotos extends BaseModel {
    protected $id;
    protected $image;
    protected $url;
    protected $uploader_id;
    protected $uploader_type;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    public function setUploaderId($uploader_id) {
        $this->uploader_id = $uploader_id;
        return $this;
    }
    public function setUploaderType($uploader_type) {
        $this->uploader_type = $uploader_type;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getImage() {
        return $this->image;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getUploaderId() {
        return $this->uploader_id;
    }
    public function getUploaderType() {
        return $this->uploader_type;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('uploader_id', 'Users', 'id', NULL);
    }
    public function beforeCreate() {
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');
    }
    public function beforeUpdate() {
        $this->updated_at = date('Y-m-d H:i:s');
    }
    public static function attachedImages($userId) {
        $attachedPicturesQuery = array('uploader_id = :uploader_id:', 'bind' => array('uploader_id' => $userId));
        $attachedPictutres = static ::find($attachedPicturesQuery);
        $attachedImages = array();
        foreach ($attachedPictutres as $attachedPic) {
            $attachedImages[] = array('id' => $attachedPic->getId(), 'url' => $attachedPic->getUrl());
        }
        return $attachedImages;
    }
    public function saveAttachedPhotos($storyId, $appId, $transaction, $type) {
        $arrPhotos = $this->getDi()->getShared('request')->getUploadedFiles();
        if (empty($arrPhotos)) {
            return false;
        }
        $objUtility = $this->getDi()->getShared('Utility');
        $objUtility->setDependencyInjection($this->getDi());
        $objImgUploader = $objUtility->getImageUploader();
        switch ($type) {
            case Opinions::MUTABLE_TYPE:
                $attachmenetsData = $objImgUploader->upload($objImgUploader::DIR_OPINIONS, $arrPhotos);
            break;
            case Stories::TYPE:
                $attachmenetsData = $objImgUploader->upload($objImgUploader::DIR_STORIES, $arrPhotos);
            break;
            case Argumentboxes::TYPE:
                $attachmenetsData = $objImgUploader->upload($objImgUploader::DIR_ARGUMENTBOX, $arrPhotos);
            break;
            default:
                $attachmenetsData = FALSE;
            break;
        }
        if ($attachmenetsData == FALSE) {
            return FALSE;
        }
        foreach ($attachmenetsData as $att) {
            if (!empty($att['error'])) {
                continue;
            }
            $this->setId(NULL);
            $this->setImage($att['new']);
            $this->setUrl($att['path'] . $att['new']);
            $this->setUploaderId($appId);
            $this->setUploaderType($type);
            $save = $this->save();
            if ($save) {
                switch ($type) {
                    case Opinions::MUTABLE_TYPE:
                        $opinionsAttachment = new OpinionsAttachments();
                        $opinionsAttachment->saveOpinionsAttachement($storyId, $this->getId(), $transaction);
                    break;
                    case Stories::TYPE:
                        $storiesAttachment = new StoriesAttachments();
                        $storiesAttachment->saveStoriesAttachement($storyId, $this->getId(), $transaction);
                    break;
                }
            } else {
                $transaction->rollback("Can't save attachment");
                return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
            }
        }
    }
    public function deleteAttachedPhotos($attachments, $transaction) {
        $AttachedPhotos = new AttachedPhotos();
        foreach ($attachments as $att) {
            $attachmentId = $att->getAttachmentId();
            if (!$att->delete()) {
                $transaction->rollback(FactoryDefault::getDefault()->get('t')->_("upload-cant-delete-attachment"));
            }
            if (!$AttachedPhotos->delete($attachmentId)) {
                $transaction->rollback(FactoryDefault::getDefault()->get('t')->_("upload-cant-delete-photo"));
            }
        }
    }
}
