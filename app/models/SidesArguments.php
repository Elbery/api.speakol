<?php
class SidesArguments extends BaseModel {
    protected $id;
    protected $side_id;
    protected $argument_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setSideId($side_id) {
        $this->side_id = $side_id;
        return $this;
    }
    public function setArgumentId($argument_id) {
        $this->argument_id = $argument_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getArgumentId() {
        return $this->argument_id;
    }
    public function initialize() {
        $this->belongsTo('side_id', 'Sides', 'id', array('foreignKey' => true));
        $this->belongsTo('argument_id', 'Arguments', 'id', array('foreignKey' => true));
    }
    public static function getArgumentsBySideId($_sideId) {
        return SidesArguments::query()->where('side_id = :side_id:')->bind(array('side_id' => $_sideId))->execute();
    }
    public static function saveArgumentSides($_argumentID, $_sideID) {
        $modelSidesArguments = new SidesArguments();
        $modelSidesArguments->setArgumentId($_argumentID);
        $modelSidesArguments->setSideId($_sideID);
        if ($modelSidesArguments->save() == FALSE) {
            $strErrors = "";
            foreach ($modelSidesArguments->getMessages() as $message) {
                $strErrors.= $message->getMessage();
                $strErrors.= ". ";
            }
            throw new \Exception("Conflict:{$strErrors}", 409);
        }
    }
}
