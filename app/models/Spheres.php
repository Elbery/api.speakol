<?php
class Spheres extends BaseModel {
    protected $id;
    protected $name;
    protected $locale;
    protected $country_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function setLocale($locale) {
        $this->locale = $locale;
        return $this;
    }
    public function setCountryId($country_id) {
        $this->country_id = $country_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getLocale() {
        return $this->locale;
    }
    public function getCountryId() {
        return $this->country_id;
    }
    public function initialize() {
        $this->hasMany('id', 'AdministratorSpheres', 'sphere_id', NULL);
        $this->hasMany('id', 'SphereApps', 'sphere_id', NULL);
        $this->hasMany('id', 'SphereComparisons', 'sphere_id', NULL);
        $this->hasMany('id', 'SphereDebates', 'sphere_id', NULL);
        $this->hasMany('id', 'SphereOpinions', 'sphere_id', NULL);
        $this->hasMany('id', 'SphereStories', 'sphere_id', NULL);
        $this->hasMany('id', 'SphereTags', 'sphere_id', NULL);
    }
}
