<?php
class Notifications extends BaseModel {
    protected $id;
    protected $notification_type;
    protected $notifiable_id;
    protected $notifiable_type;
    protected $url;
    protected $user_id;
    protected $created_at;
    protected $extra_info;
    protected $updated_at;
    protected $admin_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setNotificationType($notification_type) {
        $this->notification_type = $notification_type;
        return $this;
    }
    public function setNotifiableId($notifiable_id) {
        $this->notifiable_id = $notifiable_id;
        return $this;
    }
    public function setNotifiableType($notifiable_type) {
        $this->notifiable_type = $notifiable_type;
        return $this;
    }
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    public function setExtraInfo($extra_info) {
        $this->extra_info = $extra_info;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function set_admin_at($app_id) {
        $this->admin_at = $app_id;
    }
    public function getId() {
        return $this->id;
    }
    public function getNotificationType() {
        return $this->notification_type;
    }
    public function getNotifiableId() {
        return $this->notifiable_id;
    }
    public function getNotifiableType() {
        return $this->notifiable_type;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getUserId() {
        return $this->user_id;
    }
    public function getExtraInfo() {
        return $this->extra_info;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function get_admin_at() {
        return $this->admin_at;
    }
    public function initialize() {
        $this->hasMany('id', 'NotificationActors', 'notification_id', NULL);
        $this->hasMany('id', 'NotificationRecipients', 'notification_id', NULL);
        $this->belongsTo('user_id', 'Users', 'id', NULL);
    }
    public function insertNotification($params) {
        $this->setNotificationType($params['notification_type']);
        $this->setNotifiableId($params['notifiable_id']);
        $this->setNotifiableType($params['notifiable_type']);
        $this->setUserId($params['user_id']);
        $this->setUrl($params['url']);
        $this->setExtraInfo($params['extra_info']);
        $this->set_admin_at($params['admin_at']);
        if ($this->save()) {
            $notificationRecipients = new NotificationRecipients();
            $notificationRecipients->setUserId($params['recipient_id']);
            $notificationRecipients->setSeen(0);
            $notificationRecipients->setNotificationId($this->id);
            $notificationRecipients->save();
        }
        return false;
    }
}
