<?php
class Opinions extends BaseModel {
    const MUTABLE_TYPE = 'opinion';
    protected $id;
    protected $title;
    protected $slug;
    protected $featured;
    protected $content;
    protected $charset;
    protected $category_id;
    protected $owner_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function setSlug($slug) {
        $this->slug = preg_replace("/[^\w]+/", "-", $slug);
        return $this;
    }
    public function setFeatured($featured) {
        $this->featured = $featured;
        return $this;
    }
    public function setContent($content) {
        $this->content = $content;
        return $this;
    }
    public function setCharset($charset) {
        $this->charset = $charset;
        return $this;
    }
    public function setCategoryId($category_id) {
        $this->category_id = $category_id;
        return $this;
    }
    public function setOwnerId($owner_id) {
        $this->owner_id = $owner_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getSlug() {
        return $this->slug;
    }
    public function getFeatured() {
        return $this->featured;
    }
    public function getContent() {
        return $this->content;
    }
    public function getCharset() {
        return $this->charset;
    }
    public function getCategoryId() {
        return $this->category_id;
    }
    public function getOwnerId() {
        return $this->owner_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->hasMany('id', 'OpinionFollows', 'opinion_id', NULL);
        $this->hasMany('id', 'SphereOpinions', 'opinion_id', NULL);
        $this->belongsTo('owner_id', 'Users', 'id', NULL);
        $this->belongsTo('category_id', 'Categories', 'id', NULL);
    }
    public function retriveOpinion($slug, $userId) {
        $results = Opinions::findFirst(array("conditions" => "slug= :slug:", "bind" => array('slug' => $slug)));
        if ($results) {
            $return = array();
            $diUrl = $this->getDI()->get('url');
            $followed = OpinionFollows::followed($results->getId(), $userId);
            $sidesQuery = array('parent_id = :parent_id:', 'bind' => array('parent_id' => $results->getId()));
            $sides = Sides::find($sidesQuery);
            $sidesArr = array();
            foreach ($sides as $side) {
                $argQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $argsObj = SidesArguments::find($argQuery);
                $args = array();
                foreach ($argsObj as $arg) {
                    $repliesCountQuery = array('argument_id = :argument_id:', 'bind' => array('argument_id' => $arg->Arguments->getId()));
                    $repliesCount = Replies::count($repliesCountQuery);
                    $thumbsUpQuery = array('argumnet_id = :argumnet_id: AND thumb_up = :thumb_up:', 'bind' => array('argumnet_id' => $arg->Arguments->getId(), 'thumb_up' => '1'));
                    $thumbsUpDown = array('argumnet_id = :argumnet_id: AND thumb_up = :thumb_up:', 'bind' => array('argumnet_id' => $arg->Arguments->getId(), 'thumb_up' => '-1'));
                    $thumbsUp = ArgumentThumbs::count($thumbsUpQuery);
                    $thumbsDown = ArgumentThumbs::count($thumbsUpDown);
                    $args[] = array('id' => $arg->getArgumnetId(), 'context' => $arg->Arguments->getContext(), 'replies_count' => $repliesCount, 'thumbs_up' => $thumbsUp, 'thumbs_down' => $thumbsDown, 'voted' => false, 'attached_photos' => AttachedPhotos::attachedImages($arg->Arguments->getOwnerId()), 'owner' => Users::getOwnerInformation($arg->Arguments->getOwnerId()));
                }
                $tagsQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $tagsResults = SidesTags::find($tagsQuery);
                $tags = array();
                if ($tagsResults) {
                    foreach ($tagsResults as $tag) {
                        $tags[] = array('id' => $tag->tags->getId(), 'name' => $tag->tags->getName(), 'slug' => $tag->tags->getSlug());
                    }
                }
                $voteQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $sidesArr[] = array('id' => $side->getId(), 'votes_count' => Votes::count($voteQuery), 'owner' => Users::getOwnerInformation($side->getOwnerId()), 'arguments' => $args, 'tags' => $tags);
            }
            $sideAttach = OpinionsAttachments::find(array('opinion_id = :opinion_id:', 'bind' => array('opinion_id' => $results->getId())));
            $sideAttached = array();
            foreach ($sideAttach as $attchment) {
                $sideAttached[] = array('id' => $attchment->AttachedPhotos->getId(), 'url' => $attchment->AttachedPhotos->getUrl());
            }
            $return[] = array('id' => $results->getId(), 'title' => $results->getTitle(), 'content' => $results->getContent(), 'slug' => $results->getSlug(), 'link' => $diUrl->get("opinion/" . $results->getSlug()), 'created_at' => $results->getCreatedAt(), 'updated_at' => $results->getUpdatedAt(), 'followed' => $followed, 'owner' => Users::getOwnerInformation($userId), 'attached_photos' => $sideAttached, 'sides' => $sidesArr);
        }
        return array('status' => 'OK', 'data' => $return);
    }
    public function listOpinions($results = array()) {
        if (empty($results)) {
            $results = Opinions::find();
        }
        if ($results) {
            $return = array();
            foreach ($results as $row) {
                $sidesQuery = array('parent_id = :parent_id:', 'bind' => array('parent_id' => $row->getId()));
                $sides = Sides::find($sidesQuery);
                $sidesArr = array();
                $totalArgs = 0;
                foreach ($sides as $side) {
                    $argsNumbers = SidesArguments::count(array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId())));
                    $voteQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                    $sidesArr[] = array('id' => $side->getId(), 'votes_count' => $argsNumbers);
                    $totalArgs+= $argsNumbers;
                }
                $totalArgs = $totalArgs ? $totalArgs : 1;
                foreach ($sidesArr as $key => $side) {
                    $sidesArr[$key]['percentage'] = number_format(($side['votes_count'] / $totalArgs) * 100, 2, '.', '') . '%';
                }
                $return[] = array('id' => $row->getId(), 'title' => $row->getTitle(), 'content' => $row->getContent(), 'slug' => $row->getSlug(), 'link' => $this->getDI()->get('url')->get("opinion/" . $row->getSlug()), 'created_at' => $row->getCreatedAt(), 'updated_at' => $row->getUpdatedAt(), 'sides' => $sidesArr, 'total_arguments' => $totalArgs);
            }
        }
        return array('status' => 'OK', 'data' => $return);
    }
    public function appendDataToModel($data, $update = false) {
        if (false === $update) {
            $this->setTitle($data['title']);
            $this->setContent($data['content']);
            $this->setSlug($data['title']);
            $this->setCategoryId($data['category_id']);
            $this->setOwnerId($data['owner_id']);
            $this->setFeatured($data['featured']);
        } else {
            $this->setSlug($data['slug']);
        }
        $opinion = Opinions::findFirst(array('slug= :slug:', 'bind' => array('slug' => $this->getSlug())));
        $transactionManager = new Phalcon\Mvc\Model\Transaction\Manager();
        $transaction = $transactionManager->get();
        if (!$opinion && $this->save()) {
            $side = new Sides();
            $side->setParentId($this->getId());
            $side->setParentType(static ::MUTABLE_TYPE);
            $side->setOwnerId($data['owner_id']);
            $sideTwo = new Sides();
            $sideTwo->setParentId($this->getId());
            $sideTwo->setParentType(static ::MUTABLE_TYPE);
            $sideTwo->setOwnerId($data['owner_id']);
            if ($side->save()) {
                $sideTwo->save();
                $sidesIds = array('side_one' => $side->getId(), 'side_two' => $sideTwo->getId());
                $data['tags'] = !empty($data['tags']) ? $data['tags'] : false;
                $sideModel = new Sides();
                $sideModel->createSideDependancies($sidesIds, $data['owner_id'], $data['tags'], $transaction, $this->getId(), Opinions::MUTABLE_TYPE);
                $transaction->commit();
                return array('status' => 'OK', 'data' => array($this->getId()));
            } else {
                $transaction->rollback("Can't save Side");
                return array('status' => 'ERROR', 'messages' => 'Rejected!');
            }
            $attachedPhotos = new AttachedPhotos();
            $attachedPhotos->saveAttachedPhotos($this->getId(), $data['app_id'], $transaction, static ::TYPE);
        } else {
            if ($update) {
                $opinion->setTitle($data['title']);
                $opinion->setContent($data['content']);
                if ($opinion->update()) {
                    $transaction->commit();
                    return array('status' => 'OK', 'data' => array($opinion->getId()));
                }
                $transaction->rollback("Can't update");
                return array('status' => 'ERROR', 'messages' => 'Rejected!');
            } else {
                $transaction->rollback("Rejected");
                return array('status' => 'ERROR', 'messages' => 'Rejected!');
            }
        }
    }
    public function appendDataToModelDelete(array $data = array()) {
        try {
            if (empty($data)) {
                return array('status' => 'ERROR', 'messages' => 'Rejected!');;
            }
            $transactionManager = new Phalcon\Mvc\Model\Transaction\Manager();
            $transaction = $transactionManager->get();
            $opinion = Opinions::findFirst(array('slug= :slug:', 'bind' => array('slug' => $data['slug'])));
            if ($opinion) {
                $sides = Sides::find(array('parent_id= :parent_id: AND parent_type = :parent_type: ', 'bind' => array('parent_id' => $opinion->getId(), 'parent_type' => static ::MUTABLE_TYPE)));
                if ($sides) {
                    $sideModel = new Sides();
                    $sideModel->deleteSideDependancies($sides, $transaction);
                    $sides->delete();
                } else {
                    $transaction->rollback("Can't find side");
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
                $attachmentsResults = OpinionsAttachments::find(array('opinion_id= :opinion_id:', 'bind' => array('opinion_id' => $opinion->getId())));
                if ($attachmentsResults) {
                    $attachments = new AttachedPhotos();
                    $attachments->deleteAttachedPhotos($attachmentsResults, $transaction);
                }
                if ($opinion->delete()) {
                    $transaction->commit();
                    return array('status' => 'OK', 'data' => array(true));
                } else {
                    $transaction->rollback("Can't delete opinion");
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
            } else {
                $transaction->rollback("Can't find opinion");
                return array('status' => 'ERROR', 'messages' => 'Rejected!');
            }
        }
        catch(Phalcon\Mvc\Model\Transaction\Failed $e) {
            return array('status' => 'ERROR', 'messages' => $e->getMessage());
        }
    }
}
