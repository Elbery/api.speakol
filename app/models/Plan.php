<?php
class Plan extends BaseModel {
    protected $id;
    protected $name;
    protected $amount;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_name($name) {
        $this->name = $name;
    }
    public function set_amount($amount) {
        $this->amount = $amount;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_name() {
        return $this->name;
    }
    public function get_amount() {
        return intval($this->amount);
    }
}
