<?php
class SphereComparisons extends BaseModel {
    protected $id;
    protected $comparison_id;
    protected $sphere_id;
    protected $featured;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setComparisonId($comparison_id) {
        $this->comparison_id = $comparison_id;
        return $this;
    }
    public function setSphereId($sphere_id) {
        $this->sphere_id = $sphere_id;
        return $this;
    }
    public function setFeatured($featured) {
        $this->featured = $featured;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getComparisonId() {
        return $this->comparison_id;
    }
    public function getSphereId() {
        return $this->sphere_id;
    }
    public function getFeatured() {
        return $this->featured;
    }
    public function initialize() {
        $this->belongsTo('sphere_id', 'Spheres', 'id', NULL);
        $this->belongsTo('comparison_id', 'Comparisons', 'id', NULL);
    }
}
