<?php
class StoryFollows extends BaseModel {
    protected $id;
    protected $story_id;
    protected $follower_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setStoryId($story_id) {
        $this->story_id = $story_id;
        return $this;
    }
    public function setFollowerId($follower_id) {
        $this->follower_id = $follower_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getStoryId() {
        return $this->story_id;
    }
    public function getFollowerId() {
        return $this->follower_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('story_id', 'Stories', 'id', NULL);
        $this->belongsTo('follower_id', 'Users', 'id', NULL);
    }
    public function appendDatatoFollowModel($data, $delete = false) {
        $story = Stories::findFirst(array('slug= :slug:', 'bind' => array('slug' => $data['slug'])));
        $user = Users::findFirst($data['follower_id']);
        if ($story && $user) {
            if ($delete) {
                $deleteQuery = array('story_id= :story_id: AND follower_id = :follower_id:', 'bind' => array('story_id' => $story->getId(), 'follower_id' => $user->getId()));
                $unfollow = $this->findFirst($deleteQuery);
                if ($unfollow->delete()) {
                    return array('status' => 'OK', 'data' => array(true));
                } else {
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
            } else {
                $this->setStoryId($story->getId());
                $this->setFollowerId($user->getId());
                if ($this->save()) {
                    return array('status' => 'OK', 'data' => array($this->getId()));
                } else {
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
            }
        } else {
            return array('status' => 'ERROR', 'messages' => 'Rejected!');
        }
    }
    public function getFollowers(array $data = array()) {
        $story = Stories::findFirst(array('slug= :slug:', 'bind' => array('slug' => $data['slug'])));
        $followers = $this->find(array('story_id= :story_id:', 'bind' => array('story_id' => $story->getId())));
        $return = array();
        if ($followers) {
            foreach ($followers as $follower) {
                $return[] = Users::getOwnerInformation($follower->getFollowerId());
            }
            return array('status' => 'OK', 'data' => array('followers' => $return));
        } else {
            return array('status' => 'OK', 'data' => array('followers' => $return));
        }
    }
    public static function followed($storyId, $userId) {
        $followedQuery = array('story_id = :story_id: AND follower_id = :follower_id:', 'bind' => array('story_id' => $storyId, 'follower_id' => $userId));
        return static ::count($followedQuery);
    }
}
