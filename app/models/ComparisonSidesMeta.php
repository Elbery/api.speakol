<?php
class ComparisonSidesMeta extends BaseModel {
    protected $id;
    protected $side_id;
    protected $title;
    protected $image;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setSideId($side_id) {
        $this->side_id = $side_id;
        return $this;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getTitle() {
        return htmlspecialchars($this->title);
    }
    public function getImage() {
        return $this->image;
    }
    public function getDescription() {
        return htmlspecialchars($this->description);
    }
    public function initialize() {
        $this->belongsTo('side_id', 'Sides', 'id');
    }
    public static function getMetaBySideID($_sideId) {
        return (self::find(array("side_id = :side_id:", 'bind' => array('side_id' => $_sideId)))->toArray());
    }
}
