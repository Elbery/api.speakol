<?php
class HighlightedPlugins extends BaseModel {
    protected $id;
    protected $plugin_id;
    protected $plugin_type;
    protected $lang;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_plugin_id($plugin_id) {
        $this->plugin_id = $plugin_id;
    }
    public function set_plugin_type($plugin_type) {
        $this->plugin_type = $plugin_type;
    }
    public function set_lang($lang) {
        $this->lang = $lang;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_plugin_id() {
        return $this->plugin_id;
    }
    public function get_plugin_type() {
        return $this->plugin_type;
    }
    public function get_lang($lang) {
        $this->lang = $lang;
    }
}
