<?php
class Views extends BaseModel {
    protected $id;
    protected $viewable_type;
    protected $viewable_id;
    protected $ip;
    protected $viewer_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setViewableType($viewable_type) {
        $this->viewable_type = $viewable_type;
        return $this;
    }
    public function setViewableId($viewable_id) {
        $this->viewable_id = $viewable_id;
        return $this;
    }
    public function setIp($ip) {
        $this->ip = $ip;
        return $this;
    }
    public function setViewerId($viewer_id) {
        $this->viewer_id = $viewer_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getViewableType() {
        return $this->viewable_type;
    }
    public function getViewableId() {
        return $this->viewable_id;
    }
    public function getIp() {
        return $this->ip;
    }
    public function getViewerId() {
        return $this->viewer_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('viewer_id', 'Users', 'id', NULL);
    }
}
