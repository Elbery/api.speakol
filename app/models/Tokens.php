<?php
class Tokens extends BaseModel {
    protected $_roles = array('1' => 'Administrator', '2' => 'Moderator', '3' => 'Application', '4' => 'User', '5' => 'Guest',);
    protected $id;
    protected $token;
    protected $secret;
    protected $enabled;
    protected $weight;
    protected $created_at;
    protected $updated_at;
    protected $user_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setToken($token) {
        $this->token = $token;
        return $this;
    }
    public function setSecret($secret) {
        $this->secret = $secret;
        return $this;
    }
    public function setEnabled($enabled) {
        $this->enabled = $enabled;
        return $this;
    }
    public function setWeight($weight) {
        $this->weight = $weight;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getToken() {
        return $this->token;
    }
    public function getSecret() {
        return $this->secret;
    }
    public function getEnabled() {
        return $this->enabled;
    }
    public function getWeight() {
        return $this->weight;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function getUserId() {
        return $this->user_id;
    }
    public function initialize() {
        $this->belongsTo('user_id', 'Users', 'id', NULL);
    }
    public function registerSession($_user) {
        $this->setUserId($_user->getId());
        $this->setEnabled(1);
        $this->setSecret(Tokens::_genToken());
        $this->setToken(Tokens::_genTokenKey());
        $this->setWeight(0);
        $this->setCreatedAt(date('Y-m-d H:i:s'));
        $this->setUpdatedAt(date('Y-m-d H:i:s'));
        if ($this->save()) {
            return $this->getToken();
        } else {
            return false;
        }
    }
    private static function _genToken() {
        return sha1(uniqid(time(), true));
    }
    private static function _genTokenKey() {
        return sha1(uniqid(time(), true));
    }
    public static function checkAccess() {
        $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
        if (!array_key_exists('Authorization', $sentHeaders) || empty($sentHeaders['Authorization'])) {
            return FALSE;
        }
        $oToken = Tokens::findFirstByToken($sentHeaders['Authorization']);
        if (FALSE == $oToken) {
            return FALSE;
        }
        if ((time() - strtotime($oToken->getCreatedAt())) > (2 * 60 * 60)) {
        }
        return $oToken->getUserId();
    }
    public static function getSignedInUserId() {
        $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
        if (!isset($sentHeaders['Authorization'])) {
            return false;
        }
        $oToken = Tokens::findFirstByToken($sentHeaders['Authorization']);
        if ($oToken != FALSE) {
            return $oToken->getUserId();
        } else {
        }
    }
    public static function isCurrentUserIs($role = array()) {
        $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
        if (!isset($sentHeaders['Authorization'])) {
            return false;
        }
        $oToken = static ::find(array('token = :token:', 'bind' => array('token' => $sentHeaders['Authorization'])));
        if (empty($oToken['0'])) {
            return false;
        }
        $o = $oToken[0]->toArray();
        if (!empty($o)) {
            $roleOfUser = $oToken['0']->Users->getRoleId();
            if (in_array($roleOfUser, $role)) {
                return $roleOfUser;
            }
        } else {
            return false;
        }
    }
}
