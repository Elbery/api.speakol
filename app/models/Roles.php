<?php
class Roles extends \Phalcon\Mvc\Model {
    const SUPER_ADMIN = 1;
    const ADMIN = 2;
    const APP = 3;
    const USER = 4;
    const GUEST = 5;
    protected $id;
    protected $name;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function columnMap() {
        return array('id' => 'id', 'name' => 'name', 'created_at' => 'created_at', 'updated_at' => 'updated_at');
    }
    public function initialize() {
        $this->hasMany('id', 'Users', 'role_id', NULL);
        $this->hasMany('id', 'RolesActivities', 'role_id', NULL);
    }
    public function getRoleIdByName($_name) {
        return ($this->getDI()->getShared('modelsManager')->executeQuery("SELECT id FROM Roles WHERE name = :name:", array('name' => $_name))->getFirst()->id);
    }
    public static function getGuestRoleID() {
        $role = Roles::findFirst("name = 'guest'");
        return intval($role->getId());
    }
}
