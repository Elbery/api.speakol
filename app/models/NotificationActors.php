<?php
class NotificationActors extends BaseModel {
    protected $id;
    protected $actor_id;
    protected $notification_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setActorId($actor_id) {
        $this->actor_id = $actor_id;
        return $this;
    }
    public function setNotificationId($notification_id) {
        $this->notification_id = $notification_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getActorId() {
        return $this->actor_id;
    }
    public function getNotificationId() {
        return $this->notification_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('notification_id', 'Notifications', 'id', NULL);
    }
}
