<?php
class SphereTags extends BaseModel {
    protected $id;
    protected $tag_id;
    protected $sphere_id;
    protected $featured;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setTagId($tag_id) {
        $this->tag_id = $tag_id;
        return $this;
    }
    public function setSphereId($sphere_id) {
        $this->sphere_id = $sphere_id;
        return $this;
    }
    public function setFeatured($featured) {
        $this->featured = $featured;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getTagId() {
        return $this->tag_id;
    }
    public function getSphereId() {
        return $this->sphere_id;
    }
    public function getFeatured() {
        return $this->featured;
    }
    public function initialize() {
        $this->belongsTo('tag_id', 'Tags', 'id', NULL);
        $this->belongsTo('sphere_id', 'Spheres', 'id', NULL);
    }
}
