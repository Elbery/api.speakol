<?php
class AppsArguments extends \Phalcon\Mvc\Model {
    protected $id;
    protected $app_id;
    protected $argument_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setArgumentId($argument_id) {
        $this->argument_id = $argument_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getArgumentId() {
        return $this->argument_id;
    }
    public function columnMap() {
        return array('id' => 'id', 'app_id' => 'app_id', 'argument_id' => 'argument_id');
    }
}
