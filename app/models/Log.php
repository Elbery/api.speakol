<?php
class Log extends BaseModel {
    protected $id;
    protected $user_id;
    protected $ip;
    protected $operation_id;
    protected $operation_type;
    protected $created_at;
    public function set_id($id) {
        $this->id = $id;
        return $this;
    }
    public function set_user_id($user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    public function set_ip($ip) {
        $this->ip = $ip;
        return $this;
    }
    public function set_operation_id($operation_id) {
        $this->operation_id = $operation_id;
        return $this;
    }
    public function set_operation_type($operation_type) {
        $this->operation_type = $operation_type;
        return $this;
    }
    public function set_created_at($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_user_id() {
        return $this->user_id;
    }
    public function get_ip() {
        return $this->ip;
    }
    public function get_operation_id() {
        return $this->operation_id;
    }
    public function get_operation_type() {
        return $this->operation_type;
    }
    public function get_created_at() {
        return $this->created_at;
    }
    public function log_current_action($user_id, $ip, $operation_type, $operation_id) {
        $this->set_user_id($user_id);
        $this->set_operation_id($operation_id);
        $this->set_operation_type($operation_type);
        $this->set_created_at(date('Y-m-d H:i:s'));
        $this->set_ip($ip);
        $this->save();
    }
}
