<?php
class SendingEmailsLog extends BaseModel {
    protected $id;
    protected $email_type;
    protected $date;
    protected $end_date;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_email_type($email_type) {
        $this->email_type = $email_type;
    }
    public function set_date($date) {
        $this->date = $date;
    }
    public function set_end_date($date) {
        $this->end_date = $date;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_email_type() {
        return $this->email_type;
    }
    public function get_date() {
        return $this->date;
    }
    public function get_end_date() {
        return $this->end_date;
    }
}
