<?php
class FavoriteCategories extends BaseModel {
    protected $id;
    protected $user_id;
    protected $category_id;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_user_id($user_id) {
        $this->user_id = $user_id;
    }
    public function set_category_id($category_id) {
        $this->category_id = $category_id;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_user_id() {
        return $this->user_id;
    }
    public function get_category_id() {
        return $this->category_id;
    }
    public static function get_favorite_categories($user_id) {
        $categories = FavoriteCategories::find(array('columns' => 'category_id', 'conditions' => 'user_id=' . $user_id));
        $categories_list = array();
        foreach ($categories as $cat) {
            array_push($categories_list, $cat['category_id']);
        }
        return $categories_list;
    }
}
