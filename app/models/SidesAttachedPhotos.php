<?php
class SidesAttachedPhotos extends BaseModel {
    protected $id;
    protected $side_id;
    protected $image;
    protected $url;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setSideId($side_id) {
        $this->side_id = $side_id;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getImage() {
        return $this->image;
    }
    public function getUrl() {
        return $this->url;
    }
    public function initialize() {
        $this->belongsTo('side_id', 'sides', 'id');
    }
}
