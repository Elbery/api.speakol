<?php
class ReplyPhotos extends \Phalcon\Mvc\Model {
    protected $id;
    protected $image;
    protected $url;
    protected $reply_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    public function setReplyId($reply_id) {
        $this->reply_id = $reply_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getImage() {
        return $this->image;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getReplyId() {
        return $this->reply_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('reply_id', 'Replies', 'id', NULL);
    }
}
