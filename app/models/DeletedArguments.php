<?php
class DeletedArguments extends BaseModel {
    protected $id;
    protected $side_id;
    protected $argument_id;
    protected $created_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setSideId($side_id) {
        $this->side_id = $side_id;
        return $this;
    }
    public function setArgumentId($argument_id) {
        $this->argument_id = $argument_id;
        return $this;
    }
    public function setCreatedAt($creation_time) {
        $this->created_at = $creation_time;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getArgumentId() {
        return $this->argument_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public static function add_deleted_argument($side_id, $argument_id) {
        try {
            $deleted_argument = new DeletedArguments();
            $deleted_argument->setSideId($side_id);
            $deleted_argument->setArgumentId($argument_id);
            $deleted_argument->setCreatedAt(new DATETIME());
            $deleted_argument->save();
        }
        catch(Exception $ex) {
        }
    }
}
