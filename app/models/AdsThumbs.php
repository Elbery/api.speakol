<?php
class AdsThumbs extends BaseModel {
    protected $id;
    protected $ad_id;
    protected $thumber_id;
    protected $thumb_up;
    protected $created_at;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_ad_id($ad_id) {
        $this->ad_id = $ad_id;
    }
    public function set_thumber_id($thumber_id) {
        $this->thumber_id = $thumber_id;
    }
    public function set_thumb_up($up) {
        $this->thumb_up = $up;
    }
    public function set_created_at($time) {
        $this->created_at = $time;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_ad_id() {
        return $this->ad_id;
    }
    public function get_thumber_id() {
        return $this->thumber_id;
    }
    public function get_thumb_up() {
        return $this->thumb_up;
    }
    public function get_created_at() {
        return $this->created_at;
    }
}
