<?php
class OpinionFollows extends BaseModel {
    protected $id;
    protected $opinion_id;
    protected $follower_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setOpinionId($opinion_id) {
        $this->opinion_id = $opinion_id;
        return $this;
    }
    public function setFollowerId($follower_id) {
        $this->follower_id = $follower_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getOpinionId() {
        return $this->opinion_id;
    }
    public function getFollowerId() {
        return $this->follower_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('follower_id', 'Users', 'id', NULL);
        $this->belongsTo('opinion_id', 'Opinions', 'id', NULL);
    }
    public function appendDatatoFollowModel($data, $delete = false) {
        $opinion = Opinions::findFirst(array('slug= :slug:', 'bind' => array('slug' => $data['slug'])));
        $user = Users::findFirst($data['follower_id']);
        if ($opinion && $user) {
            if ($delete) {
                $deleteQuery = array('opinion_id= :opinion_id: AND follower_id = :follower_id:', 'bind' => array('opinion_id' => $opinion->getId(), 'follower_id' => $user->getId()));
                $unfollow = $this->findFirst($deleteQuery);
                if ($unfollow->delete()) {
                    return array('status' => 'OK', 'data' => array(true));
                } else {
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
            } else {
                $this->setOpinionId($opinion->getId());
                $this->setFollowerId($user->getId());
                if ($this->save()) {
                    return array('status' => 'OK', 'data' => array($this->getId()));
                } else {
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
            }
        } else {
            return array('status' => 'ERROR', 'messages' => 'Rejected!');
        }
    }
    public function getFollowers(array $data = array()) {
        $opinion = Opinions::findFirst(array('slug= :slug:', 'bind' => array('slug' => $data['slug'])));
        $followers = $this->find(array('opinion_id= :opinion_id:', 'bind' => array('opinion_id' => $opinion->getId())));
        $return = array();
        if ($followers) {
            foreach ($followers as $follower) {
                $return[] = Users::getOwnerInformation($follower->getFollowerId());
            }
            return array('status' => 'OK', 'data' => array('followers' => $return));
        } else {
            return array('status' => 'OK', 'data' => array('followers' => $return));
        }
    }
    public static function followed($opinionId, $userId) {
        $followedQuery = array('opinion_id = :opinion_id: AND follower_id = :follower_id:', 'bind' => array('opinion_id' => $opinionId, 'follower_id' => $userId));
        return static ::count($followedQuery);
    }
}
