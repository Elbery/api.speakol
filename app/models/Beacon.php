<?php
use Phalcon\DI\FactoryDefault;
class Beacon extends BaseModel {
    protected $id;
    protected $user_id;
    protected $last_time;
    public function setId($id) {
        $this->id = $id;
    }
    public function setUserId($user_id) {
        $this->user_id = $user_id;
    }
    public function setLastTime($time) {
        $this->last_time = $time;
    }
    public function getId() {
        return $this->id;
    }
    public function getUserId() {
        return $this->user_id;
    }
    public function getLastTime() {
        return $this->last_time;
    }
    public function addBeacon($user_id, $time) {
        $user_exist = self::findFirst(array('user_id=:user_id:', 'bind' => array('user_id' => $user_id)));
        if ($user_exist) {
            $user_exist->setLastTime($time);
            $user_exist->update();
        } else {
            $this->setUserId($user_id);
            $this->setLastTime($time);
            $this->save();
        }
    }
    public static function user_status($user_id) {
        $db = FactoryDefault::getDefault()->getShared('db');
        $db->query("SET time_zone = '+02:00'");
        $online_user = self::findFirst(array('user_id=:user_id: AND TIME_TO_SEC(TIMEDIFF(now() , last_time)) < 100', 'bind' => array('user_id' => $user_id)));
        if ($online_user) {
            return 1;
        } else {
            return 0;
        }
    }
    public static function get_users($sides, $argument_id) {
        $di = FactoryDefault::getDefault();
        if (sizeof($sides) > 1) {
            for ($i = 0;$i < sizeof($sides);$i++) {
                $side_exist = Sides::findFirst($sides[$i]);
                if (!$side_exist) {
                    return array("status" => "ERROR", "message" => "Side id " . $sides[$i] . " doesn't exist");
                }
                $side_condition.= $sides[$i] . ",";
            }
            $side_condition = substr($side_condition, 0, -1);
            try {
                $offset = ($page == 1) ? 0 : ($page - 1) * $limit;
                $connection = mysql_connect($di->getShared('config')->get('database')->get('host'), $di->getShared('config')->get('database')->get('username'), $di->getShared('config')->get('database')->get('password'));
                if (!$connection) {
                    die('Could not connect: ' . mysql_error());
                }
                mysql_select_db($di->getShared('config')->get('database')->get('dbname'), $connection);
                mysql_query("SET time_zone = '+02:00'");
                $query_result = mysql_query("SELECT owner_id,id FROM arguments WHERE side_id IN(" . $side_condition . ") GROUP BY owner_id");
                $ids = array();
                while ($r = mysql_fetch_array($query_result)) {
                    array_push($ids, $r['owner_id']);
                }
                if ($argument_id != "") {
                    $query_result = mysql_query("SELECT user_id FROM replies WHERE argument_id=" . $argument_id . " GROUP BY user_id");
                    while ($r = mysql_fetch_array($query_result)) {
                        if (!in_array($r['user_id'], $ids)) {
                            array_push($ids, $r['user_id']);
                        }
                    }
                }
                $all_users = array();
                $ids_condition = '';
                for ($i = 0;$i < sizeof($ids);$i++) {
                    $ids_condition.= $ids[$i] . ",";
                }
                $ids_condition = substr($ids_condition, 0, -1);
                if (strlen($ids_condition)) {
                    $query_result = mysql_query("SELECT id,user_id FROM beacon WHERE user_id IN(" . $ids_condition . ") AND TIME_TO_SEC(TIMEDIFF(now() , last_time)) < 100");
                    while ($r = mysql_fetch_array($query_result)) {
                        array_push($all_users, $r['user_id']);
                    }
                }
                mysql_close($connection);
                return array("status" => "ok", "data" => $all_users);
            }
            catch(Exception $ex) {
                return array("status" => "ERROR", "message" => "Error has happened");
            }
        } else {
            return array("status" => "ERROR", "message" => "Empty array of sides");
        }
    }
}
