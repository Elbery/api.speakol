<?php
use Phalcon\Mvc\Model\Validator\Email as Email;
class Administrators extends BaseModel {
    protected $id;
    protected $name;
    protected $super_admin;
    protected $email;
    protected $encrypted_password;
    protected $reset_password_token;
    protected $reset_password_sent_at;
    protected $remember_created_at;
    protected $sign_in_count;
    protected $current_sign_in_at;
    protected $last_sign_in_at;
    protected $current_sign_in_ip;
    protected $last_sign_in_ip;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function setSuperAdmin($super_admin) {
        $this->super_admin = $super_admin;
        return $this;
    }
    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }
    public function setEncryptedPassword($encrypted_password) {
        $this->encrypted_password = $encrypted_password;
        return $this;
    }
    public function setResetPasswordToken($reset_password_token) {
        $this->reset_password_token = $reset_password_token;
        return $this;
    }
    public function setResetPasswordSentAt($reset_password_sent_at) {
        $this->reset_password_sent_at = $reset_password_sent_at;
        return $this;
    }
    public function setRememberCreatedAt($remember_created_at) {
        $this->remember_created_at = $remember_created_at;
        return $this;
    }
    public function setSignInCount($sign_in_count) {
        $this->sign_in_count = $sign_in_count;
        return $this;
    }
    public function setCurrentSignInAt($current_sign_in_at) {
        $this->current_sign_in_at = $current_sign_in_at;
        return $this;
    }
    public function setLastSignInAt($last_sign_in_at) {
        $this->last_sign_in_at = $last_sign_in_at;
        return $this;
    }
    public function setCurrentSignInIp($current_sign_in_ip) {
        $this->current_sign_in_ip = $current_sign_in_ip;
        return $this;
    }
    public function setLastSignInIp($last_sign_in_ip) {
        $this->last_sign_in_ip = $last_sign_in_ip;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getSuperAdmin() {
        return $this->super_admin;
    }
    public function getEmail() {
        return $this->email;
    }
    public function getEncryptedPassword() {
        return $this->encrypted_password;
    }
    public function getResetPasswordToken() {
        return $this->reset_password_token;
    }
    public function getResetPasswordSentAt() {
        return $this->reset_password_sent_at;
    }
    public function getRememberCreatedAt() {
        return $this->remember_created_at;
    }
    public function getSignInCount() {
        return $this->sign_in_count;
    }
    public function getCurrentSignInAt() {
        return $this->current_sign_in_at;
    }
    public function getLastSignInAt() {
        return $this->last_sign_in_at;
    }
    public function getCurrentSignInIp() {
        return $this->current_sign_in_ip;
    }
    public function getLastSignInIp() {
        return $this->last_sign_in_ip;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function validation() {
        $this->validate(new Email(array('field' => 'email', 'required' => true,)));
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }
    public function initialize() {
        $this->hasMany('id', 'AdministratorSpheres', 'administrator_id', NULL);
    }
}
