<?php
class DebateSidesMeta extends BaseModel {
    protected $id;
    protected $side_id;
    protected $title;
    protected $debater_id;
    protected $image;
    protected $job_title;
    protected $opinion;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setSideId($side_id) {
        $this->side_id = $side_id;
        return $this;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function setDebaterId($debater_id) {
        $this->debater_id = $debater_id;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setJobTitle($job_title) {
        $this->job_title = $job_title;
        return $this;
    }
    public function setOpinion($opinion) {
        $this->opinion = $opinion;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getTitle() {
        return htmlspecialchars($this->title);
    }
    public function getDebaterId() {
        return $this->debater_id;
    }
    public function getImage() {
        return $this->image;
    }
    public function getJobTitle() {
        return htmlspecialchars($this->job_title);
    }
    public function getOpinion() {
        return htmlspecialchars($this->opinion);
    }
    public function initialize() {
        $this->belongsTo('debater_id', 'Users', 'id');
        $this->belongsTo('side_id', 'Sides', 'id');
    }
    public static function getMetaBySideID($_sideId) {
        $debate_side_meta = DebateSidesMeta::findFirst(array("side_id = :side_id:", 'bind' => array('side_id' => $_sideId)));
        if ($debate_side_meta) {
            return array(array('id' => $debate_side_meta->getId(), 'side_id' => $debate_side_meta->getSideId(), 'title' => $debate_side_meta->getTitle(), 'debater_id' => $debate_side_meta->getDebaterId(), 'image' => $debate_side_meta->getImage(), 'job_title' => $debate_side_meta->getJobTitle(), 'opinion' => $debate_side_meta->getOpinion()));
        }
    }
}
