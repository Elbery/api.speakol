<?php
class ProfilePhotos extends BaseModel {
    protected $id;
    protected $image;
    protected $user_id;
    protected $app_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getImage() {
        return $this->image;
    }
    public function getUserId() {
        return $this->user_id;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('app_id', 'Apps', 'id', NULL);
        $this->belongsTo('user_id', 'Users', 'id', NULL);
    }
}
