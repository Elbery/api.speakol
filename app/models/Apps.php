<?php
use Phalcon\Mvc\Model\Validator\PresenceOf, Phalcon\Mvc\Model\Validator\StringLength, Phalcon\Mvc\Model\Validator\Uniqueness, Phalcon\Mvc\Model\Validator\Email, Phalcon\DI\FactoryDefault, Phalcon\Mvc\Model\Transaction\Manager as TrManager;
class Apps extends BaseModel {
    const UNVERIFIED = 0;
    const VERIFIED = 1;
    protected $id;
    protected $name;
    protected $plugin_slider;
    protected $publisher_id;
    protected $secret;
    protected $redirect_uri;
    protected $website;
    protected $slug;
    protected $verified;
    protected $description;
    protected $category_id;
    protected $country_id;
    protected $image;
    protected $created_at;
    protected $updated_at;
    protected $plan_id;
    protected $subscription_date;
    protected $stripe_id;
    protected $pending_plan;
    protected $newsletter;
    protected $address;
    protected $network;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setPublisherId($publisher_id) {
        $this->publisher_id = $publisher_id;
        return $this;
    }
    public function setSecret($secret) {
        $this->secret = $secret;
        return $this;
    }
    public function setRedirectUri($redirect_uri) {
        $this->redirect_uri = $redirect_uri;
        return $this;
    }
    public function setWebsite($website) {
        $this->website = $website;
        return $this;
    }
    public function setSlug($slug) {
        $this->slug = preg_replace("/[^\w]+/", "-", strtolower($slug));
        return $this;
    }
    public function setVerified($verified) {
        $this->verified = $verified;
        return $this;
    }
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }
    public function setCategoryId($categoryId) {
        $this->category_id = $categoryId;
        return $this;
    }
    public function setCountryId($countryId) {
        $this->country_id = $countryId;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function set_plan($plan_id) {
        $this->plan_id = $plan_id;
    }
    public function set_subscription_date($sub_date) {
        $this->subscription_date = $sub_date;
    }
    public function set_stripe_id($sID) {
        $this->stripe_id = $sID;
    }
    public function set_pending_plan($plan) {
        $this->pending_plan = $plan;
    }
    public function setImage($image) {
        $this->image = $image;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function set_address($address) {
        $this->address = $address;
    }
    public function set_newsletter($newsletter) {
        $this->newsletter = $newsletter;
    }
    public function set_network($network) {
        $this->network = $network;
    }
    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getPluginSlider() {
        return $this->plugin_slider;
    }
    public function getPublisherId() {
        return $this->publisher_id;
    }
    public function getSecret() {
        return $this->secret;
    }
    public function getRedirectUri() {
        return $this->redirect_uri;
    }
    public function getWebsite() {
        return $this->website;
    }
    public function getSlug() {
        return $this->slug;
    }
    public function getVerified() {
        return $this->verified;
    }
    public function getDescription() {
        return $this->description;
    }
    public function getCategoryId() {
        return $this->category_id;
    }
    public function getCountryId() {
        return $this->country_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function get_plan() {
        return intval($this->plan_id);
    }
    public function get_subscription_date() {
        return $this->subscription_date;
    }
    public function get_stripe_id() {
        return $this->stripe_id;
    }
    public function get_pending_plan() {
        return intval($this->pending_plan);
    }
    public function getImage() {
        if ($this->image != "") {
            return $this->image;
        } else {
            return "/uploads/perm/no-img.png";
        }
    }
    public function get_address() {
        return $this->address;
    }
    public function get_newsletter() {
        return intval($this->newsletter);
    }
    public function get_network() {
        return $this->network;
    }
    public function initialize() {
        $this->hasMany('id', 'AppAdmins', 'app_id', NULL);
        $this->hasMany('id', 'AppFollows', 'app_id', NULL);
        $this->hasMany('id', 'ProfilePhotos', 'app_id', NULL);
        $this->hasMany('id', 'SphereApps', 'app_id', NULL);
        $this->hasMany('id', 'Stories', 'app_id', NULL);
        $this->hasMany('id', 'Argumentboxes', 'app_id', NULL);
        $this->hasMany('id', 'Arguments', 'app_id', NULL);
        $this->belongsTo('app_id', 'Categories', 'id', NULL);
        $this->belongsTo('publisher_id', 'Users', 'id', NULL);
        $this->hasMany('id', 'Debates', 'app_id', NULL);
        $this->hasMany('id', 'Comparisons', 'app_id', NULL);
    }
    public function createApp($params) {
        $response = array();
        $update_password = true;
        $name = trim($params['name']);
        $valid_name = preg_match("/^[\p{L}\p{N} -]+$/u", $name);
        if (!$valid_name) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_("invalid-name");
            return $response;
        } else if (strlen($name) < 2 || strlen($name) > 64) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_("user-name-length");
        }
        $plan_id = intval($params['card']->plan_id);
        if ($plan_id < 1 || $plan_id > 4) {
            $response['status'] = 'error';
            $response['message'] = "Invalid plan id";
            return $response;
        }
        $network = ($params['network']) ? $params['network'] : "en";
        $alreadyExist = Users::findFirst(array('email = :email: ', 'bind' => array('email' => $params['email'])));
        if ($alreadyExist) {
            if ($alreadyExist->getActive()) {
                $response['status'] = 'error';
                $response['message'] = FactoryDefault::getDefault()->get('t')->_("apps-email-exist");
                return $response;
            }
            $app_exist = Apps::findFirst('publisher_id=' . $alreadyExist->getId());
            if ($app_exist) {
                $user_card = UserCard::findFirst('app_id=' . $app_exist->getId());
                if ($user_card) {
                    $user_card->delete();
                }
                $app_exist->delete();
            }
            $alreadyExist->delete();
        }
        $email = trim($params['email']);
        $website = trim($params['website']);
        $valid_website = preg_match("/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/", $website);
        if (!$valid_website) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_("invalid-website");
            return $response;
        }
        $validEmail = filter_var($email, FILTER_VALIDATE_EMAIL);
        if ($validEmail == false) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_("user-invalid-email");
            return $response;
        }
        if (strlen($params['password']) < 8 || strlen($params['password']) > 128) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_("password-length");
            return $response;
        }
        if (empty($params['category_id'])) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_("apps-cat-missing");
            return $response;
        }
        if (empty($params['country_id'])) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_("user-country-missing");
            return $response;
        }
        $transactionManager = new TrManager();
        $transaction = $transactionManager->get();
        $di = $this->getDi()->getShared('request');
        if ($params['logo']) {
            $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
            $arrUploadedPhotos = $objImgUploader->uploadImage($objImgUploader::DIR_USERS, $params['logo']);
            $image = $arrUploadedPhotos[0]['path'] . $arrUploadedPhotos[0]['new'];
            $this->setImage($image);
        }
        $user = new Users();
        $user->set_first_name($name);
        $user->set_last_name('');
        $user->setSlug(preg_replace("/[^\w]+/", "-", strtolower($name)) . time());
        $user->setTransaction($transaction);
        $user->setEmail($email);
        $user->setActive(0);
        $user->setEncryptedPassword($params['password'], true);
        $user->setRoleId(Roles::USER);
        $user->set_newsletter(2);
        $user->set_subscribe(1);
        $user->set_init_reg("speakol");
        if ($params['logo']) {
            $user->setProfilePicture($image);
        }
        $user->setConfirmationToken($user->generateConfirmationToken($user->getEmail()));
        $user->setConfirmationSentAt(date('Y-m-d H:i:s', time()));
        if (!$user->save()) {
            $response['status'] = 'error';
            $response['message'] = $user->getMessages();
            return $response;
        }
        $this->setName($name);
        $this->setCategoryId($params['category_id']);
        $this->setCountryId($params['country_id']);
        $this->setWebsite($website);
        $this->setSecret($this->getDi()->getShared('security')->getToken());
        $this->setRedirectUri($params['redirect_uri']);
        $this->setVerified(0);
        $this->set_newsletter(1);
        $this->setPublisherId($user->getId());
        $this->set_plan($plan_id);
        $this->set_network($network);
        if (!$this->save()) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_('apps-save-app-data');
            return $response;
        }
        if ($plan_id !== 1) {
            $billing_obj = new BillingController();
            $add_card = $billing_obj->validate_add_card($params['card'], $this->getId());
            if ($add_card['status'] == "ERROR") {
                $response['status'] = 'error';
                $response['message'] = $add_card['message'];
                return $response;
            }
        }
        $app_domains = new AppDomains();
        $app_domains->set_app_id($this->getId());
        $app_domains->set_domain($params['website']);
        $app_domains->save();
        $www = substr($params['website'], 0, 4);
        $app_domains2 = new AppDomains();
        $app_domains2->set_app_id($this->getId());
        if ($www != "www.") {
            $domain = "www." . $params['website'];
        } else {
            $domain = substr($params['website'], 4, strlen($params['website']) - 1);
        }
        $app_domains2->set_domain($domain);
        $app_domains2->save();
        $appAdmins = new AppAdmins();
        if ($appAdmins->createAppAdmin($this->getId(), $user->getId(), TRUE) == FALSE) {
            $response['status'] = 'ERROR';
            $response['data'] = FactoryDefault::getDefault()->get('t')->_('apps-save-app-admin');
        }
        $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
        $transaction->commit();
        $response['status'] = 'ok';
        $response['message'] = json_encode(array('user_id' => $user->getId(), 'app_id' => $this->getId()));
        return $response;
    }
    public function updateApp($params) {
        $response = array();
        $app = self::findFirst($params['app_id']);
        if (!$app) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_('apps-general-error');
            return $response;
        }
        $owner = Users::findFirst($app->getPublisherId());
        if (!$owner) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_('apps-general-error');
            return $response;
        }
        $params['name'] = trim($params['name']);
        $valid_name = preg_match("/^[\p{L}\p{N} -]+$/u", $params['name']);
        if (empty($params['name'])) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_('apps-name-missing');
            return $response;
        } elseif (!$valid_name) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_("invalid-name");
            return $response;
        } else {
            $owner->set_first_name($params['name']);
            $app->setName($params['name']);
        }
        if (empty($params['category_id'])) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_('apps-cat-missing');
            return $response;
        } else {
            $app->setCategoryId($params['category_id']);
        }
        $params['website'][0] = trim($params['website'][0]);
        $valid_website = preg_match("/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/", $params['website'][0]);
        if (!$valid_website) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_("invalid-website");
            return $response;
        } else {
            $app->setWebsite($params['website'][0]);
        }
        for ($i = 1;$i < count($params['website']);$i++) {
            $params['website'][$i] = trim($params['website'][$i]);
            $valid_website = preg_match("/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/", $params['website'][$i]);
            if (!$valid_website) {
                $response['status'] = 'error';
                $response['message'] = FactoryDefault::getDefault()->get('t')->_("invalid-website");
                return $response;
            }
        }
        $app->set_newsletter($params['newsletter']);
        $owner->setLocale($params['lang']);
        $app->set_network($params['network']);
        if (!empty($params['old_password']) || !empty($params['password'])) {
            if (empty($params['old_password'])) {
                $response['status'] = 'error';
                $response['message'] = FactoryDefault::getDefault()->get('t')->_('apps-old-pass-not-correct');
                return $response;
            }
            $flag = \Phalcon\DI::getDefault()->get('security')->checkHash($params['old_password'], $owner->getEncryptedPassword());
            if (!$flag) {
                $response['status'] = 'error';
                $response['message'] = FactoryDefault::getDefault()->get('t')->_('apps-old-pass-not-correct');
                return $response;
            }
            $owner->setEncryptedPassword($params['password']);
        }
        $di = $this->getDi()->getShared('request');
        $arrPhotos = $di->getUploadedFiles();
        if ($params['logo']) {
            $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
            $arrUploadedPhotos = $objImgUploader->uploadImage($objImgUploader::DIR_APPS, $params['logo']);
            $image = $arrUploadedPhotos[0]['path'] . $arrUploadedPhotos[0]['new'];
            $owner->setProfilePicture($image);
            $app->setImage($image);
        }
        if (!$owner->update()) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_('apps-general-error');
            return $response;
        }
        if (!$app->update()) {
            $response['status'] = 'error';
            $response['message'] = FactoryDefault::getDefault()->get('t')->_('apps-general-error');
            return $response;
        }
        $max_domains = MaxDomains::findFirst('plan_id=' . $app->get_plan());
        $max_number_of_domains = $max_domains->get_no_domains();
        $domains = $params['website'];
        $app_domains = AppDomains::find('app_id=' . $app->getId());
        if ($app_domains) {
            $app_domains->delete();
        }
        $i = 0;
        $available_domains = array();
        if ($domains) {
            $www = substr($params['website'][0], 0, 4);
            foreach ($domains as $domain) {
                $app_domain = new AppDomains();
                $app_domain->set_app_id($app->getId());
                $app_domain->set_domain($domain);
                $app_domain->save();
                $available_domains[] = $domain;
                if ($i == 0) {
                    if ($www != "www.") {
                        $domain = "www." . $domain;
                    } else {
                        $domain = substr($domain, 4, strlen($domain) - 1);
                    }
                    $app_domain = new AppDomains();
                    $app_domain->set_app_id($app->getId());
                    $app_domain->set_domain($domain);
                    $app_domain->save();
                    $available_domains[] = $domain;
                    $i++;
                }
                $i++;
                if ($i == $max_number_of_domains) {
                    break;
                }
            }
        }
        $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
        $new_log = new Log();
        $new_log->log_current_action($owner->getId(), $sentHeaders['Ip'], "/apps/update", $app->getId());
        $appData = array('id' => $app->getId(), 'name' => $app->getName(), 'profile_picture' => $app->getImage(), 'category_id' => $app->getCategoryId(), 'website' => $available_domains, 'slug' => $app->getSlug(), 'active' => $owner->getActive(), 'locale' => $owner->getLocale(), 'newsletter' => $app->get_newsletter(), 'network' => $app->get_network());
        $response['status'] = 'OK';
        $response['data'] = $appData;
        return $response;
    }
    public function showActionHelper($_slug, $page, $perPage, $feedsFilter) {
        $app = Apps::getAppBySlug($_slug);
        if ($app == FALSE) {
            return FALSE;
        }
        $feeds = new Feeds();
        $return = $feeds->getAppsActivity($app->getId(), $page, $perPage, 'created_at DESC', $feedsFilter);
        return array('app' => array('id' => $app->getId(), 'slug' => $app->getSlug(), 'name' => $app->getName(), 'image' => $app->getImage(), 'description' => $app->getDescription(), 'followers_count' => Apps::countAppFollowers($app->getId()), 'followers' => Apps::getAppFollowers($app->getId()), 'activity' => $return));
    }
    public function sort_plugins_by_creation_time($plugins_with_creation_time) {
        $size = sizeof($plugins_with_creation_time);
        for ($i = 0;$i < $size;$i++) {
            $highestValueIndex = $i;
            $highestValue = strtotime($plugins_with_creation_time[$i]['created_at']);
            for ($j = $i + 1;$j < $size;$j++) {
                if (strtotime($plugins_with_creation_time[$j]['created_at']) > $highestValue) {
                    $highestValueIndex = $j;
                    $highestValue = strtotime($plugins_with_creation_time[$j]['created_at']);
                }
            }
            $temp = $plugins_with_creation_time[$highestValueIndex];
            $plugins_with_creation_time[$highestValueIndex] = $plugins_with_creation_time[$i];
            $plugins_with_creation_time[$i] = $temp;
        }
        return $plugins_with_creation_time;
    }
    public function appStats($appId) {
        $orderBy = 'created_at DESC';
        $utility = new Speakol\Utility($this->getDI());
        $options = array("conditions" => "app_id = :app_id:", 'bind' => array('app_id' => $appId), 'order' => $orderBy);
        $di = FactoryDefault::getDefault();
        $connection = mysql_connect($di->getShared('config')->get('database')->get('host'), $di->getShared('config')->get('database')->get('username'), $di->getShared('config')->get('database')->get('password'));
        if (!$connection) {
            die('Could not connect: ' . mysql_error());
        }
        mysql_select_db($di->getShared('config')->get('database')->get('dbname'), $connection);
        mysql_query("SET time_zone = 'Africa/Cairo'");
        $query = "(select id,created_at,title,app_id,'debates' as module from debates where app_id=$appId) UNION (select id,created_at,title,app_id," . "'comparisons' as module from comparisons where app_id=$appId) UNION (select id,created_at,title,app_id,'argumentboxes' as module" . " from argumentboxes where app_id=$appId) order by created_at desc limit 10";
        $query_result = mysql_query($query);
        $feeds = array();
        while ($r = mysql_fetch_array($query_result)) {
            array_push($feeds, $r);
        }
        $results[self::ARGUMENTSBOX] = array();
        $results[self::COMPARISON] = array();
        $results[self::DEBATE] = array();
        foreach ($feeds as $feed) {
            if ($feed['module'] == "argumentboxes") {
                $plugin = Argumentboxes::findFirst($feed['id'])->toArray();
                $plugin['url'] = $plugin['domain'] . $plugin['uri'];
                $plugin['title'] = htmlspecialchars($plugin['title']);
                array_push($results[self::ARGUMENTSBOX], $plugin);
            } else if ($feed['module'] == "debates") {
                $plugin = Debates::findFirst(array("columns" => array("id", "title", "slug", "created_at", "url"), "conditions" => "id=" . $feed['id']))->toArray();
                $plugin['title'] = htmlspecialchars($plugin['title']);
                array_push($results[self::DEBATE], $plugin);
            } else {
                $plugin = Comparisons::findFirst(array("columns" => array("id", "title", "image", "slug", "created_at", "url", "align"), "conditions" => "id=" . $feed['id']))->toArray();
                $plugin['title'] = htmlspecialchars($plugin['title']);
                array_push($results[self::COMPARISON], $plugin);
            }
        }
        $results[self::ARGUMENT] = Arguments::find(array('app_id= :app_id: ', 'bind' => array('app_id' => $appId), 'limit' => 7, 'order' => $orderBy));
        if ($results[self::ARGUMENT]) {
            $rr = array();
            foreach ($results[self::ARGUMENT] as $rep) {
                $user = Users::findFirst($rep->getOwnerId());
                $owner_id = $rep->getOwnerId();
                $owner_name = $user->get_first_name() . " " . $user->get_last_name();
                $owner_slug = $user->getSlug();
                $owner_photo = $user->getProfilePicture();
                $argument_photo = ArgumentPhotos::findFirst('argument_id=' . $rep->getId());
                $rr[$rep->getId() ] = $rep->toArray();
                $rr[$rep->getId() ]['context'] = htmlspecialchars($rr[$rep->getId() ]['context']);
                if ($argument_photo) {
                    $rr[$rep->getId() ]['attached_photos'] = $argument_photo->getUrl() . $argument_photo->getImage();
                }
                if ($rep->get_admin_at() != 0) {
                    $app = Apps::findFirst($rep->get_admin_at());
                    if ($app) {
                        $owner_photo = $app->getImage();
                        $owner_name = $app->getName();
                    }
                }
                $rr[$rep->getId() ]['owner_id'] = $owner_id;
                $rr[$rep->getId() ]['owner_name'] = $owner_name;
                $rr[$rep->getId() ]['owner_slug'] = $owner_slug;
                $rr[$rep->getId() ]['owner_photo'] = $owner_photo;
                $rr[$rep->getId() ]['admin_at'] = $rep->get_admin_at();
                $rr[$rep->getId() ]['date'] = $utility->time_elapsed_string(strtotime($rep->getCreatedAt()));
                $rr[$rep->getId() ]['thumbs_up'] = ArgumentThumbs::findThumbsUPByArgID($rep->getId());
                $rr[$rep->getId() ]['thumbs_down'] = ArgumentThumbs::findThumbsDNByArgID($rep->getId());
                if ($user->getRoleId() == Roles::APP || $user->getRoleId() == Roles::ADMIN) {
                    $app = Apps::findFirst(array('columns' => 'website', 'publisher_id=' . $user->getId()));
                    $rr[$rep->getId() ]['url'] = $app['website'];
                }
                $side_id = $rep->getSideId();
                $side = Sides::findFirst($side_id);
                if ($side->getArgumentboxId() != NULL) {
                    $plugin = Argumentboxes::findFirst($side->getArgumentboxId());
                    $plugin_title = $plugin->getTitle();
                    if ($plugin_title === "") {
                        $plugin_title = "What do you think?";
                    }
                    $rr[$rep->getId() ]['plugin_url'] = $plugin->get_domain() . $plugin->get_uri();
                    $rr[$rep->getId() ]['plugin_code'] = $plugin->get_code();
                } else if ($side->getDebateId() != NULL) {
                    $plugin = Debates::findFirst($side->getDebateId());
                    $plugin_title = $plugin->getTitle();
                    $plugin_url = $plugin->getUrl();
                    $rr[$rep->getId() ]['plugin_url'] = $plugin_url;
                } else {
                    $plugin = Comparisons::findFirst($side->getComparisonId());
                    $plugin_title = $plugin->getTitle();
                    $plugin_url = $plugin->getUrl();
                    $rr[$rep->getId() ]['plugin_url'] = $plugin_url;
                }
                $rr[$rep->getId() ]['plugin_title'] = $plugin_title;
            }
            $results[self::ARGUMENT] = $rr;
        }
        $debates = Debates::count($options);
        $comparisons = Comparisons::count($options);
        $arguments = Arguments::count($options);
        $replies = Replies::count($options);
        $data = array('debates' => $debates, 'comparisons' => $comparisons, 'arguments' => $arguments, 'replies' => $replies,);
        $results['counts'] = $data;
        $results['plugins_creation'] = AppsController::validate_plugins_creation();
        return $results;
    }
    public function deleteActionHelper($_slug) {
        $app = $this->getAppBySlug($_slug);
        if ($app == FALSE) {
            return FALSE;
        }
        if ($this->getDi()->getShared('Utility')->getImageUploader()->delete($app->getImage()) == FALSE) {
        }
        if ($this->deleteAppFollowers($app->getId()) == FALSE) {
        }
        if ($this->deleteAppAdmins($app->getId()) == FALSE) {
        }
        if ($app->delete() == FALSE) {
        }
        return TRUE;
    }
    public function followersActionHelper($_appId) {
        return $this->getAppFollowers($_appId);
    }
    public static function getAppBySlug($_slug) {
        return Apps::findFirst(array("slug = :slug:", 'bind' => array('slug' => $_slug)));
    }
    public static function countAppFollowers($_appId) {
        return AppFollows::count(array("app_id = :app_id:", 'bind' => array('app_id' => $_appId)));
    }
    public static function getAppFollowers($_appId) {
        $appFollowers = AppFollows::find(array("app_id = :app_id:", 'bind' => array('app_id' => $_appId)));
        $arrUsers = array();
        foreach ($appFollowers as $appFollower) {
            $arrUsers[] = Users::getUserMiniDetails($appFollower->getFollowerId());
        }
        return $arrUsers;
    }
    public function deleteAppProfilePhotosFromDisk($_appLogos) {
        foreach ($_appLogos as $logo) {
            if ($this->getDi()->getShared('Utility')->getImageUploader()->delete($logo->getImage()) == FALSE) {
                return FALSE;
            }
        }
        return TRUE;
    }
    public function deleteAppFollowers($_appId) {
        return ($this->getDI()->getShared('modelsManager')->executeQuery("DELETE FROM AppFollows WHERE app_id = :app_id:", array('app_id' => $_appId))->success());
    }
    public function deleteAppAdmins($_appId) {
        return ($this->getDI()->getShared('modelsManager')->executeQuery("DELETE FROM AppAdmins WHERE app_id = :app_id:", array('app_id' => $_appId))->success());
    }
    public function deleteAppProfilePhotos($_appId) {
        return ($this->getDI()->getShared('modelsManager')->executeQuery("DELETE FROM ProfilePhotos WHERE app_id = :app_id:", array('app_id' => $_appId))->success());
    }
    private function _findById($_id) {
        return Apps::findFirst(array('id = :id:', 'bind' => array('id' => $_id)));
    }
    public function retrivecomponents($appSlug, $filters = array()) {
        $appId = Apps::findFirst('slug="' . $appSlug . '"');
        $orderBy = !empty($filters['order_by']) ? $filters['order_by'] : 'ASC';
        $type = !empty($filters['component_name']) ? $filters['component_name'] : false;
        if (empty($appId)) {
            return array('status' => 'ERROR', 'messages' => array(FactoryDefault::getDefault()->get('t')->_('apps-not-found')));
        }
        $appId = $appId->getId();
        $types = array('debates', 'comparisons', 'arguments', 'argumentsbox');
        if (!empty($type)) {
            $types = in_array($type, $types) ? array($type) : array();
        } else {
            return array('status' => 'ERROR', 'messages' => 'Missing content type!');
        }
        $args = array();
        foreach ($types as $type) {
            switch ($type) {
                case 'debates':
                    $args['debates'] = Debates::find(array('conditions' => 'app_id = :app_id:', 'order' => 'created_at ' . $orderBy, 'bind' => array('app_id' => $appId)))->toArray();
                break;
                case 'comparisons':
                    $args['comparisons'] = Comparisons::find(array('conditions' => 'app_id = :app_id:', 'order' => 'created_at ' . $orderBy, 'bind' => array('app_id' => $appId)))->toArray();
                break;
                case 'argumentsbox':
                    $argruments = Argumentboxes::findByAppId($appId);
                    $args['argumentsbox'] = array();
                    if ($argruments) {
                        foreach ($argruments as $arg) {
                            $args['arguments'][] = $arg->Arguments->toArray();
                        }
                    }
                break;
                case 'arguments':
                    $argruments = AppsArguments::findByAppId($appId);
                    $args['arguments'] = array();
                    if ($argruments) {
                        foreach ($argruments as $arg) {
                            $args['arguments'][] = $arg->Arguments->toArray();
                        }
                    }
                break;
            }
        }
        return array('status' => 'OK', 'data' => $args);
    }
    public function retriveLatestArgumentsByContentType($data) {
        $args = array();
        $app = Apps::findFirst('slug="' . $data['slug'] . '"');
        $type = !empty($data['type']) ? $data['type'] : false;
        if (empty($app)) {
            return array('status' => 'ERROR', 'messages' => array(FactoryDefault::getDefault()->get('t')->_('apps-not-found')));
        }
        $appId = $app->getId();
        switch ($type) {
            case 'debates':
                $query = "SELECT " . "a.* " . "FROM " . "Debates as d,  Sides AS s, SidesArguments AS sa, Arguments as a " . "WHERE " . "d.app_id = :app_id: AND d.id = s.parent_id AND s.parent_type = :parent_type: AND s.id = sa.side_id AND sa.argument_id = a.id";
                $query = $this->getModelsManager()->createQuery($query);
                $args['debates'] = $query->execute(array('parent_type' => $type, 'app_id' => $appId))->toArray();
            break;
            case 'comparisons':
                $query = "SELECT " . "a.* " . "FROM " . "Comparisons as d,  Sides AS s, SidesArguments AS sa, Arguments as a " . "WHERE " . "d.app_id = :app_id: AND d.id = s.parent_id AND s.parent_type = :parent_type: AND s.id = sa.side_id AND sa.argument_id = a.id";
                $query = $this->getModelsManager()->createQuery($query);
                $args['comparisons'] = $query->execute(array('parent_type' => $type, 'app_id' => $appId))->toArray();
            break;
            case 'argument-box':
                $query = "SELECT " . "a.* " . "FROM " . "Argumentboxes as d,  Sides AS s, SidesArguments AS sa, Arguments as a " . "WHERE " . "d.id = s.parent_id AND s.parent_type = :parent_type: AND s.id = sa.side_id AND sa.argument_id = a.id";
                $query = $this->getModelsManager()->createQuery($query);
                $args['arguments'] = $query->execute(array('parent_type' => $type))->toArray();
            break;
        }
        return array('status' => 'OK', 'data' => $args);
    }
    public function retriveLatestRepliesByContentType($data) {
        $args = array();
        $app = Apps::findFirst('slug="' . $data['slug'] . '"');
        $type = !empty($data['type']) ? $data['type'] : false;
        if (empty($app)) {
            return array('status' => 'ERROR', 'messages' => array(FactoryDefault::getDefault()->get('t')->_('apps-not-found')));
        }
        $appId = $app->getId();
        switch ($type) {
            case 'debates':
                $query = "SELECT " . "r.* " . "FROM " . "Debates AS d, Sides AS s, SidesArguments AS sa, Arguments AS a, Replies AS r " . "WHERE " . "d.app_id = :app_id: AND d.id = s.parent_id AND s.parent_type = :parent_type: AND s.id = sa.side_id AND sa.argument_id = a.id AND a.id = r.argument_id ORDER BY a.id, r.id DESC";
                $query = $this->getModelsManager()->createQuery($query);
                $args['debates'] = $query->execute(array('parent_type' => $type, 'app_id' => $appId))->toArray();
            break;
            case 'comparisons':
                $query = "SELECT " . "a.* " . "FROM " . "Comparisons as d,  Sides AS s, SidesArguments AS sa, Arguments as a, Replies AS r " . "WHERE " . "d.app_id = :app_id: AND d.id = s.parent_id AND s.parent_type = :parent_type: AND s.id = sa.side_id AND sa.argument_id = a.id AND a.id = r.argument_id ORDER BY a.id, r.id DESC";
                $query = $this->getModelsManager()->createQuery($query);
                $args['comparisons'] = $query->execute(array('parent_type' => $type, 'app_id' => $appId))->toArray();
            break;
            case 'argument-box':
                $query = "SELECT " . "a.* " . "FROM " . "Argumentboxes as d,  Sides AS s, SidesArguments AS sa, Arguments as a " . "WHERE " . "d.id = s.parent_id AND s.parent_type = :parent_type: AND s.id = sa.side_id AND sa.argument_id = a.id";
                $query = $this->getModelsManager()->createQuery($query);
                $args['arguments'] = $query->execute(array('parent_type' => $type))->toArray();
            break;
        }
        return array('status' => 'OK', 'data' => $args);
    }
    public function deleteComponent($data) {
        $args = array();
        $app = Apps::findFirst('slug="' . $data['slug'] . '"');
        $type = !empty($data['type']) ? $data['type'] : false;
        if (empty($app)) {
            return array('status' => 'ERROR', 'messages' => array(FactoryDefault::getDefault()->get('t')->_('apps-not-found')));
        }
        $appId = $app->getId();
        switch ($type) {
            case 'debates':
                $obj = Debates::findFirst($data['id']);
                Debates::deleteDebate($obj->getSlug());
            break;
            case 'comparisons':
                $obj = Comparisons::findFirst($data['id']);
                Comparisons::deleteComparison($obj->getSlug());
            break;
            case 'argument-box':
                $obj = Argumentboxes::findFirst($data['id']);
            break;
            case 'argument':
                $sideArguments = new SidesArguments();
                $sideArguments->delete(array("argument_id = :argument_id: ", 'bind' => array('argument_id' => $data['id'])));
                $replies = new Replies();
                $replies->delete(array('argument_id = :argument_id:', 'bind' => array('argument_id' => $data['id'])));
                $argument = new Arguments();
                $argument->delete($data['id']);
            break;
            case 'reply':
                $replies = new Replies();
                $replies->delete($data['id']);
            break;
        }
        return array('status' => 'OK', 'data' => array(true));
    }
    public static function getAppStats($_appID, $_page = '1', $_limit = '2', $_sortBy = "created_at DESC", $module = "") {
        $feeds = new Feeds();
        return $feeds->getAppsActivity($_appID, $_page, $_limit, $_sortBy, $module, true);
    }
    public function getFollowers($slug, $page = '0', $perPage = '4') {
        $app = self::findFirst(array('conditions' => 'slug = :slug: ', 'bind' => array('slug' => $slug), 'columns' => array('id')));
        if ($app) {
            $count = AppFollows::count(array('conditions' => 'app_id=:app_id:', 'bind' => array('app_id' => $app->id)));
            $page = $page < 1 ? 1 : $page;
            $return = array();
            $startValue = ($page - 1) * $perPage;
            if ($count > $startValue) {
                $results = AppFollows::find(array('conditions' => 'app_id=:app_id:', 'bind' => array('app_id' => $app->id), 'limit' => array('number' => $perPage, 'offset' => $startValue)));
                if ($results) {
                    foreach ($results as $row) {
                        $return[$row->getId() ] = $row->getUsers(array('columns' => array('id', 'job_title', 'name', 'slug', 'profile_picture')));
                        $return[$row->getId() ]['followers_count'] = AppFollows::count(array('conditions' => 'app_id=:app_id:', 'bind' => array('app_id' => $row->getFollowerId())));
                    }
                }
            }
            $return = array('extra' => array('hasmore' => (($count >= (($startValue + 1) + $perPage)) ? true : FALSE)), 'data' => $return);
            return $return;
        }
        return false;
    }
    public function listApps($page = '1', $perPage = '1', $orderBy = 'created_at') {
        $count = self::count();
        $page = $page < 1 ? 1 : $page;
        $startValue = ($page - 1) * $perPage;
        if ($count > $startValue) {
            $cond = array('limit' => array('number' => $perPage, 'offset' => $startValue), "order" => $orderBy . " DESC");
            $res = self::find($cond);
            $hasMore = ($count >= (($startValue + 1) + $perPage)) ? true : FALSE;
            return array('status' => 'OK', 'data' => array('apps' => $res->toArray(), 'hasmore' => $hasMore));
        } else {
            return array('status' => 'OK', 'data' => array());
        }
    }
    public function listAppData($appId, $page = '1', $perPage = '1', $type = '', $orderBy = 'created_at DESC') {
        $page = json_decode($page, 1);
        $return = $results = $count = $hasMore = $startValue = array();
        $con = new Speakol\concreteSocialPluginFactory();
        foreach ($this->_modules as $row) {
            $obj = $con->make($row);
            empty($page[$row]) || ($page[$row] < 1) ? $page[$row] = '1' : true;
            $startValue[$row] = ($page[$row] - 1) * $perPage;
            $count[$row] = $obj->count(array('app_id = :app_id: ', 'bind' => array('app_id' => $appId)));
            $hasMore[$row] = ($count[$row] >= (($startValue[$row] + 1) + $perPage)) ? true : false;
        }
        if ($type && in_array($type, $this->_modules)) {
            $obj = $con->make($type);
            if ($obj && in_array($type, array('argument', 'reply'))) {
                if ($obj && ($count[$type] > $startValue[$type])) {
                    $utility = new Speakol\Utility($this->getDI());
                    $results[$type] = $obj->find(array('app_id= :app_id: ', 'bind' => array('app_id' => $appId), 'limit' => array('number' => $perPage, 'offset' => $startValue[$type]), 'order' => $orderBy));
                    if ($results[$type]) {
                        $rr = array();
                        foreach ($results[$type] as $replies) {
                            $rr[$replies->getId() ] = $replies->toArray();
                            if ($type == "argument") {
                                $owner_id = $replies->getOwnerId();
                                $attached_photo = ArgumentPhotos::findFirst('argument_id=' . $replies->getId());
                                $rr[$replies->getId() ]['context'] = htmlspecialchars($rr[$replies->getId() ]['context']);
                            } else {
                                $owner_id = $replies->getUserId();
                                $attached_photo = ReplyPhotos::findFirst('reply_id=' . $replies->getId());
                                $rr[$replies->getId() ]['content'] = htmlspecialchars($rr[$replies->getId() ]['content']);
                            }
                            $user = Users::findFirst($owner_id);
                            $owner_name = $user->get_first_name() . " " . $user->get_last_name();
                            $owner_slug = $user->getSlug();
                            $owner_photo = $user->getProfilePicture();
                            if ($attached_photo) {
                                $rr[$replies->getId() ]['attached_photo'] = $attached_photo->getUrl() . $attached_photo->getImage();
                            }
                            if ($replies->get_admin_at() != 0) {
                                $app = Apps::findFirst($replies->get_admin_at());
                                if ($app) {
                                    $owner_name = $app->getName();
                                    $owner_photo = $app->getImage();
                                }
                            }
                            $rr[$replies->getId() ]['owner_id'] = $owner_id;
                            $rr[$replies->getId() ]['owner_name'] = $owner_name;
                            $rr[$replies->getId() ]['owner_slug'] = $owner_slug;
                            $rr[$replies->getId() ]['owner_photo'] = $owner_photo;
                            $rr[$replies->getId() ]['admin_at'] = $replies->get_admin_at();
                            $rr[$replies->getId() ]['date'] = $utility->time_elapsed_string(strtotime($replies->getCreatedAt()));
                            $rr[$replies->getId() ]['thumbs_up'] = ArgumentThumbs::findThumbsUPByArgID($replies->getId());
                            $rr[$replies->getId() ]['thumbs_down'] = ArgumentThumbs::findThumbsDNByArgID($replies->getId());
                            if ($user->getRoleId() == Roles::APP || $user->getRoleId() == Roles::ADMIN) {
                                $app = Apps::findFirst(array('columns' => 'website', 'publisher_id=' . $user->getId()));
                                $rr[$replies->getId() ]['url'] = $app['website'];
                            }
                            $side_id = $replies->getSideId();
                            $side = Sides::findFirst($side_id);
                            if ($side->getArgumentboxId() != NULL) {
                                $plugin = Argumentboxes::findFirst($side->getArgumentboxId());
                                $plugin_title = $plugin->getTitle();
                                if ($plugin_title === "") {
                                    $plugin_title = "What do you think?";
                                }
                                $rr[$replies->getId() ]['plugin_url'] = $plugin->get_protocol() . $plugin->get_domain() . $plugin->get_uri();
                                $rr[$replies->getId() ]['plugin_code'] = $plugin->get_code();
                            } else if ($side->getDebateId() != NULL) {
                                $plugin = Debates::findFirst($side->getDebateId());
                                $plugin_title = $plugin->getTitle();
                                $plugin_url = $plugin->getUrl();
                                $rr[$replies->getId() ]['plugin_url'] = $plugin_url;
                            } else {
                                $plugin = Comparisons::findFirst($side->getComparisonId());
                                $plugin_title = $plugin->getTitle();
                                $plugin_url = $plugin->getUrl();
                                $rr[$replies->getId() ]['plugin_url'] = $plugin_url;
                            }
                            $rr[$replies->getId() ]['plugin_title'] = $plugin_title;
                            $ids[] = $replies->getId();
                        }
                        $results[$type] = $rr;
                    }
                }
            } else {
                if ($obj && $count[$type] > $startValue[$type]) {
                    $conditions = array('app_id = :app_id: ');
                    $options = array('bind' => array('app_id' => $appId), 'limit' => array('number' => $perPage, 'offset' => $startValue[$type]), 'order' => $orderBy);
                    switch ($type) {
                        case self::DEBATE:
                            $results[$type] = $obj->listMiniDebates($conditions, $options);
                        break;
                        case self::COMPARISON:
                            $results[$type] = $obj->listMiniComparison($conditions, $options);
                        break;
                        case self::ARGUMENTSBOX:
                            $results[$type] = $obj->listMiniArguments($conditions, $options);
                        break;
                    }
                }
            }
        } else {
            if ($count[self::DEBATE] > $startValue[self::DEBATE]) {
                $conditions = array('app_id = :app_id: ');
                $options = array('bind' => array('app_id' => $appId), 'limit' => array('number' => $perPage, 'offset' => $startValue[self::DEBATE]), 'order' => $orderBy);
                $debate = new Debates();
                $results[self::DEBATE] = $debate->listMiniDebates($conditions, $options);
            }
            if ($count[self::COMPARISON] > $startValue[self::COMPARISON]) {
                $conditions = array('app_id = :app_id: ');
                $options = array('bind' => array('app_id' => $appId), 'limit' => array('number' => $perPage, 'offset' => $startValue[self::COMPARISON]), 'order' => $orderBy);
                $comparisons = new Comparisons();
                $results[self::COMPARISON] = $comparisons->listMiniComparison($conditions, $options);
            }
            if ($count[self::ARGUMENTSBOX] > $startValue[self::ARGUMENTSBOX]) {
                $conditions = array('app_id = :app_id: ');
                $options = array('bind' => array('app_id' => $appId), 'limit' => array('number' => $perPage, 'offset' => $startValue[self::ARGUMENTSBOX]), 'order' => $orderBy);
                $argBox = new Argumentboxes();
                $results[self::ARGUMENTSBOX] = $argBox->listMiniArguments($conditions, $options);
            }
            if ($count[self::ARGUMENT] > $startValue[self::ARGUMENT]) {
                $utility = new Speakol\Utility($this->getDI());
                $results[self::ARGUMENT] = Arguments::find(array('app_id= :app_id: ', 'bind' => array('app_id' => $appId), 'limit' => array('number' => $perPage, 'offset' => $startValue[self::ARGUMENT]), 'order' => $orderBy));
                if ($results[self::ARGUMENT]) {
                    $rr = array();
                    foreach ($results[self::ARGUMENT] as $replies) {
                        $user = Users::findFirst($replies->getOwnerId());
                        $owner_id = $replies->getOwnerId();
                        $owner_name = $user->get_first_name() . " " . $user->get_last_name();
                        $owner_slug = $user->getSlug();
                        $owner_photo = $user->getProfilePicture();
                        $argument_photo = ArgumentPhotos::findFirst('argument_id=' . $replies->getId());
                        $rr[$replies->getId() ] = $replies->toArray();
                        $rr[$replies->getId() ]['context'] = htmlspecialchars($rr[$replies->getId() ]['context']);
                        if ($argument_photo) {
                            $rr[$replies->getId() ]['attached_photos'] = $argument_photo->getUrl() . $argument_photo->getImage();
                        }
                        if ($replies->get_admin_at() != 0) {
                            $app = Apps::findFirst($replies->get_admin_at());
                            if ($app) {
                                $owner_name = $app->getName();
                                $owner_photo = $app->getImage();
                            }
                        }
                        $rr[$replies->getId() ]['owner_id'] = $owner_id;
                        $rr[$replies->getId() ]['owner_name'] = $owner_name;
                        $rr[$replies->getId() ]['owner_slug'] = $owner_slug;
                        $rr[$replies->getId() ]['owner_photo'] = $owner_photo;
                        $rr[$replies->getId() ]['admin_at'] = $replies->get_admin_at();
                        $rr[$replies->getId() ]['date'] = $utility->time_elapsed_string(strtotime($replies->getCreatedAt()));
                        $rr[$replies->getId() ]['thumbs_up'] = ArgumentThumbs::findThumbsUPByArgID($replies->getId());
                        $rr[$replies->getId() ]['thumbs_down'] = ArgumentThumbs::findThumbsDNByArgID($replies->getId());
                        if ($user->getRoleId() == Roles::APP || $user->getRoleId() == Roles::ADMIN) {
                            $app = Apps::findFirst(array('columns' => 'website', 'publisher_id=' . $user->getId()));
                            $rr[$replies->getId() ]['url'] = $app['website'];
                        }
                        $side_id = $replies->getSideId();
                        $side = Sides::findFirst($side_id);
                        if ($side->getArgumentboxId() != NULL) {
                            $plugin = Argumentboxes::findFirst($side->getArgumentboxId());
                            $plugin_title = $plugin->getTitle();
                            if ($plugin_title === "") {
                                $plugin_title = "What do you think?";
                            }
                            $rr[$replies->getId() ]['plugin_code'] = $plugin->get_code();
                            $rr[$replies->getId() ]['plugin_url'] = $plugin->get_protocol() . $plugin->get_domain() . $plugin->get_uri();
                        } else if ($side->getDebateId() != NULL) {
                            $plugin = Debates::findFirst($side->getDebateId());
                            $plugin_title = $plugin->getTitle();
                            $plugin_url = $plugin->getUrl();
                            $rr[$replies->getId() ]['plugin_url'] = $plugin_url;
                        } else {
                            $plugin = Comparisons::findFirst($side->getComparisonId());
                            $plugin_title = $plugin->getTitle();
                            $plugin_url = $plugin->getUrl();
                            $rr[$replies->getId() ]['plugin_url'] = $plugin_url;
                        }
                        $rr[$replies->getId() ]['plugin_title'] = $plugin_title;
                        $ids[] = $replies->getId();
                    }
                    $results[self::ARGUMENT] = $rr;
                }
            }
            if ($count[self::REPLY] > $startValue[self::REPLY]) {
                $utility = new Speakol\Utility($this->getDI());
                $results[self::REPLY] = Replies::find(array('app_id= :app_id: ', 'bind' => array('app_id' => $appId), 'limit' => array('number' => $perPage, 'offset' => $startValue[self::REPLY]), 'order' => $orderBy));
                if ($results[self::REPLY]) {
                    $rr = array();
                    foreach ($results[self::REPLY] as $replies) {
                        $user = Users::findFirst($replies->getUserId());
                        $owner_id = $user->getId();
                        $owner_name = $user->get_first_name() . " " . $user->get_last_name();
                        $owner_slug = $user->getSlug();
                        $owner_photo = $user->getProfilePicture();
                        $reply_photo = ReplyPhotos::findFirst('reply_id=' . $replies->getId());
                        if ($reply_photo) {
                            $rr[$replies->getId() ]['attached_photos'] = $reply_photo->getUrl() . $reply_photo->getImage();
                        }
                        if ($replies->get_admin_at() != 0) {
                            $app = Apps::findFirst($replies->get_admin_at());
                            if ($app) {
                                $owner_name = $app->getName();
                                $owner_photo = $app->getImage();
                            }
                        }
                        $rr[$replies->getId() ] = $replies->toArray();
                        $rr[$replies->getId() ]['content'] = htmlspecialchars($rr[$replies->getId() ]['content']);
                        $rr[$replies->getId() ]['owner_id'] = $owner_id;
                        $rr[$replies->getId() ]['owner_name'] = $owner_name;
                        $rr[$replies->getId() ]['owner_slug'] = $owner_slug;
                        $rr[$replies->getId() ]['owner_photo'] = $owner_photo;
                        $rr[$replies->getId() ]['admin_at'] = $replies->get_admin_at();
                        $rr[$replies->getId() ]['date'] = $utility->time_elapsed_string(strtotime($replies->getCreatedAt()));
                        $rr[$replies->getId() ]['thumbs_up'] = ArgumentThumbs::findThumbsUPByArgID($replies->getId());
                        $rr[$replies->getId() ]['thumbs_down'] = ArgumentThumbs::findThumbsDNByArgID($replies->getId());
                        if ($user->getRoleId() == Roles::APP || $user->getRoleId() == Roles::ADMIN) {
                            $app = Apps::findFirst(array('columns' => 'website', 'publisher_id=' . $user->getId()));
                            $rr[$replies->getId() ]['url'] = $app['website'];
                        }
                        $side_id = $replies->getSideId();
                        $side = Sides::findFirst($side_id);
                        if ($side->getArgumentboxId() != NULL) {
                            $plugin = Argumentboxes::findFirst($side->getArgumentboxId());
                            $plugin_title = $plugin->getTitle();
                            if ($plugin_title === "") {
                                $plugin_title = "What do you think?";
                            }
                            $rr[$replies->getId() ]['plugin_code'] = $plugin->get_code();
                            $rr[$replies->getId() ]['plugin_url'] = $plugin->get_protocol() . $plugin->get_domain() . $plugin->get_uri();
                        } else if ($side->getDebateId() != NULL) {
                            $plugin = Debates::findFirst($side->getDebateId());
                            $plugin_title = $plugin->getTitle();
                            $plugin_url = $plugin->getUrl();
                            $rr[$replies->getId() ]['plugin_url'] = $plugin_url;
                        } else {
                            $plugin = Comparisons::findFirst($side->getComparisonId());
                            $plugin_title = $plugin->getTitle();
                            $plugin_url = $plugin->getUrl();
                            $rr[$replies->getId() ]['plugin_url'] = $plugin_url;
                        }
                        $rr[$replies->getId() ]['plugin_title'] = $plugin_title;
                        $ids[] = $replies->getId();
                    }
                    $results[self::REPLY] = $rr;
                }
            }
        }
        $retru = array('data' => $results, 'hasmore' => $hasMore, 'startValue' => $startValue);
        $return = array('data' => array(self::DEBATE => array(), self::COMPARISON => array(), self::ARGUMENTSBOX => array(), self::ARGUMENT => array(), self::REPLY => array()), 'hasmore' => array(self::DEBATE => false, self::COMPARISON => false, self::ARGUMENTSBOX => false, self::ARGUMENT => false, self::REPLY => false), 'startValue' => array(self::DEBATE => false, self::COMPARISON => false, self::ARGUMENTSBOX => false, self::ARGUMENT => false, self::REPLY => false),);
        $return = array_replace_recursive($return, $retru);
        return $return;
    }
    public static function list_apps($limit, $offset, $sort, $key) {
        if ($key == "arguments" || $key == "plugins" || $key == "country" || $key == "email" || $key == "name") {
            $apps = static ::find(array('columns' => array('name', 'website', 'created_at', 'publisher_id', 'id', 'country_id')))->toArray();
        } else {
            $apps = static ::find(array('columns' => array('name', 'website', 'created_at', 'publisher_id', 'id', 'country_id'), "order" => "lower($key)" . " " . $sort, 'limit' => array('number' => $limit, 'offset' => $offset)))->toArray();
        }
        for ($i = 0;$i < sizeof($apps);$i++) {
            $user = Users::findFirst($apps[$i]['publisher_id']);
            $apps[$i]['country'] = Country::findFirst($apps[$i]['country_id'])->getName();
            $apps[$i]['email'] = $user->getEmail();
            $number_of_arguments = Arguments::find(array('app_id=' . $apps[$i]['id'], 'columns' => 'count(*) as count'))->toArray();
            $apps[$i]['arguments'] = intVal($number_of_arguments[0]['count']);
            $number_of_argumentboxes = Argumentboxes::find(array('columns' => 'count(*) as count', 'app_id=' . $apps[$i]['id']))->toArray();
            $number_of_argumentboxes = intVal($number_of_argumentboxes[0]['count']);
            $number_of_debates = Debates::find(array('columns' => 'count(*) as count', 'app_id=' . $apps[$i]['id']))->toArray();
            $number_of_debates = intVal($number_of_debates[0]['count']);
            $number_of_comparisons = Comparisons::find(array('columns' => 'count(*) as count', 'app_id=' . $apps[$i]['id']))->toArray();
            $number_of_comparisons = intVal($number_of_comparisons[0]['count']);
            $apps[$i]['plugins_count'] = $number_of_argumentboxes + $number_of_debates + $Number_of_comparisons;
            unset($apps[$i]['country_id']);
            unset($apps[$i]['publisher_id']);
            unset($apps[$i]['id']);
        }
        if ($key == "arguments") {
            if ($sort == "asc") {
                function sort_arguments($a, $b) {
                    $t1 = $a['arguments'];
                    $t2 = $b['arguments'];
                    return $t1 - $t2;
                }
            } else {
                function sort_arguments($a, $b) {
                    $t1 = $a['arguments'];
                    $t2 = $b['arguments'];
                    return $t2 - $t1;
                }
            }
            usort($apps, 'sort_arguments');
            $apps = array_slice($apps, $offset, $limit);
        }
        if ($key == "plugins") {
            if ($sort == "asc") {
                function sort_plugins($a, $b) {
                    $t1 = $a['plugins_count'];
                    $t2 = $b['plugins_count'];
                    return $t1 - $t2;
                }
            } else {
                function sort_plugins($a, $b) {
                    $t1 = $a['plugins_count'];
                    $t2 = $b['plugins_count'];
                    return $t2 - $t1;
                }
            }
            usort($apps, 'sort_plugins');
            $apps = array_slice($apps, $offset, $limit);
        }
        if ($key == "email") {
            if ($sort == "asc") {
                function sort_email($a, $b) {
                    $t1 = strtolower($a['email']);
                    $t2 = strtolower($b['email']);
                    return strcmp($t1, $t2);
                }
            } else {
                function sort_email($a, $b) {
                    $t1 = strtolower($a['email']);
                    $t2 = strtolower($b['email']);
                    return strcmp($t2, $t1);
                }
            }
            usort($apps, 'sort_email');
            $apps = array_slice($apps, $offset, $limit);
        }
        if ($key == "country") {
            if ($sort == "asc") {
                function sort_country($a, $b) {
                    $t1 = $a['country'];
                    $t2 = $b['country'];
                    return strcmp($t1, $t2);
                }
            } else {
                function sort_country($a, $b) {
                    $t1 = $a['country'];
                    $t2 = $b['country'];
                    return strcmp($t2, $t1);
                }
            }
            usort($apps, 'sort_country');
            $apps = array_slice($apps, $offset, $limit);
        }
        if ($key == "name") {
            if ($sort == "asc") {
                function sort_name($a, $b) {
                    $t1 = $a['country'];
                    $t2 = $b['country'];
                    return strcmp($t1, $t2);
                }
            } else {
                function sort_name($a, $b) {
                    $t1 = $a['name'];
                    $t2 = $b['name'];
                    return strcmp($t2, $t1);
                }
            }
            usort($apps, 'sort_name');
            $apps = array_slice($apps, $offset, $limit);
        }
        $number_of_apps = intval(static ::count());
        $hasmore = (($number_of_apps >= (($offset + 1) + $limit)) ? true : FALSE);
        return array("apps_data" => $apps, "number_of_apps" => $number_of_apps, "has_more" => $hasmore);
    }
    public function listSiteData($page = '1', $perPage = '1', $type = '', $orderBy = 'created_at DESC') {
        $page = json_decode($page, 1);
        $return = $results = $count = $hasMore = $startValue = array();
        $con = new Speakol\concreteSocialPluginFactory();
        foreach ($this->_modules as $row) {
            $obj = $con->make($row);
            empty($page[$row]) || ($page[$row] < 1) ? $page[$row] = '1' : true;
            $startValue[$row] = ($page[$row] - 1) * $perPage;
            $count[$row] = $obj->count();
            $hasMore[$row] = ($count[$row] >= (($startValue[$row] + 1) + $perPage)) ? true : false;
        }
        if ($type && in_array($type, $this->_modules)) {
            $obj = $con->make($type);
            if ($obj && in_array($type, array('argument', 'reply'))) {
                if ($obj && ($count[$type] > $startValue[$type])) {
                    $utility = new Speakol\Utility($this->getDI());
                    $results[$type] = $obj->find(array('limit' => array('number' => $perPage, 'offset' => $startValue[$type]), 'order' => $orderBy));
                    if ($results[$type]) {
                        $rr = array();
                        foreach ($results[$type] as $replies) {
                            $rr[$replies->getId() ] = $replies->toArray();
                            $rr[$replies->getId() ]['user'] = $replies->getUsers(array('columns' => array('name', 'id', 'slug', 'profile_picture')))->toArray();
                            $rr[$replies->getId() ]['date'] = $utility->time_elapsed_string(strtotime($replies->getCreatedAt()));
                            $rr[$replies->getId() ]['thumbs_up'] = ArgumentThumbs::findThumbsUPByArgID($replies->getId());
                            $rr[$replies->getId() ]['thumbs_down'] = ArgumentThumbs::findThumbsDNByArgID($replies->getId());
                            $ids[] = $replies->getId();
                        }
                        $results[$type] = $rr;
                    }
                }
            } else {
                if ($obj && $count[$type] > $startValue[$type]) {
                    $conditions = array();
                    $options = array('limit' => array('number' => $perPage, 'offset' => $startValue[$type]), 'order' => $orderBy);
                    switch ($type) {
                        case self::DEBATE:
                            $results[$type] = $obj->listMiniDebates($conditions, $options);
                        break;
                        case self::COMPARISON:
                            $results[$type] = $obj->listMiniComparison($conditions, $options);
                        break;
                        case self::ARGUMENTSBOX:
                            $results[$type] = $obj->listMiniArguments($conditions, $options);
                        break;
                    }
                }
            }
        } else {
            if ($count[self::DEBATE] > $startValue[self::DEBATE]) {
                $conditions = array();
                $options = array('limit' => array('number' => $perPage, 'offset' => $startValue[self::DEBATE]), 'order' => $orderBy);
                $debate = new Debates();
                $results[self::DEBATE] = $debate->listMiniDebates($conditions, $options);
            }
            if ($count[self::COMPARISON] > $startValue[self::COMPARISON]) {
                $conditions = array();
                $options = array('limit' => array('number' => $perPage, 'offset' => $startValue[self::COMPARISON]), 'order' => $orderBy);
                $comparisons = new Comparisons();
                $results[self::COMPARISON] = $comparisons->listMiniComparison($conditions, $options);
            }
            if ($count[self::ARGUMENTSBOX] > $startValue[self::ARGUMENTSBOX]) {
                $conditions = array();
                $options = array('limit' => array('number' => $perPage, 'offset' => $startValue[self::ARGUMENTSBOX]), 'order' => $orderBy);
                $argBox = new Argumentboxes();
                $results[self::ARGUMENTSBOX] = $argBox->listMiniArguments($conditions, $options);
            }
            if ($count[self::ARGUMENT] > $startValue[self::ARGUMENT]) {
                $utility = new Speakol\Utility($this->getDI());
                $results[self::ARGUMENT] = Arguments::find(array('limit' => array('number' => $perPage, 'offset' => $startValue[self::ARGUMENT]), 'order' => $orderBy));
                if ($results[self::ARGUMENT]) {
                    $rr = array();
                    foreach ($results[self::ARGUMENT] as $replies) {
                        $rr[$replies->getId() ] = $replies->toArray();
                        $rr[$replies->getId() ]['user'] = $replies->getUsers(array('columns' => array('first_name', 'last_name', 'id', 'slug', 'profile_picture')))->toArray();
                        $rr[$replies->getId() ]['user']['name'] = $rr[$replies->getId() ]['user']['first_name'] . " " . $rr[$replies->getId() ]['user']['last_name'];
                        $rr[$replies->getId() ]['date'] = $utility->time_elapsed_string(strtotime($replies->getCreatedAt()));
                        $rr[$replies->getId() ]['thumbs_up'] = ArgumentThumbs::findThumbsUPByArgID($replies->getId());
                        $rr[$replies->getId() ]['thumbs_down'] = ArgumentThumbs::findThumbsDNByArgID($replies->getId());
                        $ids[] = $replies->getId();
                    }
                    $results[self::ARGUMENT] = $rr;
                }
            }
            if ($count[self::REPLY] > $startValue[self::REPLY]) {
                $utility = new Speakol\Utility($this->getDI());
                $results[self::REPLY] = Replies::find(array('limit' => array('number' => $perPage, 'offset' => $startValue[self::REPLY]), 'order' => $orderBy));
                if ($results[self::REPLY]) {
                    $rr = array();
                    foreach ($results[self::REPLY] as $replies) {
                        $rr[$replies->getId() ] = $replies->toArray();
                        $rr[$replies->getId() ]['user'] = $replies->getUsers(array('columns' => array('first_name', 'last_name', 'id', 'slug', 'profile_picture')))->toArray();
                        $rr[$replies->getId() ]['user']['name'] = $rr[$replies->getId() ]['user']['first_name'] . " " . $rr[$replies->getId() ]['user']['last_name'];
                        $rr[$replies->getId() ]['date'] = $utility->time_elapsed_string(strtotime($replies->getCreatedAt()));
                        $rr[$replies->getId() ]['thumbs_up'] = ArgumentThumbs::findThumbsUPByArgID($replies->getId());
                        $rr[$replies->getId() ]['thumbs_down'] = ArgumentThumbs::findThumbsDNByArgID($replies->getId());
                        $ids[] = $replies->getId();
                    }
                    $results[self::REPLY] = $rr;
                }
            }
        }
        $retru = array('data' => $results, 'hasmore' => $hasMore, 'startValue' => $startValue);
        $return = array('data' => array(self::DEBATE => array(), self::COMPARISON => array(), self::ARGUMENTSBOX => array(), self::ARGUMENT => array(), self::REPLY => array()), 'hasmore' => array(self::DEBATE => false, self::COMPARISON => false, self::ARGUMENTSBOX => false, self::ARGUMENT => false, self::REPLY => false), 'startValue' => array(self::DEBATE => false, self::COMPARISON => false, self::ARGUMENTSBOX => false, self::ARGUMENT => false, self::REPLY => false),);
        return array_replace_recursive($return, $retru);
    }
}
