<?php
class _dbVersion_ extends BaseModel {
    protected $id;
    protected $version;
    protected $label;
    protected $name;
    protected $sql_up;
    protected $sql_down;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setVersion($version) {
        $this->version = $version;
        return $this;
    }
    public function setLabel($label) {
        $this->label = $label;
        return $this;
    }
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function setSqlUp($sql_up) {
        $this->sql_up = $sql_up;
        return $this;
    }
    public function setSqlDown($sql_down) {
        $this->sql_down = $sql_down;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getVersion() {
        return $this->version;
    }
    public function getLabel() {
        return $this->label;
    }
    public function getName() {
        return $this->name;
    }
    public function getSqlUp() {
        return $this->sql_up;
    }
    public function getSqlDown() {
        return $this->sql_down;
    }
}
