<?php
class ArgumentThumbs extends BaseModel {
    protected $id;
    protected $argumnet_id;
    protected $thumber_id;
    protected $thumb_up;
    protected $link;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setArgumnetId($argumnet_id) {
        $this->argumnet_id = $argumnet_id;
        return $this;
    }
    public function setThumberId($thumber_id) {
        $this->thumber_id = $thumber_id;
        return $this;
    }
    public function setThumbUp($thumb_up) {
        $this->thumb_up = $thumb_up;
        return $this;
    }
    public function setLink($link) {
        $this->link = $link;
    }
    public function getId() {
        return $this->id;
    }
    public function getArgumnetId() {
        return $this->argumnet_id;
    }
    public function getThumberId() {
        return $this->thumber_id;
    }
    public function getThumbUp() {
        return $this->thumb_up;
    }
    public function getLink() {
        return $this->link;
    }
    public function initialize() {
        $this->belongsTo('thumber_id', 'Users', 'id', NULL);
        $this->belongsTo('argumnet_id', 'Arguments', 'id', NULL);
    }
    public static function findThumbsByArgIDUserID($_argumentId, $_userId) {
        return ArgumentThumbs::query()->where("argumnet_id = :argument_id:")->andWhere("thumber_id = :thumber_id:")->bind(array('argument_id' => $_argumentId, 'thumber_id' => $_userId,))->execute()->getFirst();
    }
    public static function findThumbByArgIDUserID($arg_id, $_userId) {
        $objThumb = self::findThumbsByArgIDUserID($arg_id, $_userId);
        if ($objThumb != FALSE) {
            return $objThumb->getThumbUp();
        } else {
            return false;
        }
    }
    public static function findThumbsUPByArgID($_argId) {
        return ArgumentThumbs::query()->where("argumnet_id = :argumnet_id:")->andWhere("thumb_up = :thumb_up:")->bind(array('argumnet_id' => $_argId, 'thumb_up' => 1,))->execute()->count();
    }
    public static function findThumbsDNByArgID($_argId) {
        return ArgumentThumbs::query()->where("argumnet_id = :argumnet_id:")->andWhere("thumb_up = :thumb_up:")->bind(array('argumnet_id' => $_argId, 'thumb_up' => 0,))->execute()->count();
    }
    public static function createArgumentThumbs($_argumentId, $_userId, $vote, $link) {
        $objArgumentThumbs = new ArgumentThumbs();
        $objArgumentThumbs->setArgumnetId($_argumentId);
        $objArgumentThumbs->setThumberId($_userId);
        $objArgumentThumbs->setThumbUp($vote);
        $objArgumentThumbs->setLink($link);
        if ($objArgumentThumbs->save() == FALSE) {
            $strErrors = "";
            foreach ($objArgumentThumbs->getMessages() as $message) {
                $strErrors.= $message->getMessage();
                $strErrors.= ". ";
            }
            throw new \Exception("Conflict:{$strErrors}", 409);
        } else {
            return $objArgumentThumbs->getId();
        }
    }
    public static function updateArgumentThumbs($_argumentId, $_userId, $vote, $link) {
        $objArgumentThumbs = self::findThumbsByArgIDUserID($_argumentId, $_userId);
        $objArgumentThumbs->setArgumnetId($_argumentId);
        $objArgumentThumbs->setThumberId($_userId);
        $objArgumentThumbs->setThumbUp($vote);
        $objArgumentThumbs->setLink($link);
        if ($objArgumentThumbs->update() == FALSE) {
            $strErrors = "";
            foreach ($objArgumentThumbs->getMessages() as $message) {
                $strErrors.= $message->getMessage();
                $strErrors.= ". ";
            }
            throw new \Exception("Conflict:{$strErrors}", 409);
        } else {
            return $objArgumentThumbs->getId();
        }
    }
    public static function getThumbByTrinary($_argumentId, $_userId, $_boolVote) {
        $hasVoted = ArgumentThumbs::findFirst(array('conditions' => "argumnet_id = :argument_id: AND thumber_id = :thumber_id: AND thumb_up = :vote:", 'bind' => array('argument_id' => $_argumentId, 'thumber_id' => $_userId, 'vote' => $_boolVote)));
        if ($hasVoted) {
            $hasVoted->delete();
        }
        return $hasVoted ? 1 : 0;
    }
    public static function getThumbs($_argumentID, $_boolUPDN) {
        return ArgumentThumbs::count(array('argumnet_id = :argumnet_id: AND thumb_up = :thumb_up:', 'bind' => array('argumnet_id' => $_argumentID, 'thumb_up' => $_boolUPDN)));
    }
    public static function getThumbUpByUserIDArgID($_argID, $_userID) {
        $objThumb = ArgumentThumbs::findThumbsByArgIDUserID($_argID, $_userID);
        if ($objThumb != FALSE) {
            return $objThumb->getThumbUp();
        } else {
            return false;
        }
    }
}
