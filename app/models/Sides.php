<?php
class Sides extends BaseModel {
    protected $id;
    protected $argumentbox_id;
    protected $debate_id;
    protected $comparison_id;
    protected $owner_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setArgumentboxId($argumentbox_id) {
        $this->argumentbox_id = $argumentbox_id;
        return $this;
    }
    public function setDebateId($debate_id) {
        $this->debate_id = $debate_id;
        return $this;
    }
    public function setComparisonId($comparison_id) {
        $this->comparison_id = $comparison_id;
        return $this;
    }
    public function setOwnerId($owner_id) {
        $this->owner_id = $owner_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getArgumentboxId() {
        return $this->argumentbox_id;
    }
    public function getDebateId() {
        return $this->debate_id;
    }
    public function getComparisonId() {
        return $this->comparison_id;
    }
    public function getOwnerId() {
        return $this->owner_id;
    }
    public function initialize() {
        $this->hasMany('id', 'Votes', 'side_id', NULL);
        $this->hasMany('id', 'Arguments', 'side_id', NULL);
        $this->belongsTo('owner_id', 'Users', 'id', NULL);
        $this->belongsTo('argumentbox_id', 'Argumentboxes', 'id', NULL);
        $this->belongsTo('debate_id', 'Debates', 'id', NULL);
        $this->belongsTo('comparison_id', 'Comparisons', 'id', NULL);
        $this->hasOne('id', 'SidesAttachedPhotos', 'side_id');
        $this->hasOne('id', 'ComparisonSidesMeta', 'side_id');
        $this->hasMany('id', 'DebateSidesMeta', 'side_id');
    }
    public function deleteSideDependancies($sides, $transaction) {
        $sideTags = new SidesTags();
        $arguments = new Arguments();
        foreach ($sides as $side) {
            $votes = Votes::find('side_id=' . $side->getId());
            if ($votes) {
                $votes->delete();
            }
            $arguments->deleteSideArguments($side->getId(), $transaction);
            $sideTags->deleteSideTags($side->getId(), $transaction);
        }
    }
    public function createSideDependancies($sidesIds, $userId, $tags, $transaction, $storyId, $type) {
        $attachedPhotos = new AttachedPhotos();
        $attachedPhotos->saveAttachedPhotos($storyId, $userId, $transaction, $type);
        $tagsDecoded = !empty($tags) ? json_decode($tags, 1) : FALSE;
        if (!empty($tagsDecoded)) {
            $tagsModel = new Tags();
            $tagsModel->saveTags($sidesIds, $tagsDecoded, $transaction);
        }
    }
    public static function getSideIdByArgumentBoxId($_argumentboxID) {
        return (Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT id FROM Sides WHERE argumentbox_id = :argumentbox_id:", array('argumentbox_id' => $_argumentboxID))->toArray());
    }
    public static function getSideIdByDebateId($_debateID) {
        return (Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT id FROM Sides WHERE debate_id = :debate_id:", array('debate_id' => $_debateID))->toArray());
    }
    public static function getSideIdByCompId($_comparisonId) {
        return (Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT id FROM Sides WHERE comparison_id = :comp_id:", array('comp_id' => $_comparisonId))->toArray());
    }
    public static function getSideIdByTypeId($_type, $_id) {
        switch ($_type) {
            case self::ARGUMENTSBOX:
                $field = 'argumentbox_id';
            break;
            case self::DEBATE:
                $field = 'debate_id';
            break;
            case self::COMPARISON:
                $field = 'comparison_id';
            break;
        }
        return (Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT id FROM Sides WHERE $field = :id:", array('id' => $_id))->toArray());
    }
    public static function formatSides($_parentID, $_parentType, $order_by = "most_voted") {
        switch ($_parentType) {
            case self::DEBATE:
                $di = \Phalcon\DI::getDefault();
                $perPage = $di->get('config')->pagination->debate->arguments;
                return self::formatDebatesSides(self::getSideIdByDebateId($_parentID), $order_by, 1, $perPage);
            case self::COMPARISON:
                $di = \Phalcon\DI::getDefault();
                $perPage = $di->get('config')->pagination->comparison->arguments;
                return self::formatCompSides(self::getSideIdByCompId($_parentID), $order_by, 1, $perPage);
            default:
            break;
        }
    }
    public static function formatSide($_sideId) {
        return DebateSidesMeta::getMetaBySideID($_sideId);
    }
    public static function formatCompSide($_sideId) {
        $out = array();
        $arr = ComparisonSidesMeta::getMetaBySideID($_sideId);
        if (count($arr) > 0) {
            foreach ($arr as $val) {
                $out[] = array('title' => htmlspecialchars($val['title']), 'description' => htmlspecialchars($val['description']), 'image' => ($val['image']) ? Phalcon\DI::getDefault()->getShared('Utility')->generateURLFromDirPath($val['image'], '') : NULL);
            }
        }
        return $out;
    }
    public static function formatDebatesSides($_sides, $order_by, $_pageNum = 1, $_perPage = 4) {
        $return = array();
        if (is_array($_sides)) {
            $sum = Votes::sumVotes($_sides);
            foreach ($_sides as $side) {
                $return[] = array("id" => $side['id'], "meta" => self::formatSide($side['id']), "args" => Arguments::getSideArguments($side['id'], $order_by, $_pageNum, $_perPage), "votes" => Votes::getSideVotes($side['id']), "voted" => Votes::hasVoted($side['id'], Tokens::getSignedInUserId()), "perc" => (!$sum) ? 0 : round(Votes::getSideVotes($side['id']) / $sum * 100));
            }
        }
        return $return;
    }
    public static function formatCompSides($_sides, $order_by, $_pageNum = 1, $_perPage = 2) {
        $return = array();
        if (is_array($_sides)) {
            $totalSum = Votes::sumVotes($_sides);
            $max = Arguments::maxNPages($_sides, $_perPage);
            foreach ($_sides as $side) {
                $sum = Votes::count(array('conditions' => 'side_id=:side_id:', 'bind' => array('side_id' => $side['id']),));
                $return[] = array("id" => $side['id'], "meta" => self::formatCompSide($side['id']), "arguments_count" => Arguments::count("side_id=" . $side['id']), "args" => Arguments::getSideArguments($side['id'], $order_by, $_pageNum, $_perPage), "votes" => Votes::getSideVotes($side['id']), "voted" => Votes::hasVoted($side['id'], Tokens::getSignedInUserId()), "perc" => (!$sum) ? 0 : round($sum / $totalSum * 100), "sum" => $sum, "max" => $max);
            }
        }
        return $return;
    }
    public static function debate_sides($_sides) {
        $return = array();
        if (is_array($_sides)) {
            foreach ($_sides as $side) {
                $return[] = array("id" => $side['id'], "meta" => self::formatSide($side['id']),);
            }
        }
        return $return;
    }
    public static function comparison_sides($_sides) {
        $return = array();
        if (is_array($_sides)) {
            foreach ($_sides as $side) {
                $return[] = array("id" => $side['id'], "meta" => self::formatCompSide($side['id']),);
            }
        }
        return $return;
    }
}
