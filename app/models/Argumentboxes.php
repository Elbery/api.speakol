<?php
use Phalcon\Mvc\Model\Validator\Url as UrlValidator, Phalcon\DI\FactoryDefault;
class Argumentboxes extends BaseModel {
    const TYPE = 'argumentbox';
    protected $id;
    protected $app_id;
    protected $plugin_slider;
    protected $hash;
    protected $url;
    protected $description;
    protected $created_at;
    protected $updated_at;
    protected $title;
    protected $high_interaction;
    protected $lang;
    protected $option_id;
    protected $show_title;
    protected $code;
    protected $domain;
    protected $uri;
    protected $protocol;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setHash($hash) {
        $this->hash = $hash;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }
    public function set_title($title) {
        $this->title = $title;
        return $this;
    }
    public function set_high_interaction($no_comments) {
        $this->high_interaction = $no_comments;
    }
    public function set_lang($lang) {
        $this->lang = $lang;
    }
    public function set_option_id($op_id) {
        $this->option_id = $op_id;
    }
    public function set_show_title($flag) {
        $this->show_title = $flag;
    }
    public function set_code($code) {
        $this->code = $code;
    }
    public function set_domain($domain) {
        $this->domain = $domain;
    }
    public function set_uri($uri) {
        $this->uri = $uri;
    }
    public function set_protocol($protocol) {
        $this->protocol = $protocol;
    }
    public function set_plugin_slider($plugin_slider) {
        $this->plugin_slider = $plugin_slider;
    }
    public function get_plugin_slider() {
        return $this->plugin_slider;
    }
    public function getTitle() {
        return htmlspecialchars($this->title);
    }
    public function getId() {
        return $this->id;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function getDescription() {
        return htmlspecialchars($this->description);
    }
    public function get_high_interaction() {
        return intval($this->high_interaction);
    }
    public function get_lang() {
        return $this->lang;
    }
    public function get_option_id() {
        return $this->option_id;
    }
    public function get_show_title() {
        return intval($this->show_title);
    }
    public function get_code() {
        return $this->code;
    }
    public function get_domain() {
        return $this->domain;
    }
    public function get_uri() {
        return $this->uri;
    }
    public function get_protocol() {
        return $this->protocol;
    }
    public function getHash($hash) {
        return $this->hash;
    }
    public function initialize() {
        $this->belongsTo('app_id', 'Apps', 'id', null);
        $this->hasMany('id', 'Sides', 'argumentbox_id', NULL);
        $this->hasMany('id', 'ArgumentboxRecomended', 'argumentbox_id', NULL);
    }
    public function retriveArgumentbox($url, $code, $userId, $domains, $order_by = "most_voted") {
        if ($code == "") {
            $split_url = $this->split_url_to_domain_uri($url);
            $domain = $split_url['domain'];
            $uri = $split_url['uri'];
            $results = self::findFirst(array("conditions" => "uri = :uri: AND domain = :domain:", "bind" => array('uri' => $uri, 'domain' => $domain)));
        } else {
            for ($i = 0;$i < sizeof($domains);$i++) {
                $condition = $condition . "domain = '" . $domains[$i] . "'";
                if ($i + 1 != sizeof($domains)) {
                    $condition = $condition . " OR ";
                }
            }
            $results = self::findFirst(array("conditions" => "($condition) AND code=:code:", "bind" => array('code' => $code)));
        }
        $di = $this->getDI();
        $_pageNum = 1;
        $_perPage = $di->get('config')->pagination->argumentbox->arguments;
        $return = array();
        if ($results) {
            $diUrl = $this->getDI()->get('url');
            $followed = ArgumentboxesFollows::followed($results->getId(), $userId);
            $sides = $results->sides;
            $sidesArr = array();
            $arrVotes = array();
            $currentUserVoted = false;
            foreach ($sides as $side) {
                $argsObj = Arguments::sort_arguments($side->getId(), 0, $_perPage, $order_by);
                $arrArgs = array();
                $page = 1 < 1 ? 1 : $_pageNum;
                $startValue = ($page - 1) * $_perPage;
                $count = $side->Arguments->count(array("order" => "id desc"));
                $hasmore = ($count >= (($startValue + 1) + $_perPage)) ? true : FALSE;
                $args = array();
                foreach ($argsObj as $arg) {
                    $args['args'][] = Arguments::formatSingleArg($arg);
                }
                $args['hasmore'] = $hasmore;
                $tagsQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $tagsResults = SidesTags::find($tagsQuery);
                $tags = array();
                if ($tagsResults) {
                    foreach ($tagsResults as $tag) {
                        $tags[] = array('id' => $tag->tags->getId(), 'name' => $tag->tags->getName(), 'slug' => $tag->tags->getSlug());
                    }
                }
                $voteQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $arrVotes["{$side->getId() }"] = Votes::count($voteQuery);
                $voteQuery = array('voter_id = :voter_id: and side_id = :side_id:', 'bind' => array('side_id' => $side->getId(), 'voter_id' => $userId));
                $voted = Votes::count($voteQuery);
                $sidesArr[] = array('id' => $side->getId(), 'voted' => $voted, 'owner' => Users::getOwnerInformation($side->getOwnerId()), 'arguments' => $args, 'tags' => $tags);
                $currentUserVoted = $currentUserVoted || $voted ? true : false;
            }
            $sideAttach = ArgumentboxesAttachments::find(array('argumentbox_id = :argumentbox_id:', 'bind' => array('argumentbox_id' => $results->getId())));
            $sideAttached = array();
            foreach ($sideAttach as $attchment) {
                $sideAttached[] = array('id' => $attchment->AttachedPhotos->getId(), 'url' => $attchment->AttachedPhotos->getUrl());
            }
            $ads = PluginsAds::plugin_ads("argumentbox", $results->getId(), 2);
            $app = Apps::findFirst($results->getAppId());
            $audio = PluginsFeature::validate_feature_using_plan_id($app->get_plan(), "audio_comment");
            $date = strtotime($results->getUpdatedAt());
            $date = date('F j \a\t h:i A', $date);
            $argumentbox_options = ArgumentboxOptions::findFirst($results->get_option_id());
            $return[] = array('id' => $results->getId(), 'url' => $results->get_domain() . $results->get_uri(), 'code' => $results->get_code(), 'created_at' => $results->getCreatedAt(), 'updated_at' => $results->getUpdatedAt(), 'updated_date' => $date, 'description' => $this->getDescription(), 'title' => $results->getTitle(), 'option' => array($argumentbox_options->get_option_1(), $argumentbox_options->get_option_2()), 'followed' => $followed, 'app_id' => $results->getAppId(), 'audio' => $audio, 'attached_photos' => $sideAttached, 'ads' => $ads, 'sides' => $sidesArr, 'votes' => Argumentboxes::generateVotingStats($arrVotes), 'currentUserVoted' => $currentUserVoted, 'app_name' => $app->getName(), 'app_image' => $app->getImage(), 'show_title' => $results->get_show_title(), 'plugin_slider' => $results->get_plugin_slider());
        }
        return array('status' => 'OK', 'data' => $return);
    }
    public function listArgumentboxes($appId) {
        $results = self::findFirst('app_id=' . $appId);
        $sides = $results->sides;
        foreach ($sides as $side) {
            $al = array();
            $si = array();
            $si['id'] = $side->getId();
            $arguments = $side->arguments;
            foreach ($arguments as $argument) {
                $ar = array();
                $ar['id'] = $argument->getId();
                $ar['content'] = $argument->getContext();
                $replies = $argument->replies;
                foreach ($replies as $replie) {
                    $re = array();
                    $re['id'] = $replie->getId();
                    $re['content'] = $replie->getContent();
                    $ar['replies'][] = $re;
                }
                $si['arguments'][] = $ar;
            }
            $sidesArray[] = $si;
        }
        return array('status' => 'OK', 'data' => $return);
    }
    public function listMiniArguments($conditions = null, $option = null, $sort = array()) {
        $profileUserId = Tokens::checkAccess();
        $comparisonData = array();
        $argumentboxes = self::find(array_merge($conditions, $option));
        $main = array();
        $di = new Phalcon\DI();
        $utility = new Speakol\Utility($di);
        foreach ($argumentboxes as $argumentbox) {
            $sides = $argumentbox->getSides();
            $recomended = $argumentbox->getArgumentboxRecomended()->toArray();
            $argumentbox = $argumentbox->toArray();
            $argumentbox['recomended'] = $recomended['0'];
            $argumentbox['recomended']['title'] = htmlspecialchars($argumentbox['recomended']['title']);
            $appId = $argumentbox['app_id'];
            $app = $usr = array();
            if (!is_null($appId)) {
                $app = Apps::findFirst($appId)->toArray();
            } else {
                $usr = Users::findFirst(array('conditions' => "id=$userId", 'columns' => array('id', 'first_name', 'last_name', 'profile_picture')))->toArray();
                $usr['name'] = $usr['first_name'] . " " . $usr['last_name'];
            }
            $argumentbox['module'] = self::ARGUMENTSBOX;
            $argumentbox['sort_id'] = !empty($sort) ? $sort[self::ARGUMENTSBOX . '_' . $argumentbox['id']] : 0;
            $argumentbox['created_at'] = $utility->time_elapsed_string(strtotime($argumentbox['created_at']));
            $argumentbox['description'] = $utility->splitWords($argumentbox['description']);
            $argumentbox['title'] = htmlspecialchars($argumentbox['title']);
            $argumentbox['url'] = $argumentbox['domain'] . $argumentbox['uri'];
            $main[$argumentbox['id']] = $argumentbox;
            $totalVotes = $totalComments = 0;
            $sidesArr = array();
            foreach ($sides as $side) {
                $sid['votes_count'] = Votes::count(array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId())));
                $sid['comments_count'] = Arguments::count(array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId())));
                $sid['side_id'] = $side->getId();
                $totalVotes+= $sid['votes_count'];
                $totalComments+= $sid['comments_count'];
                if ($profileUserId) {
                    $sid['voted'] = Votes::hasVoted($side->getId(), $profileUserId);
                } else {
                    $sid['voted'] = false;
                }
                $sidesArr[] = $sid;
            }
            foreach ($sidesArr as $k => $sid) {
                $sidesArr[$k]['percentage'] = !$totalVotes ? 0 : number_format($sid['votes_count'] * 100 / $totalVotes, '2', '.', '');
            }
            $main[$argumentbox['id']] = array('user' => $usr, 'app' => $app, 'owner_is_app' => $appId ? true : false, 'data' => $argumentbox, 'sides' => $sidesArr, 'counts' => array('total_votes' => $totalVotes, 'total_comments' => $totalComments,));
        }
        return $main;
    }
    function split_url_to_domain_uri($url) {
        $url = str_replace("http://", "", $url);
        $url = str_replace("https://", "", $url);
        $slash_del_pos = strpos($url, "/") ? strpos($url, "/") : strlen($url);
        $domain = substr($url, 0, $slash_del_pos);
        $del_pos = strpos($domain, ":") ? strpos($domain, ":") : strlen($domain);
        $domain = substr($domain, 0, $del_pos);
        $split_url = explode($domain, $url);
        return array("domain" => $domain, "uri" => $split_url[1]);
    }
    public function appendDataToModel($data, $update = false) {
        if (empty($data['owner_id']) && !empty($data['app_id'])) {
            $owner = AppAdmins::findFirstByAppId($data['app_id']);
            if ($owner) {
                $data['owner_id'] = $owner->getUserId();
            } else {
                return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
            }
        }
        $uuid = $this->generate_uuid();
        if (false === $update) {
            if ($data['title'] != "" || $data['title'] != NULL) {
                $title = $data['title'];
            } else {
                $title = NULL;
            }
            $app = Apps::findFirst($data['app_id']);
            if ($app->get_plan() > 1) {
                $this->set_show_title(1);
            } else {
                $this->set_show_title(0);
            }
            $interaction = Interaction::findFirst(array("url = :url: ", 'bind' => array('url' => $data['url'])));
            if (!$interaction) {
                $uuid = $this->generate_uuid();
                $hash = new Interaction;
                $hash->setHash($uuid);
                $hash->setCreatedAt(date("Y-m-d H:i:s"));
                $hash->setUpdatedAt(date("Y-m-d H:i:s"));
                $hash->setCount(0);
                $hash->setUrl($data['url']);
                $hash->save();
            }
            $split_url = $this->split_url_to_domain_uri($data['url']);
            $this->setAppId($data['app_id']);
            $this->set_domain($split_url['domain']);
            $this->set_uri($split_url['uri']);
            $this->set_code($data['code']);
            $this->setDescription($data['description']);
            $this->set_title($title);
            $this->set_high_interaction(0);
            $this->set_lang($data['lang']);
            $this->set_option_id($data['option']);
            $this->set_plugin_slider(1);
            $this->setHash($uuid);
        }
        $argumentbox = self::findFirst(array('uri = :uri: AND domain = :domain: AND code= :code:', 'bind' => array('uri' => $this->get_uri(), 'domain' => $this->get_domain(), 'code' => $this->get_code())));
        $transactionManager = new Phalcon\Mvc\Model\Transaction\Manager();
        $transaction = $transactionManager->get();
        $curr_date = date('Y-m-d H:i:s');
        $this->setCreatedAt($curr_date);
        $this->setUpdatedAt($curr_date);
        $protocol = (stristr($data['url'], "https://")) ? "https://" : "http://";
        $this->set_protocol($protocol);
        if (!$argumentbox && $this->save()) {
            $side = new Sides();
            $side->setArgumentboxId($this->getId());
            $side->setOwnerId($data['owner_id']);
            $sideTwo = new Sides();
            $sideTwo->setArgumentboxId($this->getId());
            $sideTwo->setOwnerId($data['owner_id']);
            if ($side->save() && $sideTwo->save()) {
                $sidesIds = array('side_one' => $side->getId(), 'side_two' => $sideTwo->getId());
                $data['tags'] = !empty($data['tags']) ? $data['tags'] : false;
                $sideModel = new Sides();
                $sideModel->createSideDependancies($sidesIds, $data['owner_id'], $data['tags'], $transaction, $this->getId(), static ::TYPE);
                $transaction->commit();
                return array('status' => 'OK', 'data' => array($this->getId()));
            } else {
                $transaction->rollback("Can't save Side");
                $errors = array();
                foreach ($side->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                foreach ($sideTwo->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                return array('status' => 'ERROR', 'messages' => $errors);
            }
            $attachedPhotos = new AttachedPhotos();
            $attachedPhotos->saveAttachedPhotos($this->getId(), $data['app_id'], $transaction, static ::TYPE);
        } else {
            if ($update) {
                $argumentbox->setDescription($data['content']);
                if ($argumentbox->update()) {
                    $transaction->commit();
                    return array('status' => 'OK', 'data' => array($argumentbox->getId()));
                }
                $transaction->rollback(FactoryDefault::getDefault()->get('t')->_("argbox-cant-update"));
                return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("argbox-cant-update"));
            } else {
                $transaction->rollback(FactoryDefault::getDefault()->get('t')->_("rejected"));
                $errors = array();
                foreach ($this->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                return array('status' => 'ERROR', 'messages' => $errors);
            }
        }
    }
    public function delete_argumentbox_data($data) {
        try {
            $transactionManager = new Phalcon\Mvc\Model\Transaction\Manager();
            $transaction = $transactionManager->get();
            $argumentbox = self::findFirst(array('CONCAT(domain,uri) = :url: AND app_id = :app_id: AND code=:code:', 'bind' => array('url' => $data['url'], 'app_id' => $data['app_id'], 'code' => $data['code'])));
            if ($argumentbox) {
                $notifications = Notifications::find("notifiable_id=" . $argumentbox->getId() . " AND notification_type='argumentsbox'");
                if ($notifications) {
                    foreach ($notifications as $notification) {
                        $notification_recipients = NotificationRecipients::find('notification_id=' . $notification->getId());
                        if ($notification_recipients) {
                            $notification_recipients->delete();
                        }
                        $notification->delete();
                    }
                }
                $sides = Sides::find(array('argumentbox_id = :argumentbox_id:', 'bind' => array('argumentbox_id' => $argumentbox->getId())));
                if ($sides) {
                    $sideModel = new Sides();
                    $sideModel->deleteSideDependancies($sides, $transaction);
                    $sides->delete();
                } else {
                    $transaction->rollback("Can't find side");
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
                if ($argumentbox->delete()) {
                    $transaction->commit();
                    return array('status' => 'OK', 'data' => "Argumentbox removed successfully");
                } else {
                    $transaction->rollback("Can't delete argumentbox");
                    return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
                }
            } else {
                $transaction->rollback("Can't find argumentbox");
                return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
            }
        }
        catch(Phalcon\Mvc\Model\Transaction\Failed $e) {
            return array('status' => 'ERROR', 'messages' => $e->getMessage());
        }
    }
    public function appendDataToModelDelete(array $data = array()) {
        try {
            if (empty($data)) {
                return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));;
            }
            $transactionManager = new Phalcon\Mvc\Model\Transaction\Manager();
            $transaction = $transactionManager->get();
            $argumentbox = self::findFirst(array('slug= :slug: AND app_id = :app_id: ', 'bind' => array('slug' => $data['slug'], 'app_id' => $data['app_id'])));
            if ($argumentbox) {
                $sides = Sides::find(array('parent_id= :parent_id: AND parent_type = :parent_type: ', 'bind' => array('parent_id' => $argumentbox->getId(), 'parent_type' => static ::TYPE)));
                if ($sides) {
                    $sideModel = new Sides();
                    $sideModel->deleteSideDependancies($sides, $transaction);
                    $sides->delete();
                } else {
                    $transaction->rollback("Can't find side");
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
                $attachmentsResults = ArgumentboxesAttachments::find(array('argumentbox_id= :argumentbox_id:', 'bind' => array('argumentbox_id' => $argumentbox->getId())));
                if ($attachmentsResults) {
                    $attachments = new AttachedPhotos();
                    $attachments->deleteAttachedPhotos($attachmentsResults, $transaction);
                }
                if ($argumentbox->delete()) {
                    $transaction->commit();
                    return array('status' => 'OK', 'data' => array(true));
                } else {
                    $transaction->rollback("Can't delete argumentbox");
                    return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
                }
            } else {
                $transaction->rollback("Can't find argumentbox");
                return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
            }
        }
        catch(Phalcon\Mvc\Model\Transaction\Failed $e) {
            return array('status' => 'ERROR', 'messages' => $e->getMessage());
        }
    }
    public static function getArgumentsBoxByURL($_url, $code) {
        $_url = str_replace("http://", "", $_url);
        $_url = str_replace("https://", "", $_url);
        $rowArgumentBox = Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT id FROM Argumentboxes WHERE CONCAT(domain,uri) = :url: AND code = :code:", array('url' => $_url, 'code' => $code))->getFirst();
        if ($rowArgumentBox == FALSE) {
            return FALSE;
        } else {
            return (intval($rowArgumentBox->id));
        }
    }
    public static function generateVotingStats($arrData) {
        $objData = new stdClass();
        $objData->intSumVotes = 0;
        $objData->sides = array();
        foreach ($arrData as $key => $value) {
            $objData->intSumVotes+= $value;
        }
        $counter = 0;
        foreach ($arrData as $key => $value) {
            $side = array();
            $side['id'] = $key;
            $side['percentage'] = round(($objData->intSumVotes == 0) ? 0 : $value / $objData->intSumVotes * 100);
            $side['percentage'].= '%';
            $side['votes'] = $value;
            array_push($objData->sides, $side);
            if ($counter == 0) {
                $objData->proVotes = intval($value);
            } else {
                $objData->conVotes = intval($value);
            }
            $counter++;
        }
        return $objData;
    }
    public static function voteDevoteActionHelper($_argumentsBoxId, $_sideId, $_userId, $link) {
        $sidesTotalVotes = array();
        $sides = array();
        $arrSides = Sides::getSideIdByArgumentBoxId($_argumentsBoxId);
        if (is_array($arrSides)) {
            foreach ($arrSides as $key => $value) {
                if (Votes::unvote($value["id"], $_userId) == False) {
                    return FALSE;
                }
                $sides[] = $value["id"];
            }
            $vote_id = votes::vote($_sideId, $_userId, $link);
            if ($vote_id) {
                $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
                $new_log = new Log();
                $new_log->log_current_action($_userId, $sentHeaders['Ip'], "/argumentsbox/vote/" . $_argumentsBoxId . "/" . $_sideId, $vote_id);
                if (is_array($sides)) {
                    foreach ($sides as $side) {
                        $sidesInfo = array();
                        $sidesInfo['side_id'] = $side;
                        $sidesInfo['votes'] = Votes::getSideVotes($side);
                        $sidesTotalVotes[] = $sidesInfo;
                    }
                    return $sidesTotalVotes;
                }
            }
        }
        return false;
    }
    public static function getFavoredSideOfArgumentsBox($objData) {
        $arrSides = Sides::getSideIdByArgumentBoxId($objData->argumentsBoxID);
        if (is_array($arrSides)) {
            foreach ($arrSides as $key => $value) {
                if (Votes::hasVoted($value["id"], $objData->signedInUserID) == TRUE) {
                    return $value["id"];
                }
            }
        }
        throw new Exception(FactoryDefault::getDefault()->get('t')->_("argbox-user-not-voted"), 400);
    }
    public static function doesExist($_argumentsboxID) {
        $argumentbox = Argumentboxes::findFirst($_argumentsboxID);
        if (FALSE != $argumentbox) {
            return $argumentbox;
        } else {
        }
    }
}
