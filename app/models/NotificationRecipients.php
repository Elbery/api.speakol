<?php
class NotificationRecipients extends BaseModel {
    protected $id;
    protected $user_id;
    protected $notification_id;
    protected $seen;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    public function setNotificationId($notification_id) {
        $this->notification_id = $notification_id;
        return $this;
    }
    public function setSeen($seen) {
        $this->seen = $seen;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getUserId() {
        return $this->user_id;
    }
    public function getNotificationId() {
        return $this->notification_id;
    }
    public function getSeen() {
        return $this->seen;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('notification_id', 'Notifications', 'id', NULL);
        $this->belongsTo('user_id', 'Users', 'id', NULL);
    }
    public function updateRows($query) {
        $query = $this->getModelsManager()->createQuery($query);
        return $query->execute();
    }
}
