<?php
class MaxDomains extends BaseModel {
    protected $id;
    protected $plan_id;
    protected $no_domains;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_plan_id($plan_id) {
        $this->plan_id = $plan_id;
    }
    public function set_no_domains($no_domains) {
        $this->no_domains = $no_domains;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_plan_id() {
        return $this->plan_id;
    }
    public function get_no_domains() {
        return $this->no_domains;
    }
}
