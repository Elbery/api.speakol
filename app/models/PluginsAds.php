<?php
class PluginsAds extends BaseModel {
    protected $id;
    protected $plugin_id;
    protected $plugin_type;
    protected $ad_id;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_plugin_id($plugin_id) {
        $this->plugin_id = $plugin_id;
    }
    public function set_plugin_type($p_type) {
        $this->plugin_type = $p_type;
    }
    public function set_ad_id($ad_id) {
        $this->ad_id = $ad_id;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_plugin_id() {
        return $this->plugin_id;
    }
    public function get_plugin_type() {
        return $this->plugin_type;
    }
    public function get_ad_id() {
        return $this->ad_id;
    }
    public static function plugin_ads($plugin_type, $id, $limit) {
        $plugin_ads = PluginsAds::find(array('conditions' => 'plugin_id=' . $id . " AND plugin_type='" . $plugin_type . "'", 'limit' => $limit))->toArray();
        $ads = array();
        $current_user = Tokens::getSignedInUserId();
        for ($i = 0;$i < sizeof($plugin_ads);$i++) {
            $ad = Ads::findFirst($plugin_ads[$i]['ad_id']);
            if ($ad) {
                $ad = $ad->toArray();
                $date = strtotime($ad['created_at']);
                $date = date('F jS', $date);
                $ad['created_at'] = $date;
                if ($ad['attached_photos']) {
                    $ad['attached_photos'] = array(array('url' => ((empty($_SERVER['HTTPS'])) ? 'http' : 'https') . '://' . $_SERVER['SERVER_NAME'] . $ad['attached_photos']));
                }
                $current_user_voted = FALSE;
                if ($current_user) {
                    $current_user_voted = AdsThumbs::findFirst('thumber_id=' . $current_user . ' AND ad_id=' . $ad['id']);
                }
                $ad['replies_count'] = AdsReplies::count(array('conditions' => 'ad_id=' . $ad['id'] . ' AND parent_reply IS NULL'));
                $ad['thumbs_up'] = AdsThumbs::count(array('conditions' => 'thumb_up=1 AND ad_id=' . $ad['id']));
                $ad['thumbs_down'] = AdsThumbs::count(array('conditions' => 'thumb_up=0 AND ad_id=' . $ad['id']));
                $ad['currentUserhasVoted'] = (FALSE != $current_user_voted);
                $ad['currentUserVote'] = (FALSE != $current_user_voted ? $current_user_voted->get_thumb_up() : FALSE);
                array_push($ads, $ad);
            }
        }
        return $ads;
    }
}
