<?php
class SpeakolIntro extends BaseModel {
    protected $id;
    protected $description;
    protected $lang;
    protected $dashboard;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_description($desc) {
        $this->description = $desc;
    }
    public function set_lang($lang) {
        $this->lang = $lang;
    }
    public function set_dashboard($intro) {
        $this->dashboard = $intro;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_description() {
        return $this->description;
    }
    public function get_lang() {
        return $this->lang;
    }
    public function get_dashboard() {
        return $this->dashboard;
    }
}
