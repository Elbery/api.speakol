<?php
class SidesTags extends BaseModel {
    protected $id;
    protected $side_id;
    protected $tag_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setSideId($side_id) {
        $this->side_id = $side_id;
        return $this;
    }
    public function setTagId($tag_id) {
        $this->tag_id = $tag_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getTagId() {
        return $this->tag_id;
    }
    public function initialize() {
        $this->belongsTo('tag_id', 'Tags', 'id', array('foreignKey' => true));
        $this->belongsTo('side_id', 'Sides', 'id', array('foreignKey' => true));
    }
    public function saveTagsSides($tagId, $sideId, $transaction) {
        $this->setId(NULL);
        $this->setTagId($tagId);
        $this->setSideId($sideId);
        if (!$this->save()) {
            $transaction->rollback("Can't save Tag and side id frogin keys");
        }
    }
    public function deleteSideTags($sideId, $transaction) {
        $sideTags = static ::find(array('side_id= :side_id:', 'bind' => array('side_id' => $sideId)));
        if ($sideTags) {
            $tags = new Tags();
            foreach ($sideTags as $tag) {
                $tagId = $tag->getTagId();
                if (!$tag->delete()) {
                    $transaction->rollback("Can't delete tags");
                }
                $tags->delete($tagId);
            }
        }
    }
}
