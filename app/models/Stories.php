<?php
class Stories extends BaseModel {
    const TYPE = 'story';
    protected $id;
    protected $title;
    protected $slug;
    protected $featured;
    protected $content;
    protected $charset;
    protected $foreign_id;
    protected $category_id;
    protected $app_id;
    protected $url;
    protected $created_at;
    protected $updated_at;
    public static $createStories = array('title' => array('key' => 'title', 'required' => true), 'content' => array('key' => 'content', 'required' => true), 'featured' => array('key' => 'featured', 'required' => true), 'category_id' => array('key' => 'category_id', 'required' => true), 'app_id' => array('key' => 'app_id', 'required' => true), 'foreign_id' => array('key' => 'foreign_id', 'required' => true), 'tags' => array('key' => 'tags', 'required' => true), 'url' => array('key' => 'url', 'required' => true));
    public static $updateStories = array('title' => array('key' => 'title', 'required' => true), 'content' => array('key' => 'content', 'required' => true));
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function setSlug($slug) {
        $this->slug = preg_replace("/[^\w]+/", "-", $slug);
        return $this;
    }
    public function setFeatured($featured) {
        $this->featured = $featured;
        return $this;
    }
    public function setContent($content) {
        $this->content = $content;
        return $this;
    }
    public function setCharset($charset) {
        $this->charset = $charset;
        return $this;
    }
    public function setForeignId($foreign_id) {
        $this->foreign_id = $foreign_id;
        return $this;
    }
    public function setCategoryId($category_id) {
        $this->category_id = $category_id;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getSlug() {
        return $this->slug;
    }
    public function getFeatured() {
        return $this->featured;
    }
    public function getContent() {
        return $this->content;
    }
    public function getCharset() {
        return $this->charset;
    }
    public function getForeignId() {
        return $this->foreign_id;
    }
    public function getCategoryId() {
        return $this->category_id;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->hasMany('id', 'SphereStories', 'story_id', NULL);
        $this->hasMany('id', 'StoryFollows', 'story_id', NULL);
        $this->belongsTo('category_id', 'Categories', 'id', NULL);
        $this->belongsTo('app_id', 'Apps', 'id', NULL);
    }
    public function validateEmptyFields($defaults, $data = array()) {
        if (empty($defaults) || empty($data)) {
            return array('status' => 'ERROR', 'messages' => 'Rejected!');
        }
        foreach ($defaults as $def) {
            $value = $data[$def['key']];
            if (empty($value) && TRUE === $def['required']) {
                return array('status' => 'ERROR', 'messages' => 'Some fields are missing!');
            }
        }
        return array('status' => 'OK', 'data' => true);
    }
    public function listStories($results = array()) {
        if (empty($results)) {
            $results = static ::find();
        }
        if ($results) {
            $return = array();
            foreach ($results as $row) {
                $sidesQuery = array('parent_id = :parent_id:', 'bind' => array('parent_id' => $row->getId()));
                $sides = Sides::find($sidesQuery);
                $sidesArr = array();
                $totalArgs = 0;
                foreach ($sides as $side) {
                    $argsNumbers = SidesArguments::count(array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId())));
                    $voteQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                    $sidesArr[] = array('id' => $side->getId(), 'votes_count' => $argsNumbers);
                    $totalArgs+= $argsNumbers;
                }
                $totalArgs = $totalArgs ? $totalArgs : 1;
                foreach ($sidesArr as $key => $side) {
                    $sidesArr[$key]['percentage'] = number_format(($side['votes_count'] / $totalArgs) * 100, 2, '.', '') . '%';
                }
                $return[] = array('id' => $row->getId(), 'title' => $row->getTitle(), 'content' => $row->getContent(), 'slug' => $row->getSlug(), 'link' => $this->getDI()->get('url')->get("opinion/" . $row->getSlug()), 'created_at' => $row->getCreatedAt(), 'updated_at' => $row->getUpdatedAt(), 'sides' => $sidesArr, 'total_arguments' => $totalArgs);
            }
        }
        return array('status' => 'OK', 'data' => $return);
    }
    public function retriveStory($slug, $userId) {
        $results = static ::findFirst(array("conditions" => "slug= :slug:", "bind" => array('slug' => $slug)));
        if ($results) {
            $return = array();
            $diUrl = $this->getDI()->get('url');
            $followed = OpinionFollows::followed($results->getId(), $userId);
            $sidesQuery = array('parent_id = :parent_id:', 'bind' => array('parent_id' => $results->getId()));
            $sides = Sides::find($sidesQuery);
            $sidesArr = array();
            foreach ($sides as $side) {
                $argQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $argsObj = SidesArguments::find($argQuery);
                $args = array();
                foreach ($argsObj as $arg) {
                    $repliesCountQuery = array('argument_id = :argument_id:', 'bind' => array('argument_id' => $arg->Arguments->getId()));
                    $repliesCount = Replies::count($repliesCountQuery);
                    $thumbsUpQuery = array('argumnet_id = :argumnet_id: AND thumb_up = :thumb_up:', 'bind' => array('argumnet_id' => $arg->Arguments->getId(), 'thumb_up' => '1'));
                    $thumbsUpDown = array('argumnet_id = :argumnet_id: AND thumb_up = :thumb_up:', 'bind' => array('argumnet_id' => $arg->Arguments->getId(), 'thumb_up' => '-1'));
                    $thumbsUp = ArgumentThumbs::count($thumbsUpQuery);
                    $thumbsDown = ArgumentThumbs::count($thumbsUpDown);
                    $args[] = array('id' => $arg->getArgumnetId(), 'context' => $arg->Arguments->getContext(), 'replies_count' => $repliesCount, 'thumbs_up' => $thumbsUp, 'thumbs_down' => $thumbsDown, 'voted' => false, 'attached_photos' => AttachedPhotos::attachedImages($arg->Arguments->getOwnerId()), 'owner' => Users::getOwnerInformation($arg->Arguments->getOwnerId()));
                }
                $tagsQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $tagsResults = SidesTags::find($tagsQuery);
                $tags = array();
                if ($tagsResults) {
                    foreach ($tagsResults as $tag) {
                        $tags[] = array('id' => $tag->tags->getId(), 'name' => $tag->tags->getName(), 'slug' => $tag->tags->getSlug());
                    }
                }
                $voteQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $sidesArr[] = array('id' => $side->getId(), 'votes_count' => Votes::count($voteQuery), 'owner' => Users::getOwnerInformation($side->getOwnerId()), 'arguments' => $args, 'tags' => $tags);
            }
            $sideAttach = OpinionsAttachments::find(array('opinion_id = :opinion_id:', 'bind' => array('opinion_id' => $results->getId())));
            $sideAttached = array();
            foreach ($sideAttach as $attchment) {
                $sideAttached[] = array('id' => $attchment->AttachedPhotos->getId(), 'url' => $attchment->AttachedPhotos->getUrl());
            }
            $app = array('profile_photo' => AttachedPhotos::attachedImages($userId), 'id' => $results->Apps->getId(), 'name' => $results->Apps->getName(), 'slug' => $results->Apps->getSlug(),);
            $return[] = array('id' => $results->getId(), 'title' => $results->getTitle(), 'app' => $app, 'content' => $results->getContent(), 'slug' => $results->getSlug(), 'link' => $diUrl->get("opinion/" . $results->getSlug()), 'created_at' => $results->getCreatedAt(), 'updated_at' => $results->getUpdatedAt(), 'followed' => $followed, 'owner' => Users::getOwnerInformation($userId), 'attached_photos' => $sideAttached, 'sides' => $sidesArr);
        }
        return array('status' => 'OK', 'data' => $return);
    }
    public function appendDataToModel($data, $update = false) {
        if (false === $update) {
            $app = false;
            if (!empty($data['app_id'])) {
                $app = Apps::findFirst($data['app_id']);
            }
            if (!$app) {
                return array('status' => 'ERROR', 'messages' => 'App id not wrong!');
            }
            $this->setTitle($data['title']);
            $this->setContent($data['content']);
            $this->setSlug($data['title']);
            $this->setCategoryId($data['category_id']);
            $this->setAppId($data['app_id']);
            $this->setFeatured($data['featured']);
            $this->setForeignId($data['foreign_id']);
            $this->setUrl($data['url']);
        } else {
            $this->setSlug($data['slug']);
        }
        $story = static ::findFirst(array('slug= :slug:', 'bind' => array('slug' => $this->getSlug())));
        $transactionManager = new Phalcon\Mvc\Model\Transaction\Manager();
        $transaction = $transactionManager->get();
        if (!$update && !$story && $this->save()) {
            $side = new Sides();
            $side->setParentId($this->getId());
            $side->setParentType(static ::TYPE);
            $side->setOwnerId($data['app_id']);
            $sideTwo = new Sides();
            $sideTwo->setParentId($this->getId());
            $sideTwo->setParentType(static ::TYPE);
            $sideTwo->setOwnerId($data['app_id']);
            if ($side->save()) {
                $sideTwo->save();
                $sidesIds = array('side_one' => $side->getId(), 'side_two' => $sideTwo->getId());
                $data['tags'] = !empty($data['tags']) ? $data['tags'] : false;
                $sideModel = new Sides();
                $sideModel->createSideDependancies($sidesIds, $data['app_id'], $data['tags'], $transaction, $this->getId(), static ::TYPE);
                $transaction->commit();
                return array('status' => 'OK', 'data' => array($this->getId()));
            } else {
                $transaction->rollback("Can't save Side");
                return array('status' => 'ERROR', 'messages' => 'Rejected!');
            }
            $attachedPhotos = new AttachedPhotos();
            $attachedPhotos->saveAttachedPhotos($this->getId(), $data['app_id'], $transaction, static ::TYPE);
        } else {
            if ($update) {
                $story->setTitle($data['title']);
                $story->setContent($data['content']);
                if ($story->update()) {
                    $transaction->commit();
                    return array('status' => 'OK', 'data' => array($story->getId()));
                }
                $transaction->rollback("Can't update");
                return array('status' => 'ERROR', 'messages' => 'Rejected!');
            } else {
                $transaction->rollback("Rejected");
                return array('status' => 'ERROR', 'messages' => 'Rejected!');
            }
        }
    }
    public function appendDataToModelDelete(array $data = array()) {
        try {
            if (empty($data)) {
                return array('status' => 'ERROR', 'messages' => 'Rejected!');;
            }
            $transactionManager = new Phalcon\Mvc\Model\Transaction\Manager();
            $transaction = $transactionManager->get();
            $story = static ::findFirst(array('slug= :slug:', 'bind' => array('slug' => $data['slug'])));
            if ($story) {
                $sides = Sides::find(array('parent_id= :parent_id: AND parent_type = :parent_type: ', 'bind' => array('parent_id' => $story->getId(), 'parent_type' => static ::TYPE)));
                if ($sides) {
                    $sideModel = new Sides();
                    $sideModel->deleteSideDependancies($sides, $transaction);
                    $sides->delete();
                } else {
                    $transaction->rollback("Can't find side");
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
                $attachmentsResults = StoriesAttachments::find(array('story_id= :story_id:', 'bind' => array('story_id' => $story->getId())));
                if ($attachmentsResults) {
                    $attachments = new AttachedPhotos();
                    $attachments->deleteAttachedPhotos($attachmentsResults, $transaction);
                }
                if ($story->delete()) {
                    $transaction->commit();
                    return array('status' => 'OK', 'data' => array(true));
                } else {
                    $transaction->rollback("Can't delete story");
                    return array('status' => 'ERROR', 'messages' => 'Rejected!');
                }
            } else {
                $transaction->rollback("Can't find opinion");
                return array('status' => 'ERROR', 'messages' => 'Rejected!');
            }
        }
        catch(Phalcon\Mvc\Model\Transaction\Failed $e) {
            return array('status' => 'ERROR', 'messages' => $e->getMessage());
        }
    }
    public function getStoriesByAppIdAndForginId($data) {
        if (empty($data)) {
            return array('status' => 'ERROR', 'messages' => 'Rejected!');
        }
        $app = Apps::findFirst(array('uid = :app_uid: ', 'bind' => array('app_uid' => $data['app_uid'])));
        if ($app) {
            $story = static ::findFirst(array('app_id = :app_id: AND foreign_id = :foreign_id:', 'bind' => array('app_id' => $app->getId(), 'foreign_id' => $data['foreign_id'])));
            return $this->retriveStory($story->getSlug(), 6);
        } else {
            return array('status' => 'ERROR', 'messages' => 'Rejected!');
        }
    }
}
