<?php class PluginsFeature extends BaseModel {
    protected $id;
    protected $feature_id;
    protected $plan_id;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_feature_id($fid) {
        $this->feature_id = $fid;
    }
    public function set_plan_id($pid) {
        $this->plan_id = $pid;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_feature_id() {
        return $this->feature_id;
    }
    public function get_plan_id() {
        return $this->plan_id;
    }
    public static function validate_feature_using_plan_id($plan_id, $feature_name) {
        $feature = Feature::findFirst(array('conditions' => "name=:feature_name:", 'bind' => array('feature_name' => $feature_name)));
        $available_feature = PluginsFeature::findFirst('feature_id=' . $feature->get_id() . ' AND plan_id=' . $plan_id);
        if ($available_feature) {
            return true;
        }
        return false;
    }
    public static function validate_feature($feature_name) {
        $signedInUserID = Tokens::checkAccess();
        $app_admin = AppAdmins::findFirst("user_id=$signedInUserID");
        $app = Apps::findFirst($app_admin->getAppId());
        $feature = Feature::findFirst(array('conditions' => "name=:feature_name:", 'bind' => array('feature_name' => $feature_name)));
        $available_feature = PluginsFeature::findFirst('feature_id=' . $feature->get_id() . ' AND plan_id=' . $app->get_plan());
        if ($available_feature) {
            return true;
        }
        return false;
    }
    public static function max_moderators($app_id) {
        $moderators_count = AppAdmins::count("app_id=$app_id");
        $app = Apps::findFirst($app_id);
        if ($app->get_plan() == 4) {
            $max_publishers = "Unlimited";
        } else {
            $max_publishers = MaxPublishers::findFirst('plan_id=' . $app->get_plan());
            if ($moderators_count == $max_publishers->get_max_publishers()) {
                return array('status' => 'error', 'message' => 'Your app reached max number of moderators');
            }
        }
        return array('status' => 'ok', 'data' => array('max' => $max_publishers->get_max_publishers(), 'used' => $moderators_count));
    }
}
