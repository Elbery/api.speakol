<?php
class Receipt extends BaseModel {
    protected $id;
    protected $invoice_id;
    protected $gateway_charge_id;
    protected $created_at;
    protected $address;
    protected $website;
    protected $user_name;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_invoice_id($invoice_id) {
        $this->invoice_id = $invoice_id;
    }
    public function set_gateway_charge_id($gateway_charge_id) {
        $this->gateway_charge_id = $gateway_charge_id;
    }
    public function set_created_at($creation_time) {
        $this->created_at = $creation_time;
    }
    public function set_address($address) {
        $this->address = $address;
    }
    public function set_website($site) {
        $this->website = $site;
    }
    public function set_user_name($u_name) {
        $this->user_name = $u_name;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_invoice_id() {
        return $this->invoice_id;
    }
    public function get_gateway_charge_id() {
        return $this->gateway_charge_id;
    }
    public function get_created_at() {
        return $this->created_at;
    }
    public function get_address() {
        return $this->address;
    }
    public function get_website() {
        return $this->website;
    }
    public function get_user_name() {
        return $this->user_name;
    }
}
