<?php
class Categories extends BaseModel {
    protected $id;
    protected $name;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function initialize() {
        $this->hasMany('id', 'Opinions', 'category_id', NULL);
        $this->hasMany('id', 'Stories', 'category_id', NULL);
        $this->hasMany('id', 'Apps', 'category_id', NULL);
        $this->belongsTo('id', 'Debates', 'category_id', NULL);
    }
    public function viewCats($catId, $page = '0', $perPage = '5', $orderBy = 'created_at DESC') {
        $cond = array('category_id = :category_id: ', 'bind' => array('category_id' => $catId));
        $return = array(self::DEBATE => array(), self::COMPARISON => array(),);
        $cats = self::findFirst($catId)->toArray();
        $debatsCount = Debates::count($cond);
        $comparisonCount = Comparisons::count($cond);
        $startValue = ($page - 1) * $perPage;
        $hasmore = false;
        if ($debatsCount > $startValue) {
            $debates = new Debates();
            $conditions = array('category_id = :category_id: ');
            $options = array('bind' => array('category_id' => $catId), 'limit' => array('number' => $perPage, 'offset' => $startValue));
            $return[self::DEBATE] = $debates->listMiniDebates($conditions, $options, array());
            $hasmore = ($debatsCount >= (($startValue + 1) + $perPage)) ? true : false;
        }
        if ($comparisonCount > $startValue) {
            $comparison = new Comparisons();
            $conditions = array('category_id = :category_id: ');
            $options = array('bind' => array('category_id' => $catId), 'limit' => array('number' => $perPage, 'offset' => $startValue));
            $return[self::COMPARISON] = $comparison->listMiniComparison($conditions, $options, array());
            $hasmore = ($comparisonCount >= (($startValue + 1) + $perPage)) ? true : false;
        }
        $return2 = array();
        foreach ($return as $key => $row) {
            $return2 = array_merge($return2, $row);
        }
        return array('cat' => $cats, 'data' => $return2, 'hasmore' => $hasmore);
    }
}
