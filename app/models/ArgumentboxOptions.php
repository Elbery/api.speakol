<?php
class ArgumentboxOptions extends BaseModel {
    protected $id;
    protected $option_1;
    protected $option_2;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_option_1($op1) {
        $this->option_1 = $op1;
    }
    public function set_option_2($op2) {
        $this->option_2 = $op2;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_option_1() {
        return $this->option_1;
    }
    public function get_option_2() {
        return $this->option_2;
    }
}
