<?php
class SphereStories extends BaseModel {
    protected $id;
    protected $story_id;
    protected $sphere_id;
    protected $featured;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setStoryId($story_id) {
        $this->story_id = $story_id;
        return $this;
    }
    public function setSphereId($sphere_id) {
        $this->sphere_id = $sphere_id;
        return $this;
    }
    public function setFeatured($featured) {
        $this->featured = $featured;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getStoryId() {
        return $this->story_id;
    }
    public function getSphereId() {
        return $this->sphere_id;
    }
    public function getFeatured() {
        return $this->featured;
    }
    public function initialize() {
        $this->belongsTo('story_id', 'Stories', 'id', NULL);
        $this->belongsTo('sphere_id', 'Spheres', 'id', NULL);
    }
}
