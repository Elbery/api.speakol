<?php
use Phalcon\DI\FactoryDefault;
class Interaction extends BaseModel {
    protected $id;
    protected $url;
    protected $hash;
    protected $count;
    protected $created_at;
    protected $updated_at;
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function setUrl($url) {
        $this->url = $url;
    }
    public function setHash($hash) {
        $this->hash = $hash;
    }
    public function setCount($count) {
        $this->count = $count;
    }
    public function getId() {
        return $this->id;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getHash() {
        return $this->hash;
    }
    public function getCount() {
        return $this->count;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
}
