<?php
class Links extends BaseModel {
    protected $id;
    protected $url;
    protected $description;
    protected $title;
    protected $photo_url;
    protected $video_url;
    protected $clicks;
    protected $video;
    protected $linkable_type;
    protected $linkable_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function setPhotoUrl($photo_url) {
        $this->photo_url = $photo_url;
        return $this;
    }
    public function setVideoUrl($video_url) {
        $this->video_url = $video_url;
        return $this;
    }
    public function setClicks($clicks) {
        $this->clicks = $clicks;
        return $this;
    }
    public function setVideo($video) {
        $this->video = $video;
        return $this;
    }
    public function setLinkableType($linkable_type) {
        $this->linkable_type = $linkable_type;
        return $this;
    }
    public function setLinkableId($linkable_id) {
        $this->linkable_id = $linkable_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getDescription() {
        return $this->description;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getPhotoUrl() {
        return $this->photo_url;
    }
    public function getVideoUrl() {
        return $this->video_url;
    }
    public function getClicks() {
        return $this->clicks;
    }
    public function getVideo() {
        return $this->video;
    }
    public function getLinkableType() {
        return $this->linkable_type;
    }
    public function getLinkableId() {
        return $this->linkable_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
}
