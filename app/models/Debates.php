<?php
use Phalcon\Mvc\Model\Validator\PresenceOf, Phalcon\Mvc\Model\Validator\StringLength, Phalcon\Mvc\Model\Validator\Uniqueness, Phalcon\Mvc\Model\Validator\Email, Phalcon\Mvc\Model\Transaction\Manager as TrManager, Phalcon\DI\FactoryDefault;
class Debates extends BaseModel {
    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;
    const TYPE_PRIVATE = 0;
    const TYPE_PUBLIC = 1;
    protected $id;
    protected $title;
    protected $hash;
    protected $slug;
    protected $category_id;
    protected $image;
    protected $plugin_slider;
    protected $url;
    protected $started_at;
    protected $ends_at;
    protected $max_days;
    protected $user_id;
    protected $type;
    protected $status;
    protected $created_at;
    protected $updated_at;
    protected $app_id;
    protected $high_interaction;
    protected $lang;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setHash($hash) {
        $this->hash = $hash;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function set_plugin_slider($plugin_slider) {
        $this->plugin_slider = $plugin_slider;
    }
    public function setSlug($slug) {
        $countSlug = self::find(array('conditions' => 'title=:title:', 'bind' => array('title' => $slug)))->count();
        if ($countSlug > 0) {
            $this->slug = parent::uniqueSlug($slug . '-' . $countSlug);
        } else {
            $this->slug = parent::uniqueSlug($slug);
        }
        return $this;
    }
    public function setCategoryId($category_id) {
        $this->category_id = $category_id;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    public function setStartedAt($started_at) {
        $this->started_at = $started_at;
        return $this;
    }
    public function setEndsAt($ends_at) {
        $this->ends_at = $ends_at;
        return $this;
    }
    public function setMaxDays($max_days) {
        $this->max_days = $max_days;
        return $this;
    }
    public function setUserId($userId) {
        $this->user_id = $userId;
        return $this;
    }
    public function setType($type) {
        $this->type = $type;
        return $this;
    }
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function set_high_interaction($no_comments) {
        $this->high_interaction = $no_comments;
    }
    public function set_lang($lang) {
        $this->lang = $lang;
    }
    public function getId() {
        return $this->id;
    }
    public function getTitle() {
        return htmlspecialchars($this->title);
    }
    public function getSlug() {
        return htmlspecialchars($this->slug);
    }
    public function getCategoryId() {
        return $this->category_id;
    }
    public function getImage() {
        return $this->image;
    }
    public function get_plugin_slider() {
        return $this->plugin_slider;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getHash($hash) {
        return $this->hash;
    }
    public function getStartedAt() {
        return $this->started_at;
    }
    public function getEndsAt() {
        return $this->ends_at;
    }
    public function getMaxDays() {
        return $this->max_days;
    }
    public function getUserId() {
        return $this->user_id;
    }
    public function getType() {
        return $this->type;
    }
    public function getStatus() {
        return $this->status;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function get_high_interaction() {
        return intval($this->high_interaction);
    }
    public function get_lang() {
        return $this->lang;
    }
    public function initialize() {
        $this->hasMany('id', 'DebateFollows', 'debate_id', NULL);
        $this->hasMany('id', 'SphereDebates', 'debate_id', NULL);
        $this->hasMany('id', 'Sides', 'debate_id', NULL);
        $this->hasMany('id', 'DebateChallengers', 'debate_id', NULL);
        $this->belongsTo('app_id', 'Apps', 'id', NULL);
        $this->belongsTo('user_id', 'Users', 'id', NULL);
        $this->belongsTo('category_id', 'Categories', 'id', NULL);
    }
    protected function validation() {
        $this->validate(new PresenceOf(array('field' => 'title', 'message' => 'Title is required.',)));
        $this->validate(new PresenceOf(array('field' => 'slug', 'message' => FactoryDefault::getDefault()->get('t')->_("debate-slug-missing"),)));
        $this->validate(new Uniqueness(array('field' => 'slug', 'message' => FactoryDefault::getDefault()->get('t')->_("debate-cat-required"),)));
        $this->validate(new PresenceOf(array('field' => 'category_id', 'message' => FactoryDefault::getDefault()->get('t')->_("conflict"),)));
        if ($this->validationHasFailed() == true) {
            return false;
        }
        return true;
    }
    public function createDebate($params) {
        $di = $this->getDi()->getShared('request');
        $arrPhotos = $di->getUploadedFiles();
        $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
        $isPublisher = true;
        if (empty($params['app_id'])) {
            $isPublisher = false;
        }
        foreach ($arrPhotos as $arrPhoto) {
            switch ($arrPhoto->getKey()) {
                case 'debater_cover':
                    $debateCover = $objImgUploader->upload($objImgUploader::DIR_DEBATES, array($arrPhoto));
                break;
                case 'debater_1_pic':
                    $debater_1_pic = $objImgUploader->upload($objImgUploader::DIR_DEBATES, array($arrPhoto));
                break;
                case 'debater_2_pic':
                    $debater_2_pic = $objImgUploader->upload($objImgUploader::DIR_DEBATES, array($arrPhoto));
                break;
            }
        }
        $transactionManager = new TrManager();
        $transaction = $transactionManager->get();
        $this->setTransaction($transaction);
        $title = trim($params['title']);
        if ($title == "") {
            return array('status' => 'ERROR', 'message' => FactoryDefault::getDefault()->get('t')->_("invalid-title"));
        }
        $curr_date = date('Y-m-d H:i:s');
        $this->setTitle($title);
        $this->setSlug($title . rand(1, 10000));
        $this->setCategoryId($params['category_id']);
        $this->setUserId($params['user_id']);
        $this->setType($params['type']);
        $this->setStatus(self::STATUS_PENDING);
        $this->set_high_interaction(0);
        $this->setCreatedAt($curr_date);
        $this->setUpdatedAt($curr_date);
        $this->set_plugin_slider(1);
        $app = Apps::findFirst($params['app_id']);
        $user = Users::findFirst($app->getPublisherId());
        $this->set_lang($user->getLocale());
        if ($isPublisher == true) {
            $this->setStatus(self::STATUS_ACTIVE);
        }
        if ($isPublisher == false && !empty($debateCover)) {
            $this->setImage($debateCover[0]['new']);
            $this->setUrl($debateCover[0]['path']);
        }
        if ($isPublisher) {
            $this->setAppId($params['app_id']);
        }
        $this->setMaxDays($params['max_days']);
        if ($this->save() == false) {
            $messages = $this->getMessages();
            $message = $messages[0];
            $transaction->rollback($message->getMessage());
        }
        $sidesData = json_decode($params['sides'], true);
        $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
        $new_log = new Log();
        $new_log->log_current_action(Tokens::getSignedInUserId(), $sentHeaders['Ip'], "/debates/create", $this->getId());
        if ($isPublisher == true) {
            if ($debater_1_pic[0]['status'] != 1) {
                return array('status' => 'ERROR', 'message' => $debater_1_pic[0]['error']);
            } else if ($debater_2_pic[0]['status'] != 1) {
                return array('status' => 'ERROR', 'message' => $debater_2_pic[0]['error']);
            }
            $sidesData[0]['image'] = $debater_1_pic;
            $sidesData[1]['image'] = $debater_2_pic;
        }
        for ($i = 0;$i < 2;$i++) {
            $sides = new Sides();
            $sides->setDebateId($this->getId());
            if ($sides->save() == false) {
                $messages = $sides->getMessages();
                $message = $messages[0];
                $transaction->rollback($message->getMessage());
            }
            $debateSideMeta = new DebateSidesMeta();
            $debateSideMeta->setSideId($sides->getId());
            $side_title = trim($sidesData[$i]['title']);
            if ($side_title == "") {
                $transaction->rollback(FactoryDefault::getDefault()->get('t')->_("invalid-side-title"));
                $transaction->commit();
            }
            $debateSideMeta->setTitle($side_title);
            $job_title = trim($sidesData[$i]['job_title']);
            $debateSideMeta->setJobTitle($job_title);
            if ($isPublisher) {
                $debateSideMeta->setImage($sidesData[$i]['image'][0]['path'] . '' . $sidesData[$i]['image'][0]['new']);
            } else if ($isPublisher == false) {
                $debateSideMeta->setImage($sidesData[$i]['image']);
            }
            $debateSideMeta->setDebaterId($sidesData[$i]['id']);
            $debateSideMeta->setOpinion(trim($sidesData[$i]['opinion']));
            if ($debateSideMeta->save() == false) {
                $messages = $debateSideMeta->getMessages();
                $message = $messages[0];
                $transaction->rollback($message->getMessage());
            }
        }
        $title = Tags::filterTitle(trim($params['title']));
        $tags = new Tags();
        $tags->createTag($title['tags']);
        $transaction->commit();
        return array('status' => 'ok', 'messages' => $this->getSlug());
    }
    public function updateDebate($slug, $params) {
        $debate = self::findDebateBySlug($slug);
        if ($debate->valid()) {
            $debate = $debate->getFirst();
            $sides = $debate->getSides();
            $debaterTwoSide = $sides[0]->getDebateSidesMeta()->getDebateSidesMeta();
            $debate->setTitle($params['title']);
            $debate->setMaxDays($params['max_days']);
            if ($debate->save()) {
                return $debate;
            }
            return false;
        }
        return false;
    }
    static public function viewOneDebate($option) {
        $debatesData = array();
        $debates = self::findFirst($option);
        if ($debates == false) {
            return FALSE;
        }
        $usr = $debates->getUsers(array('columns' => array('id', 'first_name', 'last_name', 'job_title', 'profile_picture')))->toArray();
        $usr['name'] = $usr['first_name'] . " " . $usr['last_name'];
        $debateInfo = $debates->toArray();
        $debateInfo['debate_id'] = $debateInfo['id'];
        $debateInfo['created_at'] = $debateInfo['created_at'];
        $debateInfo['category_name'] = $debates->getCategories()->getName();
        unset($debateInfo['id']);
        $main = array_merge($debateInfo, $usr);
        $sides = $debates->getSides();
        $related = array();
        $currentUser = Tokens::checkAccess();
        foreach ($sides as $side) {
            $queryCondition = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
            $totleVotes = Votes::count($queryCondition);
            $totleComments = Arguments::count($queryCondition);
            $sidesMeta = $side->getDebateSidesMeta();
            $sidesMeta = $sidesMeta->toArray();
            $debaterId = $sidesMeta[0]['debater_id'];
            $u = Users::findFirst(array('conditions' => "id=$debaterId", 'columns' => array('id', 'slug', 'first_name', 'last_name', 'profile_picture')))->toArray();
            $u['name'] = $u['first_name'] . " " . $u['last_name'];
            $sidesMeta[0]['total_votes'] = $totleVotes;
            $sidesMeta[0]['total_comments'] = $totleComments;
            $sidesMeta[0]['isvoted'] = Votes::hasVoted($side->getId(), $currentUser);
            $related[] = array_merge($sidesMeta[0], $u);
        }
        $debatesData['main'] = $main;
        $debatesData['related'] = $related;
        return $debatesData;
    }
    public static function getArguments($slug, $pageNum, $perPage) {
        $debate = self::findFirstBySlug($slug);
        $arguments = array();
        foreach ($debate->getSides() as $side) {
            $arguments[$side->getId() ] = Arguments::getSideArguments($side->getId(), $pageNum, $perPage);
        }
        return $arguments;
    }
    static function getRelatedDebates($conditions) {
        return self::find($conditions)->toArray();
    }
    static public function listSiteDebates($conditions = array()) {
        $debatesData = array();
        $debates = self::find($conditions);
        $main = array();
        $counter = 0;
        foreach ($debates as $debate) {
            $main = $debate->toArray();
            $main['total_challengers'] = $debate->getDebateChallengers()->count();
            $sides = $debate->getSides();
            $related = array();
            foreach ($sides as $side) {
                $queryCondition = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $totleVotes = Votes::count($queryCondition);
                $totleComments = Arguments::count($queryCondition);
                $sideMeta = $side->getDebateSidesMeta(array('columns' => array('debater_id', 'opinion')));
                foreach ($sideMeta as $meta) {
                    $debater = Users::findFirst(array('conditions' => 'id=:debater_id:', 'bind' => array('debater_id' => $meta->debater_id), 'columns' => array('id', 'slug', 'first_name', 'last_name', 'job_title', 'profile_picture'), 'order' => 'id desc',));
                    $debaterData = $debater->toArray();
                    $debaterData['name'] = $debaterData['first_name'] . " " . $debaterData['last_name'];
                    $debaterData['side_id'] = $side->getId();
                    $debaterData['opinion'] = $meta->opinion;
                    $debaterData['total_votes'] = $totleVotes;
                    $debaterData['total_comments'] = $totleComments;
                    $related[] = $debaterData;
                }
            }
            $debatesData[$counter]['main'] = $main;
            $debatesData[$counter]['related'] = $related;
            $counter++;
        }
        return $debatesData;
    }
    static public function listMiniDebates($conditions = null, $option = null, $sort = array()) {
        $profileUserId = Tokens::checkAccess();
        $debatesData = array();
        $debates = self::find(array_merge($conditions, $option));
        $main = array();
        $di = new Phalcon\DI();
        $utility = new Speakol\Utility($di);
        foreach ($debates as $debate) {
            $userInfo = $debate->toArray();
            $sides = $debate->getSides();
            $appId = $userInfo['app_id'];
            $userId = $userInfo['user_id'];
            $app = $usr = array();
            if (!is_null($appId)) {
                $app = Apps::findFirst($userInfo['app_id'])->toArray();
            } else {
                $usr = Users::findFirst(array('conditions' => "id=$userId", 'columns' => array('id', 'slug', 'first_name', 'last_name', 'profile_picture')))->toArray();
                $usr['name'] = $usr['first_name'] . " " . $usr['last_name'];
            }
            $debate = $debate->toArray();
            $debate['title'] = htmlspecialchars($debate['title']);
            $debate['module'] = self::DEBATE;
            $debate['sort_id'] = !empty($sort[self::DEBATE . '_' . $debate['id']]) ? $sort[self::DEBATE . '_' . $debate['id']] : 0;
            $debate['created_at'] = $utility->time_elapsed_string(strtotime($debate['created_at']));
            $totalVotes = $totalComments = 0;
            foreach ($sides as $side) {
                $voteQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $side->getId()));
                $sideMeta = $side->getDebateSidesMeta(array('columns' => array('debater_id', 'opinion', 'title', 'image')));
                foreach ($sideMeta as $meta) {
                    $debater = Users::findFirst(array('conditions' => 'id=' . $meta->debater_id, 'columns' => array('id', 'slug', 'first_name', 'last_name', 'job_title', 'profile_picture')));
                    $sid = $debater->toArray();
                    $sid['opinion'] = htmlspecialchars($utility->splitWords($meta->opinion));
                    $sid['title'] = htmlspecialchars($meta->title);
                    $sid['image'] = $meta->image;
                }
                $sid['side_id'] = $side->getId();
                $sid['votes_count'] = Votes::count($voteQuery);
                $sid['comments_count'] = Arguments::count($voteQuery);
                if ($profileUserId) {
                    $sid['voted'] = Votes::hasVoted($side->getId(), $profileUserId);
                } else {
                    $sid['voted'] = false;
                }
                $totalVotes+= $sid['votes_count'];
                $totalComments+= $sid['comments_count'];
                $sidesArr[] = $sid;
            }
            foreach ($sidesArr as $k => $sid) {
                $sidesArr[$k]['percentage'] = !$totalVotes ? 50 : number_format($sid['votes_count'] * 100 / $totalVotes, '0', '.', '');
            }
            $main[$debate['id']] = array('user' => $usr, 'app' => $app, 'owner_is_app' => $appId ? true : false, 'data' => $debate, 'sides' => $sidesArr, 'counts' => array('total_votes' => $totalVotes, 'total_comments' => $totalComments,));
            $sidesArr = array();
        }
        return $main;
    }
    static public function findDebateBySlug($_slug) {
        $objectDebate = self::find(array("conditions" => "slug= :slug:", "bind" => array('slug' => $_slug)));
        if ($objectDebate->count() != 0) {
            return $objectDebate;
        } else {
            throw new Exception(FactoryDefault::getDefault()->get('t')->_("not-found"), 404);
        }
    }
    static public function deleteDebate($slug, $userId) {
        $isAdmin = Tokens::isCurrentUserIs(array(1, 2));
        if (!$isAdmin) {
            $app = AppAdmins::findFirst(array('user_id = :user_id: AND super_admin = 1', 'bind' => array('user_id' => $userId)));
            $isAppAdmin = $app ? $app->getAppId() : false;
        } else {
            $isAppAdmin = false;
        }
        if ($isAdmin) {
            $debate = self::findFirst(array('conditions' => 'slug = :slug: ', 'bind' => array('slug' => $slug)));
        } elseif ($isAppAdmin) {
            $debate = self::findFirst(array('conditions' => 'slug= :slug: AND app_id = :app_id:', 'bind' => array('slug' => $slug, 'app_id' => $isAppAdmin)));
        } else {
            $debate = self::findFirst(array('conditions' => "slug='$slug' AND user_id=$userId"));
        }
        if ($debate != false) {
            $notifications = Notifications::find("notifiable_id=" . $debate->getId() . " AND notification_type='debate'");
            if ($notifications) {
                foreach ($notifications as $notification) {
                    $notification_recipients = NotificationRecipients::find('notification_id=' . $notification->getId());
                    if ($notification_recipients) {
                        $notification_recipients->delete();
                    }
                    $notification->delete();
                }
            }
            $image = $debate->getUrl() . $debate->getImage();
            $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
            $new_log = new Log();
            $new_log->log_current_action($userId, $sentHeaders['Ip'], "/debates/" . $slug . "/delete", $debate->getId());
            if ($debate->delete()) {
                $feed = Feeds::find(array('conditions' => 'module=:module: AND parent_id=:parent_id:', 'bind' => array('module' => BaseModel::DEBATE, 'parent_id' => $debate->getId())));
                if ($feed) {
                    $feed->delete();
                }
                if (is_file($_SERVER['DOCUMENT_ROOT'] . $image)) {
                    unlink($_SERVER['DOCUMENT_ROOT'] . $image);
                }
                return true;
            }
            return false;
        }
        return false;
    }
    public static function formatDebate($_debate, $order_by) {
        $ads = PluginsAds::plugin_ads("debate", $_debate->getId(), 2);
        $app = Apps::findFirst($_debate->getAppId());
        $audio = PluginsFeature::validate_feature_using_plan_id($app->get_plan(), "audio_comment");
        $date = strtotime($_debate->getUpdatedAt());
        $date = date('F j \a\t h:i A', $date);
        return array('id' => $_debate->getId(), 'app_id' => $_debate->getAppId(), 'slug' => $_debate->getSlug(), 'image' => $_debate->getImage(), 'url' => $_debate->getUrl(), 'title' => $_debate->getTitle(), 'audio' => $audio, 'started_at' => $_debate->getStartedAt(), 'max_days' => $_debate->getMaxDays(), 'ends_at' => $_debate->getEndsAt(), 'created_at' => $_debate->getCreatedAt(), 'updated_at' => $_debate->getUpdatedAt(), 'plugin_slider' => $_debate->get_plugin_slider(), 'updated_date' => $date, 'follows' => DebateFollows::listFollowersByID($_debate->getId()), 'ads' => $ads, 'sides' => Sides::formatSides($_debate->getId(), self::DEBATE, $order_by), 'app_name' => $app->getName(), 'app_image' => $app->getImage(),);
    }
    public static function showHelper($_slug, $order_by) {
        $_debate = self::findDebateBySlug($_slug);
        return self::formatDebate($_debate->getFirst(), $order_by);
    }
    public static function voteDevoteActionHelper($debateId, $_sideId, $_userId, $link) {
        $sidesTotalVotes = array();
        $sides = array();
        $arrSides = Sides::getSideIdByDebateId($debateId);
        if (is_array($arrSides)) {
            foreach ($arrSides as $key => $side) {
                if (Votes::unvote($side["id"], $_userId) == False) {
                    return false;
                }
                $sides[] = $side["id"];
            }
        }
        $vote_id = votes::vote($_sideId, $_userId, $link);
        if ($vote_id) {
            $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
            $new_log = new Log();
            $new_log->log_current_action($_userId, $sentHeaders['Ip'], "/vote on debate", $vote_id);
            if (is_array($sides)) {
                foreach ($sides as $side) {
                    $sidesInfo = array();
                    $sidesInfo['side_id'] = $side;
                    $sidesInfo['votes'] = Votes::getSideVotes($side);
                    $sidesTotalVotes[] = $sidesInfo;
                }
                return $sidesTotalVotes;
            }
        }
        return false;
    }
    public static function unvoteActionHelper($debateId, $_sideId, $_userId) {
        $sidesTotalVotes = array();
        $sides = array();
        $arrSides = Sides::getSideIdByDebateId($debateId);
        if (is_array($arrSides)) {
            foreach ($arrSides as $key => $side) {
                if (Votes::unvote($side["id"], $_userId) == False) {
                    return false;
                }
                $sides[] = $side["id"];
            }
        }
        if (is_array($sides)) {
            foreach ($sides as $side) {
                $sidesInfo = array();
                $sidesInfo['side_id'] = $side;
                $sidesInfo['votes'] = Votes::getSideVotes($side);
                $sidesTotalVotes[] = $sidesInfo;
            }
            return $sidesTotalVotes;
        }
        return false;
    }
    public static function doesExist($_debateID) {
        $debate = self::findFirst($_debateID);
        if (FALSE != $debate) {
            return $debate;
        } else {
            throw new Exception(FactoryDefault::getDefault()->get('t')->_("not-found"), 404);
        }
    }
    public static function getFavoredSideOfDebate($objData) {
        $arrSides = Sides::getSideIdByDebateId($objData->debateID);
        if (is_array($arrSides)) {
            foreach ($arrSides as $key => $value) {
                if (Votes::hasVoted($value["id"], $objData->signedInUserID) == TRUE) {
                    return $value["id"];
                }
            }
        }
        throw new Exception(FactoryDefault::getDefault()->get('t')->_("debate-user-not-voted"), 400);
    }
}
