<?php
use Phalcon\DI\FactoryDefault;
class BaseModel extends \Phalcon\Mvc\Model {
    const DEBATE = "debate";
    const COMPARISON = "comparison";
    const OPINION = "opinion";
    const ARGUMENTSBOX = "argumentsbox";
    const ARGUMENT = "argument";
    const REPLY = "reply";
    const APP = "app";
    protected $_allowedFeedsTable = array('debates', 'comparisons', 'argumentboxes', 'arguments', 'votes', 'debate_sides_meta');
    protected $_modules = array('argumentbox_id' => self::ARGUMENTSBOX, 'debate_id' => self::DEBATE, 'comparison_id' => self::COMPARISON, 'reply_id' => self::REPLY, 'argument_id' => self::ARGUMENT,);
    public function beforeCreate() {
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');
    }
    private function handleFeeds($type) {
        $table = $this->getSource();
        $api = API::instance();
        $application = $api->getApp();
        $router = $application['router'];
        if ($router) {
            $_activity = $router->getMatchedRoute()->getPattern();
        }
        if (empty($table) || !in_array($table, $this->_allowedFeedsTable) || ($table == 'debate_sides_meta' && $_activity != '/debates/approve') || ($table == 'debate_sides_meta' && $type = 'upate') || ($table == 'debate_sides_meta' && $type = 'delete')) {
            return false;
        }
        $activityID = \Activities::getActivityIdByAction($_activity);
        $userId = Tokens::checkAccess();
        $configs = json_decode(json_encode(\API::instance()->getConfig('routesConfiguration')), 1);
        $exist = Speakol\ACL::searchForRoute($configs, $_activity);
        $id = $this->getId();
        if ($table == 'debate_sides_meta') {
            $userId = $this->getDebaterId();
        }
        if (!empty($id) && !empty($exist['feed']) && !empty($activityID) && !empty($userId)) {
            $moduleName = $parentId = $appId = false;
            $moduleName = $exist['feed']['module'];
            if ($table == 'replies') {
                $argId = Arguments::findFirst($this->getArgumentId())->getSideId();
                $parents = Sides::findFirst($argId);
            }
            if (in_array($table, array('arguments', 'votes', 'debate_sides_meta'))) {
                $parents = Sides::findFirst($this->getSideId());
            }
            if (in_array($table, array('replies', 'arguments', 'votes', 'debate_sides_meta'))) {
                foreach ($parents->toArray() as $key => $row) {
                    if (!is_null($row) && $key != 'id') {
                        $parentId = $row;
                        $moduleName = $this->_modules[$key];
                        break;
                    }
                }
                $con = new Speakol\concreteSocialPluginFactory();
                $obj = $con->make($moduleName);
                if ($obj) {
                    $obj = $obj->findFirst($parentId);
                }
                $appId = $obj->getAppId();
            }
            $feeds = new Feeds();
            if (empty($parentId)) {
                $parentId = $this->getId();
            }
            if (method_exists($this, 'getAppId')) {
                $appId = $this->getAppId();
                $feeds->setAppId($appId);
            }
            $feeds->setParentId($parentId);
            $feeds->setModule($moduleName);
            $feeds->setUserId($userId);
            $feeds->setActivityId($activityID);
            $feeds->setElementId($this->getId());
            $feeds->setAppId($appId);
            $feeds->setCreatedAt(date('Y-m-d H:i:s'));
            if ($type == 'delete') {
                switch ($table) {
                    default:
                        $oldFeeds = Feeds::find(array('conditions' => ' user_id = :user_id: AND activity_id = :activity_id: AND module = :module: AND parent_id = :parent_id: ', 'bind' => array('user_id' => $feeds->getUserId(), 'activity_id' => $feeds->getActivityId(), 'module' => $feeds->getModule(), 'parent_id' => $feeds->getParentId())));
                        if ($oldFeeds->toArray()) {
                            foreach ($oldFeeds as $oldFeed) {
                                $oldFeed->delete();
                            }
                        }
                    break;
                }
            } else {
                $feeds->save();
            }
        }
    }
    public function afterCreate() {
        $this->handleFeeds('create');
    }
    public function afterUpdate() {
        $this->handleFeeds('update');
    }
    public function afterDelete() {
        $this->handleFeeds('delete');
    }
    protected static function uniqueSlug($_slug, $replacement = '-') {
        $quotedReplacement = preg_quote($replacement, '/');
        $merge = array('/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ', '/\\s+/' => $replacement, sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => '',);
        return preg_replace(array_keys($merge), array_values($merge), $_slug);
    }
    protected function compareValues($value_1, $value_2) {
        if ($value_1 === $value_2) {
            return true;
        }
        return false;
    }
    public static function doesExist($_id) {
        $plugin = self::findFirst($_id);
        if (FALSE != $plugin) {
            return $plugin;
        } else {
            throw new Exception(FactoryDefault::getDefault()->get('t')->_("not-found"), 404);
        }
    }
    public function generate_uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }
}
