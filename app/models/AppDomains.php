<?php
class AppDomains extends BaseModel {
    protected $id;
    protected $app_id;
    protected $domain;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_app_id($app_id) {
        $this->app_id = $app_id;
    }
    public function set_domain($domain) {
        $this->domain = strtolower($domain);
    }
    public function get_id() {
        return $this->id;
    }
    public function get_app_id() {
        return $this->app_id;
    }
    public function get_domain() {
        return strtolower($this->domain);
    }
}
