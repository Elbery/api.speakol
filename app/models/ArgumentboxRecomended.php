<?php
use Phalcon\DI\FactoryDefault, Phalcon\Mvc\Model\Validator\Url as UrlValidator;
class ArgumentboxRecomended extends \Phalcon\Mvc\Model {
    protected $id;
    protected $title;
    protected $image;
    protected $domain;
    protected $argumentbox_id;
    protected $code;
    protected $uri;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setDomain($domain) {
        $this->domain = $domain;
        return $this;
    }
    public function setArgumentboxId($argumentbox_id) {
        $this->argumentbox_id = $argumentbox_id;
        return $this;
    }
    public function set_code($code) {
        $this->code = $code;
    }
    public function set_uri($uri) {
        $this->uri = $uri;
    }
    public function getId() {
        return $this->id;
    }
    public function getTitle() {
        return htmlspecialchars($this->title);
    }
    public function getImage() {
        return $this->image;
    }
    public function getDomain() {
        return $this->domain;
    }
    public function getArgumentboxId() {
        return $this->argumentbox_id;
    }
    public function get_code() {
        return $this->code;
    }
    public function get_uri() {
        return $this->uri;
    }
    public function initialize() {
        $this->belongsTo('argumentbox_id', 'Argumentboxes', 'id', null);
    }
    public function appCreate($data) {
        $arg = Argumentboxes::findFirst($data['argumentbox_id']);
        $data['url'] = str_replace("http://", "", $data['url']);
        $data['url'] = str_replace("https://", "", $data['url']);
        $urlCheck = self::findFirst(array("conditions" => "CONCAT(domain,uri) = :url: AND code=:code:", "bind" => array('url' => $data['url'], 'code' => $data['code'])));
        if ($urlCheck) {
            return array('status' => 'OK', 'data' => array('argumentbox_id' => $urlCheck->getId()));
        }
        $split_url = $this->split_url_to_domain_uri($data['url']);
        $title = $image = $content = '';
        $argId = $data['argumentbox_id'];
        $title = !empty($data['title']) ? strip_tags($data['title']) : 'No title';
        $image = !empty($data['image']) ? $data['image'] : false;
        $code = ($data['code']) ? $data['code'] : "";
        $this->setTitle($title);
        $this->setImage($image);
        $this->set_uri($split_url['uri']);
        $this->setDomain($split_url['domain']);
        $this->setArgumentboxId($argId);
        $this->set_code($code);
        if ($this->save()) {
            return array('status' => 'OK', 'data' => array('argumentbox_id' => $this->getId()));
        }
        return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
    }
    function split_url_to_domain_uri($url) {
        $url = str_replace("http://", "", $url);
        $url = str_replace("https://", "", $url);
        $slash_del_pos = strpos($url, "/") ? strpos($url, "/") : strlen($url);
        $domain = substr($url, 0, $slash_del_pos);
        $del_pos = strpos($domain, ":") ? strpos($domain, ":") : strlen($domain);
        $domain = substr($domain, 0, $del_pos);
        $split_url = explode($domain, $url);
        return array("domain" => $domain, "uri" => $split_url[1]);
    }
    public function getRandomRecomendation($appId) {
        $catgeroyID = Apps::findFirst($appId);
        $catgeroyID = $catgeroyID->getCategoryId();
        $query = "SELECT recomended.url,recomended.title,recomended.domain, recomended.image FROM ArgumentboxRecomended AS recomended, Argumentboxes AS arg, Apps AS app
                    WHERE
                        app.category_id = :cat_id: AND
                        app.id <> :app_id: AND
                        arg.app_id = app.id AND
                        arg.id = recomended.argumentbox_id
                        
                    ORDER BY RAND()
                    LIMIT 5";
        $query = $this->getModelsManager()->createQuery($query);
        $results = $query->execute(array('app_id' => $appId, 'cat_id' => $catgeroyID));
        if ($results) {
            $return = array();
            $i = 0;
            foreach ($results as $row) {
                $file_headers = @get_headers($row->url);
                if ($file_headers[0] != 'HTTP/1.1 404 Not Found') {
                    $return[] = $row;
                    $i++;
                }
                if (3 === $i) {
                    break;
                }
            }
            return array('status' => 'OK', 'data' => $return);
        }
    }
    public function getDomainName($url) {
        if (!$url) return false;
        $url = explode('.', $url);
        $key = array_search('www', $url);
        if ($key === false) $key = array_search('http://www', $url);
        if ($key === false) {
            $key = array_search('http', $url);
            $exp = explode('//', $url[$key]);
            $return['www'] = false;
            $return['domain'] = $exp[1];
            $return['ext'] = rtrim($url[$key + 1], '/');
            $com = explode('/', $return['ext']);
            $return['domain'] = $return['domain'] . '.' . $com['0'];
        } else {
            $return['www'] = $url[$key];
            $return['domain'] = $url[$key + 1];
            $return['ext'] = rtrim($url[$key + 2], '/');
            $com = explode('/', $return['ext']);
            $return['domain'] = $return['domain'] . '.' . $com['0'];
        }
        if (!empty($url[$key + 3])) {
            $return['ext2'] = rtrim($url[$key + 3], '/');
        }
        return $return;
    }
    public function getCommunity($appId, $url) {
        $conditions = array("conditions" => "type=:type: AND app_id = :app_id: ", "bind" => array('type' => BaseModel::ARGUMENTSBOX, 'app_id' => $appId), "group" => "module_id", "order" => "rowcount DESC",);
        $limit = 7;
        if ($limit) {
            $conditions['limit'] = $limit;
        }
        $args = Arguments::count($conditions);
        $return = array();
        foreach ($args as $arg) {
            $re = self::findFirst(array('conditions' => 'argumentbox_id=:id:', 'bind' => array('id' => $arg->module_id),))->toArray();
            if ($re['url'] == $url) {
                continue;
            }
            $sides = Sides::find(array('conditions' => 'argumentbox_id=:id:', 'bind' => array('id' => $re['argumentbox_id']),));
            $re['replies_count'] = $re['arguments_count'] = $re['votes_count'] = 0;
            foreach ($sides as $row) {
                $re['replies_count']+= Replies::count(array('side_id = :side_id:', 'bind' => array('side_id' => $row->getId())));
                $re['arguments_count']+= Arguments::count(array('side_id = :side_id:', 'bind' => array('side_id' => $row->getId())));
                $re['votes_count']+= Votes::count(array('side_id = :side_id:', 'bind' => array('side_id' => $row->getId())));
            }
            $re['interactions'] = $re['replies_count'] + $re['arguments_count'] + $re['votes_count'];
            $return[] = $re;
        }
        $users = $this->getCommunityUsers($appId);
        return array('status' => 'OK', 'data' => array('top' => $return, 'leaderboard' => $users));
    }
    public function getCommunityUsers($appId) {
        $conditions = array("conditions" => "type=:type: AND app_id = :app_id: ", "bind" => array('type' => BaseModel::ARGUMENTSBOX, 'app_id' => $appId), "group" => "owner_id", "order" => "rowcount DESC",);
        $limit = 7;
        if ($limit) {
            $conditions['limit'] = $limit;
        }
        $args = Arguments::count($conditions);
        $users = array();
        foreach ($args as $arg) {
            $user = Users::findFirst(array('id = :id:', 'bind' => array('id' => $arg->owner_id), 'columns' => array('profile_picture', 'slug', 'name')))->toArray();
            $user['total_arguments'] = $arg->rowcount;
            $users[] = $user;
        }
        return $users;
    }
}
