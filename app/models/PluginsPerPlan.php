<?php
class PluginsPerPlan extends BaseModel {
    protected $id;
    protected $plan_id;
    protected $max_plugins;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_plan_id($pid) {
        $this->plan_id = $pid;
    }
    public function set_max_plugins($max_plugins) {
        $this->max_plugins = $max_plugins;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_plan_id() {
        return $this->plan_id;
    }
    public function get_max_plugins() {
        return $this->max_plugins;
    }
}
