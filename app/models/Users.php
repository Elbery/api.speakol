<?php
use Phalcon\Mvc\Model\Validator\Email, Phalcon\Mvc\Model\Validator\Uniqueness, Phalcon\DI\FactoryDefault;
class Users extends BaseModel {
    protected $id;
    protected $role_id;
    protected $subscribe;
    protected $slug;
    protected $first_name;
    protected $last_name;
    protected $job_title;
    protected $employer;
    protected $dob;
    protected $gender;
    protected $verified;
    protected $locale;
    protected $took_tour;
    protected $active;
    protected $country_id;
    protected $welcome_step;
    protected $email;
    protected $encrypted_password;
    protected $reset_password_token;
    protected $reset_password_sent_at;
    protected $remember_created_at;
    protected $confirmation_token;
    protected $confirmed_at;
    protected $confirmation_sent_at;
    protected $created_at;
    protected $updated_at;
    protected $profile_picture;
    protected $_user;
    protected $newsletter;
    protected $init_reg;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setRoleId($role_id) {
        $this->role_id = $role_id;
        return $this;
    }
    public function setSlug($slug) {
        $this->slug = parent::uniqueSlug($slug);
        return $this;
    }
    public function set_first_name($name) {
        $this->first_name = ucwords($name);
        return $this;
    }
    public function set_last_name($name) {
        $this->last_name = ucwords($name);
        return $this;
    }
    public function set_subscribe($subscribe) {
        $this->subscribe = $subscribe;
        return $this;
    }
    public function setJobTitle($job_title) {
        $this->job_title = $job_title;
        return $this;
    }
    public function setEmployer($employer) {
        $this->employer = $employer;
        return $this;
    }
    public function setDob($dob) {
        $this->dob = $dob;
        return $this;
    }
    public function setGender($gender) {
        $this->gender = $gender;
        return $this;
    }
    public function setPoints($points) {
        $this->points = $points;
        return $this;
    }
    public function setVerified($verified) {
        $this->verified = $verified;
        return $this;
    }
    public function setBanned($banned) {
        $this->banned = $banned;
        return $this;
    }
    public function setLocale($locale) {
        $this->locale = $locale;
        return $this;
    }
    public function setTookTour($took_tour) {
        $this->took_tour = $took_tour;
        return $this;
    }
    public function setSkippedQuiz($skipped_quiz) {
        $this->skipped_quiz = $skipped_quiz;
        return $this;
    }
    public function setActive($active) {
        $this->active = $active;
        return $this;
    }
    public function setCountryId($country_id) {
        $this->country_id = $country_id;
        return $this;
    }
    public function setWelcomeStep($welcome_step) {
        $this->welcome_step = $welcome_step;
        return $this;
    }
    public function setTimeZone($time_zone) {
        $this->time_zone = $time_zone;
        return $this;
    }
    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }
    public function setEncryptedPassword($encrypted_password, $encyrpt = true) {
        if ($encyrpt) {
            $this->encrypted_password = \Phalcon\DI::getDefault()->get('security')->hash($encrypted_password);
        } else {
            $this->encrypted_password = $encrypted_password;
        }
        return $this;
    }
    public function setResetPasswordToken($reset_password_token) {
        $this->reset_password_token = $reset_password_token;
        return $this;
    }
    public function setResetPasswordSentAt($reset_password_sent_at) {
        $this->reset_password_sent_at = $reset_password_sent_at;
        return $this;
    }
    public function setRememberCreatedAt($remember_created_at) {
        $this->remember_created_at = $remember_created_at;
        return $this;
    }
    public function setConfirmationToken($confirmation_token) {
        $this->confirmation_token = $confirmation_token;
        return $this;
    }
    public function setConfirmedAt($confirmed_at) {
        $this->confirmed_at = $confirmed_at;
        return $this;
    }
    public function setConfirmationSentAt($confirmation_sent_at) {
        $this->confirmation_sent_at = $confirmation_sent_at;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function setProfilePicture($profile_picture) {
        $this->profile_picture = $profile_picture;
        return $this;
    }
    public function set_newsletter($newsletter) {
        $this->newsletter = $newsletter;
    }
    public function set_init_reg($init_reg) {
        $this->init_reg = $init_reg;
    }
    public function getId() {
        return intval($this->id);
    }
    public function getRoleId() {
        return intval($this->role_id);
    }
    public function getSlug() {
        return $this->slug;
    }
    public function get_first_name() {
        return $this->first_name;
    }
    public function get_last_name() {
        return $this->last_name;
    }
    public function getJobTitle() {
        return $this->job_title;
    }
    public function getEmployer() {
        return $this->employer;
    }
    public function getDob() {
        return $this->dob;
    }
    public function getGender() {
        return $this->gender;
    }
    public function getVerified() {
        return $this->verified;
    }
    public function getLocale() {
        return $this->locale;
    }
    public function get_subscribe() {
        return $this->subscribe;
    }
    public function getTookTour() {
        return $this->took_tour;
    }
    public function getSkippedQuiz() {
        return $this->skipped_quiz;
    }
    public function getActive() {
        return $this->active;
    }
    public function getCountryId() {
        return $this->country_id;
    }
    public function getWelcomeStep() {
        return $this->welcome_step;
    }
    public function getTimeZone() {
        return $this->time_zone;
    }
    public function getEmail() {
        return $this->email;
    }
    public function getEncryptedPassword() {
        return $this->encrypted_password;
    }
    public function getResetPasswordToken() {
        return $this->reset_password_token;
    }
    public function getResetPasswordSentAt() {
        return $this->reset_password_sent_at;
    }
    public function getRememberCreatedAt() {
        return $this->remember_created_at;
    }
    public function getConfirmationToken() {
        return $this->confirmation_token;
    }
    public function getConfirmedAt() {
        return $this->confirmed_at;
    }
    public function getConfirmationSentAt() {
        return $this->confirmation_sent_at;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function getProfilePicture() {
        if ($this->profile_picture != "") {
            return $this->profile_picture;
        } else {
            return "/uploads/perm/no-img.png";
        }
    }
    public function get_newsletter() {
        return intval($this->newsletter);
    }
    public function get_init_reg() {
        return $this->init_reg;
    }
    public function validation() {
        $this->validate(new Email(array('field' => 'email', 'message' => 'EMail is not valid')));
        $this->validate(new Phalcon\Mvc\Model\Validator\Uniqueness(array('field' => 'email')));
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }
    public function initialize() {
        $this->hasMany('id', 'AppAdmins', 'user_id', NULL);
        $this->hasMany('id', 'AppFollows', 'follower_id', NULL);
        $this->hasMany('id', 'ArgumentThumbs', 'thumber_id', NULL);
        $this->hasMany('id', 'AttachedPhotos', 'uploader_id', NULL);
        $this->hasMany('id', 'ComparisonFollows', 'follower_id', NULL);
        $this->hasMany('id', 'DebateFollows', 'follower_id', NULL);
        $this->hasMany('id', 'NotificationRecipients', 'user_id', NULL);
        $this->hasMany('id', 'Notifications', 'user_id', NULL);
        $this->hasMany('id', 'OpinionFollows', 'follower_id', NULL);
        $this->hasMany('id', 'Opinions', 'owner_id', NULL);
        $this->hasMany('id', 'ProfilePhotos', 'user_id', NULL);
        $this->hasMany('id', 'ReplyThumbs', 'thumber_id', NULL);
        $this->hasMany('id', 'Sides', 'owner_id', NULL);
        $this->hasMany('id', 'Views', 'viewer_id', NULL);
        $this->hasMany('id', 'Votes', 'voter_id', NULL);
        $this->hasMany('id', 'Tokens', 'user_id', NULL);
        $this->hasMany('id', 'Replies', 'user_id', NULL);
        $this->hasMany('id', 'Arguments', 'owner_id', NULL);
        $this->belongsTo('role_id', 'Roles', 'id', NULL);
        $this->belongsTo('country_id', 'Country', 'id', NULL);
        $this->hasMany('id', 'DebateSidesMeta', 'debater_id', NULL);
        $this->hasOne('id', 'Apps', 'publisher_id', NULL);
        $this->hasMany('id', 'Debates', 'user_id', NULL);
        $this->hasMany('id', 'DebateChallengers', 'challenger_id', NULL);
    }
    private function _passwordHashing($password) {
        if ($password) {
            return sha1($password);
        }
        return false;
    }
    public function generateConfirmationToken($value) {
        return sha1(uniqid($value, true));
    }
    private function validateRequest($request, $forward = false) {
        $this->_response = new \Phalcon\Http\Response();
        $this->_response->setHeader("Content-Type", "application/json");
        $request = empty($request->user) ? $request : $request->getJsonRawBody();
        $this->_user = $this;
        if (isset($request->user->id) && !empty($request->user->id)) {
            $this->_userData = $this->findFirst(intval($request->user->id));
        } else {
            if ($this->_userData = $this->findFirst(array('email = :email:', 'bind' => array('email' => $request->user->email)))) {
                $errors[] = $this->getDi()->get('t')->_('conflict');
                if (!$forward) {
                    $this->_response->setStatusCode(409, "Conflict");
                    $this->_response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
                }
                return array('code' => '0', 'errors' => $errors);
            }
        }
        if (!$request->user->password) {
            $errors[] = $this->getDi()->get('t')->_('bad-request');
            if (!$forward) {
                $this->_response->setStatusCode(400, "Bad Request");
                $this->_response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
            }
            return array('code' => '0', 'errors' => $errors);
        }
        return array('code' => '1', 'errors' => array());
    }
    public function signIn($params, $social = false) {
        $email = $params['email'];
        $password = ((isset($params['password'])) ? $params['password'] : '');
        $user = self::findFirst("email='$email'");
        if ($user) {
            if ($social) {
                $flag = TRUE;
            } else {
                $flag = \Phalcon\DI::getDefault()->get('security')->checkHash($password, $user->getEncryptedPassword());
            }
            if ($flag == FALSE) {
                return array('status' => 'ERROR', 'code' => 603, 'message' => $this->getDi()->get('t')->_('user-wrong-email-password'));
            }
            if ($user->getActive() != 1 && ($user->getRoleId() != 3)) {
                return array('status' => 'ERROR', 'code' => 601, 'message' => $this->getDi()->get('t')->_('user-account-verification'));
            }
            if (empty($password) && $social !== true) {
                return array('status' => 'ERROR', 'code' => 602, 'message' => $this->getDi()->get('t')->_('user-password-missing'));
            }
            if (intval($params['role']) == 3 && $user->getRoleId() == 4) {
                return array('status' => 'ERROR', 'code' => 603, 'message' => $this->getDi()->get('t')->_('user-wrong-email-password'));
            }
            if (!empty($params['role']) && !is_array($params['role'])) {
                $params['role'] = array($params['role']);
            }
            if ($flag) {
                $tokens = new Tokens();
                $signinToken = $tokens->registerSession($user);
                if ($signinToken == FALSE) {
                    $errors = array();
                    $errors[] = 'Conflict';
                    return array('status' => 'ERROR', 'code' => 404, 'message' => $errors);
                }
                $beacon = new Beacon();
                $current_date_time = new DateTime();
                $beacon->addBeacon($user->getId(), $current_date_time->format('Y-m-d H:i:s'));
                if ($user->role_id == Roles::APP || $user->role_id == Roles::ADMIN) {
                    $app_admin = AppAdmins::findFirst('user_id=' . $user->getId());
                    $app = Apps::findFirst($app_admin->getAppId());
                    $downgraded_log = DowngradedAppsLog::findFirst('app_id=' . $app->getId());
                    $downgrade_data = array('downgraded' => false);
                    if ($downgraded_log) {
                        $downgrade_data['downgraded'] = true;
                        if ($downgraded_log->get_card_number() != "") {
                            $downgrade_data['card_number'] = $downgraded_log->get_card_number();
                            $downgrade_data['payment_method'] = $downgraded_log->get_payment_method();
                        }
                    }
                    $domains = array();
                    $app_domains = AppDomains::find('app_id=' . $app->getId());
                    foreach ($app_domains as $app_domain) {
                        $domains[] = $app_domain->get_domain();
                    }
                    $arrUser = array('user' => array('token' => $signinToken, 'id' => $user->getId(), 'name' => $user->get_first_name() . " " . $user->get_last_name(), 'slug' => $user->getSlug(), 'role_id' => $user->getRoleId(), 'profile_picture' => $user->getProfilePicture(), 'locale' => $user->getLocale(), 'took_tour' => $user->getTookTour(), 'email' => $user->getEmail(), 'app' => array('id' => $app->getId(), 'name' => $user->apps->getName(), 'image' => $user->apps->getImage(), 'plan_id' => $app->get_plan(), 'category_id' => $app->getCategoryId(), 'website' => $domains, 'newsletter' => $app->get_newsletter(), 'downgrade' => $downgrade_data, 'network' => $app->get_network()),));
                } else {
                    $arrUser = array('user' => array('name' => $user->get_first_name() . " " . $user->get_last_name(), 'slug' => $user->getSlug(), 'email' => $user->getEmail(), 'profile_picture' => $user->getProfilePicture(), 'token' => $signinToken, 'id' => $user->getId(), 'locale' => $user->getLocale(), 'role_id' => $user->getRoleId(), 'profile_picture' => $user->getProfilePicture(), 'newsletter' => $user->get_newsletter(),));
                }
                return array('status' => 'OK', 'data' => $arrUser);
            }
        }
        return array('status' => 'ERROR', 'code' => 603, 'message' => $this->getDi()->get('t')->_('user-wrong-email-password'));
    }
    public function getUsers($ids) {
        $results = false;
        if ($ids) {
            $results = self::find(array('id IN (:ids:) ', 'columns' => 'id,role_id,slug,name,job_title,email,dob,time_zone,profile_picture', 'bind' => array('ids' => $ids)));
            $query = "SELECT id,role_id,slug,name,job_title,email,dob,time_zone,profile_picture " . "FROM " . " Users " . "WHERE " . "id IN ($ids) ";
            $query = $this->getModelsManager()->createQuery($query);
            $results = $query->execute();
        }
        if ($results) {
            return $results->toArray();
        } else {
            return array();
        }
    }
    public function currentUser($userId) {
        $user = self::findFirst($userId);
        if ($user) {
            $sphere = Spheres::findFirst(array("country_id = :country_id:", 'bind' => array('country_id' => $user->getCountryid())));
            $arrSphere = array();
            if ($sphere != FALSE) {
                $arrSphere = array('id' => $sphere->getID(), 'name' => $sphere->getName(),);
            }
            $sentHeaders = \Phalcon\DI::getDefault()->get('headers');
            $signinToken = !empty($sentHeaders['Authorization']) ? $sentHeaders['Authorization'] : false;
            if ($user->role_id == Roles::APP || $user->role_id == Roles::ADMIN) {
                $app_admin = AppAdmins::findFirst('user_id=' . $user->getId());
                $app = Apps::findFirst($app_admin->getAppId());
                $arrUser = array('user' => array('token' => $signinToken, 'id' => $user->getId(), 'name' => $user->get_first_name() . " " . $user->get_last_name(), 'slug' => $user->getSlug(), 'role_id' => $user->getRoleId(), 'profile_picture' => $user->getProfilePicture(), 'locale' => $user->getLocale(), 'took_tour' => $user->getTookTour(), 'email' => $user->getEmail(), 'app' => array('id' => $app->getId(), 'name' => $user->apps->getName(), 'image' => $user->apps->getImage(), 'plan_id' => $app->get_plan(), 'category_id' => $app->getCategoryId(), 'website' => $app->getWebsite(), 'newsletter' => $app->get_newsletter(), 'network' => $app->get_network()),));
            } else {
                $pending = PendingEmail::findFirst('user_id=' . $user->getId());
                if ($pending) {
                    $pending = true;
                }
                $from = new \DateTime(date('Y-m-d', strtotime($user->getDob())));
                $to = new \DateTime('today');
                $age = $from->diff($to)->y;
                $arrUser = array('user' => array('name' => $user->get_first_name() . " " . $user->get_last_name(), 'slug' => $user->getSlug(), 'email' => $user->getEmail(), 'age' => $age, 'pending' => $pending, 'employer' => $user->getEmployer(), 'country' => $user->getCountryId(), 'country_data' => $user->Country ? $user->Country->toArray() : array(), 'token' => $signinToken, 'id' => $user->getId(), 'active' => $user->getActive(), 'profile_picture' => $user->getProfilePicture(), 'role_id' => $user->getRoleId(), 'newsletter' => $user->get_newsletter(),));
            }
            $favorites = FavoriteCategories::get_favorite_categories($user->getId());
            $arrUser['favorites'] = $favorites;
            return array('status' => 'OK', 'data' => $arrUser);
        }
        $errors = array();
        $errors[] = 'Not Found';
        return array('status' => 'ERROR', 'messages' => $errors);
    }
    public function create_user_account($user_data, $connect_type) {
        $user = new Users();
        $user->setEmail($user_data['email']);
        $user->setRoleId(Roles::USER);
        $user->set_first_name($user_data['first_name']);
        $user->set_last_name($user_data['last_name']);
        $user->setJobTitle($user_data['job_title']);
        $user->setEncryptedPassword(NULL, false);
        $user->setLocale(substr($user_data['locale'], 0, 2));
        $user->setActive(1);
        $user->setSlug(preg_replace("/[^\w]+/", "-", strtolower($user_data['first_name'] . $user_data['last_name'])) . time());
        $user->setDOB($user_data['dob']);
        $user->setCountryId($user_data['country_id']);
        $user->set_newsletter(1);
        $user->set_subscribe(1);
        if (strtolower($user_data['gender']) == "male") {
            $user_data['gender'] = 1;
        } else if (strtolower($user_data['gender']) == "female") {
            $user_data['gender'] = 0;
        }
        $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
        $arrUploadedPhotos = $objImgUploader->saveRemoteFile($user_data['image'], $objImgUploader::DIR_USERS);
        $user->setProfilePicture($arrUploadedPhotos['dir']);
        $user->set_init_reg($connect_type);
        if (!$user->save()) {
            $response['status'] = 'error';
            $response['message'] = $user->getMessages();
        } else {
            $response['status'] = 'ok';
        }
        return $response;
    }
    public function facebook_image($fb_id) {
        $url = 'https://graph.facebook.com/' . $fb_id . '/picture?width=256&height=256';
        $array = get_headers($url, 1);
        return (isset($array['Location']) ? $array['Location'] : false);
    }
    public function get_user_social_info($access_token, $connect_type) {
        $curl_obj = curl_init();
        if ($connect_type == "facebook") {
            $url = 'https://graph.facebook.com/me?access_token=' . $access_token;
        } else {
            $auth = 'Authorization : Bearer ' . $access_token;
            $url = 'https://www.googleapis.com/plus/v1/people/me';
            curl_setopt($curl_obj, CURLOPT_HTTPHEADER, array($auth));
        }
        curl_setopt($curl_obj, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_obj, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_obj, CURLOPT_URL, $url);
        $user_data = json_decode(curl_exec($curl_obj));
        curl_close($curl_obj);
        return array('status' => "ok", 'data' => $user_data);
    }
    public function authenticate_user($access_token, $connect_type) {
        if ($connect_type == "google") {
            $user_data = $this->get_user_social_info($access_token, "google");
            if ($user_data['data']->error) {
                return array("code" => "401", 'message' => "We can not access your google account anymore. Please reconnect");
            }
            $user_data = $user_data['data'];
            $email = $user_data->emails[0]->value;
        } else {
            $user_data = $this->get_user_social_info($access_token, "facebook");
            if ($user_data['data']->error) {
                return array("code" => "401", 'message' => "We can not access your facebook account anymore. Please reconnect");
            }
            $email = $user_data['data']->email;
        }
        if ($email == NULL) {
            return array("code" => "600", "message" => "You haven't granted us your email");
        } else {
            $user_exist = Users::findFirst('email="' . $email . '"');
            if ($user_exist) {
                if (!$user_exist->getActive()) {
                    $app_exist = Apps::findFirst('publisher_id=' . $user_exist->getId());
                    if ($app_exist) {
                        $app_exist->delete();
                    }
                    $user_exist->delete();
                } else {
                    $logged_user = $this->signIn(array('email' => $user_exist->getEmail()), true);
                    return array('code' => "200", 'data' => $logged_user['data']);
                }
            }
            if ($connect_type == "facebook") {
                $image = $this->facebook_image($user_data['data']->id);
                $country = explode(',', $user_data['data']->hometown->name);
                $country_name = trim($country[sizeof($country) - 1]);
                $country = Country::findFirst("lower(name) =lower('" . $country_name . "')");
                $country_id = ($country != NULL) ? $country->getId() : 0;
                $dob = date("Y-m-d", strtotime($user_data['data']->birthday));
                $user_data = array('job_title' => $user_data['data']->work[0]->position->name, 'dob' => $dob, 'country_id' => $country_id, 'access_token' => $access_token, 'email' => $user_data['data']->email, 'first_name' => $user_data['data']->first_name, 'last_name' => $user_data['data']->last_name, 'image' => $image, 'locale' => $user_data['data']->locale, 'gender' => $user_data['data']->gender);
            } else {
                $image = explode("sz=", $user_data->image->url);
                $image_url = $image[0] . "sz=256";
                $user_data = array('access_token' => $access_token, 'email' => $user_data->emails[0]->value, 'first_name' => $user_data->name->givenName, 'last_name' => $user_data->name->familyName, 'image' => $image_url, 'locale' => $user_data->language, 'gender' => $user_data->gender);
            }
            $user = $this->create_user_account($user_data, $connect_type);
            if ($user['status'] == 'ok') {
                $logged_user = $this->signIn(array('email' => $email), true);
                return array('code' => "200", 'data' => $logged_user['data']);
            } else {
                return array('code' => '600', 'message' => $user['message']);
            }
        }
    }
    private function _registerSession($user) {
        $this->session->start();
        $this->session->set('auth', array('id' => $user->getId(), 'email' => $user->getEmail()));
    }
    public function userDeactivate($slug) {
        $user = $this->findFirst(array("slug ='$slug'"));
        $user->setActive(self::INACTIVE);
        $user->save();
        return true;
    }
    public static function findSlugById($_userID) {
        return Users::query()->column('slug')->where('id = :id:')->bind(array('id' => $_userID))->limit(1)->execute();
    }
    public static function getOwnerInformation($userId) {
        $profilePictureQuery = array('user_id = :user_id:', 'bind' => array('user_id' => $userId));
        $profilePicture = ProfilePhotos::findFirst($profilePictureQuery);
        $attachedImages = AttachedPhotos::attachedImages($userId);
        $user = static ::findFirst($userId);
        $owners = array('id' => $user->getId(), 'slug' => $user->getSlug(), 'name' => $user->get_first_name() . " " . $user->get_last_name(), 'profile_photo' => array('id' => $profilePicture ? $profilePicture->getId() : FALSE, 'image' => $profilePicture ? $profilePicture->getImage() : FALSE), 'attached_photos' => $attachedImages);
        return $owners;
    }
    public static function getUserBySlug($_slug) {
        return Users::findFirst(array("slug = :slug: AND active = '1'", 'bind' => array('slug' => $_slug)));
    }
    public static function getUserMiniDetails($_userId) {
        $user = Users::findFirst($_userId);
        return array("name" => $user->get_first_name() . " " . $user->get_last_name(), "slug" => $user->getSlug(), "id" => $user->getId(), "profile_photo" => $user->getProfilePicture());
    }
    public function getMinifiedProfilePhotos($_user) {
        $photos = array();
        foreach ($_user->getProfilePhotos() as $photo) {
            $photos[] = array('id' => $photo->getId(), 'image' => $photo->getImage(), 'url' => Phalcon\DI::getDefault()->getShared('Utility')->generateURLFromDirPath($photo->getImage(), ''));
        }
        return $photos;
    }
    public static function getRoleIDByUserID($_userID) {
        return Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT role_id FROM Users WHERE id = :id:", array('id' => $_userID))->getFirst()->role_id;
    }
    public static function list_users($limit, $offset, $sort, $key) {
        $users = array();
        if ($key == "arguments" || $key == "country" || $key == "votes") {
            $di = FactoryDefault::getDefault();
            $connection = mysql_connect($di->getShared('config')->get('database')->get('host'), $di->getShared('config')->get('database')->get('username'), $di->getShared('config')->get('database')->get('password'));
            if (!$connection) {
                die('Could not connect: ' . mysql_error());
            }
            mysql_select_db($di->getShared('config')->get('database')->get('dbname'), $connection);
            mysql_query("SET time_zone = '+02:00'");
            if ($key == "arguments") {
                $query = "SELECT u.id,u.first_name,u.last_name,u.country_id,u.dob,u.job_title,u.created_at,u.email,(SELECT count(id) FROM arguments WHERE owner_id=u.id) AS count FROM users u WHERE u.active=1 AND u.role_id= 4 order by count $sort limit $limit offset  $offset";
            } else if ($key == "votes") {
                $query = "SELECT u.id,u.first_name,u.last_name,u.country_id,u.dob,u.job_title,u.created_at,u.email,(SELECT count(id) FROM votes WHERE voter_id=u.id) AS votes FROM users u WHERE u.active=1 AND u.role_id= 4 order by votes $sort limit $limit offset  $offset";
            } else {
                $query = "select u.id,u.email,u.first_name,u.last_name,u.dob,u.created_at,u.job_title,(SELECT name FROM country c WHERE c.id=u.country_id) AS name from users u WHERE u.active=1 AND u.role_id=4 order by name $sort limit $limit offset $offset";
            }
            $query_result = mysql_query($query);
            while ($r = mysql_fetch_array($query_result)) {
                $user['id'] = $r['id'];
                $user['email'] = $r['email'];
                $user['created_at'] = $r['created_at'];
                $user['job_title'] = ($r['job_title'] == NULL) ? "" : $r['job_title'];
                $user['dob'] = ($r['dob'] == NULL) ? "" : $r['dob'];
                $user['name'] = ($r['first_name'] == NULL) ? "" : $r['first_name'] . " " . $r['last_name'];
                if ($key == "arguments") {
                    $user['country'] = ($r['country_id']) ? Country::findFirst($r['country_id'])->getName() : "";
                    $user['arguments'] = ($r['count'] == NULL) ? 0 : $r['count'];
                    $user['votes'] = Votes::count('voter_id=' . $user['id']);
                } else if ($key == "votes") {
                    $user['country'] = ($r['country_id']) ? Country::findFirst($r['country_id'])->getName() : "";
                    $number_of_arguments = Arguments::find(array('columns' => 'count(id) as count', 'conditions' => 'owner_id=' . $user['id']))->toArray();
                    $user['arguments'] = $number_of_arguments[0]['count'];
                    $user['votes'] = ($r['votes'] == NULL) ? 0 : $r['votes'];
                } else {
                    $user['country'] = ($r['name'] == NULL) ? "" : $r['name'];
                    $number_of_arguments = Arguments::find(array('columns' => 'count(id) as count', 'conditions' => 'owner_id=' . $user['id']))->toArray();
                    $user['arguments'] = $number_of_arguments[0]['count'];
                    $user['votes'] = Votes::count('voter_id=' . $user['id']);
                }
                array_push($users, $user);
            }
        } else {
            if ($key == "name") {
                $key = "first_name";
            }
            $users = static ::find(array('conditions' => "role_id=4 AND active=1", 'columns' => array('id', 'first_name', 'last_name', 'email', 'dob', 'job_title', 'created_at', 'country_id'), "order" => "lower($key)" . " " . $sort, 'limit' => array('number' => $limit, 'offset' => $offset)))->toArray();
            for ($i = 0;$i < sizeof($users);$i++) {
                $users[$i]['job_title'] = ($users[$i]['job_title'] == NULL) ? "" : $users[$i]['job_title'];
                $users[$i]['dob'] = ($users[$i]['dob'] == NULL) ? "" : $users[$i]['dob'];
                $users[$i]['name'] = ($users[$i]['first_name'] == NULL) ? "" : $users[$i]['first_name'] . " " . $users[$i]['last_name'];
                $users[$i]['country'] = ($users[$i]['country_id']) ? Country::findFirst($users[$i]['country_id'])->getName() : "";
                $number_of_arguments = Arguments::find(array('columns' => 'count(id) as count', 'conditions' => 'owner_id=' . $users[$i]['id']))->toArray();
                $users[$i]['arguments'] = $number_of_arguments[0]['count'];
                $users[$i]['votes'] = Votes::count('voter_id=' . $users[$i]['id']);
                unset($users[$i]['country_id']);
                unset($users[$i]['first_name']);
                unset($users[$i]['last_name']);
            }
        }
        $number_of_users = intval(static ::count('active=1 AND role_id=4'));
        $hasmore = (($number_of_users >= (($offset + 1) + $limit)) ? true : FALSE);
        return array("users_data" => $users, "number_of_users" => $number_of_users, "has_more" => $hasmore);
    }
}
