<?php
class Feeds extends BaseModel {
    protected $id;
    protected $user_id;
    protected $activity_id;
    protected $element_id;
    protected $app_id;
    protected $parent_id;
    protected $created_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    public function setActivityId($activity_id) {
        $this->activity_id = $activity_id;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setParentId($parent_id) {
        $this->parent_id = $parent_id;
        return $this;
    }
    public function setModule($module) {
        $this->module = $module;
        return $this;
    }
    public function setElementId($element_id) {
        $this->element_id = $element_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getUserId($withData = false) {
        if ($withData) {
            $user = new Users();
            $user->currentUser($this->user_id);
            return $user->data->user;
        }
        return $this->user_id;
    }
    public function getActivityId($withLabel = false) {
        if ($withLabel) {
            $activites = \Activities::findFirst($withLabel);
            $_activity = $activites->getAction();
            $configs = json_decode(json_encode(\API::instance()->getConfig('routesConfiguration')), 1);
            $exist = Speakol\ACL::searchForRoute($configs, $_activity);
            if (isset($exist['feed_label'])) {
                return $exist['feed_label'];
            }
            return false;
        }
        return $this->activity_id;
    }
    public function getElementId($withData = false) {
        return $this->element_id;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getParentId() {
        return $this->parent_id;
    }
    public function getModule() {
        return $this->module;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function searchMDArray($arr, $val) {
        $method = __METHOD__;
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                return $this->$method();
            } else {
                $this->call();
            }
        }
    }
    public function getUsersActivity($users, $page = '1', $perPage = '1', $orderBy = 'created_at DESC', $feedFilter = '', $extraData = true) {
        $page = $page < 1 ? 1 : $page;
        $startValue = ($page - 1) * $perPage;
        if (!empty($users) && is_array($users)) {
            $arrKeys = array_keys($users);
            array_walk($arrKeys, function (&$value) {
                $value = ':' . $value . ':';
            });
            $condition = "user_id IN (" . implode(",", $arrKeys) . ") AND app_id is NULL ";
            $bind = $users;
        } else {
            $condition = "user_id = :user_id: AND app_id is NULL ";
            $bind = array('user_id' => $users);
        }
        if ($feedFilter && in_array($feedFilter, array(self::DEBATE, self::COMPARISON))) {
            $condition.= ' AND module = :module: ';
            $bind['module'] = $feedFilter;
        }
        $query = new Phalcon\Mvc\Model\Query\Builder();
        $query->columns(array('count( DISTINCT parent_id) as count'));
        $query->from('Feeds');
        $query->where($condition, $bind);
        $query->orderBy("created_at DESC");
        $count = $query->getQuery()->execute();
        $count = $count['0']->count;
        $results = array();
        if ($count > $startValue) {
            $query = new Phalcon\Mvc\Model\Query\Builder();
            $query->columns(array('created_at', 'parent_id', "activity_user" => "substring_index(GROUP_CONCAT(CONCAT(activity_id,'|',user_id,'|',created_at,'|',module,'|',element_id) ), ',', 10)"));
            $query->from('Feeds');
            $query->groupBy(array('parent_id'));
            $query->orderBy("created_at DESC");
            $query->where($condition, $bind);
            $query->limit($perPage, $startValue);
            $results = $query->getQuery()->execute();
        }
        $data = array('result' => $results, 'count' => $count, 'start_value' => $startValue, 'order' => $orderBy);
        if ($extraData) {
            return array('data' => $this->handleFeedsResults($data), 'extra' => array('hasmore' => (($count >= (($startValue + 1) + $perPage)) ? true : FALSE),));
        }
        return $this->handleFeedsResults($data);
    }
    public function getHomeActivity($page = '1', $perPage = '1', $orderBy = 'created_at DESC', $feedFilter = '', $extraData = false) {
        $count = self::count();
        $page = $page < 1 ? 1 : $page;
        $startValue = ($page - 1) * $perPage;
        $results = array();
        $condition = 'app_id is NULL ';
        $bind = array();
        if ($feedFilter && in_array($feedFilter, array(self::DEBATE, self::COMPARISON))) {
            $condition.= ' AND module = :module: ';
            $bind['module'] = $feedFilter;
        }
        $query = new Phalcon\Mvc\Model\Query\Builder();
        $query->columns(array('count( DISTINCT parent_id) as count'));
        $query->from('Feeds');
        $query->where($condition, $bind);
        $query->orderBy("created_at DESC");
        $count = $query->getQuery()->execute();
        $count = $count['0']->count;
        if ($count > $startValue) {
            $query = new Phalcon\Mvc\Model\Query\Builder();
            $query->columns(array('parent_id', "activity_user" => "substring_index(GROUP_CONCAT(CONCAT(activity_id,'|',user_id,'|',created_at,'|',module,'|',element_id) ), ',', 10)"));
            $query->from('Feeds');
            $query->groupBy(array('parent_id'));
            $query->orderBy("created_at DESC");
            $query->where($condition, $bind);
            $query->limit($perPage, $startValue);
            $results = $query->getQuery()->execute();
        }
        $data = array('result' => $results, 'count' => $count, 'start_value' => $startValue, 'order' => $orderBy,);
        if ($extraData) {
            return array('data' => $this->handleFeedsResults($data), 'extra' => array('hasmore' => (($count >= (($startValue + 1) + $perPage)) ? true : FALSE),));
        }
        return $this->handleFeedsResults($data);
    }
    public function getAppsActivity($appId, $page = '1', $perPage = '1', $orderBy = 'created_at DESC', $feedFilter = '', $extraData = true) {
        $page = $page < 1 ? 1 : $page;
        $startValue = ($page - 1) * $perPage;
        $condition = 'app_id = :app_id: ';
        $bind = array('app_id' => $appId);
        $results = array();
        $configs = json_decode(json_encode(\API::instance()->getConfig('routesConfiguration')), 1);
        $activitiesIds = Activities::findFirst(array('conditions' => " action in ('/debates/create','/comparisons/create','/argument-box/create') ", 'columns' => array('GROUP_CONCAT( id ) AS id'),))->id;
        if ($activitiesIds) {
            $condition.= ' AND activity_id IN (' . $activitiesIds . ') ';
        }
        if ($feedFilter && in_array($feedFilter, array(self::DEBATE, self::COMPARISON, self::ARGUMENTSBOX, 'argumentbox'))) {
            $condition.= ' AND module = :module: ';
            $bind['module'] = $feedFilter;
        }
        $query = new Phalcon\Mvc\Model\Query\Builder();
        $query->columns(array('count( DISTINCT parent_id) as count'));
        $query->from('Feeds');
        $query->orderBy("created_at DESC");
        $query->where($condition, $bind);
        $count = $query->getQuery()->execute();
        $count = $count['0']->count;
        if ($count > $startValue) {
            $query = new Phalcon\Mvc\Model\Query\Builder();
            $query->columns(array('parent_id', "activity_user" => "substring_index(GROUP_CONCAT(CONCAT(activity_id,'|',user_id,'|',created_at,'|',module,'|',element_id) ), ',', 10)"));
            $query->from('Feeds');
            $query->groupBy(array('parent_id'));
            $query->orderBy("created_at DESC");
            $query->where($condition, $bind);
            $query->limit($perPage, $startValue);
            $results = $query->getQuery()->execute();
        }
        $data = array('result' => $results, 'count' => $count, 'start_value' => $startValue, 'order' => $orderBy);
        if ($extraData) {
            return array('data' => $this->handleFeedsResults($data), 'extra' => array('hasmore' => (($count >= (($startValue + 1) + $perPage)) ? true : FALSE),));
        }
        return $this->handleFeedsResults($data);
    }
    public function getActiveUsers($startDate = false, $endDate = false, $page = '1', $perPage = '1', $orderBy = 'user_id,created_at', $extraData = true) {
        $startDate = $startDate ? $startDate : date('Y-m-d 00:00:00', time());
        $endDate = $endDate ? $endDate : date('Y-m-d 23:59:59', time());
        $page = $page < 1 ? 1 : $page;
        $startValue = ($page - 1) * $perPage;
        $results = array();
        $query = new Phalcon\Mvc\Model\Query\Builder();
        $query->columns(array('count( DISTINCT parent_id) as count'));
        $query->from('Feeds');
        $query->orderBy("created_at DESC");
        $count = $query->getQuery()->execute();
        $count = $count['0']->count;
        if ($count > $startValue) {
            $query = new Phalcon\Mvc\Model\Query\Builder();
            $query->columns(array('parent_id', "activity_user" => "substring_index(GROUP_CONCAT(CONCAT(activity_id,'|',user_id,'|',created_at,'|',module,'|',element_id) ), ',', 10)"));
            $query->from('Feeds');
            $query->groupBy(array('parent_id'));
            $query->orderBy("created_at DESC");
            $query->limit($perPage, $startValue);
            $results = $query->getQuery()->execute();
        }
        $resultsArr = $results->toArray();
        if (empty($resultsArr)) {
            throw new Exception('No feeds for today!');
        }
        $data = array('result' => $results, 'count' => $count, 'start_value' => $startValue, 'order' => $orderBy,);
        if ($extraData) {
            return array('data' => $this->handleFeedsResults($data), 'extra' => array('hasmore' => (($count >= (($startValue + 1) + $perPage)) ? true : FALSE),));
        }
        return $this->handleFeedsResults($data);
    }
    public function handleFeedsResults($data) {
        $results = $data['result'];
        $count = $data['count'];
        $startValue = $data['start_value'];
        $orderBy = $data['order'];
        $return = array();
        if ($results && $results->toArray()) {
            $data = array();
            $user = new Users();
            $configs = json_decode(json_encode(\API::instance()->getConfig('routesConfiguration')), 1);
            $utility = new Speakol\Utility($this->getDI());
            $sort = array();
            foreach ($results as $row) {
                $da = explode(',', $row->activity_user);
                $arr = $columns = array();
                foreach ($da as $r) {
                    list($columns['activity_id'], $columns['user_id'], $columns['created_at'], $columns['module'], $columns['element_id']) = explode('|', $r);
                    $arr[strtotime($columns['created_at']) ] = $columns;
                    krsort($arr);
                }
                foreach ($arr as $r) {
                    $columns = (object)$r;
                    if (!isset($sort[$columns->module . '_' . $row->parent_id])) {
                        $count = count($sort);
                        $sort[$columns->module . '_' . $row->parent_id] = $count + 1;
                    }
                    $activity = Activities::findFirst(array($columns->activity_id, 'columns' => 'action'));
                    $exist = Speakol\ACL::searchForRoute($configs, $activity->action, true);
                    if (empty($exist['route']['feed']['action'])) {
                        continue;
                    }
                    $key = $exist['route']['feed']['module'];
                    $users[$columns->user_id] = !empty($users[$columns->user_id]) ? $users[$columns->user_id] : $user->currentUser($columns->user_id);
                    $users[$columns->user_id] = isset($users[$columns->user_id]['data']['user']) ? $users[$columns->user_id]['data']['user'] : $users[$columns->user_id];
                    $data['modules'][$columns->module]['ids'][] = $row->parent_id;
                    $itemData = array('user' => $users[$columns->user_id], 'date' => $utility->time_elapsed_string(strtotime($columns->created_at)), 'id' => $columns->element_id);
                    if ($exist['route']['feed']['action'] == $configs['ArgumentsController']['/arguments/create']['feed']['action']) {
                        $itemData['content'] = Arguments::findFirst($columns->element_id);
                        $itemData['content'] = !empty($itemData['content']) ? $itemData['content']->toArray() : array();
                    } else {
                        $itemData['content'] = array();
                    }
                    $userSLug = isset($itemData['user']['slug']) ? $itemData['user']['slug'] : $itemData['user']['app']['slug'];
                    $data['feeds'][$columns->module][$row->parent_id][$exist['route']['feed']['action']][$userSLug] = $itemData;
                }
            }
            $return = $this->getModuleResults($data, $orderBy, $sort);
            return $this->mergeResultsWithFeeds($return, $data);
        }
        return false;
    }
    public function getModuleResults($data, $orderBy, $sort) {
        foreach ($data['modules'] as $module => $dat) {
            $use[$module] = array();
            $debates = new Debates();
            $comparisons = new Comparisons();
            $argumentboxes = new Argumentboxes();
            switch ($module) {
                case 'debate':
                    $arrKeys = array_keys($dat['ids']);
                    array_walk($arrKeys, function (&$value) {
                        $value = ':' . $value . ':';
                    });
                    $conditions = array("id IN (" . implode(",", $arrKeys) . ") ");
                    $options = array('bind' => $dat['ids'], "order" => $orderBy);
                    $return[$module] = $debates->listMiniDebates($conditions, $options, $sort);
                break;
                case 'comparison':
                    $arrKeys = array_keys($dat['ids']);
                    array_walk($arrKeys, function (&$value) {
                        $value = ':' . $value . ':';
                    });
                    $conditions = array("id IN (" . implode(",", $arrKeys) . ") ");
                    $options = array('bind' => $dat['ids']);
                    $return[$module] = $comparisons->listMiniComparison($conditions, $options, $sort);
                break;
                case 'argumentbox':
                    $arrKeys = array_keys($dat['ids']);
                    array_walk($arrKeys, function (&$value) {
                        $value = ':' . $value . ':';
                    });
                    $conditions = array("id IN (" . implode(",", $arrKeys) . ") ");
                    $options = array('bind' => $dat['ids']);
                    $return[$module] = $argumentboxes->listMiniArguments($conditions, $options, $sort);
                break;
                default:
                break;
            }
        }
        return $return;
    }
    public function mergeResultsWithFeeds($return, $data) {
        foreach ($data['feeds'] as $k => $val) {
            if (!empty($return[$k])) {
                foreach ($val as $k2 => $val2) {
                    if (isset($return[$k][$k2])) {
                        $return[$k][$k2]['feeds'] = !empty($val2) ? $val2 : array();
                    }
                }
            }
        }
        $return2 = array();
        if ($return) {
            foreach ($return as $key => $row) {
                $return2 = array_merge($return2, $row);
            }
        }
        uasort($return2, function ($a, $b) {
            if (!empty($a['data']) && !empty($b['data'])) return ($a['data']['sort_id'] > $b['data']['sort_id'] ? 1 : -1);
        });
        return $return2;
    }
}
