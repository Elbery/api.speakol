<?php
class ArgumentboxesAttachments extends \Phalcon\Mvc\Model {
    protected $id;
    protected $argumentbox_id;
    protected $attachment_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setArgumentboxId($argumentbox_id) {
        $this->argumentbox_id = $argumentbox_id;
        return $this;
    }
    public function setAttachmentId($attachment_id) {
        $this->attachment_id = $attachment_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getArgumentboxId() {
        return $this->argumentbox_id;
    }
    public function getAttachmentId() {
        return $this->attachment_id;
    }
    public function columnMap() {
        return array('id' => 'id', 'argumentbox_id' => 'argumentbox_id', 'attachment_id' => 'attachment_id');
    }
}
