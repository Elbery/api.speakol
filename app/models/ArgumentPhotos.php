<?php
class ArgumentPhotos extends BaseModel {
    protected $id;
    protected $image;
    protected $url;
    protected $argument_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    public function setArgumentId($argument_id) {
        $this->argument_id = $argument_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getImage() {
        return $this->image;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getArgumentId() {
        return $this->argument_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('argument_id', 'Arguments', 'id', NULL);
    }
    public static function getArgumentPhotos($_argumentId, $_uploaderType = BaseModel::ARGUMENT) {
        return ArgumentPhotos::query()->where('argument_id = :id:')->bind(array('id' => $_argumentId,))->execute()->toArray();
    }
    public static function saveArgumentPhotos($_argumentID, $arrUploadedPhotos) {
        foreach ($arrUploadedPhotos as $photo) {
            if ($photo['status'] == FALSE) {
                continue;
            }
            $modelArgumentPhotos = new ArgumentPhotos();
            $modelArgumentPhotos->setArgumentId($_argumentID);
            $modelArgumentPhotos->setUrl($photo['path']);
            $modelArgumentPhotos->setImage($photo['new']);
            $modelArgumentPhotos->setCreatedAt(strftime('%Y-%m-%d', time()));
            $modelArgumentPhotos->setUpdatedAt(strftime('%Y-%m-%d', time()));
            if ($modelArgumentPhotos->save() == FALSE) {
                \Phalcon\DI::getDefault()->getShared('Utility')->handleDataBaseSaveErros($modelArgumentPhotos);
            }
        }
    }
    public static function filterUrl($_arrPhotos) {
        if (!is_array($_arrPhotos)) {
            return null;
        }
        $_arrReturn = array();
        foreach ($_arrPhotos as $photo) {
            $_arrReturn[] = array('id' => $photo['id'], 'image' => $photo['image'], 'url' => Phalcon\DI::getDefault()->getShared('Utility')->generateURLFromDirPath($photo['url'], $photo['image']));
        }
        return $_arrReturn;
    }
}
