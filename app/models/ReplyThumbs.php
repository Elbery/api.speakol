<?php
class ReplyThumbs extends BaseModel {
    protected $id;
    protected $reply_id;
    protected $thumber_id;
    protected $thumb_up;
    protected $created_at;
    protected $updated_at;
    protected $link;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setReplyId($reply_id) {
        $this->reply_id = $reply_id;
        return $this;
    }
    public function setThumberId($thumber_id) {
        $this->thumber_id = $thumber_id;
        return $this;
    }
    public function setThumbUp($thumb_up) {
        $this->thumb_up = $thumb_up;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function setLink($link) {
        $this->link = $link;
    }
    public function getId() {
        return $this->id;
    }
    public function getReplyId() {
        return $this->reply_id;
    }
    public function getThumberId() {
        return $this->thumber_id;
    }
    public function getThumbUp() {
        return $this->thumb_up;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function getLink() {
        return $this->link;
    }
    public function initialize() {
        $this->belongsTo('thumber_id', 'Users', 'id', NULL);
        $this->belongsTo('reply_id', 'Replies', 'id', NULL);
    }
    public static function findThumbsByReplyIDUserID($_replyId, $_userId) {
        return ReplyThumbs::query()->where("reply_id = :reply_id:")->andWhere("thumber_id = :thumber_id:")->bind(array('reply_id' => $_replyId, 'thumber_id' => $_userId,))->execute()->getFirst();
    }
    public static function createVote($_replyId, $_userId, $vote, $link) {
        $objReplyThumbs = new ReplyThumbs();
        $objReplyThumbs->setReplyId($_replyId);
        $objReplyThumbs->setThumberId($_userId);
        $objReplyThumbs->setThumbUp($vote);
        $objReplyThumbs->setLink($link);
        if ($objReplyThumbs->save() == FALSE) {
            $strErrors = "";
            foreach ($objReplyThumbs->getMessages() as $message) {
                $strErrors.= $message->getMessage();
                $strErrors.= ". ";
            }
            throw new \Exception("Conflict:{$strErrors}", 409);
        } else {
            return $objReplyThumbs->getId();
        }
    }
    public static function updateVote($_replyId, $_userId, $vote, $link) {
        $objReplyThumbs = self::findThumbsByReplyIDUserID($_replyId, $_userId);
        $objReplyThumbs->setReplyId($_replyId);
        $objReplyThumbs->setThumberId($_userId);
        $objReplyThumbs->setThumbUp($vote);
        $objReplyThumbs->setLink($link);
        if ($objReplyThumbs->update() == FALSE) {
            $strErrors = "";
            foreach ($objReplyThumbs->getMessages() as $message) {
                $strErrors.= $message->getMessage();
                $strErrors.= ". ";
            }
            throw new \Exception("Conflict:{$strErrors}", 409);
        } else {
            return $objReplyThumbs->getId();
        }
    }
    public static function findThumbsUPByReplyID($_replyId) {
        return ReplyThumbs::query()->where("reply_id = :reply_id:")->andWhere("thumb_up = :thumb_up:")->bind(array('reply_id' => $_replyId, 'thumb_up' => 1,))->execute()->count();
    }
    public static function findThumbsDNByReplyID($_replyId) {
        return ReplyThumbs::query()->where("reply_id = :reply_id:")->andWhere("thumb_up = :thumb_up:")->bind(array('reply_id' => $_replyId, 'thumb_up' => 0,))->execute()->count();
    }
    public static function findThumbByReplyIDUserID($_replyId, $_userId) {
        $objThumb = self::findThumbsByReplyIDUserID($_replyId, $_userId);
        if ($objThumb != FALSE) {
            return $objThumb->getThumbUp();
        } else {
            return false;
        }
    }
    public static function findThumbsByTrinary($_replyId, $_userId, $_boolVote) {
        $hasVoted = ReplyThumbs::findFirst(array('conditions' => "reply_id = :reply_id: AND thumber_id = :thumber_id: AND thumb_up = :thumb_up:", 'bind' => array('reply_id' => $_replyId, 'thumber_id' => $_userId, 'thumb_up' => $_boolVote)));
        if ($hasVoted) {
            $hasVoted->delete();
        }
        return $hasVoted ? 1 : 0;
    }
}
