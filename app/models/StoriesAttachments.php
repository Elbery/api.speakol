<?php
class StoriesAttachments extends \Phalcon\Mvc\Model {
    protected $id;
    protected $story_id;
    protected $attachment_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setStoryId($story_id) {
        $this->story_id = $story_id;
        return $this;
    }
    public function setAttachmentId($attachment_id) {
        $this->attachment_id = $attachment_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getStoryId() {
        return $this->story_id;
    }
    public function getAttachmentId() {
        return $this->attachment_id;
    }
    public function initialize() {
        $this->belongsTo('attachment_id', 'AttachedPhotos', 'id', array('foreignKey' => true));
        $this->belongsTo('story_id', 'Stories', 'id', array('foreignKey' => true));
    }
    public function saveStoriesAttachement($storyId, $attachmentId, $transaction) {
        $storyAttachment = new StoriesAttachments();
        $storyAttachment->setStoryId($storyId);
        $storyAttachment->setAttachmentId($attachmentId);
        if (!$storyAttachment->save()) {
            $transaction->rollback("Can't save attachment forign key!");
        }
    }
}
