<?php
class DebateFollows extends BaseModel {
    protected $id;
    protected $debate_id;
    protected $follower_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setDebateId($debate_id) {
        $this->debate_id = $debate_id;
        return $this;
    }
    public function setFollowerId($follower_id) {
        $this->follower_id = $follower_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getDebateId() {
        return $this->debate_id;
    }
    public function getFollowerId() {
        return $this->follower_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('follower_id', 'Users', 'id', NULL);
        $this->belongsTo('debate_id', 'Debates', 'id', NULL);
    }
    public function isFollow($slug, $id) {
        $debate = Debates::findDebateBySlug($slug);
        if (count($debate) == 0) {
            return false;
        }
        $debateId = $debate->getFirst()->getId();
        $follower = Users::find($id);
        if (count($follower) == 0) {
            return false;
        }
        $conditions = "debate_id= :debate_id: AND follower_id= :follower_id:";
        $parameters = array('debate_id' => $debateId, 'follower_id' => $id);
        $result = self::find(array("conditions" => $conditions, "bind" => $parameters));
        if (count($result) > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public function insertFollow($slug, $id) {
        $debate = Debates::findDebateBySlug($slug);
        if (count($debate) == 0) {
            return false;
        }
        $debateId = $debate->getFirst()->getId();
        $follower = Users::find($id);
        if (count($follower) == 0) {
            return false;
        }
        $conditions = "debate_id= :debate_id: AND follower_id= :follower_id:";
        $parameters = array('debate_id' => $debateId, 'follower_id' => $id);
        $result = self::find(array("conditions" => $conditions, "bind" => $parameters));
        if (count($result) > 0) {
            return 1;
        }
        $this->setDebateId($debateId);
        $this->setFollowerId($id);
        $this->setCreatedAt('');
        $this->setUpdatedAt('');
        if ($this->save()) {
            return true;
        }
        return false;
    }
    public function deleteFollow($slug, $id) {
        $debate = Debates::findDebateBySlug($slug);
        if (count($debate) == 0) {
            return false;
        }
        $debateId = $debate->getFirst()->getId();
        $conditions = "debate_id= :debate_id: AND follower_id= :follower_id:";
        $parameters = array('debate_id' => $debateId, 'follower_id' => $id);
        $result = self::find(array("conditions" => $conditions, "bind" => $parameters));
        if (count($result) > 0) {
            if ($result->delete()) {
                return true;
            }
        }
        return false;
    }
    static public function listFollowers($slug) {
        $debate = Debates::findDebateBySlug($slug);
        if (count($debate) == 0) {
            return false;
        }
        $debateId = $debate->getFirst()->getId();
        return self::listFollowersByID($debateId);
    }
    public static function listFollowersByID($debateId) {
        $result = self::find(array("conditions" => "debate_id= :debate_id:", "bind" => array('debate_id' => $debateId)));
        return $result->toArray();
    }
}
