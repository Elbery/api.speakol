<?php
class AdministratorSpheres extends BaseModel {
    protected $id;
    protected $administrator_id;
    protected $sphere_id;
    protected $super_admin;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setAdministratorId($administrator_id) {
        $this->administrator_id = $administrator_id;
        return $this;
    }
    public function setSphereId($sphere_id) {
        $this->sphere_id = $sphere_id;
        return $this;
    }
    public function setSuperAdmin($super_admin) {
        $this->super_admin = $super_admin;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getAdministratorId() {
        return $this->administrator_id;
    }
    public function getSphereId() {
        return $this->sphere_id;
    }
    public function getSuperAdmin() {
        return $this->super_admin;
    }
    public function initialize() {
        $this->belongsTo('sphere_id', 'Spheres', 'id', NULL);
        $this->belongsTo('administrator_id', 'Administrators', 'id', NULL);
    }
}
