<?php
class EmailSettings extends BaseModel {
    protected $id;
    protected $follow;
    protected $debate_invitation;
    protected $message;
    protected $debate_ended;
    protected $reply_on_argument;
    protected $updates;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setFollow($follow) {
        $this->follow = $follow;
        return $this;
    }
    public function setDebateInvitation($debate_invitation) {
        $this->debate_invitation = $debate_invitation;
        return $this;
    }
    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }
    public function setDebateEnded($debate_ended) {
        $this->debate_ended = $debate_ended;
        return $this;
    }
    public function setReplyOnArgument($reply_on_argument) {
        $this->reply_on_argument = $reply_on_argument;
        return $this;
    }
    public function setUpdates($updates) {
        $this->updates = $updates;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getFollow() {
        return $this->follow;
    }
    public function getDebateInvitation() {
        return $this->debate_invitation;
    }
    public function getMessage() {
        return $this->message;
    }
    public function getDebateEnded() {
        return $this->debate_ended;
    }
    public function getReplyOnArgument() {
        return $this->reply_on_argument;
    }
    public function getUpdates() {
        return $this->updates;
    }
}
