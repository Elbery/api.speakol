<?php
use Phalcon\Crypt;
class UserCard extends BaseModel {
    protected $id;
    protected $app_id;
    protected $payment_method;
    protected $name;
    protected $number;
    protected $expiry_year;
    protected $expiry_month;
    protected $code;
    protected $default_card;
    protected $postal_code;
    protected $city;
    protected $state;
    protected $address;
    protected $country_id;
    protected $address2;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_app_id($aid) {
        $this->app_id = $aid;
    }
    public function set_payment_method($p_method) {
        $this->payment_method = $p_method;
    }
    public function set_name($name) {
        $this->name = ucwords($name);
    }
    public function set_number($number) {
        $this->number = $number;
    }
    public function set_expiry_year($expiry_y) {
        $this->expiry_year = $expiry_y;
    }
    public function set_expiry_month($expiry_m) {
        $this->expiry_month = $expiry_m;
    }
    public function set_code($code) {
        $crypt = new Crypt();
        $crypt->setCipher('blowfish');
        $key = 'Zadbfe7';
        $crypt->setKey($key);
        $this->code = $crypt->encryptBase64($code);
    }
    public function set_default_card($default) {
        $this->default_card = $default;
    }
    public function set_postal_code($code) {
        $this->postal_code = $code;
    }
    public function set_city($city) {
        $this->city = $city;
    }
    public function set_state($state) {
        $this->state = $state;
    }
    public function set_address($address) {
        $this->address = $address;
    }
    public function set_address2($address) {
        $this->address2 = $address;
    }
    public function set_country_id($id) {
        $this->country_id = $id;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_app_id() {
        return $this->app_id;
    }
    public function get_payment_method() {
        return $this->payment_method;
    }
    public function get_name() {
        return $this->name;
    }
    public function get_number() {
        return $this->number;
    }
    public function get_expiry_year() {
        return $this->expiry_year;
    }
    public function get_expiry_month() {
        return $this->expiry_month;
    }
    public function get_code() {
        $crypt = new Crypt();
        $crypt->setCipher('blowfish');
        $key = 'Zadbfe7';
        $crypt->setKey($key);
        $code = intval($crypt->decryptBase64($this->code));
        return $code;
    }
    public function get_default_card() {
        return $this->default_card;
    }
    public function get_postal_code() {
        return $this->postal_code;
    }
    public function get_city() {
        return $this->city;
    }
    public function get_state() {
        return $this->state;
    }
    public function get_address() {
        return $this->address;
    }
    public function get_address2() {
        return $this->address2;
    }
    public function get_country_id() {
        return $this->country_id;
    }
}
