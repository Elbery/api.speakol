<?php
class UserEmailsLog extends BaseModel {
    protected $id;
    protected $user_id;
    protected $email_type;
    protected $last_date;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_user_id($user_id) {
        $this->user_id = $user_id;
    }
    public function set_email_type($email_type) {
        $this->email_type = $email_type;
    }
    public function set_last_date($last_date) {
        $this->last_date = $last_date;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_user_id() {
        return $this->user_id;
    }
    public function get_email_type() {
        return $this->email_type;
    }
    public function get_last_date() {
        return $this->last_date;
    }
}
