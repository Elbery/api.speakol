<?php
class PendingEmail extends BaseModel {
    protected $id;
    protected $email;
    protected $user_id;
    protected $comfirmation_token;
    protected $created_at;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_email($email) {
        $this->email = $email;
    }
    public function set_user_id($user_id) {
        $this->user_id = $user_id;
    }
    public function set_confirmation_token($token) {
        $this->confirmation_token = $token;
    }
    public function set_created_at($date) {
        $this->created_at = $date;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_email() {
        return $this->email;
    }
    public function get_user_id() {
        return $this->user_id;
    }
    public function get_confirmation_token() {
        return $this->confirmation_token;
    }
    public function get_created_at() {
        return $this->created_at;
    }
}
