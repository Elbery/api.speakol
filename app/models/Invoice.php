<?php
class Invoice extends BaseModel {
    protected $id;
    protected $amount;
    protected $date;
    protected $user_id;
    protected $user_email;
    protected $status;
    protected $paying_date;
    protected $card_no;
    protected $payment_method;
    protected $plan;
    protected $address;
    protected $website;
    protected $user_name;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_amount($amount) {
        $this->amount = $amount;
    }
    public function set_date($date) {
        $this->date = $date;
    }
    public function set_user_id($uId) {
        $this->user_id = $uId;
    }
    public function set_user_email($uEmail) {
        $this->user_email = $uEmail;
    }
    public function set_status($status) {
        $this->status = $status;
    }
    public function set_paying_date($date) {
        $this->paying_date = $date;
    }
    public function set_payment_method($method) {
        $this->payment_method = $method;
    }
    public function set_card_no($card) {
        $this->card_no = $card;
    }
    public function set_plan($plan) {
        $this->plan = $plan;
    }
    public function set_address($address) {
        $this->address = $address;
    }
    public function set_website($site) {
        $this->website = $site;
    }
    public function set_user_name($u_name) {
        $this->user_name = $u_name;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_amount() {
        return $this->amount;
    }
    public function get_date() {
        return $this->date;
    }
    public function get_user_id() {
        return $this->user_id;
    }
    public function get_user_email() {
        return $this->user_email;
    }
    public function get_status() {
        return $this->status;
    }
    public function get_paying_date() {
        return $this->paying_date;
    }
    public function get_payment_method() {
        return $this->payment_method;
    }
    public function get_card_no() {
        return $this->card_no;
    }
    public function get_plan() {
        return $this->plan;
    }
    public function get_address() {
        return $this->address;
    }
    public function get_website() {
        return $this->website;
    }
    public function get_user_name() {
        return $this->user_name;
    }
}
