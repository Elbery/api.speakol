<?php
class ComparisonFollows extends BaseModel {
    protected $id;
    protected $comparison_id;
    protected $follower_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setComparisonId($comparison_id) {
        $this->comparison_id = $comparison_id;
        return $this;
    }
    public function setFollowerId($follower_id) {
        $this->follower_id = $follower_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getComparisonId() {
        return $this->comparison_id;
    }
    public function getFollowerId() {
        return $this->follower_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('follower_id', 'Users', 'id', array("foreignKey" => array("message" => "The part_id does not exist on the Parts model")));
        $this->belongsTo('comparison_id', 'Comparisons', 'id', NULL);
    }
    public function isFollowed($slug, $id) {
        $comparison = Comparisons::findComparisonBySlug($slug);
        if (count($comparison) == 0) {
            return false;
        }
        $comparisonId = $comparison->getFirst()->getId();
        $follower = Users::find($id);
        if (count($follower) == 0) {
            return false;
        }
        $conditions = "comparison_id= :comparison_id: AND follower_id= :follower_id:";
        $parameters = array('comparison_id' => $comparisonId, 'follower_id' => $id);
        $result = self::find(array("conditions" => $conditions, "bind" => $parameters));
        if (count($result) > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public function insertFollow($slug, $id) {
        $comparison = Comparisons::findComparisonBySlug($slug);
        if (count($comparison) == 0) {
            return false;
        }
        $comparisonId = $comparison->getFirst()->getId();
        $follower = Users::find($id);
        if (count($follower) == 0) {
            return false;
        }
        $conditions = "comparison_id= :comparison_id: AND follower_id= :follower_id:";
        $parameters = array('comparison_id' => $comparisonId, 'follower_id' => $id);
        $result = self::find(array("conditions" => $conditions, "bind" => $parameters));
        if (count($result) > 0) {
            return 1;
        }
        $this->setComparisonId($comparisonId);
        $this->setFollowerId($id);
        if ($this->save()) {
            return true;
        }
        return false;
    }
    public function deleteFollow($slug, $id) {
        $comparisons = Comparisons::findComparisonBySlug($slug);
        if (count($comparisons) == 0) {
            return false;
        }
        $comparisonsId = $comparisons->getFirst()->getId();
        $conditions = "comparison_id= :comparison_id: AND follower_id= :follower_id:";
        $parameters = array('comparison_id' => $comparisonsId, 'follower_id' => $id);
        $result = self::find(array("conditions" => $conditions, "bind" => $parameters));
        if (count($result) > 0) {
            if ($result->delete()) {
                return true;
            }
        }
        return false;
    }
    static public function listFollowers($slug) {
        $comparisons = Comparisons::findComparisonBySlug($slug);
        if (count($comparisons) == 0) {
            return false;
        }
        $comparisonsId = $comparisons->getFirst()->getId();
        $result = self::find(array("conditions" => "comparison_id= :comparison_id:", "bind" => array('comparison_id' => $comparisonsId)));
        return $result->toArray();
    }
}
