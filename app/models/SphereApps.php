<?php
class SphereApps extends BaseModel {
    protected $id;
    protected $app_id;
    protected $sphere_id;
    protected $featured;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setSphereId($sphere_id) {
        $this->sphere_id = $sphere_id;
        return $this;
    }
    public function setFeatured($featured) {
        $this->featured = $featured;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getSphereId() {
        return $this->sphere_id;
    }
    public function getFeatured() {
        return $this->featured;
    }
    public function initialize() {
        $this->belongsTo('sphere_id', 'Spheres', 'id', NULL);
        $this->belongsTo('app_id', 'Apps', 'id', NULL);
    }
}
