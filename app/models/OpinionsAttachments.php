<?php
use Phalcon\DI\FactoryDefault;
class OpinionsAttachments extends BaseModel {
    protected $id;
    protected $opinion_id;
    protected $attachment_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setOpinionId($opinion_id) {
        $this->opinion_id = $opinion_id;
        return $this;
    }
    public function setAttachmentId($attachment_id) {
        $this->attachment_id = $attachment_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getOpinionId() {
        return $this->opinion_id;
    }
    public function getAttachmentId() {
        return $this->attachment_id;
    }
    public function initialize() {
        $this->belongsTo('opinion_id', 'Opinions', 'id', array('foreignKey' => true));
        $this->belongsTo('attachment_id', 'AttachedPhotos', 'id', array('foreignKey' => true));
    }
    public function saveOpinionsAttachement($opinionId, $attachmentId, $transaction) {
        $this->setId(NULL);
        $this->setOpinionId($opinionId);
        $this->setAttachmentId($attachmentId);
        if (!$this->save()) {
            $transaction->rollback(FactoryDefault::getDefault()->get('t')->_("tags-not-save-attachment-fo-ky"));
        }
    }
}
