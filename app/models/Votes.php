<?php
use Phalcon\DI\FactoryDefault;
class Votes extends BaseModel {
    protected $id;
    protected $side_id;
    protected $voter_id;
    protected $created_at;
    protected $updated_at;
    protected $admin_at;
    protected $link;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setSideId($side_id) {
        $this->side_id = $side_id;
        return $this;
    }
    public function setVoterId($voter_id) {
        $this->voter_id = $voter_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function set_admin_at($app_id) {
        $this->admin_at = $app_id;
    }
    public function set_link($link) {
        $this->link = $link;
    }
    public function getId() {
        return $this->id;
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getVoterId() {
        return $this->voter_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function get_admin_at() {
        return $this->admin_at;
    }
    public function get_link() {
        return $this->link;
    }
    public function initialize() {
        $this->belongsTo('side_id', 'Sides', 'id', NULL);
        $this->belongsTo('voter_id', 'Users', 'id', NULL);
    }
    public function appendDatatoVotesModel($data, $delete = false) {
        $side = Sides::findFirst($data['side_id']);
        $voter = Users::findFirst($data['voter_id']);
        if ($side && $voter) {
            if ($delete) {
                $deleteQuery = array('side_id= :side_id: AND voter_id = :voter_id:', 'bind' => array('side_id' => $side->getId(), 'voter_id' => $voter->getId()));
                $unvote = $this->findFirst($deleteQuery);
                if ($unvote->delete()) {
                    return array('status' => 'OK', 'data' => array(true));
                } else {
                    return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
                }
            } else {
                $this->setSideId($side->getId());
                $this->setVoterId($voter->getId());
                if ($this->save()) {
                    return array('status' => 'OK', 'data' => array($this->getId()));
                } else {
                    return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
                }
            }
        } else {
            return array('status' => 'ERROR', 'messages' => FactoryDefault::getDefault()->get('t')->_("rejected"));
        }
    }
    public static function hasVoted($_sideId, $_userId) {
        return (0 < count(Votes::query()->where("side_id = :side_id:")->andWhere("voter_id = :voter_id:")->bind(array('side_id' => $_sideId, 'voter_id' => $_userId,))->execute()));
    }
    public static function unvote($_sideId, $_userId) {
        return Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("DELETE FROM Votes WHERE side_id = :side_id: AND voter_id = :voter_id:", array('side_id' => $_sideId, 'voter_id' => $_userId,))->success();
    }
    public static function vote($_sideId, $_userId, $link = "") {
        $hasVoted = self::hasVoted($_sideId, $_userId);
        if ($hasVoted) {
            return false;
        }
        $app_admin = AppAdmins::findFirst("user_id=$_userId");
        $objectVote = new Votes();
        $objectVote->setSideId($_sideId);
        $objectVote->setVoterId($_userId);
        $objectVote->set_link($link);
        if ($app_admin) {
            $objectVote->set_admin_at($app_admin->getAppId());
        } else {
            $objectVote->set_admin_at(0);
        }
        $objectVote->save();
        return $objectVote->getId();
    }
    public static function generateVotingStats($arrData) {
        $objData = new stdClass();
        $objData->intSumVotes = 0;
        $objData->sides = array();
        foreach ($arrData as $key => $value) {
            $objData->intSumVotes+= $value;
        }
        $counter = 0;
        foreach ($arrData as $key => $value) {
            $objData->sides[$key] = new stdClass();
            $objData->sides[$key]->percentage = (($objData->intSumVotes == 0) ? 0 : $value / $objData->intSumVotes * 100);
            $objData->sides[$key]->percentage.= '%';
            $objData->sides[$key]->votes = $value;
            if ($counter == 0) {
                $objData->proVotes = intval($value);
            } else {
                $objData->conVotes = intval($value);
            }
            $counter++;
        }
        return $objData;
    }
    public static function getSideVotes($_sideID) {
        $voteQuery = array('side_id = :side_id:', 'bind' => array('side_id' => $_sideID));
        return Votes::count($voteQuery);
    }
    public static function sumVotes($_sides) {
        $sum = 0;
        if (is_array($_sides)) {
            foreach ($_sides as $side) {
                $sum+= Votes::count(array('conditions' => 'side_id=:side_id:', 'bind' => array('side_id' => $side['id']),));
            }
        }
        return $sum;
    }
}
