<?php
class LastSeenCategories extends BaseModel {
    protected $id;
    protected $user_id;
    protected $last_seen_dates;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_user_id($user_id) {
        $this->user_id = $user_id;
    }
    public function set_last_seen_dates($json_dates) {
        $this->last_seen_dates = $json_dates;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_user_id() {
        return $this->user_id;
    }
    public function get_last_seen_dates() {
        return $this->last_seen_dates;
    }
}
