<?php
class RolesActivities extends \Phalcon\Mvc\Model {
    protected $id;
    protected $role_id;
    protected $activity_id;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setRoleId($role_id) {
        $this->role_id = $role_id;
        return $this;
    }
    public function setActivityId($activity_id) {
        $this->activity_id = $activity_id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getRoleId() {
        return $this->role_id;
    }
    public function getActivityId() {
        return $this->activity_id;
    }
    public function columnMap() {
        return array('id' => 'id', 'role_id' => 'role_id', 'activity_id' => 'activity_id');
    }
    public function initialize() {
        $this->belongsTo('role_id', 'Roles', 'id', array('foreignKey' => true));
        $this->belongsTo('activity_id', 'Activities', 'id', array('foreignKey' => true));
    }
    public static function isRoleIdAuthorizedToQueryURI($_roleId, $_activityId) {
        if (in_array($_roleId, array(1))) {
            return (Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT id FROM RolesActivities WHERE activity_id = :activity_id:", array('activity_id' => $_activityId))->count());
        }
        return (Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT id FROM RolesActivities WHERE role_id = :role_id: AND activity_id = :activity_id:", array('role_id' => $_roleId, 'activity_id' => $_activityId))->count());
    }
}
