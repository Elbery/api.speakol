<?php
class Activities extends BaseModel {
    protected $id;
    protected $action;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setAction($action) {
        $this->action = $action;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getAction() {
        return $this->action;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function columnMap() {
        return array('id' => 'id', 'action' => 'action', 'created_at' => 'created_at', 'updated_at' => 'updated_at');
    }
    public function initialize() {
        $this->hasMany('id', 'RolesActivities', 'activity_id', NULL);
    }
    public static function getActivityIdByAction($_action) {
        $rowActivity = Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery("SELECT id FROM Activities WHERE action = :action:", array('action' => $_action))->getFirst();
        if ($rowActivity != FALSE) {
            return $rowActivity->id;
        } else {
            return FALSE;
        }
    }
}
