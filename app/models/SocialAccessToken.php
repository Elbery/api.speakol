<?php
class SocialAccessToken extends BaseModel {
    protected $id;
    protected $user_id;
    protected $access_token;
    protected $login_date;
    protected $social_type;
    public function set_id($id) {
        $this->id = $id;
    }
    public function set_user_id($user_id) {
        $this->user_id = $user_id;
    }
    public function set_access_token($access_token) {
        $this->access_token = $access_token;
    }
    public function set_login_date($date) {
        $this->login_date = $date;
    }
    public function set_social_type($social_type) {
        $this->social_type = $social_type;
    }
    public function get_id() {
        return $this->id;
    }
    public function get_user_id() {
        return $this->user_id;
    }
    public function get_access_token() {
        return $this->access_token;
    }
    public function get_login_date() {
        return $this->login_date;
    }
    public function get_social_type() {
        return $this->social_type;
    }
}
