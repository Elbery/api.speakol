<?php
class AppAdmins extends BaseModel {
    protected $id;
    protected $app_id;
    protected $user_id;
    protected $super_admin;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    public function setSuperAdmin($super_admin) {
        $this->super_admin = $super_admin;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function getUserId() {
        return $this->user_id;
    }
    public function getSuperAdmin() {
        return $this->super_admin;
    }
    public function initialize() {
        $this->belongsTo('user_id', 'Users', 'id', NULL);
        $this->belongsTo('app_id', 'Apps', 'id', NULL);
    }
    public function createAppAdmin($_appId, $_userId, $_isSuperAdmin = false) {
        $this->setAppId($_appId);
        $this->setUserId($_userId);
        $this->setSuperAdmin($_isSuperAdmin);
        return $this->save();
    }
}
