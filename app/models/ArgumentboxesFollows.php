<?php
class ArgumentboxesFollows extends \Phalcon\Mvc\Model {
    protected $id;
    protected $argumentbox_id;
    protected $follower_id;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setArgumentboxId($argumentbox_id) {
        $this->argumentbox_id = $argumentbox_id;
        return $this;
    }
    public function setFollowerId($follower_id) {
        $this->follower_id = $follower_id;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getArgumentboxId() {
        return $this->argumentbox_id;
    }
    public function getFollowerId() {
        return $this->follower_id;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function columnMap() {
        return array('id' => 'id', 'argumentbox_id' => 'argumentbox_id', 'follower_id' => 'follower_id', 'created_at' => 'created_at', 'updated_at' => 'updated_at');
    }
    public static function followed($argboxId, $userId) {
        $followedQuery = array('argumentbox_id = :argumentbox_id: AND follower_id = :follower_id:', 'bind' => array('argumentbox_id' => $argboxId, 'follower_id' => $userId));
        return static ::count($followedQuery);
    }
}
