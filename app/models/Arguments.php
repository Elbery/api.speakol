<?php
use Phalcon\DI\FactoryDefault;
class Arguments extends BaseModel {
    protected $id;
    protected $owner_id;
    protected $context;
    protected $created_at;
    protected $updated_at;
    protected $link;
    protected $side_id;
    protected $module_id;
    protected $type;
    protected $app_id;
    protected $admin_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setOwnerId($owner_id) {
        $this->owner_id = $owner_id;
        return $this;
    }
    public function setContext($context) {
        $this->context = $context;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function setLink($link) {
        $this->link = $link;
        return $this;
    }
    public function setSideId($sideId) {
        $this->side_id = $sideId;
        return $this;
    }
    public function setModuleId($moduleId) {
        $this->module_id = $moduleId;
        return $this;
    }
    public function setType($type) {
        $this->type = $type;
        return $this;
    }
    public function setAppId($appId) {
        $this->app_id = $appId;
        return $this;
    }
    public function set_admin_at($app_id) {
        $this->admin_at = $app_id;
    }
    public function getId() {
        return $this->id;
    }
    public function getOwnerId() {
        return $this->owner_id;
    }
    public function getContext() {
        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        preg_match('#((?:http(s)?://)?(?:www\.)?(?:youtube\.com/(?:v/|watch\?v=)|youtu\.be/)([\w-]+)(?:\S+)?)#', $this->context, $match);
        if (isset($match[3])) {
            $embed = <<<YOUTUBE
        <div align="center">
            <iframe title="YouTube video player" width="100%" height="200" src="#ytzs#$match[3]?autoplay=0" frameborder="0" allowfullscreen></iframe>
        </div>
YOUTUBE;
            $this->context = str_replace($match[0], $embed, $this->context);
        }
        $this->context = preg_replace("/(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?/iu", '<a href="\0" target="blank">\0</a>', $this->context);
        $this->context = str_replace('"#ytzs#', 'http://www.youtube.com/embed/', $this->context);
        return htmlspecialchars($this->context);
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function getLink() {
        return $this->link;
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getModuleId() {
        return $this->module_id;
    }
    public function getType() {
        return $this->type;
    }
    public function getAppId() {
        return $this->app_id;
    }
    public function get_admin_at() {
        return $this->admin_at;
    }
    public function initialize() {
        $this->hasMany('id', 'ArgumentThumbs', 'argumnet_id', NULL);
        $this->hasMany('id', 'ArgumentPhotos', 'argument_id', NULL);
        $this->hasMany('id', 'Replies', 'argument_id', NULL);
        $this->belongsTo('side_id', 'Sides', 'id', NULL);
        $this->belongsTo('owner_id', 'Users', 'id', NULL);
        $this->belongsTo('app_id', 'Apps', 'id', NULL);
    }
    public function createActionHelper($_userId) {
        $intSideId = $this->getDi()->getShared('request')->getPost('side_id');
        $txtContext = $this->getDi()->getShared('request')->getPost('context');
        $txtLink = $this->getDi()->getShared('request')->getPost('link');
        $moduleId = $this->getDi()->getShared('request')->getPost('module_id');
        $moduleType = $this->getDi()->getShared('request')->getPost('type');
        $arrPhotos = $this->getDi()->getShared('request')->getUploadedFiles();
        $objImgUploader = $this->getDi()->getShared('Utility')->getImageUploader();
        $arrUploadedPhotos = $objImgUploader->upload($objImgUploader::DIR_ARGUMENTS, $arrPhotos);
        $this->setOwnerId($_userId);
        if (empty($txtContext)) {
            return FALSE;
        }
        $this->setContext($txtContext);
        $this->setLink($txtLink);
        $this->setModuleId($moduleId);
        $this->setType($moduleType);
        $this->setSideId($intSideId);
        if ($this->save() == FALSE) {
            return FALSE;
        }
        $modelSidesArguments = new SidesArguments();
        $modelSidesArguments->setArgumentId($this->getId());
        $modelSidesArguments->setSideId($intSideId);
        if ($modelSidesArguments->save() == FALSE) {
            return FALSE;
        }
        if ($arrUploadedPhotos) {
            foreach ($arrUploadedPhotos as $photo) {
                if ($photo['status'] == FALSE) {
                    continue;
                }
                $modelArgumentPhotos = new ArgumentPhotos();
                $modelArgumentPhotos->setArgumentId($this->getId());
                $modelArgumentPhotos->setUrl($photo['path']);
                $modelArgumentPhotos->setImage($photo['new']);
                if ($modelArgumentPhotos->save() == FALSE) {
                    return FALSE;
                }
            }
        }
        return $this->getId();
    }
    public function showActionHelper($_sideId, $order_by = "most_voted", $_pageNum, $_perPage) {
        $arrArgs = self::getSideArguments($_sideId, $order_by, $_pageNum, $_perPage);
        if ($arrArgs == FALSE) {
            throw new Exception($this->getDi()->get('t')->_('bad-request'), 400);
        }
        return $arrArgs;
    }
    private static function _getArgumentsByAppId($_sideId, $order_by = "most_voted", $_page = 1, $_limit = 2) {
        $_offset = ($_page == 1) ? 0 : ($_page - 1) * $_limit;
        return Arguments::sort_arguments($_sideId, $_offset, $_limit, $order_by);
    }
    public static function formatArgument($_argument) {
        $arrAttachments = array();
        foreach ($_argument->getArgumentPhotos() as $photo) {
            $arrAttachments[] = array('id' => $photo->getId(), 'image' => $photo->getImage(), 'url' => Phalcon\DI::getDefault()->getShared('Utility')->generateURLFromDirPath($photo->getUrl(), $photo->getImage()));
        }
        $perPage = Phalcon\DI::getDefault()->getShared('config')->application->commentsPagination;
        $repliesCount = Replies::countRepliesPerArgument($_argument->getId());
        $user = Users::findFirst($_argument->getOwnerId());
        $owner_id = $user->getId();
        $owner_name = $user->get_first_name() . " " . $user->get_last_name();
        $owner_photo = $user->getProfilePicture();
        if ($_argument->get_admin_at() != 0) {
            $app = Apps::findFirst($_argument->get_admin_at());
            if ($app) {
                $owner_name = $app->getName();
                $owner_photo = $app->getImage();
            }
        }
        $owner_slug = $user->getSlug();
        $date = strtotime($_argument->getCreatedAt());
        $date = date('F jS', $date);
        $argument_data = array('id' => $_argument->getId(), 'context' => $_argument->getContext(), 'attached_photos' => $arrAttachments, 'owner_id' => $owner_id, 'owner_name' => $owner_name, 'owner_status' => Beacon::user_status($owner_id), 'owner_slug' => $owner_slug, 'owner_photo' => $owner_photo, 'created_at' => $date, 'thumbs_up' => ArgumentThumbs::findThumbsUPByArgID($_argument->getId()), 'thumbs_down' => ArgumentThumbs::findThumbsDNByArgID($_argument->getId()), 'replies_count' => $repliesCount, 'currentUserhasVoted' => (FALSE != ArgumentThumbs::findThumbsByArgIDUserID($_argument->getId(), Tokens::getSignedInUserId())), 'currentUserVote' => (FALSE != ArgumentThumbs::findThumbByArgIDUserID($_argument->getId(), Tokens::getSignedInUserId())), 'admin_at' => $_argument->get_admin_at(), 'total_replies_pages' => intval($perPage ? $repliesCount / $perPage : $repliesCount / 2));
        if ($user->getRoleId() == Roles::APP || $user->getRoleId() == Roles::ADMIN) {
            $app = Apps::findFirst(array('columns' => 'website', 'publisher_id=' . $user->getId()));
            $argument_data['url'] = $app['website'];
        }
        return $argument_data;
    }
    public function deleteArgument($argument_id) {
        $signedInUserID = Tokens::getSignedInUserId();
        $arguments = self::findFirst(array('conditions' => 'id=:id: AND owner_id=:owner_id:', 'bind' => array('id' => $argument_id, 'owner_id' => $signedInUserID),));
        if (!$arguments) {
            $app = AppAdmins::findFirst(array('user_id = :user_id: AND super_admin=1', 'bind' => array('user_id' => $signedInUserID)));
            $isAppAdmin = $app ? $app->getAppId() : false;
            if ($isAppAdmin) {
                $arguments = self::findFirst(array('conditions' => 'id=:id: AND app_id=:app_id:', 'bind' => array('id' => $argument_id, 'app_id' => $isAppAdmin)));
            }
        }
        if ($arguments) {
            return $arguments->delete();
        } else {
            return false;
        }
    }
    public function voteActionHelper($_argumentId, $_userId, $vote, $link) {
        if (FALSE == ArgumentThumbs::findThumbsByArgIDUserID($_argumentId, $_userId)) {
            return ArgumentThumbs::createArgumentThumbs($_argumentId, $_userId, $vote, $link);
        } else {
            return ArgumentThumbs::updateArgumentThumbs($_argumentId, $_userId, $vote, $link);
        }
    }
    private function _getArgumentsBySideId($_sideId) {
        $arrArguments = array();
        foreach (SidesArguments::getArgumentsBySideId($_sideId) as $argument) {
            $arrArguments[] = Arguments::findFirst($argument->getArgumentId());
        }
        return $arrArguments;
    }
    public function hasVoted($_argumentId, $_userId, $_vote) {
        return (0 < ArgumentThumbs::getThumbByTrinary($_argumentId, $_userId, $_vote));
    }
    public function deleteSideArguments($sideId, $transaction) {
        $arguments = SidesArguments::find(array(" side_id = :side_id: ", 'bind' => array('side_id' => $sideId)));
        if ($arguments) {
            foreach ($arguments as $argument) {
                $arg_thumbs = ArgumentThumbs::find('argumnet_id=' . $argument->getId());
                if ($arg_thumbs) {
                    $arg_thumbs->delete();
                }
                $all_replies = Replies::find('argument_id=' . $argument->getId());
                foreach ($all_replies as $reply) {
                    $rep_thumbs = ReplyThumbs::find('reply_id=' . $reply->getId());
                    if ($rep_thumbs) {
                        $rep_thumbs->delete();
                    }
                }
                if ($all_replies) {
                    if (!$all_replies->delete()) {
                        $transaction->rollback(FactoryDefault::getDefault()->get('t')->_("reply-cant-delete"));
                    }
                }
            }
            if (!$arguments->delete()) {
                $transaction->rollback(FactoryDefault::getDefault()->get('t')->_("arg-cant-delete"));
            }
        }
    }
    public static function appendToModel($objData) {
        $arrUploadedPhotos = "";
        if (!empty($objData->files)) {
            $arrUploadedPhotos = self::uploadAttachedFiles($objData->files, $objData->audio);
            if ($arrUploadedPhotos[0]['status'] != 1) {
                return array('status' => 'ERROR', 'message' => $arrUploadedPhotos[0]['error']);
            }
        }
        $argumentID = self::createArgument($objData);
        if ($argumentID == "ERROR") {
            return array('status' => 'ERROR', 'message' => \Phalcon\DI::getDefault()->get('t')->_('arg-body'));
        }
        SidesArguments::saveArgumentSides($argumentID, $objData->sideID);
        if (!empty($arrUploadedPhotos)) {
            ArgumentPhotos::saveArgumentPhotos($argumentID, $arrUploadedPhotos);
        }
        return array('status' => 'OK', 'id' => $argumentID);
    }
    public static function uploadAttachedFiles($_arrFiles, $audio) {
        $objImgUploader = \Phalcon\DI::getDefault()->getShared('Utility')->getImageUploader();
        if (intval($audio) === 0) {
            $arrUploadedPhotos = $objImgUploader->upload($objImgUploader::DIR_ARGUMENTS, $_arrFiles);
        } else {
            $arrUploadedPhotos = $objImgUploader->upload($objImgUploader::DIR_ARGUMENTS, $_arrFiles, null, array(), true);
        }
        if ($arrUploadedPhotos == FALSE) {
            throw new \Exception($this->getDi()->get('t')->_('upload-file-bad-request'), 400);
        } else {
            return $arrUploadedPhotos;
        }
    }
    public static function createArgument($objData) {
        $argument = trim($objData->context);
        if (empty($argument) && empty($objData->files)) {
            return "ERROR";
        }
        $modelArgument = new Arguments();
        $modelArgument->setOwnerId($objData->signedInUserID);
        $modelArgument->setContext($objData->context);
        $modelArgument->setLink($objData->link);
        $modelArgument->setSideId($objData->sideID);
        $modelArgument->setModuleId($objData->moduleId ? $objData->moduleId : $objData->container_id);
        $modelArgument->setType($objData->type ? $objData->type : $objData->container_type);
        $moduleName = $modelArgument->getType() ? $modelArgument->getType() : $objData->container_type;
        $admin = AppAdmins::findFirst("user_id=" . $objData->signedInUserID);
        if ($admin) {
            $modelArgument->set_admin_at($admin->getAppId());
        } else {
            $modelArgument->set_admin_at(0);
        }
        $con = new Speakol\concreteSocialPluginFactory();
        $obj = $con->make($moduleName);
        if ($obj) {
            $parentId = $modelArgument->getModuleId();
            $obj = $obj->findFirst($parentId);
            $modelArgument->setAppId($obj->getAppId());
        }
        if ($modelArgument->save() == FALSE) {
            $strErrors = "";
            foreach ($modelArgument->getMessages() as $message) {
                $strErrors.= $message->getMessage();
                $strErrors.= ". ";
            }
            throw new \Exception(FactoryDefault::getDefault()->get('t')->_('conflict') . ":{$strErrors}", 409);
        } else {
            return $modelArgument->getId();
        }
    }
    public static function showSingleByArgID($_argumentID) {
        $objArgument = self::findFirst($_argumentID);
        if ($objArgument == FALSE) {
            throw new Exception($this->getDi()->get('t')->_('bad-request'), 400);
        }
        return self::formatSingleArg($objArgument);
    }
    public static function formatSingleArg($_objArg) {
        $user = Users::findFirst($_objArg->getOwnerId());
        $owner_id = $_objArg->getOwnerId();
        $owner_name = $user->get_first_name() . " " . $user->get_last_name();
        $owner_slug = $user->getSlug();
        $owner_photo = $user->getProfilePicture();
        if ($_objArg->get_admin_at() != 0) {
            $app = Apps::findFirst($_objArg->get_admin_at());
            if ($app) {
                $owner_name = $app->getName();
                $owner_photo = $app->getImage();
            }
        }
        $date = strtotime($_objArg->getCreatedAt());
        $date = date('F jS', $date);
        $argument_data = array('id' => $_objArg->getId(), 'owner_id' => $owner_id, 'owner_name' => $owner_name, 'owner_slug' => $owner_slug, 'owner_photo' => $owner_photo, 'owner_status' => Beacon::user_status($owner_id), 'created_at' => $date, 'context' => $_objArg->getContext(), 'attached_photos' => ArgumentPhotos::filterUrl(ArgumentPhotos::getArgumentPhotos($_objArg->getId())), 'replies_count' => Replies::countRepliesPerArgument($_objArg->getId()), 'thumbs_up' => ArgumentThumbs::getThumbs($_objArg->getId(), TRUE), 'thumbs_down' => ArgumentThumbs::getThumbs($_objArg->getId(), FALSE), 'currentUserhasVoted' => (FALSE != ArgumentThumbs::findThumbsByArgIDUserID($_objArg->getId(), Tokens::getSignedInUserId())), 'currentUserVote' => (FALSE != ArgumentThumbs::getThumbUpByUserIDArgID($_objArg->getId(), Tokens::getSignedInUserId())), 'admin_at' => $_objArg->get_admin_at(),);
        if ($user->getRoleId() == Roles::APP) {
            $app = Apps::findFirst(array('columns' => 'website', 'publisher_id=' . $user->getId()));
            $argument_data['url'] = $app['website'];
        }
        return $argument_data;
    }
    public static function doesExist($_argumentID) {
        if (self::count($_argumentID) == FALSE) {
            throw new Exception(Phalcon\DI::getDefault()->get('t')->_('not-found'), 404);
        }
        return true;
    }
    public static function getSideArguments($_sideId, $order_by = "most_voted", $_pageNum, $_perPage) {
        $arrArgs = array();
        $page = $_pageNum < 1 ? 1 : $_pageNum;
        $startValue = ($page - 1) * $_perPage;
        $count = static ::count(array('side_id = :side_id:', 'bind' => array('side_id' => $_sideId), "order" => "id desc",));
        $hasmore = (($count >= (($startValue + 1) + $_perPage)) ? true : FALSE);
        foreach (self::_getArgumentsByAppId($_sideId, $order_by, $_pageNum, $_perPage) as $argument) {
            $arrArgs[] = self::formatArgument($argument);
        }
        return array('args' => $arrArgs, 'hasmore' => $hasmore);
    }
    public static function countSideArguments($_sideId) {
        return static ::count(array('side_id = :side_id:', 'bind' => array('side_id' => $_sideId),));
    }
    public static function maxNPages($_sides, $_perPage = 2) {
        $max = 0;
        if (is_array($_sides)) {
            foreach ($_sides as $side) {
                $argsPages = intval(Arguments::countSideArguments($side['id']) / $_perPage);
                if ($argsPages > $max) {
                    $max = $argsPages;
                }
            }
        }
        return $max;
    }
    public static function row_to_argument($argument) {
        $temp_argument = new Arguments();
        $temp_argument->setId($argument['id']);
        $temp_argument->setOwnerId($argument['owner_id']);
        $temp_argument->setContext($argument['context']);
        $temp_argument->setSideId($argument['side_id']);
        $temp_argument->setType($argument['type']);
        $temp_argument->setModuleId($argument['module_id']);
        $temp_argument->setAppId($argument['app_id']);
        $temp_argument->setLink($argument['link']);
        $temp_argument->setCreatedAt($argument['created_at']);
        $temp_argument->setUpdatedAt($argument['updated_at']);
        $temp_argument->set_admin_at($argument['admin_at']);
        return $temp_argument;
    }
    public static function sort_arguments($side_id, $offset, $limit = 2, $order_by = "most_voted") {
        if ($order_by == "most_voted") {
            $most_voted_query = "SELECT a.id,a.owner_id,a.context,a.side_id,a.type,a.module_id,a.app_id,a.admin_at," . "a.created_at,a.updated_at,a.link  FROM ArgumentThumbs RIGHT " . "JOIN arguments a ON a.id = argumnet_id WHERE a.side_id =" . $side_id . " group by a.id order by count(argumnet_id) desc limit " . $limit . " offset " . $offset;
            $arguments = Phalcon\DI::getDefault()->getShared('modelsManager')->executeQuery($most_voted_query)->toArray();
            for ($i = 0;$i < sizeof($arguments);$i++) $arguments[$i] = Arguments::row_to_argument($arguments[$i]);
        } else {
            $order_by = "created_at " . $order_by;
            $arguments = static ::find(array('side_id = :side_id:', 'bind' => array('side_id' => $side_id), 'order' => $order_by, 'limit' => array('number' => $limit, 'offset' => $offset)));
            foreach ($arguments as $arg) {
                $arg->setContext(htmlspecialchars($arg->getContext()));
            }
        }
        return $arguments;
    }
}
