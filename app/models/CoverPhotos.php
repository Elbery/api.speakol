<?php
class CoverPhotos extends BaseModel {
    protected $id;
    protected $image;
    protected $uploader_id;
    protected $uploader_type;
    protected $created_at;
    protected $updated_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    public function setUploaderId($uploader_id) {
        $this->uploader_id = $uploader_id;
        return $this;
    }
    public function setUploaderType($uploader_type) {
        $this->uploader_type = $uploader_type;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getImage() {
        return $this->image;
    }
    public function getUploaderId() {
        return $this->uploader_id;
    }
    public function getUploaderType() {
        return $this->uploader_type;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    public function initialize() {
        $this->belongsTo('uploader_id', 'Users', 'id', NULL);
    }
}
