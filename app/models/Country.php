<?php
class Country extends BaseModel {
    protected $id;
    protected $name;
    protected $iso_code_2;
    protected $iso_code_3;
    protected $address_format;
    protected $postcode_required;
    protected $status;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function setIsoCode2($iso_code_2) {
        $this->iso_code_2 = $iso_code_2;
        return $this;
    }
    public function setIsoCode3($iso_code_3) {
        $this->iso_code_3 = $iso_code_3;
        return $this;
    }
    public function setAddressFormat($address_format) {
        $this->address_format = $address_format;
        return $this;
    }
    public function setPostcodeRequired($postcode_required) {
        $this->postcode_required = $postcode_required;
        return $this;
    }
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getIsoCode2() {
        return $this->iso_code_2;
    }
    public function getIsoCode3() {
        return $this->iso_code_3;
    }
    public function getAddressFormat() {
        return $this->address_format;
    }
    public function getPostcodeRequired() {
        return $this->postcode_required;
    }
    public function getStatus() {
        return $this->status;
    }
    public function initialize() {
        $this->hasMany('id', 'Users', 'country_id', NULL);
    }
}
