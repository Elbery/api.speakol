<?php
class DebateChallengers extends BaseModel {
    protected $id;
    protected $debate_id;
    protected $side_id;
    protected $debate_slug;
    protected $challenger_id;
    protected $challenger_opinion;
    protected $created_at;
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setDebateId($debate_id) {
        $this->debate_id = $debate_id;
        return $this;
    }
    public function setSideId($side_id) {
        $this->side_id = $side_id;
        return $this;
    }
    public function setDebateSlug($debate_slug) {
        $this->debate_slug = $debate_slug;
        return $this;
    }
    public function setChallengerId($challenger_id) {
        $this->challenger_id = $challenger_id;
        return $this;
    }
    public function setChallengerOpinion($challenger_opinion) {
        $this->challenger_opinion = $challenger_opinion;
        return $this;
    }
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    public function getDebateId() {
        return $this->debate_id;
    }
    public function getSideId() {
        return $this->side_id;
    }
    public function getDebateSlug() {
        return $this->debate_slug;
    }
    public function getChallengerId() {
        return $this->challenger_id;
    }
    public function getChallengerOpinion() {
        return $this->challenger_opinion;
    }
    public function getCreatedAt() {
        return $this->created_at;
    }
    public function initialize() {
        $this->belongsTo('debate_id', 'Debates', 'id', NULL);
        $this->belongsTo('challenger_id', 'Users', 'id', NULL);
    }
    public function getChallengers($params) {
        if (isset($params['user_id'])) {
            $data = $this->findFirst(array('conditions' => "debate_slug=:slug: AND challenger_id=:user_id:", 'bind' => array('slug' => $params['debate_slug'], 'user_id' => $params['user_id'])));
        } else {
            $challengers = $this->find(array('conditions' => "debate_slug=:slug:", 'bind' => array('slug' => $params['debate_slug']), 'order' => 'id desc'));
            $data = array();
            if ($challengers->valid()) {
                foreach ($challengers as $challenger) {
                    $main = array();
                    $main['debate'] = $challenger->getDebates()->toArray();
                    $main['challenger'] = $challenger->getUsers(array('columns' => array('id', 'slug', 'name', 'job_title', 'profile_picture')))->toArray();
                    $main['challenger']['challenge_id'] = $challenger->getId();
                    $main['challenger']['opinion'] = $challenger->getChallengerOpinion();
                    $data[] = $main;
                }
            }
        }
        return $data;
    }
    public function addChalleners($params) {
        $isChallenged = $this->getChallengers($params);
        if ($isChallenged == false) {
            $this->setChallengerId($params['user_id']);
            $this->setChallengerOpinion($params['challenger_opinion']);
            $this->setDebateId($params['debate_id']);
            $this->setSideId($params['side_id']);
            $this->setDebateSlug($params['debate_slug']);
            if ($this->save()) {
                return Debates::findFirst($params['debate_id'])->getUsers(array('columns' => array('id', 'email', 'slug', 'name', 'profile_picture')))->toArray();
            }
        } else {
            $isChallenged->setChallengerOpinion($params['challenger_opinion']);
            return $isChallenged->update();
        }
        return false;
    }
    public function approveChallenge($params) {
        $challenge_id = $params['challenge_id'];
        $challenge = self::findFirst($challenge_id);
        if ($challenge == false) {
            return false;
        }
        $user = $challenge->getUsers(array('columns' => array('id', 'email', 'slug', 'name', 'job_title', 'profile_picture')));
        if ($challenge != false) {
            $debateSidesMeta = DebateSidesMeta::findFirst(array('conditions' => 'side_id=:side_id:', 'bind' => array('side_id' => $challenge->getSideId())));
            $debateSidesMeta->setDebaterId($challenge->getChallengerId());
            $debateSidesMeta->setImage($user->profile_picture);
            $debateSidesMeta->setJobTitle($user->job_title);
            $debateSidesMeta->setTitle($user->name);
            $debateSidesMeta->setOpinion($challenge->getChallengerOpinion());
            if ($debateSidesMeta->update()) {
                $debateData = $debate = Debates::findFirst($challenge->debate_id);
                $debate->setStatus(Debates::STATUS_ACTIVE);
                $debate->setStartedAt(date('Y-m-d H:i:s'));
                if ($debate->getMaxDays() == - 1) {
                    $debate->setEndsAt('');
                } else {
                    $debate->setEndsAt(date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' + ' . $debate->getMaxDays() . ' day')));
                }
                if ($debate->save()) {
                    $challenges = $this->find(array('conditions' => "debate_slug=:slug:", 'bind' => array('slug' => $challenge->debate_slug)));
                    $challenges->delete();
                }
            }
            return array('debate' => $debateData->toArray(), 'challenger' => $user);
        }
        return false;
    }
    public function rejectChallenge($params) {
        $challenge = self::findFirst($params['challenge_id']);
        if ($challenge->delete()) {
            return true;
        }
        return false;
    }
}
