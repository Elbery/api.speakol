<?php
$messages = array('mail-title-verifiy' => 'Speakol | Verifiy your Account', 'mail-title-welcome' => 'Speakol | Welcome to Speakol', 'mail-title-actived' => 'Speakol | Your account has been successfully activated', 'mail-title-reset-password' => 'Speakol | Reset your account password', '' => '', '' => '', '' => '', 'speakol' => 'Speakol', 'error-went-wrong' => 'Something went wrong, please try again later.', 'ok' => 'OK', 'not-found' => 'Not Found', 'bad-request' => 'Bad Request', 'not-found' => 'Not Found', 'found' => 'Found', 'unauthorized' => 'Unauthorized', 'not-following' => 'Not Following', 'conflict' => 'Conflict', 'not-reported' => 'Not Reported', 'missing-arguments' => 'Url of argument box is missing.', 'missing-data' => 'some data is missing.', 'no-com-found' => 'No comparison found.', 'some-thing-went-wrong' => 'something went wrong.', 'rejected' => 'Rejected', 'year' => 'year', 'two-year' => '2 years', 'years' => 'years', 'month' => 'month', 'two-month' => '2 months', 'months' => 'months', 'day' => 'day', 'two-day' => '2 days', 'days' => 'days', 'hour' => 'hour', 'two-hour' => '2 hours', 'hours' => 'hours', 'minute' => 'minute', 'two-minute' => '2 minutes', 'minutes' => 'minutes', 'second' => 'second', 'two-second' => '2 seconds', 'seconds' => 'seconds', 'ago' => 'ago', 'user-account-verification' => 'You can\'t login because your account need to be verified!.', 'user-password-missing' => 'Password is missing!', 'user-email-not-match' => "Email doesn't match", 'user-country-missing' => "Country is missing", 'user-job-missing' => "Job title is missing", 'user-dob-missing' => 'Date of birth is missing', 'user-no-permission' => "You don't have permission to create user with this role", 'user-old-pass-not-correct' => "Your old password is incorrect!", 'user-password-not-match' => "Password doesn't match", 'user-signup-title' => 'You have registered for Speakol', '' => '', '' => '', 'upload-file-exceed-the-max' => "The uploaded file exceeds the upload_max_filesize directive in php.ini.", 'upload-file-exceed-the-file-size' => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.", 'upload-file-only-part' => "The uploaded file was only partially uploaded.", 'upload-file-no-file' => "No file was uploaded.", 'upload-file-missing-tempory-folder' => "Missing a temporary folder.", 'upload-file-failed-to-write' => "Failed to write file to disk.", 'upload-file-stopped-by-extension' => "File upload stopped by extension.", 'upload-file-not-allowed-file' => "File uploaded is not an allowed image file.", 'upload-file-bad-request' => "Bad Request: File uploads", 'upload-cant-delete-photo' => "Can't delete attachment photo!", 'upload-cant-delete-attachment' => "Can't delete attachment", 'arg-not-reported' => "Found: You have not reported this argument", 'arg-reported-already' => "Found: You have already reported this argument", 'arg-cant-delete' => "Can't delete arguments!", 'apps-email-exist' => 'Email already exists', 'apps-email-not-match' => "Email doesn't match", 'apps-pass-not-match' => 'Password doesn\'t match', 'apps-save-publisher-data' => 'can not save Publisher data', 'apps-save-app-data' => 'can not save APP data', 'apps-save-app-admin' => 'Can not save app admin', 'apps-general-error' => 'Something went wrong. Please try again later', 'apps-name-missing' => 'Application name is required!', 'apps-cat-missing' => 'Application category is required!', 'apps-website-missing' => 'Application website is required!', 'apps-old-pass-not-correct' => "Old password isn't correct!", 'apps-pass-not-much' => "Passwords don't match!", 'apps-not-found' => 'App not found!', 'apps-' => "", 'apps-' => "", 'apps-' => "", '' => '', '' => '', '' => '', '' => '', '' => '', '' => '', '' => '', 'reply-not-reported' => "Found: You have not reported this reply", 'reply-reported-already' => "Found: You have already reported this reply", 'reply-cant-delete' => "Can't delete arguments!", 'reply-emty-comments' => "Conflict:Empty Comments are not allowed", 'reply-upload-photo' => "Conflict:Upload Photos", 'reply-' => '', 'reply-' => '', 'reply-' => '', 'reply-' => '', 'reply-' => '', 'debate-user-not-voted' => "Bad Request: User did not vote for any side", 'debate-slug-missing' => 'Slug is required.', 'debate-slug-unique' => 'Slug should be unique.', 'debate-cat-required' => 'Category is required.', 'debate-' => '', 'debate-' => '', 'debate-' => '', 'debate-' => '', 'debate-' => '', 'comparison-user-not-voted' => "Bad Request: User did not vote for any side", 'comparison-two-items' => "Two items at minimum should be provided.", 'comparison-four-items' => "Four items only as maximum per comparison", 'comparison-cat-required' => 'Category is required.', 'comparison-' => '', 'argbox-user-not-voted' => "Bad Request: User did not vote for any side", 'argbox-url-invalid' => 'Url isn\'t valid!', 'argbox-cant-update' => "Can't update", 'tags-name-missing' => 'Name is missing!', 'tags-not-exist' => 'Tag not exist!', 'tags-not-save' => "Can't save Tag!", 'tags-not-save-attachment-fo-ky' => "Can't save attachment foreign key!", 'hi' => 'Hi', 'account-created' => 'Thank you for creating an account with Speakol, enjoy the best debate experience around your favourite content. We want to help you to Speak your opinion, Out Loud.
<br/><br/>
We kindly request you to be courteous to fellow debaters, and refrain from using profanity within your contributions. We want to build a constructive and awesome debate community!
<br/><br/>
Think. Speak. Debate The Speakol Team', 'app-activated' => 'Welcome to Speakol.
<br/>
<br/>
You can now embed any Speakol plugins across your site. Follow these steps:
<br/>
<br/>
1) <a href="http://plugins.speakol.com/apps/login">Login</a> to your account <br/>
2) Choose to set up a debate, comparison or argument related to your article/content. Insert all relevant content correctly.<br/>
3) Copy the generated HTML code in the following screen<br/>
4) Finally, paste the code below your article body (or wherever your CMS allows you to - maybe there is a separate footer for additional content).<br/>
<br/>
You’re then on your way to building a debate community within your site\'s content. Please note that our plugins will work alongside your existing commenting systems.
<br/>
<br/>
If you have any questions, technical issues or even if you just need some tips to help get you started - you can always contact us via Email (ask@speakol.com), or through our Twitter handle (<a href="http://www.twitter.com/speakol_">@speakol_</a>)
<br/>
<br/>
We will shortly be launching our blog, to be updated regularly to help you get more community building info, news and best-practice advice.
<br/>
<br/>
In the meantime, feel free to write us anytime and on anything you would like to discuss or if you have feedback, we’re here to help you.
<br/>
<br/>
Thank you for thinking of us - and let’s build a great debate community together.
<br/>
<br/>
The Speakol Team<br/>
www.speakol.com<br/>', 'sign-up-content' => 'Please click the below link to complete your registration process', 'sign-up-link' => 'Click here to activate your account', 'app-sign-up-content' => 'Please click the below link to complete your registration process', 'app-sign-up-link' => 'Click here to activate your account', 'app-signup-title' => 'You have registered for Speakol', 'welcome-body' => 'Thank you for creating an account with Speakol, enjoy the best debate experience around your favourite content. We want to help you to Speak your opinion, Out Loud.
<br/><br/>
We kindly request you to be courteous to fellow debaters, and refrain from using profanity within your contributions. We want to build a constructive and awesome debate community!
<br/><br/>
Think. Speak. Debate The Speakol Team', 'mail-title-password-reset' => 'Your Password has been reset', 'clk-here-rst-ur-passwd' => 'Click here to reset your password', 'frgt-passwd-content' => 'You have requested to reset your password. Please follow this link to do this:', 'frgt-passwd-content-2' => 'If you didn\'t ask for a reset, please ignore this email.', 'psswrd-reset' => 'You have successfully reset your password. Please store this in a safe place.
        <br/>
        <br/>
        If you would like to contact us about any security issues, please send an email to <a href="mailto:ask@speakol.com">ask@speakol.com<a/>
        <br/>
        <br/>
        Happy Debating!
        <br/>
        The Speakol Team
        ', 'speakol' => 'Speakol', 'mail_footer' => 'In the future, please Go to your account setting in Speakol website and change the notification settings', 'unsubscribe' => 'If you don\'t want to receive these emails from',);
