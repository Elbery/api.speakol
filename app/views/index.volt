<!nliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="margin: 0; padding: 0;">
    <head>
        <!-- If you delete this meta tag, Half Life 3 will never be released. -->
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>{{ title }}</title>
    </head>
    <body bgcolor="#f7f7f7" style="-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; font-family: 'Source Sans Pro'; font-weight: normal; margin: 0; padding: 0;"><style type="text/css">
            a:hover {
                text-decoration: underline;
            }
            .btn:hover {
                opacity: 1;
            }
            @media only screen and (max-width: 600px) {
                a[class="btn"] {
                    display: block !important; margin-bottom: 10px !important; background-image: none !important; margin-right: 0 !important;
                }
                div[class="column"] {
                    width: auto !important; float: none !important;
                }
                table.social div[class="column"] {
                    width: auto !important;
                }
            }
        </style>

        <!-- HEADER -->
        {{ this.getContent() }}

        <!-- FOOTER --><table class="footer-wrap" style="width: 100%; clear: both !important; margin: 0; padding: 0;"><tr style="margin: 0; padding: 0;"><td style="margin: 0; padding: 0;"></td>
                <td class="container" style="display: block !important; max-width: 600px !important; clear: both !important; border-radius: 10px; margin: 0 auto; padding: 0;">

                    <!-- content -->
                    <div class="content" style="max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                        <table style="color:#888888;width: 100%; margin: 0; padding: 0;">
<tr style="display: table; width: 100%; margin: 0 0 10px; padding: 0;"><td align="center" style="margin: 0; padding: 0;">
                                    {% if logo!="top" %}
                                    <a href="#" style="color: #5bb767; text-decoration: none; margin: 0; padding: 0;"><img src="{{ config.application.webservice }}/img/Speakol_footer.gif" alt="" style="max-width: 100%; margin: 0; padding: 0;" /></a>
                                    {% endif %}
                                </td>
                            </tr><tr style="display: table; width: 100%; margin: 0 0 0px; padding: 0;"><td align="center" style="margin: 0; padding: 0;">
                                    <p class="small_msg" style="color: #888888; font-size: 12px; font-weight: normal; line-height: 1.6; margin: 0 0 0px; padding: 0;">
                                      Copyright © 2015 - All rights reserved, Speakol.
                                    </p>
                                </td>
                            </tr>
                            <tr style="display: table; width: 100%; margin: 0 0 10px; padding: 0;"><td align="center" style="margin: 0; padding: 0;">
                                    <p class="small_msg" style="color: #888888; font-size: 12px; font-weight: normal; line-height: 1.6; margin: 0 0 10px; padding: 0;">
                                   <b>  <a style="color:#888888;text-decoration: none;" href="mailto:ask@speakol.com">ask@speakol.com</a> | <a style="color:#888888;text-decoration: none;" href="{{update}}">Notification Settings</a></b>
                                    </p>
                                </td>
                            </tr>
                           </table></div><!-- /content -->
                </td>
                <td style="margin: 0; padding: 0;"></td>
            </tr></table><!-- /FOOTER -->

    </body>
</html>
