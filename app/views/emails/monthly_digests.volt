<table class="body-wrap" style="width: 100%; margin: 0; padding: 0;font-size:13px;color:black;">
    <tr>
        <td></td>
        <td> 
           <a href="#" style="color: #5bb767; text-decoration: none; margin: 0; padding: 0;">
             <img src="{{ config.application.webservice }}/img/Speakol_footer.gif" alt="" style="max-width: 100%; margin-left: 17%; padding: 0;" />
           </a>
        </td>
    </tr>
    <tr style="margin: 0; padding: 0;">
        <td style="margin: 0; padding: 0;"></td>
        <td class="container" bgcolor="#FFFFFF" style="display: block !important; max-width: 600px !important; clear: both !important; border-radius: 10px; margin: 0 auto; padding: 0;">
            <div class="content" style="max-width: 600px; display: block; margin: 0 auto; padding: 5%;padding-top:0%;padding-bottom:0%;">
                <table style="width: 100%; margin: 0; padding: 0;">
                    <tr style="display: table; width: 100%; margin: 0 0 10px; padding: 0;">
                         <td class="va_t" style="vertical-align: top; margin: 0; padding: 0;" valign="top">
                              <td style="margin: 0; padding: 0;color:#666666;font-size:14px;">
                                  {% if lang == "ar" %}
                                  <div style="direction:rtl;border-radius:5px;border:1px solid #5bb767;padding:0px;min-height:100%;width:98%">
                                      <div style="color:#5bb767;text-align:center;border-bottom:1px solid #5bb767;height:100px;line-height:100px;font-size:24px;">
                                          <b>{{t._('monthly_digest')}}</b>
                                      </div>
                                      <div>
				          <table style="direction:rtl;color:#666666;margin-left:5%;margin-right:5%;">
                                              <tr style="direction:rtl;"><td> <p style="color:#444444;font-weight:normal;font-size:16px;line-height:1.6;margin-top:7%;margin-bottom:3%;padding:0">
                                                  <b>{{ t._('hello') }} <span style="margin:0;padding:0"></span>{{name}}</b>, </p>
                                              </td></tr>
                                              <tr style="direction:rtl;"><td style="padding:5px 0px 10px 0px">
                                                  {{t._('monthly_digest-1')}}!
                                              </td></tr>
                                              <tr style="direction:rtl;"><td style="padding:5px 0px 10px 0px">
                                                  {{t._('monthly_digest-2')}}.
                                              </td></tr>
                                              <tr style="direction:rtl;"><td style="padding:5px 0px 10px 0px;font-size:20px;">
                                                  {{t._('monthly_digest-3')}}:
                                              </td></tr>
                                              {% set classes = [ '2' : 'width:50%', '3' : 'width:33.3333333333%', '4': 'width:25%']%}
                                              {% for plugin in plugins %}
                                                   <tr style="direction:rtl;"><td style="padding:5px 0px 10px 0px">
						       {% if plugin['type'] == "comparison" %}
						             <div class="sp-block-group feed-item radius-5">
                                                                 <div class="sp-block-group">
                                                                     <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                         <img src="{{ config.application.webservice }}/img/comparison.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                     </h2>
                                                                 </div>
								 <div style="direction:rtl;margin-bottom:15px !important;margin-top:15px !important;display:table;line-height0;box-sizing:border-box;clear:both;">
                                              		             {% if plugin['data']['align'] %}
                                                                     {% for side in plugin['sides'] %}
								         <div style="margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;{{ loop.length < 5? classes[loop.length] : '' }};box-sizing:border-box;">
									    <div style="padding:10px;border-radius:5px;margin-left:auto;margin-right:auto;box-sizing:border-box;list-style-type:none;;{{ loop.length < 5 and plugin.data.align ? 'width:90%' : '' }}">
									        <div style="margin-top:0;overflow:hidden;zoom:1;">
										    {% if side['side_data']['image'] !="" %}
										    <div style="margin-right:0px;float:none;text-align:center;box-sizing:border-box;zoom:1;list-style-type:none;">
										        <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:96px;min-height:96px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                     								    </div>
										    {% endif %}
       										    <div style="overflow:hidden;zoom:1;box-sizing:border-box;">
                                                                 		        <div style="margin-buttom:5px;margin-top:15px;text-align:center;zoom:1;">
                                                                                            <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;text-align:center;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                        </div>
                                                                                        <div style="width:91.66666666667%;position:relative;box-sizing:border-box;zoom:1;font-size:1.5em;font-weight:normal;line-height:1.5;color:#404040;margin-left:auto;margin-right:auto;">
                                                                                            <img src="{{side['vote_img']}}"/>
                                                                                        </div>
                                                                                    </div>
										</div>
									    </div>
                                                                         </div>
								     {% endfor %}
                                                                     {% else %}
                                                                         {% for side in plugin['sides'] %}
                                                                             <div style="direction:rtl;margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;width:100%;box-sizing:border-box;">
                                                                                 <div style="padding:10px;border-radius:5px;margin-left:auto;margin-right:auto;box-sizing:border-box;list-style-type:none;">
                                                                                     <div style="margin-top:0;overflow:hidden;zoom:1;">
											{% if side['side_data']['image'] !="" %}
                                                                                         <div style="margin-right:10px;float:left!important;box-sizing:border-box;zoom:1;list-style-type:none;">
                                                                                             <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:96px;min-height:96px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                                                         </div>
											 {% endif %}
                                                                                         <div style="overflow:hidden;zoom:1;box-sizing:border-box;">
                                                                                             <div style="margin-bottom:5px;text-align:left;zoom:1;">
                                                                                                 <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;text-align:left;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                             </div>
                                                                                             <div style="width:91.66666666667%;position:relative;box-sizing:border-box;zoom:1;font-size:1.5em;font-weight:normal;line-height:1.5;color:#404040;">
                                                                                                 <img src="{{side['vote_img']}}"/>
                                                                                             </div>
                                                                                         </div>
                                                                                     </div>
                                                                                 </div>
                                                                             </div>
                                                                         {% endfor %}
                                                                     {% endif %}
							         </div>
                                                             </div>
                                                             <div style="text-align:center;">
                                                                 <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;"><a href="http://www.speakol.com/newsfeed/comparison?slug={{ plugin['data']['slug'] }}&app={{plugin['data']['app_id']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a></button>
							     </div>
						       {% elseif plugin['type'] =="debate" %}
						           <div class="sp-block-group feed-item radius-5">
                                                               <div class="sp-block-group">
                                                                   <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                       <img src="{{ config.application.webservice }}/img/debate.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                   </h2>
                                                               </div>
                                                               <div style="direction:rtl;margin-bottom:15px !important;margin-top:15px !important;display:table;line-height0;box-sizing:border-box;clear:both;">
                                                                   {% for side in plugin['sides'] %}
                                                                       {% set right = false %}
                                                                       {% if loop.last %}
                                                                           {% set right = true %}
                                                                       {% endif %}
                                                                        <div style="margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;width:50%;box-sizing:border-box;">
									    <div style="">
									        <div style="margin-top:0;overflow:hidden;zoom:1;">
										      <div style="margin-right:0px;float:{{right?'right' : 'left'}};text-align:center;box-sizing:border-box;zoom:1;list-style-type:none;">
										          <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:70px;height:70px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                                                      </div>
										      <div style="margin-buttom:5px;text-align:{{right ? 'right':'left'}};zoom:1;">
                                                                                          <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                      </div>
 										      <div style="margin-buttom:5px;text-align:{{right ? 'right':'left'}};zoom:1;">
                                                                        	          <h4 style="color:#676767;font-size:12px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;zoom:1;">{{side['side_data']['job_title']}}</h4>
                                                                 		      </div>

                                                                                </div>
									    </div>
									</div>
 							           {% endfor %}
                                                               </div>
                                                               <div style="width:100%">
                                                                   <img src="{{plugin['img']}}" style="max-width:100%;height:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                               </div>
                                                               <div style="text-align:center;">
                                                                   <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;">
                                                                       <a href="http://www.speakol.com/newsfeed/debate?slug={{ plugin['data']['slug'] }}&app={{plugin['data']['app_id']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                                   </button>
							       </div>
                                                       {% else %}
                                                           <div class="sp-block-group feed-item radius-5">
                                                               <div class="sp-block-group">
                                                                   <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                       <img src="{{ config.application.webservice }}/img/argumentbox.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                   </h2>
                                                               </div>
                                                               <div style="width:100%">
                                                                   <img src="{{plugin['img']}}" style="max-width:100%;height:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                               </div>
                                                               <div style="text-align:center;">
                                                                   <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;">
                                                                       <a href="http://www.speakol.com/newsfeed/argumentsbox?url={{ plugin['data']['url'] }}&app={{plugin['data']['app_id']}}&uuid={{ plugin['data']['code'] }}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                                   </button>
                                                               </div>
                                                           </div>
                                                       {% endif %}
                                                   </td></tr>
                                                   <tr><td colspan="2"><hr style="border-color:#ebebed;border-bottom:0px;"></td></tr>
                                              {% endfor %}
                                              <tr><td style="padding:5px 0px 10px 0px">{{t._('monthly_digest-4')}}?</td></tr>
                                              <tr><td style="padding:5px 0px 10px 0px">{{t._('monthly_digest-5')}}:</td></tr>
                                              {% for topic in hot_topics %}
                                                  <tr><td style="padding:5px 0px 10px 0px">
                                                      <table style="direction:rtl;width:100%;">
                                                          <tr><td rowspan="2" style="width:26%;"> 
                                                              <img src="http:{{topic['image']}}" style="width:98%;height:90%;border-radius:5px;">
                                                          </td>
                                                          <td style="width:73%;font-size:18px;color:#3e88c2">
      						              {{topic['title']}}
                                                          </td></tr>
                                                          <tr><td> 
                                                              {{topic['description']}}
                                                          </td></tr>
                                                          <tr><td></td>
                                                          <td> <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:53%;border:0px;font-size:16px;">
                                                              <a href="{{topic['link']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                          </button></td>
                                                          </tr>
                                                          <tr><td colspan="2"><hr style="border-color:#ebebed;border-bottom:0px;"></td></tr>
                                                      </table> 
                                                  </td></tr>
                                              {% endfor %}
                                              <tr><td style="padding:5px 0px 10px 0px">{{t._('monthly_digest-6')}}:</td></tr>
                                              {% set classes = [ '2' : 'width:50%', '3' : 'width:33.3333333333%', '4': 'width:25%']%}
                                              {% for plugin in high_communities %}
                                                   <tr><td style="padding:5px 0px 10px 0px">
						       {% if plugin['type'] == "comparison" %}
						             <div class="sp-block-group feed-item radius-5">
                                                                 <div class="sp-block-group">
                                                                     <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                         <img src="{{ config.application.webservice }}/img/comparison.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                     </h2>
                                                                 </div>
								 <div style="margin-bottom:15px !important;margin-top:15px !important;display:table;line-height0;box-sizing:border-box;clear:both;">
                                              		             {% if plugin['data']['align'] %}
                                                                     {% for side in plugin['sides'] %}
								         <div style="margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;{{ loop.length < 5? classes[loop.length] : '' }};box-sizing:border-box;">
									    <div style="padding:10px;border-radius:5px;margin-left:auto;margin-right:auto;box-sizing:border-box;list-style-type:none;;{{ loop.length < 5 and plugin.data.align ? 'width:90%' : '' }}">
									        <div style="margin-top:0;overflow:hidden;zoom:1;">
										    {% if side['side_data']['image'] !="" %}
										    <div style="margin-right:0px;float:none;text-align:center;box-sizing:border-box;zoom:1;list-style-type:none;">
										        <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:96px;min-height:96px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                     								    </div>
										    {% endif %}
       										    <div style="overflow:hidden;zoom:1;box-sizing:border-box;">
                                                                 		        <div style="margin-buttom:5px;margin-top:15px;text-align:center;zoom:1;">
                                                                                            <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;text-align:center;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                        </div>
                                                                                        <div style="width:91.66666666667%;position:relative;box-sizing:border-box;zoom:1;font-size:1.5em;font-weight:normal;line-height:1.5;color:#404040;margin-left:auto;margin-right:auto;">
                                                                                            <img src="{{side['vote_img']}}"/>
                                                                                        </div>
                                                                                    </div>
										</div>
									    </div>
                                                                         </div>
								     {% endfor %}
                                                                     {% else %}
                                                                         {% for side in plugin['sides'] %}
                                                                             <div style="margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;width:100%;box-sizing:border-box;">
                                                                                 <div style="padding:10px;border-radius:5px;margin-left:auto;margin-right:auto;box-sizing:border-box;list-style-type:none;">
                                                                                     <div style="margin-top:0;overflow:hidden;zoom:1;">
											 {% if side['side_data']['image'] !="" %}
                                                                                         <div style="margin-right:10px;float:left!important;box-sizing:border-box;zoom:1;list-style-type:none;">
                                                                                             <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:96px;min-height:96px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                                                         </div>
											 {% endif %}
                                                                                         <div style="overflow:hidden;zoom:1;box-sizing:border-box;">
                                                                                             <div style="margin-bottom:5px;text-align:left;zoom:1;">
                                                                                                 <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;text-align:left;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                             </div>
                                                                                             <div style="width:91.66666666667%;position:relative;box-sizing:border-box;zoom:1;font-size:1.5em;font-weight:normal;line-height:1.5;color:#404040;">
                                                                                                 <img src="{{side['vote_img']}}"/>
                                                                                             </div>
                                                                                         </div>
                                                                                     </div>
                                                                                 </div>
                                                                             </div>
                                                                         {% endfor %}
                                                                     {% endif %}
							         </div>
                                                             </div>
                                                             <div style="text-align:center;">
                                                                 <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;"><a href="http://www.speakol.com/newsfeed/comparison?slug={{ plugin['data']['slug'] }}&app={{plugin['data']['app_id']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a></button>
							     </div>
						       {% elseif plugin['type'] =="debate" %}
						           <div class="sp-block-group feed-item radius-5">
                                                               <div class="sp-block-group">
                                                                   <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                       <img src="{{ config.application.webservice }}/img/debate.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                   </h2>
                                                               </div>
                                                               <div style="margin-bottom:15px !important;margin-top:15px !important;display:table;line-height0;box-sizing:border-box;clear:both;">
                                                                   {% for side in plugin['sides'] %}
                                                                       {% set right = false %}
                                                                       {% if loop.last %}
                                                                           {% set right = true %}
                                                                       {% endif %}
                                                                        <div style="margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;width:50%;box-sizing:border-box;">
									    <div style="">
									        <div style="margin-top:0;overflow:hidden;zoom:1;">
										      <div style="margin-right:0px;float:{{right?'right' : 'left'}};text-align:center;box-sizing:border-box;zoom:1;list-style-type:none;">
										          <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:70px;height:70px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                                                      </div>
										      <div style="margin-buttom:5px;text-align:{{right ? 'right':'left'}};zoom:1;">
                                                                                          <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                      </div>
 										      <div style="margin-buttom:5px;text-align:{{right ? 'right':'left'}};zoom:1;">
                                                                        	          <h4 style="color:#676767;font-size:12px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;zoom:1;">{{side['side_data']['job_title']}}</h4>
                                                                 		      </div>

                                                                                </div>
									    </div>
									</div>
 							           {% endfor %}
                                                               </div>
                                                               <div style="width:100%">
                                                                   <img src="{{plugin['img']}}" style="max-width:100%;height:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                               </div>
                                                               <div style="text-align:center;">
                                                                   <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;">
                                                                       <a href="http://www.speakol.com/newsfeed/debate?slug={{ plugin['data']['slug'] }}&app={{plugin['data']['app_id']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                                   </button>
							       </div>
                                                       {% else %}
                                                           <div class="sp-block-group feed-item radius-5">
                                                               <div class="sp-block-group">
                                                                   <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                       <img src="{{ config.application.webservice }}/img/argumentbox.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                   </h2>
                                                               </div>
                                                               <div style="width:100%">
                                                                   <img src="{{plugin['img']}}" style="max-width:100%;height:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                               </div>
                                                               <div style="text-align:center;">
                                                                   <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;">
                                                                       <a href="http://www.speakol.com/newsfeed/argumentsbox?url={{ plugin['data']['url'] }}&app={{plugin['data']['app_id']}}&uuid={{ plugin['data']['code'] }}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                                   </button>
                                                               </div>
                                                           </div> 
                                                       {% endif %}
                                                   </td></tr>
                                                   <tr><td colspan="2"><hr style="border-color:#ebebed;border-bottom:0px;"></td></tr>
                                              {% endfor %}
                                              <tr><td style="padding:5px 0px 10px 0px">{{t._('monthly_digest-7')}}?</td></tr>
                                              <tr><td style="padding:5px 0px 10px 0px">{{t._('now')}},{{t._('monthly_digest-8')}}.</td></tr>
                                              <tr><td style="color:#444444;padding:5px 0px 0px 0px">{{t._('thanks')}} Speakol! {{t._('enjoy')}}!</td></tr>
                                              <tr><td style="color:#444444;padding:0px 0px 30px 0px">{{t._('team')}} Speakol</td></tr>
                                          </table>
				      </div>
                                  </div>
                                  {% else %}
                                  <div style="border-radius:5px;border:1px solid #5bb767;padding:0px;min-height:100%;width:98%">
                                      <div style="color:#5bb767;text-align:center;border-bottom:1px solid #5bb767;height:100px;font-size:24px;line-height:100px;">
                                          {{t._('monthly-digest')}}
                                      </div>
                                      <div>
				          <table style="color:#666666;margin-left:5%;margin-right:5%;">
                                              <tr><td> <p style="color:#444444;font-weight:normal;font-size:16px;line-height:1.6;margin-top:7%;margin-bottom:3%;padding:0">
                                                  <b>{{ t._('hello') }} <span style="margin:0;padding:0"></span>{{name}}</b>, </p>
                                              </td></tr>
                                              <tr><td style="padding:5px 0px 10px 0px">
                                                  {{t._('monthly_digest-1')}}!
                                              </td></tr>
                                              <tr><td style="padding:5px 0px 10px 0px">
                                                  {{t._('monthly_digest-2')}}.
                                              </td></tr>
                                              <tr><td style="padding:5px 0px 10px 0px;font-size:20px;">
                                                  {{t._('monthly_digest-3')}} <b>{{t._('monthly_digest-4')}}</b>:
                                              </td></tr>
                                              {% set classes = [ '2' : 'width:50%', '3' : 'width:33.3333333333%', '4': 'width:25%']%}
                                              {% for plugin in plugins %}
                                                   <tr><td style="padding:5px 0px 10px 0px">
						       {% if plugin['type'] == "comparison" %}
						             <div >
                                                                 <div >
                                                                     <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                         <img src="{{ config.application.webservice }}/img/comparison.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                     </h2>
                                                                 </div>
								 <div style="margin-bottom:15px !important;margin-top:15px !important;display:table;line-height0;box-sizing:border-box;clear:both;">
                                              		             {% if plugin['data']['align'] %}
                                                                     {% for side in plugin['sides'] %}
								         <div style="margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;{{ loop.length < 5? classes[loop.length] : '' }};box-sizing:border-box;">
									    <div style="padding:10px;border-radius:5px;margin-left:auto;margin-right:auto;box-sizing:border-box;list-style-type:none;;{{ loop.length < 5 and plugin.data.align ? 'width:90%' : '' }}">
									        <div style="margin-top:0;overflow:hidden;zoom:1;">
                                                                                    {% if side['side_data']['image'] !="" %}
										    <div style="margin-right:0px;float:none;text-align:center;box-sizing:border-box;zoom:1;list-style-type:none;">
										        <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:96px;min-height:96px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                     								    </div>
										    {% endif %}
       										    <div style="overflow:hidden;zoom:1;box-sizing:border-box;">
                                                                 		        <div style="margin-buttom:5px;margin-top:15px;text-align:center;zoom:1;">
                                                                                            <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;text-align:center;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                        </div>
                                                                                        <div style="width:91.66666666667%;position:relative;box-sizing:border-box;zoom:1;font-size:1.5em;font-weight:normal;line-height:1.5;color:#404040;margin-left:auto;margin-right:auto;">
                                                                                            <img src="{{side['vote_img']}}"/>
                                                                                        </div>
                                                                                    </div>
										</div>
									    </div>
                                                                         </div>
								     {% endfor %}
                                                                     {% else %}
                                                                         {% set rtl = (plugin['data']['lang'] == "ar")? 'right':'left' %}
                                                                         {% for side in plugin['sides'] %}
                                                                             <div style="margin-bottom:5px !important;margin-top:5px !important;float:{{rtl}};position:relative;min-height:1px;width:100%;box-sizing:border-box;">
                                                                                 <div style="padding:10px;border-radius:5px;margin-left:auto;margin-right:auto;box-sizing:border-box;list-style-type:none;">
                                                                                     <div style="margin-top:0;overflow:hidden;zoom:1;">
											 {% if side['side_data']['image'] !="" %}
                                                                                         <div style="margin-right:10px;float:{{rtl}}!important;box-sizing:border-box;zoom:1;list-style-type:none;">
                                                                                             <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:96px;min-height:96px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                                                         </div>
											 {% endif %}
                                                                                         <div style="overflow:hidden;zoom:1;box-sizing:border-box;">
                                                                                             <div style="margin-bottom:5px;text-align:left;zoom:1;">
                                                                                                 <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;text-align:{{rtl}};zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                             </div>
                                                                                             <div style="width:91.66666666667%;position:relative;box-sizing:border-box;zoom:1;font-size:1.5em;font-weight:normal;line-height:1.5;color:#404040;">
                                                                                                 <img src="{{side['vote_img']}}"/>
                                                                                             </div>
                                                                                         </div>
                                                                                     </div>
                                                                                 </div>
                                                                             </div>
                                                                         {% endfor %}
                                                                     {% endif %}
							         </div>
                                                             </div>
                                                             <div style="text-align:center;">
                                                                 <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;"><a href="http://www.speakol.com/newsfeed/comparison?slug={{ plugin['data']['slug'] }}&app={{plugin['data']['app_id']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a></button>
							     </div>
						       {% elseif plugin['type'] =="debate" %}
						           <div class="sp-block-group feed-item radius-5">
                                                               <div class="sp-block-group">
                                                                   <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                       <img src="{{ config.application.webservice }}/img/debate.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                   </h2>
                                                               </div>
                                                               <div style="margin-bottom:15px !important;margin-top:15px !important;display:table;line-height0;box-sizing:border-box;clear:both;">
                                                                   {% for side in plugin['sides'] %}
                                                                       {% set right = false %}
                                                                       {% if loop.last %}
                                                                           {% set right = true %}
                                                                       {% endif %}
                                                                        <div style="margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;width:50%;box-sizing:border-box;">
									    <div style="">
									        <div style="margin-top:0;overflow:hidden;zoom:1;">
										      <div style="margin-right:0px;float:{{right?'right' : 'left'}};text-align:center;box-sizing:border-box;zoom:1;list-style-type:none;">
										          <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:70px;height:70px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                                                      </div>
										      <div style="margin-buttom:5px;text-align:{{right ? 'right':'left'}};zoom:1;">
                                                                                          <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                      </div>
 										      <div style="margin-buttom:5px;text-align:{{right ? 'right':'left'}};zoom:1;">
                                                                        	          <h4 style="color:#676767;font-size:12px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;zoom:1;">{{side['side_data']['job_title']}}</h4>
                                                                 		      </div>

                                                                                </div>
									    </div>
									</div>
 							           {% endfor %}
                                                               </div>
                                                               <div style="width:100%">
                                                                   <img src="{{plugin['img']}}" style="max-width:100%;height:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                               </div>
                                                               <div style="text-align:center;">
                                                                   <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;">
                                                                       <a href="http://www.speakol.com/newsfeed/debate?slug={{ plugin['data']['slug'] }}&app={{plugin['data']['app_id']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                                   </button>
							       </div>
                                                       {% else %}
                                                           <div class="sp-block-group feed-item radius-5">
                                                               <div class="sp-block-group">
                                                                   <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                       <img src="{{ config.application.webservice }}/img/argumentbox.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                   </h2>
                                                               </div>
                                                               <div style="width:100%">
                                                                   <img src="{{plugin['img']}}" style="max-width:100%;height:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                               </div>
                                                               <div style="text-align:center;">
                                                                   <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;">
                                                                       <a href="http://www.speakol.com/newsfeed/argumentsbox?url={{ plugin['data']['url'] }}&app={{plugin['data']['app_id']}}&uuid={{ plugin['data']['code'] }}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                                   </button>
                                                               </div>
                                                           </div>
                                                       {% endif %}
                                                   </td></tr>
                                                   <tr><td colspan="2"><hr style="border-color:#ebebed;border-bottom:0px;"></td></tr>
                                              {% endfor %}
                                              <tr><td style="padding:5px 0px 10px 0px;font-size:20px;">{{t._('monthly_digest-5')}}?</td></tr>
                                              <tr><td style="padding:5px 0px 10px 0px">{{t._('monthly_digest-6')}}:</td></tr>
                                              {% for topic in hot_topics %}
                                                  <tr><td style="padding:5px 0px 10px 0px">
                                                      <table style="width:100%;">
                                                          <tr><td rowspan="2" style="width:26%;"> 
                                                              <img src="http:{{topic['image']}}" style="width:98%;height:90%;border-radius:5px;">
                                                          </td>
                                                          <td style="width:73%;font-size:18px;color:#3e88c2">
      						              {{topic['title']}}
                                                          </td></tr>
                                                          <tr><td> 
                                                              {{topic['description']}}
                                                          </td></tr>
                                                          <tr><td></td>
                                                          <td> <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:53%;border:0px;font-size:16px;">
                                                              <a href="{{topic['link']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                          </button></td>
                                                          </tr>
                                                          <tr><td colspan="2"><hr style="border-color:#ebebed;border-bottom:0px;"></td></tr>
                                                      </table> 
                                                  </td></tr>
                                              {% endfor %}
                                              <tr><td style="padding:5px 0px 10px 0px">{{t._('monthly_digest-7')}}:</td></tr>
                                              {% set classes = [ '2' : 'width:50%', '3' : 'width:33.3333333333%', '4': 'width:25%']%}
                                              {% for plugin in high_communities %}
                                                   <tr><td style="padding:5px 0px 10px 0px">
						       {% if plugin['type'] == "comparison" %}
						             <div class="sp-block-group feed-item radius-5">
                                                                 <div class="sp-block-group">
                                                                     <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                         <img src="{{ config.application.webservice }}/img/comparison.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                     </h2>
                                                                 </div>
								 <div style="margin-bottom:15px !important;margin-top:15px !important;display:table;line-height0;box-sizing:border-box;clear:both;">
                                              		             {% if plugin['data']['align'] %}
                                                                     {% for side in plugin['sides'] %}
								         <div style="margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;{{ loop.length < 5? classes[loop.length] : '' }};box-sizing:border-box;">
									    <div style="padding:10px;border-radius:5px;margin-left:auto;margin-right:auto;box-sizing:border-box;list-style-type:none;;{{ loop.length < 5 and plugin.data.align ? 'width:90%' : '' }}">
									        <div style="margin-top:0;overflow:hidden;zoom:1;">
										    {% if side['side_data']['image'] !="" %}
										    <div style="margin-right:0px;float:none;text-align:center;box-sizing:border-box;zoom:1;list-style-type:none;">
										        <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:96px;min-height:96px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                     								    </div>
										    {% endif %}
       										    <div style="overflow:hidden;zoom:1;box-sizing:border-box;">
                                                                 		        <div style="margin-buttom:5px;margin-top:15px;text-align:center;zoom:1;">
                                                                                            <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;text-align:center;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                        </div>
                                                                                        <div style="width:91.66666666667%;position:relative;box-sizing:border-box;zoom:1;font-size:1.5em;font-weight:normal;line-height:1.5;color:#404040;margin-left:auto;margin-right:auto;">
                                                                                            <img src="{{side['vote_img']}}"/>
                                                                                        </div>
                                                                                    </div>
										</div>
									    </div>
                                                                         </div>
								     {% endfor %}
                                                                     {% else %}
                                                                         {% set rtl = (plugin['data']['lang'] == "ar")? 'right':'left' %}
                                                                         {% for side in plugin['sides'] %}
                                                                             <div style="margin-bottom:5px !important;margin-top:5px !important;float:{{rtl}};position:relative;min-height:1px;width:100%;box-sizing:border-box;">
                                                                                 <div style="padding:10px;border-radius:5px;margin-left:auto;margin-right:auto;box-sizing:border-box;list-style-type:none;">
                                                                                     <div style="margin-top:0;overflow:hidden;zoom:1;">
											{% if side['side_data']['image'] !="" %}
                                                                                         <div style="margin-right:10px;float:{{rtl}}!important;box-sizing:border-box;zoom:1;list-style-type:none;">
                                                                                             <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:96px;min-height:96px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                                                         </div>
											 {% endif %}
                                                                                         <div style="overflow:hidden;zoom:1;box-sizing:border-box;">
                                                                                             <div style="margin-bottom:5px;text-align:left;zoom:1;">
                                                                                                 <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;text-align:{{rtl}};zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                             </div>
                                                                                             <div style="width:91.66666666667%;position:relative;box-sizing:border-box;zoom:1;font-size:1.5em;font-weight:normal;line-height:1.5;color:#404040;">
                                                                                                 <img src="{{side['vote_img']}}"/>
                                                                                             </div>
                                                                                         </div>
                                                                                     </div>
                                                                                 </div>
                                                                             </div>
                                                                         {% endfor %}
                                                                     {% endif %}
							         </div>
                                                             </div>
                                                             <div style="text-align:center;">
                                                                 <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;"><a href="http://www.speakol.com/newsfeed/comparison?slug={{ plugin['data']['slug'] }}&app={{plugin['data']['app_id']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a></button>
							     </div>
						       {% elseif plugin['type'] =="debate" %}
						           <div class="sp-block-group feed-item radius-5">
                                                               <div class="sp-block-group">
                                                                   <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                       <img src="{{ config.application.webservice }}/img/debate.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                   </h2>
                                                               </div>
                                                               <div style="margin-bottom:15px !important;margin-top:15px !important;display:table;line-height0;box-sizing:border-box;clear:both;">
                                                                   {% for side in plugin['sides'] %}
                                                                       {% set right = false %}
                                                                       {% if loop.last %}
                                                                           {% set right = true %}
                                                                       {% endif %}
                                                                        <div style="margin-bottom:5px !important;margin-top:5px !important;float:left;position:relative;min-height:1px;width:50%;box-sizing:border-box;">
									    <div style="">
									        <div style="margin-top:0;overflow:hidden;zoom:1;">
										      <div style="margin-right:0px;float:{{right?'right' : 'left'}};text-align:center;box-sizing:border-box;zoom:1;list-style-type:none;">
										          <img src="{{side['side_data']['image']}}" style="border-radius:5px;width:70px;height:70px;max-width:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                                                      </div>
										      <div style="margin-buttom:5px;text-align:{{right ? 'right':'left'}};zoom:1;">
                                                                                          <h4 style="color:#1177bb;font-size:20px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;zoom:1;">{{side['side_data']['title']}}</h4>
                                                                                      </div>
 										      <div style="margin-buttom:5px;text-align:{{right ? 'right':'left'}};zoom:1;">
                                                                        	          <h4 style="color:#676767;font-size:12px;margin-bottom:10px;margin:0;font-weight:500;line-height:1.1;box-sizing:border-box;zoom:1;">{{side['side_data']['job_title']}}</h4>
                                                                 		      </div>

                                                                                </div>
									    </div>
									</div>
 							           {% endfor %}
                                                               </div>
                                                               <div style="width:100%">
                                                                   <img src="{{plugin['img']}}" style="max-width:100%;height:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                               </div>
                                                               <div style="text-align:center;">
                                                                   <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;">
                                                                       <a href="http://www.speakol.com/newsfeed/debate?slug={{ plugin['data']['slug'] }}&app={{plugin['data']['app_id']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                                   </button>
							       </div>
                                                       {% else %}
                                                           <div class="sp-block-group feed-item radius-5">
                                                               <div class="sp-block-group">
                                                                   <h2 style="color:#1177bb;font-size:20px;vertical-align:bottom;">
                                                                       <img src="{{ config.application.webservice }}/img/argumentbox.png" style="vertical-align:middle;"/>&nbsp;{{ plugin['data']['title'] }}
                                                                   </h2>
                                                               </div>
                                                               <div style="width:100%">
                                                                   <img src="{{plugin['img']}}" style="max-width:100%;height:100%;vertical-align:middle;border:0;box-sizing:border-box;zoom:1;"/>
                                                               </div>
                                                               <div style="text-align:center;">
                                                                   <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:38%;border:0px;font-size:16px;">
                                                                       <a href="http://www.speakol.com/newsfeed/argumentsbox?url={{ plugin['data']['url'] }}&app={{plugin['data']['app_id']}}&uuid={{ plugin['data']['code'] }}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a>
                                                                   </button>
                                                               </div>
                                                           </div>
                                                       {% endif %}
                                                   </td></tr>
                                                   <tr><td colspan="2"><hr style="border-color:#ebebed;border-bottom:0px;"></td></tr>
                                              {% endfor %}
                                              <tr><td style="padding:5px 0px 10px 0px">{{t._('monthly_digest-8')}}?!</td></tr>
                                              <tr><td style="padding:5px 0px 10px 0px">{{t._('now')}},{{t._('monthly_digest-9')}}.</td></tr>
                                              <tr><td style="color:#444444;padding:5px 0px 0px 0px">{{t._('thanks')}} Speakol! {{t._('enjoy')}}!</td></tr>
                                              <tr><td style="color:#444444;padding:0px 0px 30px 0px">Speakol {{t._('team')}}</td></tr>
                                          </table>
				      </div>
                                  </div>
                                 {% endif %}
                              </td>
                         </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="margin: 0; padding: 0;"></td>
    </tr>
</table>
