<table class="body-wrap" style="width: 100%; margin: 0; padding: 0;font-size:13px;color:black;">
    <tr>
        <td></td>
        <td> 
           <a href="#" style="color: #5bb767; text-decoration: none; margin: 0; padding: 0;">
             <img src="{{ config.application.webservice }}/img/Speakol_footer.gif" alt="" style="max-width: 100%; margin-left: 17%; padding: 0;" />
           </a>
        </td>
    </tr>
    <tr style="margin: 0; padding: 0;"><td style="margin: 0; padding: 0;"></td>
        <td class="container" bgcolor="#FFFFFF" style="display: block !important; max-width: 600px !important; clear: both !important; border-radius: 10px; margin: 0 auto; padding: 0;">
            <div class="content" style="max-width: 600px; display: block; margin: 0 auto; padding: 5%;padding-top:0%;padding-bottom:0%;">
                <table style="width: 100%; margin: 0; padding: 0;"><tr style="display: table; width: 100%; margin: 0 0 10px; padding: 0;"><td class="va_t" style="vertical-align: top; margin: 0; padding: 0;" valign="top">
                        </td>
                        <td style="margin: 0; padding: 0;color:#666666;font-size:14px;">
                            {% if lang == "ar" %}
                            <div style="directon:rtl;border-radius:5px;border:1px solid #5bb767;padding:0px;min-height:100%;width:98%">
                            <div style="color:#5bb767;text-align:center;border-bottom:1px solid #5bb767;height:100px;"><span style="line-height:100px;font-size:22px"><b>{{t._('optional-newsletter-1')}}</b></span>
                            </div>
                            <div>
                            <table style="direction:rtl;color:#666666;margin-left:5%;margin-right:5%;">
                            <tr><td> <p style="color:#444444;font-weight:normal;font-size:15px;line-height:1.6;margin-top:7%;margin-bottom:3%;padding:0">
                            <b>{{ t._('hello') }} <span style="margin:0;padding:0"></span>{{name}}</b>، </p></td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('optional-newsletter-2')}}</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('optional-newsletter-3')}}</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px;"> {{t._('optional-newsletter-4')}}</td></tr>
                            {% for topic in hot_topics %}
                                  <tr><td style="padding:5px 0px 10px 0px">
                                       <table style="width:100%;directon:rtl;"><tr style="directon:rtl;"><td rowspan="2" style="width:26%;"> <img src="http:{{topic['image']}}" style="width:98%;height:90%;border-radius:5px;">
                                             </td><td style="width:73%;font-size:18px;color:#3e88c2">
                                                {{topic['title']}}</td>
                                             </tr>
                                             <tr><td> {{topic['description']}}</td></tr>
                                             <tr><td></td><td> <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:53%;border:0px;font-size:16px;"><a href="{{topic['link']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a></button></td></tr>
                                            {% if !loop.last %}<tr><td colspan="2"><hr style="border-color:#ebebed;border-bottom:0px;"></td></tr>{% endif %}
                                       </table>
                                  </td></tr>
                            {% endfor %}
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('optional-newsletter-5')}}</td></tr>
                            <tr><td style="color:#444444;padding:5px 0px 0px 0px">{{t._('thanks')}} Speakol! {{t._('enjoy')}}!</td></tr>
                            <tr><td style="color:#444444;padding:0px 0px 30px 0px">{{t._('team')}} Speakol</td></tr>
                            </table></div></div>
                            {% else %}
                           <div style="border-radius:5px;border:1px solid #5bb767;padding:0px;min-height:100%;width:98%">
                            <div style="color:#5bb767;text-align:center;border-bottom:1px solid #5bb767;height:100px;"><span style="line-height:100px;font-size:22px"><b>{{t._('optional-newsletter-title')}}</b></span></div>
                           <div>
                            <table style="color:#666666;margin-left:5%;margin-right:5%;">
                            <tr><td> <p style="color:#444444;font-weight:normal;font-size:16px;line-height:1.6;margin-top:7%;margin-bottom:3%;padding:0">
                            <b>{{ t._('hello') }} <span style="margin:0;padding:0"></span>{{name}}</b>, </p></td></tr>
                            <tr><td style="padding:5px 0px 0px 0px;font-size:20px;">{{t._('optional-newsletter-2')}}</td></tr>
                            <tr><td style="padding:2px 0px 10px 0px;">{{t._('optional-newsletter-4')}}</td></tr>
                            {% for topic in hot_topics %}
                                  <tr><td style="padding:5px 0px 10px 0px">
                                       <table style="width:100%;"><tr><td rowspan="2" style="width:26%;"> <img src="http:{{topic['image']}}" style="width:98%;height:90%;border-radius:5px;">
                                             </td><td style="width:73%;font-size:18px;color:#3e88c2">
      						{{topic['title']}}</td>
                                             </tr>
                                             <tr><td> {{topic['description']}}</td></tr>
                                             <tr><td></td><td> <button style="background-color:#5bb767;border-radius:5px;color:white;height:35px;width:53%;border:0px;font-size:16px;"><a href="{{topic['link']}}" style="color: white; text-decoration: none;">{{t._('view-discussion')}}</a></button></td></tr>
                                            {% if !loop.last %}<tr><td colspan="2"><hr style="border-color:#ebebed;border-bottom:0px;"></td></tr>{% endif %}
                                       </table> 
                                  </td></tr>
                            {% endfor %}
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('optional-newsletter-5')}}</td></tr>
                            <tr><td style="color:#444444;padding:5px 0px 0px 0px">{{t._('thanks')}} Speakol! {{t._('enjoy')}}!</td></tr>
                            <tr><td style="color:#444444;padding:0px 0px 30px 0px">Speakol {{t._('team')}}</td></tr>
                            </table></div>
                           </div>
                            {% endif %}
                        </td>
                    </tr></table></div><!-- /content -->
        </td>
        <td style="margin: 0; padding: 0;"></td>
    </tr>
</table>
