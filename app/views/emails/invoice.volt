<table class="body-wrap" style="width: 100%; margin: 0; padding: 0;font-size:13px;color:black;"><tr style="margin: 0; padding: 0;"><td style="margin: 0; padding: 0;"></td>
        <td class="container" bgcolor="#FFFFFF" style="display: block !important; max-width: 600px !important; clear: both !important; border-radius: 10px; margin: 0 auto; padding: 0;">
            <div class="content" style="max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table style="width: 100%; margin: 0; padding: 0;"><tr style="display: table; width: 100%; margin: 0 0 10px; padding: 0;"><td class="va_t" style="vertical-align: top; margin: 0; padding: 0;" valign="top">
                        </td>
                        <td style="margin: 0; padding: 0;;color:#666666;font-size:14px;">
                            {% if lang == "ar" %}
 <div style="directon:rtl;border-radius:5px;border:1px solid #5bb767;padding:0px;min-height:100%;width:98%">
                           <div style="background-color:#5bb767;color:white;text-align:center;min-height:100px"><span style="line-height:100px;font-size:22px"><b>{{t._('invoice-title')}}</b></span></div>
                  <div>
                            <table style="direction:rtl;color:#888888;margin-left:25px;margin-right:20px">
                            <tr><td> <p style="color:#444444;font-weight:normal;font-size:16px;line-height:1.6;margin:33px 0px 10px;padding:0">
                            <b>{{ t._('hello') }} <span style="margin:0;padding:0"></span>{{name}}</b>، </p></td></tr>
                            <tr><td style="padding:5px 0px 0px 0px">
                            {{t._('invoice-1')}} {{date}}.</td></tr>
                            <tr><td style="padding:0px 0px 10px 0px">{{t._('invoice-2')}}.</td></tr>
                             <tr><td style="padding:5px 0px 10px 0px">{{t._('invoice-3')}} {{amount}} {{t._('invoice-4')}}.</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('invoice-5')}}.</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('invoice-6')}}. {{t._('invoice-7')}}</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('invoice-8')}} <a href="http://plugins.speakol.com/contactus" style="color:#5bb767;text-decoration: none">{{t._('here')}}</a></td></tr>
                            <tr><td style="color:#444444;padding:5px 0px 0px 0px"><b>{{t._('thanks')}} Speakol! {{t._('enjoy')}}!</b></td></tr>
                            <tr><td style="color:#444444;padding:0px 0px 30px 0px"><b>{{t._('team')}} Speakol</b></td></tr>
                        </table></div></div>
                            {% else %}
    <div style="border-radius:5px;border:1px solid #5bb767;padding:0px;min-height:100%;width:98%">
                           <div style="background-color:#5bb767;color:white;text-align:center;min-height:100px"><span style="line-height:100px;font-size:22px"><b>{{t._('invoice-title')}}</b></span></div>
                  <div>
                            <table style="color:#888888;margin-left:25px;margin-right:20px">
                            <tr><td> <p style="color:#444444;font-weight:normal;font-size:16px;line-height:1.6;margin:33px 0px 10px;padding:0">
                            <b>{{ t._('hello') }} <span style="margin:0;padding:0"></span>{{name}}</b>, </p></td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">
                            {{t._('invoice-1')}} {{date}}.</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('invoice-2')}}. {{t._('invoice-3')}} {{amount}} {{t._('invoice-4')}}.</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('invoice-5')}}.</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('invoice-6')}}.</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('invoice-7')}}</td></tr>
                            <tr><td style="padding:5px 0px 10px 0px">{{t._('invoice-8')}} <a href="http://plugins.speakol.com/contactus" style="color:#5bb767;text-decoration: none">{{t._('here')}}</a></td></tr>
                            <tr><td style="color:#444444;padding:5px 0px 0px 0px"><b>{{t._('thanks')}} Speakol! {{t._('enjoy')}}!</b></td></tr>
                            <tr><td style="color:#444444;padding:0px 0px 30px 0px"><b>Speakol {{t._('team')}}</b></td></tr>
                        </table></div></div>
                            {% endif %}

                        </td>
                    </tr></table></div><!-- /content -->
        </td>
        <td style="margin: 0; padding: 0;"></td>
    </tr>
</table>
