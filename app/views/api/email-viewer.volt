<div class="form_row full mt">
    <div class="input-group">
        <span class="input-group-addon country_ico"></span>
        {{ form('method': 'get', 'style': 'position: fixed;top: 25%;border: 1px solid #ccc;padding: 17px 10px;background: white;border-radius: 0 7px 7px 0;') }}
        <label>Choose Template</label>
        <br/>
        <br/>
        {{ select("template", templates,"onchange":"this.form.submit()",  'using': ['id', 'name'], 'class': 'form-control', 'style':'width: 156px;background: white;padding: 5px;border: 1px solid #ccc;') }}
        <br/>
        <br/>
        {{ select("lang", langs,"onchange":"this.form.submit()",  'using': ['id', 'name'], 'class': 'form-control', 'style':'width: 156px;background: white;padding: 5px;border: 1px solid #ccc;') }}
        <hr style="border: 1px solid #ccc;margin: 15px 0;width:96%;"/>
        <label>Send Demo Mail</label>
        <br/>
        <br/>
        {{ textField({"email", "placeholder": t._('Enter email'), "class": "form-control", 'style':'background: white;border: 1px solid #ccc;padding: 5px;width: 144px;'}) }}
        <br/>
        <br/>
        {{ submit_button('Send','style':'width: 98%;padding: 5px;background: #59b867;border: none;color: #fff;cursor: pointer;') }}
        {{ endForm() }}
        <br/>
    </div>
</div>

{{ partial("emails/"~ templateName) }}
