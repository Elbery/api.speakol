<?php
use Phalcon\Mvc\Model\Migration;
class InitMigration_100 extends Migration {
    function up() {
        $sql = "
--
-- Database: `speakol`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(125) NOT NULL COMMENT 'controller/action',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `super_admin` tinyint(1) DEFAULT '0',
  `email` varchar(128) NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) NOT NULL DEFAULT '',
  `reset_password_token` varchar(128) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` bigint(20) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(64) DEFAULT NULL,
  `last_sign_in_ip` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `administrator_spheres`
--

CREATE TABLE IF NOT EXISTS `administrator_spheres` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `administrator_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sphere_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `super_admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`administrator_id`,`sphere_id`),
  KEY `administrator_id` (`administrator_id`),
  KEY `sphere_id` (`sphere_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `apps`
--

CREATE TABLE IF NOT EXISTS `apps` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `image` text,
  `category_id` int(20) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `publisher_id` bigint(20) unsigned NOT NULL,
  `secret` text NOT NULL,
  `redirect_uri` text,
  `website` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '0',
  `mongo_id` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `user_id` (`publisher_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `apps_arguments`
--

CREATE TABLE IF NOT EXISTS `apps_arguments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique Id',
  `app_id` bigint(20) unsigned NOT NULL,
  `argument_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`,`argument_id`),
  KEY `argument_id` (`argument_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `app_admins`
--

CREATE TABLE IF NOT EXISTS `app_admins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `super_admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `app_follows`
--

CREATE TABLE IF NOT EXISTS `app_follows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) unsigned DEFAULT NULL,
  `follower_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  KEY `follower_id` (`follower_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `argumentboxes`
--

CREATE TABLE IF NOT EXISTS `argumentboxes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique Id',
  `app_id` bigint(20) unsigned NOT NULL,
  `url` text NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `argumentboxes_attachments`
--

CREATE TABLE IF NOT EXISTS `argumentboxes_attachments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `argumentbox_id` bigint(20) unsigned NOT NULL,
  `attachment_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `argumentbox_id` (`argumentbox_id`),
  KEY `attachment_id` (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `argumentboxes_follows`
--

CREATE TABLE IF NOT EXISTS `argumentboxes_follows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `argumentbox_id` bigint(20) unsigned DEFAULT NULL,
  `follower_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `argumentbox_id` (`argumentbox_id`,`follower_id`),
  KEY `follower_id` (`follower_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `argumentbox_recomended`
--

CREATE TABLE IF NOT EXISTS `argumentbox_recomended` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(500) CHARACTER SET utf16 DEFAULT NULL,
  `title` varchar(250) CHARACTER SET utf16 DEFAULT NULL,
  `image` varchar(250) CHARACTER SET utf16 DEFAULT NULL,
  `domain` varchar(250) CHARACTER SET utf16 DEFAULT NULL,
  `argumentbox_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `argumentbox_id` (`argumentbox_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `arguments`
--

CREATE TABLE IF NOT EXISTS `arguments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) unsigned NOT NULL,
  `context` text,
  `side_id` bigint(20) unsigned NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `module_id` bigint(20) DEFAULT NULL,
  `app_id` bigint(20) DEFAULT NULL,
  `mongo_id` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `link` text NOT NULL COMMENT 'link [String]',
  PRIMARY KEY (`id`),
  KEY `argument_user_id` (`owner_id`),
  KEY `side_id` (`side_id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `argument_photos`
--

CREATE TABLE IF NOT EXISTS `argument_photos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` text,
  `url` text,
  `argument_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uploader_id` (`argument_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `argument_thumbs`
--

CREATE TABLE IF NOT EXISTS `argument_thumbs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `argumnet_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `thumber_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `thumb_up` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`argumnet_id`,`thumber_id`),
  KEY `argumnet_id` (`argumnet_id`),
  KEY `thumber_id` (`thumber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `attached_photos`
--

CREATE TABLE IF NOT EXISTS `attached_photos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` text,
  `url` text,
  `uploader_id` bigint(20) unsigned DEFAULT NULL,
  `uploader_type` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uploader_id` (`uploader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `mongo_id` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `mongo_id`) VALUES
(1, 'Arts & Culture', NULL),
(2, 'Entertainment', NULL),
(3, 'Economics', NULL),
(4, 'Philosophy', NULL),
(5, 'Politics', NULL),
(6, 'Religion', NULL),
(7, 'Science', NULL),
(8, 'Travel', NULL),
(9, 'Cars', NULL),
(10, 'Education', NULL),
(11, 'Technology', NULL),
(12, 'Health', NULL),
(13, 'Sports', NULL),
(14, 'Miscellaneous', NULL),
(15, 'News', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comparisons`
--

CREATE TABLE IF NOT EXISTS `comparisons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(225) NOT NULL,
  `image` text,
  `category_id` int(20) unsigned NOT NULL,
  `slug` varchar(300) DEFAULT NULL,
  `app_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `mongo_id` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `app_id` (`app_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comparison_follows`
--

CREATE TABLE IF NOT EXISTS `comparison_follows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comparison_id` bigint(20) unsigned DEFAULT NULL,
  `follower_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comparison_id` (`comparison_id`),
  KEY `follower_id` (`follower_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comparison_sides_meta`
--

CREATE TABLE IF NOT EXISTS `comparison_sides_meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `side_id` bigint(20) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `side_id` (`side_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_forms`
--

CREATE TABLE IF NOT EXISTS `contact_forms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(256) DEFAULT NULL,
  `message` text,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL DEFAULT '',
  `favorite` tinyint(1) DEFAULT NULL,
  `reply` text,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `mongo_id` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `iso_code_2` varchar(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `iso_code_3` varchar(3) COLLATE utf8_bin NOT NULL DEFAULT '',
  `address_format` text COLLATE utf8_bin NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=240 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegowina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D''Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TP', 'TMP', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(73, 'France', 'FR', 'FRA', '', 0, 1),
(74, 'France, Metropolitan', 'FX', 'FXX', '', 0, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'Macedonia', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '', 0, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(236, 'Yugoslavia', 'YU', 'YUG', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cover_photos`
--

CREATE TABLE IF NOT EXISTS `cover_photos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` text,
  `uploader_id` bigint(20) unsigned DEFAULT NULL,
  `uploader_type` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uploader_id` (`uploader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `debates`
--

CREATE TABLE IF NOT EXISTS `debates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(300) DEFAULT NULL,
  `category_id` int(20) unsigned NOT NULL,
  `image` text,
  `url` text,
  `started_at` datetime DEFAULT NULL,
  `ends_at` datetime DEFAULT NULL,
  `max_days` bigint(20) DEFAULT '30',
  `app_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `mongo_id` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  KEY `category_id` (`category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `debate_challengers`
--

CREATE TABLE IF NOT EXISTS `debate_challengers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `debate_id` bigint(20) unsigned NOT NULL,
  `side_id` bigint(20) unsigned NOT NULL,
  `debate_slug` varchar(255) NOT NULL,
  `challenger_id` bigint(20) unsigned NOT NULL,
  `challenger_opinion` mediumtext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`debate_id`,`challenger_id`),
  KEY `debate_id` (`debate_id`),
  KEY `challenger_id` (`challenger_id`),
  KEY `side_id` (`side_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `debate_follows`
--

CREATE TABLE IF NOT EXISTS `debate_follows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `debate_id` bigint(20) unsigned DEFAULT NULL,
  `follower_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `debate_id` (`debate_id`),
  KEY `follower_id` (`follower_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `debate_sides_meta`
--

CREATE TABLE IF NOT EXISTS `debate_sides_meta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `side_id` bigint(20) unsigned NOT NULL,
  `title` text NOT NULL,
  `debater_id` bigint(20) unsigned NOT NULL,
  `image` text,
  `job_title` varchar(255) DEFAULT NULL,
  `opinion` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `side_id` (`side_id`),
  KEY `debater_id` (`debater_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `follow` tinyint(1) DEFAULT NULL,
  `debate_invitation` tinyint(1) DEFAULT NULL,
  `message` tinyint(1) DEFAULT NULL,
  `debate_ended` tinyint(1) DEFAULT NULL,
  `reply_on_argument` tinyint(1) DEFAULT NULL,
  `updates` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feeds`
--

CREATE TABLE IF NOT EXISTS `feeds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL,
  `element_id` bigint(20) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `module` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `app_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`activity_id`,`created_at`),
  KEY `element_id` (`element_id`),
  KEY `app_id` (`app_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(256) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `photo_url` varchar(256) DEFAULT NULL,
  `video_url` varchar(256) DEFAULT NULL,
  `clicks` bigint(20) DEFAULT NULL,
  `video` tinyint(1) DEFAULT NULL,
  `linkable_type` varchar(128) DEFAULT NULL,
  `linkable_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mutes`
--

CREATE TABLE IF NOT EXISTS `mutes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mutable_type` varchar(128) DEFAULT NULL,
  `mutable_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `notification_type` varchar(128) DEFAULT NULL,
  `notifiable_id` bigint(20) unsigned DEFAULT NULL,
  `notifiable_type` varchar(128) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `mongo_id` varchar(128) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `extra_info` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification_actors`
--

CREATE TABLE IF NOT EXISTS `notification_actors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `actor_id` bigint(20) unsigned DEFAULT NULL,
  `notification_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `actor_id` (`actor_id`),
  KEY `notification_id` (`notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification_recipients`
--

CREATE TABLE IF NOT EXISTS `notification_recipients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `notification_id` bigint(20) unsigned DEFAULT NULL,
  `seen` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notification_id` (`notification_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opinions`
--

CREATE TABLE IF NOT EXISTS `opinions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(300) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT '0',
  `content` text,
  `charset` varchar(10) DEFAULT NULL,
  `mongo_id` varchar(128) DEFAULT NULL,
  `category_id` int(20) unsigned DEFAULT NULL,
  `owner_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`,`owner_id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opinions_attachments`
--

CREATE TABLE IF NOT EXISTS `opinions_attachments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `opinion_id` bigint(20) unsigned NOT NULL,
  `attachment_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `opinion_id` (`opinion_id`),
  KEY `attachment_id` (`attachment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opinion_follows`
--

CREATE TABLE IF NOT EXISTS `opinion_follows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `opinion_id` bigint(20) unsigned DEFAULT NULL,
  `follower_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `opinion_id` (`opinion_id`,`follower_id`),
  KEY `follower_id` (`follower_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_photos`
--

CREATE TABLE IF NOT EXISTS `profile_photos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` text,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `app_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `publishers`
--

CREATE TABLE IF NOT EXISTS `publishers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE IF NOT EXISTS `replies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `argument_id` bigint(20) unsigned NOT NULL,
  `parent_reply` bigint(20) unsigned DEFAULT NULL,
  `content` varchar(512) DEFAULT NULL,
  `mongo_id` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `app_id` bigint(20) DEFAULT NULL,
  `side_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `argument_reply_id` (`argument_id`),
  KEY `user_id` (`user_id`),
  KEY `parent_reply` (`parent_reply`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reply_photos`
--

CREATE TABLE IF NOT EXISTS `reply_photos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` text,
  `url` text,
  `reply_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uploader_id` (`reply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reply_thumbs`
--

CREATE TABLE IF NOT EXISTS `reply_thumbs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reply_id` bigint(20) unsigned DEFAULT NULL,
  `thumber_id` bigint(20) unsigned DEFAULT NULL,
  `thumb_up` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reply_id` (`reply_id`),
  KEY `thumber_id` (`thumber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reportable_id` bigint(20) unsigned NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `reportable_type` varchar(32) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `reporter_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reportable_id` (`reportable_id`),
  KEY `reporter_id` (`reporter_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_reporters`
--

CREATE TABLE IF NOT EXISTS `report_reporters` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` bigint(20) unsigned DEFAULT NULL,
  `reporter_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `reporter_id` (`reporter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='roles for users such as super admin, admin, app, visitor' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'super admin', '2014-07-23 00:00:00', '2014-07-23 00:00:00'),
(2, 'admin', '2014-07-23 00:00:00', '2014-07-23 00:00:00'),
(3, 'app', '2014-07-23 00:00:00', '2014-07-23 00:00:00'),
(4, 'user', '2014-07-23 00:00:00', '2014-07-23 00:00:00'),
(5, 'guest', '2014-08-03 00:00:00', '2014-08-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles_activities`
--

CREATE TABLE IF NOT EXISTS `roles_activities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL COMMENT 'one role has many activities',
  `activity_id` bigint(20) unsigned NOT NULL COMMENT 'one activity is accessed by many roles',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`,`activity_id`),
  KEY `activity_id` (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='many roles have many activities' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sides`
--

CREATE TABLE IF NOT EXISTS `sides` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `argumentbox_id` bigint(20) unsigned DEFAULT NULL,
  `debate_id` bigint(20) unsigned DEFAULT NULL,
  `comparison_id` bigint(20) unsigned DEFAULT NULL,
  `owner_id` bigint(20) unsigned DEFAULT NULL,
  `mongo_id` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `argumentbox_id` (`argumentbox_id`),
  KEY `debate_id` (`debate_id`),
  KEY `comparison_id` (`comparison_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sides_arguments`
--

CREATE TABLE IF NOT EXISTS `sides_arguments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique Id',
  `side_id` bigint(20) unsigned NOT NULL,
  `argument_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `side_id` (`side_id`,`argument_id`),
  KEY `argument_id` (`argument_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sides_attached_photos`
--

CREATE TABLE IF NOT EXISTS `sides_attached_photos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `side_id` bigint(20) unsigned NOT NULL,
  `image` text NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `side_id` (`side_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sides_tags`
--

CREATE TABLE IF NOT EXISTS `sides_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto increment id',
  `side_id` bigint(20) unsigned NOT NULL COMMENT 'Id of side',
  `tag_id` bigint(20) unsigned NOT NULL COMMENT 'Id of tag',
  PRIMARY KEY (`id`),
  KEY `side_id` (`side_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `social_providers`
--

CREATE TABLE IF NOT EXISTS `social_providers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `token` text,
  `refresh_token` varchar(255) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `expires` tinyint(1) DEFAULT NULL,
  `provider_type` varchar(64) DEFAULT NULL,
  `owner_id` bigint(20) unsigned DEFAULT '0',
  `mongo_id` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `social_provider_settings`
--

CREATE TABLE IF NOT EXISTS `social_provider_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `argument` tinyint(1) DEFAULT '1',
  `opinion` tinyint(1) DEFAULT '1',
  `voted` tinyint(1) DEFAULT '1',
  `social_provider_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `spheres`
--

CREATE TABLE IF NOT EXISTS `spheres` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `locale` varchar(8) DEFAULT NULL,
  `country_id` int(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sphere_apps`
--

CREATE TABLE IF NOT EXISTS `sphere_apps` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sphere_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`,`app_id`,`sphere_id`),
  KEY `app_id` (`app_id`),
  KEY `sphere_id` (`sphere_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sphere_comparisons`
--

CREATE TABLE IF NOT EXISTS `sphere_comparisons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comparison_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sphere_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`,`comparison_id`,`sphere_id`),
  KEY `comparison_id` (`comparison_id`),
  KEY `sphere_id` (`sphere_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sphere_debates`
--

CREATE TABLE IF NOT EXISTS `sphere_debates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `debate_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sphere_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`,`debate_id`,`sphere_id`),
  KEY `debate_id` (`debate_id`),
  KEY `sphere_id` (`sphere_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sphere_opinions`
--

CREATE TABLE IF NOT EXISTS `sphere_opinions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `opinion_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sphere_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`,`opinion_id`,`sphere_id`),
  KEY `opinion_id` (`opinion_id`),
  KEY `sphere_id` (`sphere_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sphere_stories`
--

CREATE TABLE IF NOT EXISTS `sphere_stories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `story_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sphere_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`,`story_id`,`sphere_id`),
  KEY `story_id` (`story_id`),
  KEY `sphere_id` (`sphere_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sphere_tags`
--

CREATE TABLE IF NOT EXISTS `sphere_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sphere_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`,`tag_id`,`sphere_id`),
  KEY `sphere_id` (`sphere_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE IF NOT EXISTS `stories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `content` text,
  `charset` varchar(512) DEFAULT NULL,
  `category_id` int(20) unsigned DEFAULT NULL,
  `mongo_id` varchar(64) DEFAULT NULL,
  `app_id` bigint(20) unsigned DEFAULT NULL,
  `url` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreign_id` (`category_id`,`app_id`),
  KEY `category_id` (`category_id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stories_attachments`
--

CREATE TABLE IF NOT EXISTS `stories_attachments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `story_id` bigint(20) unsigned NOT NULL,
  `attachment_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `story_id` (`story_id`),
  KEY `attachment_id` (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `story_follows`
--

CREATE TABLE IF NOT EXISTS `story_follows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `story_id` bigint(20) unsigned DEFAULT NULL,
  `follower_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `story_id` (`story_id`),
  KEY `follower_id` (`follower_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `ordering_factor` bigint(20) DEFAULT '0',
  `charset` varchar(512) DEFAULT NULL,
  `promoted` tinyint(1) DEFAULT '0',
  `image` varchar(500) DEFAULT NULL,
  `mongo_id` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tag_follows`
--

CREATE TABLE IF NOT EXISTS `tag_follows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) unsigned DEFAULT NULL,
  `follower_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tag_id` (`tag_id`),
  KEY `follower_id` (`follower_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE IF NOT EXISTS `tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique token ID',
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The api token.',
  `secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The api secret.',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Boolean indicating whether the token is enabled (1) -The api can not be used if enabled - or not (0).',
  `weight` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The weight of the token in listings and the user interface.',
  `created_at` datetime DEFAULT NULL COMMENT 'The date when the token was created.',
  `modified_at` datetime DEFAULT NULL COMMENT 'The date when the token was modified.',
  `user_id` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key: users.id to which the token is assigned.',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Stores api tokens.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL COMMENT 'one user has one role',
  `slug` varchar(255) DEFAULT NULL,
  `first_name` varchar(127) DEFAULT NULL,
  `last_name` varchar(127) DEFAULT NULL,
  `job_title` varchar(127) DEFAULT NULL,
  `employer` varchar(127) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '1',
  `points` bigint(20) DEFAULT '0',
  `verified` tinyint(1) DEFAULT '0',
  `banned` tinyint(1) DEFAULT '0',
  `locale` varchar(8) DEFAULT 'en',
  `position_x` bigint(20) DEFAULT '0',
  `position_y` bigint(20) DEFAULT '0',
  `took_tour` tinyint(1) DEFAULT '0',
  `skipped_quiz` tinyint(1) DEFAULT '0',
  `preferred_content_filter` varchar(50) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `show_color` tinyint(1) DEFAULT '1',
  `country_id` int(20) unsigned DEFAULT NULL,
  `welcome_step` bigint(20) DEFAULT '1',
  `time_zone` varchar(64) DEFAULT NULL,
  `x_movement` text,
  `y_movement` text,
  `votes_for_colors` text,
  `color_id` bigint(20) unsigned DEFAULT NULL,
  `system_network_id` bigint(20) unsigned DEFAULT NULL,
  `city_id` bigint(20) unsigned DEFAULT NULL,
  `mongo_id` varchar(64) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `encrypted_password` varchar(255) DEFAULT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` bigint(20) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(64) DEFAULT NULL,
  `last_sign_in_ip` varchar(64) DEFAULT NULL,
  `confirmation_token` varchar(64) DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmation_sent_at` datetime DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `invitation_token` varchar(64) DEFAULT NULL,
  `invitation_sent_at` datetime DEFAULT NULL,
  `invitation_accepted_at` datetime DEFAULT NULL,
  `invitation_limit` bigint(20) DEFAULT NULL,
  `invited_by_id` bigint(20) unsigned DEFAULT NULL,
  `invited_by_type` varchar(127) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `profile_picture` text,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_follows`
--

CREATE TABLE IF NOT EXISTS `user_follows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `follower_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `follower_id` (`follower_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_setting`
--

CREATE TABLE IF NOT EXISTS `user_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `setting_key` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `prefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `setting_value` longtext CHARACTER SET utf16,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE IF NOT EXISTS `views` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `viewable_type` varchar(127) DEFAULT NULL,
  `viewable_id` bigint(20) unsigned DEFAULT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `viewer_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `viewer_id` (`viewer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `side_id` bigint(20) unsigned DEFAULT NULL,
  `voter_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `side_id` (`side_id`,`voter_id`),
  KEY `voter_id` (`voter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `__db_version__`
--

CREATE TABLE IF NOT EXISTS `__db_version__` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(20) NOT NULL DEFAULT '0',
  `label` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sql_up` longtext,
  `sql_down` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `administrator_spheres`
--
ALTER TABLE `administrator_spheres`
  ADD CONSTRAINT `administrator_spheres_ibfk_2` FOREIGN KEY (`sphere_id`) REFERENCES `spheres` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `administrator_spheres_ibfk_1` FOREIGN KEY (`administrator_id`) REFERENCES `administrators` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `apps`
--
ALTER TABLE `apps`
  ADD CONSTRAINT `apps_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `apps_ibfk_3` FOREIGN KEY (`publisher_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `apps_arguments`
--
ALTER TABLE `apps_arguments`
  ADD CONSTRAINT `apps_arguments_ibfk_4` FOREIGN KEY (`argument_id`) REFERENCES `arguments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `apps_arguments_ibfk_3` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `app_admins`
--
ALTER TABLE `app_admins`
  ADD CONSTRAINT `app_admins_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `app_admins_ibfk_3` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `app_follows`
--
ALTER TABLE `app_follows`
  ADD CONSTRAINT `app_follows_ibfk_4` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `app_follows_ibfk_3` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `argumentboxes`
--
ALTER TABLE `argumentboxes`
  ADD CONSTRAINT `argumentboxes_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `argumentboxes_attachments`
--
ALTER TABLE `argumentboxes_attachments`
  ADD CONSTRAINT `argumentboxes_attachments_ibfk_2` FOREIGN KEY (`attachment_id`) REFERENCES `attached_photos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `argumentboxes_attachments_ibfk_1` FOREIGN KEY (`argumentbox_id`) REFERENCES `argumentboxes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `argumentboxes_follows`
--
ALTER TABLE `argumentboxes_follows`
  ADD CONSTRAINT `argumentboxes_follows_ibfk_2` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `argumentboxes_follows_ibfk_1` FOREIGN KEY (`argumentbox_id`) REFERENCES `argumentboxes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `argumentbox_recomended`
--
ALTER TABLE `argumentbox_recomended`
  ADD CONSTRAINT `adasdasd` FOREIGN KEY (`argumentbox_id`) REFERENCES `argumentboxes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `arguments`
--
ALTER TABLE `arguments`
  ADD CONSTRAINT `arguments_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `arguments_ibfk_2` FOREIGN KEY (`side_id`) REFERENCES `sides` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `argument_photos`
--
ALTER TABLE `argument_photos`
  ADD CONSTRAINT `argument_photos_ibfk_1` FOREIGN KEY (`argument_id`) REFERENCES `arguments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `argument_thumbs`
--
ALTER TABLE `argument_thumbs`
  ADD CONSTRAINT `argument_thumbs_ibfk_2` FOREIGN KEY (`thumber_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `argument_thumbs_ibfk_1` FOREIGN KEY (`argumnet_id`) REFERENCES `arguments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `attached_photos`
--
ALTER TABLE `attached_photos`
  ADD CONSTRAINT `attached_photos_ibfk_1` FOREIGN KEY (`uploader_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comparisons`
--
ALTER TABLE `comparisons`
  ADD CONSTRAINT `comparisons_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `comparisons_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `comparisons_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comparison_follows`
--
ALTER TABLE `comparison_follows`
  ADD CONSTRAINT `comparison_follows_ibfk_3` FOREIGN KEY (`comparison_id`) REFERENCES `comparisons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comparison_follows_ibfk_2` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comparison_sides_meta`
--
ALTER TABLE `comparison_sides_meta`
  ADD CONSTRAINT `comparison_sides_meta_ibfk_2` FOREIGN KEY (`side_id`) REFERENCES `sides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contact_forms`
--
ALTER TABLE `contact_forms`
  ADD CONSTRAINT `contact_forms_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cover_photos`
--
ALTER TABLE `cover_photos`
  ADD CONSTRAINT `cover_photos_ibfk_1` FOREIGN KEY (`uploader_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `debates`
--
ALTER TABLE `debates`
  ADD CONSTRAINT `debates_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `debates_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `debates_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `debate_challengers`
--
ALTER TABLE `debate_challengers`
  ADD CONSTRAINT `debate_challengers_ibfk_3` FOREIGN KEY (`side_id`) REFERENCES `sides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `debate_challengers_ibfk_1` FOREIGN KEY (`debate_id`) REFERENCES `debates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `debate_challengers_ibfk_2` FOREIGN KEY (`challenger_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `debate_follows`
--
ALTER TABLE `debate_follows`
  ADD CONSTRAINT `debate_follows_ibfk_4` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `debate_follows_ibfk_3` FOREIGN KEY (`debate_id`) REFERENCES `debates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `debate_sides_meta`
--
ALTER TABLE `debate_sides_meta`
  ADD CONSTRAINT `debate_sides_meta_ibfk_2` FOREIGN KEY (`debater_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `debate_sides_meta_ibfk_3` FOREIGN KEY (`side_id`) REFERENCES `sides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mutes`
--
ALTER TABLE `mutes`
  ADD CONSTRAINT `mutes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notification_recipients`
--
ALTER TABLE `notification_recipients`
  ADD CONSTRAINT `notification_recipients_ibfk_2` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `notification_recipients_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `opinions`
--
ALTER TABLE `opinions`
  ADD CONSTRAINT `opinions_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `opinions_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `opinions_attachments`
--
ALTER TABLE `opinions_attachments`
  ADD CONSTRAINT `opinions_attachments_ibfk_2` FOREIGN KEY (`attachment_id`) REFERENCES `attached_photos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `opinions_attachments_ibfk_1` FOREIGN KEY (`opinion_id`) REFERENCES `opinions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `opinion_follows`
--
ALTER TABLE `opinion_follows`
  ADD CONSTRAINT `opinion_follows_ibfk_2` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `opinion_follows_ibfk_1` FOREIGN KEY (`opinion_id`) REFERENCES `opinions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `profile_photos`
--
ALTER TABLE `profile_photos`
  ADD CONSTRAINT `profile_photos_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `profile_photos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `replies`
--
ALTER TABLE `replies`
  ADD CONSTRAINT `reply_reply_id` FOREIGN KEY (`parent_reply`) REFERENCES `replies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `replies_ibfk_3` FOREIGN KEY (`argument_id`) REFERENCES `arguments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `replies_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reply_photos`
--
ALTER TABLE `reply_photos`
  ADD CONSTRAINT `reply_photos_ibfk_3` FOREIGN KEY (`reply_id`) REFERENCES `replies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reply_thumbs`
--
ALTER TABLE `reply_thumbs`
  ADD CONSTRAINT `reply_thumbs_ibfk_4` FOREIGN KEY (`thumber_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reply_thumbs_ibfk_3` FOREIGN KEY (`reply_id`) REFERENCES `replies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sides`
--
ALTER TABLE `sides`
  ADD CONSTRAINT `sides_ibfk_4` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sides_ibfk_1` FOREIGN KEY (`argumentbox_id`) REFERENCES `argumentboxes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sides_ibfk_2` FOREIGN KEY (`debate_id`) REFERENCES `debates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sides_ibfk_3` FOREIGN KEY (`comparison_id`) REFERENCES `comparisons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sides_arguments`
--
ALTER TABLE `sides_arguments`
  ADD CONSTRAINT `sides_arguments_ibfk_2` FOREIGN KEY (`argument_id`) REFERENCES `arguments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sides_arguments_ibfk_1` FOREIGN KEY (`side_id`) REFERENCES `sides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_follows`
--
ALTER TABLE `user_follows`
  ADD CONSTRAINT `user_follows_ibfk_2` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_follows_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;";
        self::$_connection->execute($sql);
    }
}
