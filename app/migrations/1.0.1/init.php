<?php
use Phalcon\Mvc\Model\Migration;
class InitMigration_101 extends Migration {
    function up() {
        $sql = "ALTER TABLE  `argument_thumbs` DROP FOREIGN KEY  `argument_thumbs_ibfk_1` ,
ADD FOREIGN KEY (  `argumnet_id` ) REFERENCES  `arguments` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `argument_thumbs` DROP FOREIGN KEY  `argument_thumbs_ibfk_2` ,
ADD FOREIGN KEY (  `thumber_id` ) REFERENCES  `users` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;


ALTER TABLE  `attached_photos` DROP FOREIGN KEY  `attached_photos_ibfk_1` ,
ADD FOREIGN KEY (  `uploader_id` ) REFERENCES  `users` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `argument_photos` DROP FOREIGN KEY  `argument_photos_ibfk_1` ,
ADD FOREIGN KEY (  `argument_id` ) REFERENCES  `arguments` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `arguments` DROP FOREIGN KEY  `arguments_ibfk_1` ,
ADD FOREIGN KEY (  `owner_id` ) REFERENCES  `users` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `arguments` DROP FOREIGN KEY  `arguments_ibfk_2` ,
ADD FOREIGN KEY (  `side_id` ) REFERENCES  `sides` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `argumentboxes_attachments` DROP FOREIGN KEY  `argumentboxes_attachments_ibfk_1` ,
ADD FOREIGN KEY (  `argumentbox_id` ) REFERENCES  `argumentboxes` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `argumentboxes_attachments` DROP FOREIGN KEY  `argumentboxes_attachments_ibfk_2` ,
ADD FOREIGN KEY (  `attachment_id` ) REFERENCES  `attached_photos` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `argumentboxes_follows` DROP FOREIGN KEY  `argumentboxes_follows_ibfk_1` ,
ADD FOREIGN KEY (  `argumentbox_id` ) REFERENCES  `argumentboxes` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `argumentboxes_follows` DROP FOREIGN KEY  `argumentboxes_follows_ibfk_2` ,
ADD FOREIGN KEY (  `follower_id` ) REFERENCES  `users` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `argumentbox_recomended` DROP FOREIGN KEY  `adasdasd` ,
ADD FOREIGN KEY (  `argumentbox_id` ) REFERENCES  `argumentboxes` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;";
        self::$_connection->execute($sql);
    }
}
