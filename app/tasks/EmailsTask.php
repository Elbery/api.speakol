<?php
use Phalcon\Mvc\Dispatcher;
use \Phalcon\Mvc\View as View;
class EmailsTask extends \Phalcon\CLI\Task {
    public function mainAction() {
        echo "\nThis is the default task and the default action\n";
    }
    public function monthlyNewsletterEnAction() {
        $apps = new AppsController();
        $apps->send_monthly_newsletter("en");
    }
    public function monthlyNewsletterArAction() {
        $apps = new AppsController();
        $apps->send_monthly_newsletter("ar");
    }
    public function sendOptionalNewsletterEnAction() {
        $apps = new AppsController();
        $apps->send_optional_newsletter("en");
    }
    public function sendOptionalNewsletterArAction() {
        $apps = new AppsController();
        $apps->send_optional_newsletter("ar");
    }
    public function followInactiveAppEnAction() {
        $apps = new AppsController();
        $apps->follow_inactive_app("en");
    }
    public function followInactiveAppArAction() {
        $apps = new AppsController();
        $apps->follow_inactive_app("ar");
    }
    public function longInactivityEnAction() {
        $apps = new AppsController();
        $apps->long_inactivity("en");
    }
    public function longInactivityArAction() {
        $apps = new AppsController();
        $apps->long_inactivity("ar");
    }
    public function userFollowupEnAction() {
        $users = new UsersController();
        $users->user_followup("en");
    }
    public function userFollowupArAction() {
        $users = new UsersController();
        $users->user_followup("ar");
    }
    public function sendUserNewsletterEnAction() {
        $users = new UsersController();
        $users->send_user_newsletter("en");
    }
    public function sendUserNewsletterArAction() {
        $users = new UsersController();
        $users->send_user_newsletter("ar");
    }
    public function sendInvoiceEnAction() {
        $billing = new BillingController();
        $billing->send_invoices("en");
    }
    public function sendInvoiceArAction() {
        $billing = new BillingController();
        $billing->send_invoices("ar");
    }
    public function chargingEnAction() {
        $billing = new BillingController();
        $billing->charging("en");
    }
    public function chargingArAction() {
        $billing = new BillingController();
        $billing->charging("ar");
    }
}
