<?php
class FacebookLibrary extends Phalcon\Mvc\User\Component {
    protected $_facebook;
    protected $scope;
    public function __construct($facebook, $scope) {
        $this->_facebook = $facebook;
        $this->scope = $scope;
    }
    public function facebookLoginURL($redirect_url) {
        return $this->_facebook->getLoginUrl(array('redirect_uri' => $redirect_url, 'scope' => $this->scope, 'display' => "popup",));
    }
    public function fbLogin($accessToken = NULL, $redirectUrl = '') {
        $accessToken = is_null($accessToken) ? $this->_facebook->getAccessToken($redirectUrl) : $accessToken;
        $fb_id = $this->_facebook->getUser();
        if (isset($fb_id)) {
            $this->_facebook->setAccessToken($accessToken);
            $params = array('access_token' => $accessToken);
            $user_profile = $this->_facebook->api('/me', $params);
            $user_profile['image'] = $this->getFacebookImage($user_profile['id']);
            return array('user' => $user_profile, 'access_token' => $accessToken);
        } else {
            throw new Exception("User Id not found", 400);
        }
    }
    public function getFacebookImage($fb_id) {
        $url = 'https://graph.facebook.com/' . $fb_id . '/picture?type=square';
        if (function_exists('get_headers')) {
            $array = get_headers($url, 1);
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $response = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $array = $this->httpParseHeaders(substr($response, 0, $header_size));
        }
        return (isset($array['Location']) ? $array['Location'] : false);
    }
    function httpParseHeaders($raw_headers) {
        $headers = array();
        $key = '';
        foreach (explode("\n", $raw_headers) as $i => $h) {
            $h = explode(':', $h, 2);
            if (isset($h[1])) {
                if (!isset($headers[$h[0]])) $headers[$h[0]] = trim($h[1]);
                elseif (is_array($headers[$h[0]])) {
                    $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1])));
                } else {
                    $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1])));
                }
                $key = $h[0];
            } else {
                if (substr($h[0], 0, 1) == "\t") $headers[$key].= "\r\n\t" . trim($h[0]);
                elseif (!$key) $headers[0] = trim($h[0]);
                trim($h[0]);
            }
        }
        return $headers;
    }
    public function revokeFacebookAccount($accessToken, $userId) {
        $params = array('method' => 'delete', 'access_token' => $accessToken);
        $revoke = $this->_facebook->api(array('method' => 'auth.revokeAuthorization', 'uid' => $userId, 'access_token' => $accessToken));
        return $revoke;
    }
    public function postToMyWall($data) {
        $accessToken = SocialProviders::findFirst(array('owner_id = :owner_id: AND provider_type = :provider_type:', 'bind' => array('owner_id' => $data['user_id'], 'provider_type' => 'facebook')));
        if (!$accessToken) {
            throw new Exception('User not found!');
        }
        $this->getDI()->get('google_oauth')->setAccessToken($accessToken->getToken());
        $user_id = $this->getDI()->get('google_oauth');
        if ($user_id) {
            $ret_obj = $this->_facebook->api('/me/feed', 'POST', array('link' => $data['link'], 'message' => $data['message']));
        } else {
            $redirect_url = 'http://ggo.com';
            $login_url = $this->facebookLoginURL($redirect_url);
            $login_url = $login_url . '&a=b';
            return $login_url;
        }
    }
}
