<?php
require_once 'service/apiModel.php';
require_once 'service/apiService.php';
require_once 'service/apiServiceRequest.php';
class UrlchannelsServiceResource extends apiServiceResource {
    public function listUrlchannels($adClientId, $optParams = array()) {
        $params = array('adClientId' => $adClientId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new UrlChannels($data);
        } else {
            return $data;
        }
    }
}
class AdunitsServiceResource extends apiServiceResource {
    public function listAdunits($adClientId, $optParams = array()) {
        $params = array('adClientId' => $adClientId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new AdUnits($data);
        } else {
            return $data;
        }
    }
    public function get($adClientId, $adUnitId, $optParams = array()) {
        $params = array('adClientId' => $adClientId, 'adUnitId' => $adUnitId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new AdUnit($data);
        } else {
            return $data;
        }
    }
}
class AdunitsCustomchannelsServiceResource extends apiServiceResource {
    public function listAdunitsCustomchannels($adClientId, $adUnitId, $optParams = array()) {
        $params = array('adClientId' => $adClientId, 'adUnitId' => $adUnitId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new CustomChannels($data);
        } else {
            return $data;
        }
    }
}
class AdclientsServiceResource extends apiServiceResource {
    public function listAdclients($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new AdClients($data);
        } else {
            return $data;
        }
    }
}
class ReportsServiceResource extends apiServiceResource {
    public function generate($startDate, $endDate, $optParams = array()) {
        $params = array('startDate' => $startDate, 'endDate' => $endDate);
        $params = array_merge($params, $optParams);
        $data = $this->__call('generate', array($params));
        if ($this->useObjects()) {
            return new AdsenseReportsGenerateResponse($data);
        } else {
            return $data;
        }
    }
}
class AccountsServiceResource extends apiServiceResource {
    public function listAccounts($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new Accounts($data);
        } else {
            return $data;
        }
    }
    public function get($accountId, $optParams = array()) {
        $params = array('accountId' => $accountId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new Account($data);
        } else {
            return $data;
        }
    }
}
class AccountsUrlchannelsServiceResource extends apiServiceResource {
    public function listAccountsUrlchannels($accountId, $adClientId, $optParams = array()) {
        $params = array('accountId' => $accountId, 'adClientId' => $adClientId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new UrlChannels($data);
        } else {
            return $data;
        }
    }
}
class AccountsAdunitsServiceResource extends apiServiceResource {
    public function listAccountsAdunits($accountId, $adClientId, $optParams = array()) {
        $params = array('accountId' => $accountId, 'adClientId' => $adClientId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new AdUnits($data);
        } else {
            return $data;
        }
    }
    public function get($accountId, $adClientId, $adUnitId, $optParams = array()) {
        $params = array('accountId' => $accountId, 'adClientId' => $adClientId, 'adUnitId' => $adUnitId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new AdUnit($data);
        } else {
            return $data;
        }
    }
}
class AccountsAdunitsCustomchannelsServiceResource extends apiServiceResource {
    public function listAccountsAdunitsCustomchannels($accountId, $adClientId, $adUnitId, $optParams = array()) {
        $params = array('accountId' => $accountId, 'adClientId' => $adClientId, 'adUnitId' => $adUnitId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new CustomChannels($data);
        } else {
            return $data;
        }
    }
}
class AccountsAdclientsServiceResource extends apiServiceResource {
    public function listAccountsAdclients($accountId, $optParams = array()) {
        $params = array('accountId' => $accountId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new AdClients($data);
        } else {
            return $data;
        }
    }
}
class AccountsReportsServiceResource extends apiServiceResource {
    public function generate($startDate, $endDate, $optParams = array()) {
        $params = array('startDate' => $startDate, 'endDate' => $endDate);
        $params = array_merge($params, $optParams);
        $data = $this->__call('generate', array($params));
        if ($this->useObjects()) {
            return new AdsenseReportsGenerateResponse($data);
        } else {
            return $data;
        }
    }
}
class AccountsCustomchannelsServiceResource extends apiServiceResource {
    public function listAccountsCustomchannels($accountId, $adClientId, $optParams = array()) {
        $params = array('accountId' => $accountId, 'adClientId' => $adClientId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new CustomChannels($data);
        } else {
            return $data;
        }
    }
    public function get($accountId, $adClientId, $customChannelId, $optParams = array()) {
        $params = array('accountId' => $accountId, 'adClientId' => $adClientId, 'customChannelId' => $customChannelId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new CustomChannel($data);
        } else {
            return $data;
        }
    }
}
class AccountsCustomchannelsAdunitsServiceResource extends apiServiceResource {
    public function listAccountsCustomchannelsAdunits($accountId, $adClientId, $customChannelId, $optParams = array()) {
        $params = array('accountId' => $accountId, 'adClientId' => $adClientId, 'customChannelId' => $customChannelId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new AdUnits($data);
        } else {
            return $data;
        }
    }
}
class CustomchannelsServiceResource extends apiServiceResource {
    public function listCustomchannels($adClientId, $optParams = array()) {
        $params = array('adClientId' => $adClientId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new CustomChannels($data);
        } else {
            return $data;
        }
    }
    public function get($adClientId, $customChannelId, $optParams = array()) {
        $params = array('adClientId' => $adClientId, 'customChannelId' => $customChannelId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new CustomChannel($data);
        } else {
            return $data;
        }
    }
}
class CustomchannelsAdunitsServiceResource extends apiServiceResource {
    public function listCustomchannelsAdunits($adClientId, $customChannelId, $optParams = array()) {
        $params = array('adClientId' => $adClientId, 'customChannelId' => $customChannelId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new AdUnits($data);
        } else {
            return $data;
        }
    }
}
class apiAdsenseService extends apiService {
    public $urlchannels;
    public $adunits;
    public $adunits_customchannels;
    public $adunits_customchannels_customchannels;
    public $adclients;
    public $reports;
    public $accounts;
    public $accounts_urlchannels;
    public $accounts_urlchannels_urlchannels;
    public $accounts_urlchannels_adunits;
    public $accounts_urlchannels_adclients;
    public $accounts_urlchannels_reports;
    public $accounts_urlchannels_customchannels;
    public $accounts_adunits;
    public $accounts_adunits_urlchannels;
    public $accounts_adunits_adunits;
    public $accounts_adunits_adclients;
    public $accounts_adunits_reports;
    public $accounts_adunits_customchannels;
    public $accounts_adclients;
    public $accounts_adclients_urlchannels;
    public $accounts_adclients_adunits;
    public $accounts_adclients_adclients;
    public $accounts_adclients_reports;
    public $accounts_adclients_customchannels;
    public $accounts_reports;
    public $accounts_reports_urlchannels;
    public $accounts_reports_adunits;
    public $accounts_reports_adclients;
    public $accounts_reports_reports;
    public $accounts_reports_customchannels;
    public $accounts_customchannels;
    public $accounts_customchannels_urlchannels;
    public $accounts_customchannels_adunits;
    public $accounts_customchannels_adclients;
    public $accounts_customchannels_reports;
    public $accounts_customchannels_customchannels;
    public $customchannels;
    public $customchannels_adunits;
    public $customchannels_adunits_adunits;
    public function __construct(apiClient $apiClient) {
        $this->rpcPath = '/rpc';
        $this->restBasePath = '/adsense/v1.1/';
        $this->version = 'v1.1';
        $this->serviceName = 'adsense';
        $apiClient->addService($this->serviceName, $this->version);
        $this->urlchannels = new UrlchannelsServiceResource($this, $this->serviceName, 'urlchannels', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}}, "id": "adsense.urlchannels.list", "httpMethod": "GET", "path": "adclients/{adClientId}/urlchannels", "response": {"$ref": "UrlChannels"}}}}', true));
        $this->adunits = new AdunitsServiceResource($this, $this->serviceName, 'adunits', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"includeInactive": {"type": "boolean", "location": "query"}, "pageToken": {"type": "string", "location": "query"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}}, "id": "adsense.adunits.list", "httpMethod": "GET", "path": "adclients/{adClientId}/adunits", "response": {"$ref": "AdUnits"}}, "get": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"adClientId": {"required": true, "type": "string", "location": "path"}, "adUnitId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.adunits.get", "httpMethod": "GET", "path": "adclients/{adClientId}/adunits/{adUnitId}", "response": {"$ref": "AdUnit"}}}}', true));
        $this->adunits_customchannels = new AdunitsCustomchannelsServiceResource($this, $this->serviceName, 'customchannels', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "adUnitId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}}, "id": "adsense.adunits.customchannels.list", "httpMethod": "GET", "path": "adclients/{adClientId}/adunits/{adUnitId}/customchannels", "response": {"$ref": "CustomChannels"}}}}', true));
        $this->adclients = new AdclientsServiceResource($this, $this->serviceName, 'adclients', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}}, "response": {"$ref": "AdClients"}, "httpMethod": "GET", "path": "adclients", "id": "adsense.adclients.list"}}}', true));
        $this->reports = new ReportsServiceResource($this, $this->serviceName, 'reports', json_decode('{"methods": {"generate": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"sort": {"repeated": true, "type": "string", "location": "query"}, "startDate": {"required": true, "type": "string", "location": "query"}, "endDate": {"required": true, "type": "string", "location": "query"}, "locale": {"type": "string", "location": "query"}, "metric": {"repeated": true, "type": "string", "location": "query"}, "maxResults": {"format": "int32", "maximum": "50000", "minimum": "0", "location": "query", "type": "integer"}, "filter": {"repeated": true, "type": "string", "location": "query"}, "currency": {"type": "string", "location": "query"}, "startIndex": {"format": "int32", "maximum": "5000", "minimum": "0", "location": "query", "type": "integer"}, "dimension": {"repeated": true, "type": "string", "location": "query"}, "accountId": {"repeated": true, "type": "string", "location": "query"}}, "id": "adsense.reports.generate", "httpMethod": "GET", "path": "reports", "response": {"$ref": "AdsenseReportsGenerateResponse"}}}}', true));
        $this->accounts = new AccountsServiceResource($this, $this->serviceName, 'accounts', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}}, "response": {"$ref": "Accounts"}, "httpMethod": "GET", "path": "accounts", "id": "adsense.accounts.list"}, "get": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"tree": {"type": "boolean", "location": "query"}, "accountId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.accounts.get", "httpMethod": "GET", "path": "accounts/{accountId}", "response": {"$ref": "Account"}}}}', true));
        $this->accounts_urlchannels = new AccountsUrlchannelsServiceResource($this, $this->serviceName, 'urlchannels', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}, "accountId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.accounts.urlchannels.list", "httpMethod": "GET", "path": "accounts/{accountId}/adclients/{adClientId}/urlchannels", "response": {"$ref": "UrlChannels"}}}}', true));
        $this->accounts_adunits = new AccountsAdunitsServiceResource($this, $this->serviceName, 'adunits', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"includeInactive": {"type": "boolean", "location": "query"}, "pageToken": {"type": "string", "location": "query"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}, "accountId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.accounts.adunits.list", "httpMethod": "GET", "path": "accounts/{accountId}/adclients/{adClientId}/adunits", "response": {"$ref": "AdUnits"}}, "get": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"adClientId": {"required": true, "type": "string", "location": "path"}, "adUnitId": {"required": true, "type": "string", "location": "path"}, "accountId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.accounts.adunits.get", "httpMethod": "GET", "path": "accounts/{accountId}/adclients/{adClientId}/adunits/{adUnitId}", "response": {"$ref": "AdUnit"}}}}', true));
        $this->accounts_adunits_customchannels = new AccountsAdunitsCustomchannelsServiceResource($this, $this->serviceName, 'customchannels', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "adUnitId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}, "accountId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.accounts.adunits.customchannels.list", "httpMethod": "GET", "path": "accounts/{accountId}/adclients/{adClientId}/adunits/{adUnitId}/customchannels", "response": {"$ref": "CustomChannels"}}}}', true));
        $this->accounts_adclients = new AccountsAdclientsServiceResource($this, $this->serviceName, 'adclients', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}, "accountId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.accounts.adclients.list", "httpMethod": "GET", "path": "accounts/{accountId}/adclients", "response": {"$ref": "AdClients"}}}}', true));
        $this->accounts_reports = new AccountsReportsServiceResource($this, $this->serviceName, 'reports', json_decode('{"methods": {"generate": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"sort": {"repeated": true, "type": "string", "location": "query"}, "startDate": {"required": true, "type": "string", "location": "query"}, "endDate": {"required": true, "type": "string", "location": "query"}, "locale": {"type": "string", "location": "query"}, "metric": {"repeated": true, "type": "string", "location": "query"}, "maxResults": {"format": "int32", "maximum": "50000", "minimum": "0", "location": "query", "type": "integer"}, "filter": {"repeated": true, "type": "string", "location": "query"}, "currency": {"type": "string", "location": "query"}, "startIndex": {"format": "int32", "maximum": "5000", "minimum": "0", "location": "query", "type": "integer"}, "dimension": {"repeated": true, "type": "string", "location": "query"}, "accountId": {"type": "string", "location": "path"}}, "id": "adsense.accounts.reports.generate", "httpMethod": "GET", "path": "accounts/{accountId}/reports", "response": {"$ref": "AdsenseReportsGenerateResponse"}}}}', true));
        $this->accounts_customchannels = new AccountsCustomchannelsServiceResource($this, $this->serviceName, 'customchannels', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}, "accountId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.accounts.customchannels.list", "httpMethod": "GET", "path": "accounts/{accountId}/adclients/{adClientId}/customchannels", "response": {"$ref": "CustomChannels"}}, "get": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"customChannelId": {"required": true, "type": "string", "location": "path"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "accountId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.accounts.customchannels.get", "httpMethod": "GET", "path": "accounts/{accountId}/adclients/{adClientId}/customchannels/{customChannelId}", "response": {"$ref": "CustomChannel"}}}}', true));
        $this->accounts_customchannels_adunits = new AccountsCustomchannelsAdunitsServiceResource($this, $this->serviceName, 'adunits', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"includeInactive": {"type": "boolean", "location": "query"}, "customChannelId": {"required": true, "type": "string", "location": "path"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}, "pageToken": {"type": "string", "location": "query"}, "accountId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.accounts.customchannels.adunits.list", "httpMethod": "GET", "path": "accounts/{accountId}/adclients/{adClientId}/customchannels/{customChannelId}/adunits", "response": {"$ref": "AdUnits"}}}}', true));
        $this->customchannels = new CustomchannelsServiceResource($this, $this->serviceName, 'customchannels', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}}, "id": "adsense.customchannels.list", "httpMethod": "GET", "path": "adclients/{adClientId}/customchannels", "response": {"$ref": "CustomChannels"}}, "get": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"customChannelId": {"required": true, "type": "string", "location": "path"}, "adClientId": {"required": true, "type": "string", "location": "path"}}, "id": "adsense.customchannels.get", "httpMethod": "GET", "path": "adclients/{adClientId}/customchannels/{customChannelId}", "response": {"$ref": "CustomChannel"}}}}', true));
        $this->customchannels_adunits = new CustomchannelsAdunitsServiceResource($this, $this->serviceName, 'adunits', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adsense", "https://www.googleapis.com/auth/adsense.readonly"], "parameters": {"includeInactive": {"type": "boolean", "location": "query"}, "pageToken": {"type": "string", "location": "query"}, "customChannelId": {"required": true, "type": "string", "location": "path"}, "adClientId": {"required": true, "type": "string", "location": "path"}, "maxResults": {"format": "int32", "maximum": "10000", "minimum": "0", "location": "query", "type": "integer"}}, "id": "adsense.customchannels.adunits.list", "httpMethod": "GET", "path": "adclients/{adClientId}/customchannels/{customChannelId}/adunits", "response": {"$ref": "AdUnits"}}}}', true));
    }
}
class Account extends apiModel {
    public $kind;
    public $id;
    protected $__subAccountsType = 'Account';
    protected $__subAccountsDataType = 'array';
    public $subAccounts;
    public $name;
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setSubAccounts($subAccounts) {
        $this->assertIsArray($subAccounts, 'Account', __METHOD__);
        $this->subAccounts = $subAccounts;
    }
    public function getSubAccounts() {
        return $this->subAccounts;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
}
class Accounts extends apiModel {
    public $nextPageToken;
    protected $__itemsType = 'Account';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public $etag;
    public function setNextPageToken($nextPageToken) {
        $this->nextPageToken = $nextPageToken;
    }
    public function getNextPageToken() {
        return $this->nextPageToken;
    }
    public function setItems($items) {
        $this->assertIsArray($items, 'Account', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setEtag($etag) {
        $this->etag = $etag;
    }
    public function getEtag() {
        return $this->etag;
    }
}
class AdClient extends apiModel {
    public $productCode;
    public $kind;
    public $id;
    public $supportsReporting;
    public function setProductCode($productCode) {
        $this->productCode = $productCode;
    }
    public function getProductCode() {
        return $this->productCode;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setSupportsReporting($supportsReporting) {
        $this->supportsReporting = $supportsReporting;
    }
    public function getSupportsReporting() {
        return $this->supportsReporting;
    }
}
class AdClients extends apiModel {
    public $nextPageToken;
    protected $__itemsType = 'AdClient';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public $etag;
    public function setNextPageToken($nextPageToken) {
        $this->nextPageToken = $nextPageToken;
    }
    public function getNextPageToken() {
        return $this->nextPageToken;
    }
    public function setItems($items) {
        $this->assertIsArray($items, 'AdClient', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setEtag($etag) {
        $this->etag = $etag;
    }
    public function getEtag() {
        return $this->etag;
    }
}
class AdUnit extends apiModel {
    public $status;
    public $kind;
    public $code;
    public $id;
    public $name;
    public function setStatus($status) {
        $this->status = $status;
    }
    public function getStatus() {
        return $this->status;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setCode($code) {
        $this->code = $code;
    }
    public function getCode() {
        return $this->code;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
}
class AdUnits extends apiModel {
    public $nextPageToken;
    protected $__itemsType = 'AdUnit';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public $etag;
    public function setNextPageToken($nextPageToken) {
        $this->nextPageToken = $nextPageToken;
    }
    public function getNextPageToken() {
        return $this->nextPageToken;
    }
    public function setItems($items) {
        $this->assertIsArray($items, 'AdUnit', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setEtag($etag) {
        $this->etag = $etag;
    }
    public function getEtag() {
        return $this->etag;
    }
}
class AdsenseReportsGenerateResponse extends apiModel {
    public $kind;
    public $rows;
    public $warnings;
    public $totals;
    protected $__headersType = 'AdsenseReportsGenerateResponseHeaders';
    protected $__headersDataType = 'array';
    public $headers;
    public $totalMatchedRows;
    public $averages;
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setRows($rows) {
        $this->assertIsArray($rows, 'string', __METHOD__);
        $this->rows = $rows;
    }
    public function getRows() {
        return $this->rows;
    }
    public function setWarnings($warnings) {
        $this->assertIsArray($warnings, 'string', __METHOD__);
        $this->warnings = $warnings;
    }
    public function getWarnings() {
        return $this->warnings;
    }
    public function setTotals($totals) {
        $this->assertIsArray($totals, 'string', __METHOD__);
        $this->totals = $totals;
    }
    public function getTotals() {
        return $this->totals;
    }
    public function setHeaders($headers) {
        $this->assertIsArray($headers, 'AdsenseReportsGenerateResponseHeaders', __METHOD__);
        $this->headers = $headers;
    }
    public function getHeaders() {
        return $this->headers;
    }
    public function setTotalMatchedRows($totalMatchedRows) {
        $this->totalMatchedRows = $totalMatchedRows;
    }
    public function getTotalMatchedRows() {
        return $this->totalMatchedRows;
    }
    public function setAverages($averages) {
        $this->assertIsArray($averages, 'string', __METHOD__);
        $this->averages = $averages;
    }
    public function getAverages() {
        return $this->averages;
    }
}
class AdsenseReportsGenerateResponseHeaders extends apiModel {
    public $currency;
    public $type;
    public $name;
    public function setCurrency($currency) {
        $this->currency = $currency;
    }
    public function getCurrency() {
        return $this->currency;
    }
    public function setType($type) {
        $this->type = $type;
    }
    public function getType() {
        return $this->type;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
}
class CustomChannel extends apiModel {
    public $kind;
    public $code;
    protected $__targetingInfoType = 'CustomChannelTargetingInfo';
    protected $__targetingInfoDataType = '';
    public $targetingInfo;
    public $id;
    public $name;
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setCode($code) {
        $this->code = $code;
    }
    public function getCode() {
        return $this->code;
    }
    public function setTargetingInfo(CustomChannelTargetingInfo $targetingInfo) {
        $this->targetingInfo = $targetingInfo;
    }
    public function getTargetingInfo() {
        return $this->targetingInfo;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
}
class CustomChannelTargetingInfo extends apiModel {
    public $location;
    public $adsAppearOn;
    public $siteLanguage;
    public $description;
    public function setLocation($location) {
        $this->location = $location;
    }
    public function getLocation() {
        return $this->location;
    }
    public function setAdsAppearOn($adsAppearOn) {
        $this->adsAppearOn = $adsAppearOn;
    }
    public function getAdsAppearOn() {
        return $this->adsAppearOn;
    }
    public function setSiteLanguage($siteLanguage) {
        $this->siteLanguage = $siteLanguage;
    }
    public function getSiteLanguage() {
        return $this->siteLanguage;
    }
    public function setDescription($description) {
        $this->description = $description;
    }
    public function getDescription() {
        return $this->description;
    }
}
class CustomChannels extends apiModel {
    public $nextPageToken;
    protected $__itemsType = 'CustomChannel';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public $etag;
    public function setNextPageToken($nextPageToken) {
        $this->nextPageToken = $nextPageToken;
    }
    public function getNextPageToken() {
        return $this->nextPageToken;
    }
    public function setItems($items) {
        $this->assertIsArray($items, 'CustomChannel', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setEtag($etag) {
        $this->etag = $etag;
    }
    public function getEtag() {
        return $this->etag;
    }
}
class UrlChannel extends apiModel {
    public $kind;
    public $id;
    public $urlPattern;
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setUrlPattern($urlPattern) {
        $this->urlPattern = $urlPattern;
    }
    public function getUrlPattern() {
        return $this->urlPattern;
    }
}
class UrlChannels extends apiModel {
    public $nextPageToken;
    protected $__itemsType = 'UrlChannel';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public $etag;
    public function setNextPageToken($nextPageToken) {
        $this->nextPageToken = $nextPageToken;
    }
    public function getNextPageToken() {
        return $this->nextPageToken;
    }
    public function setItems($items) {
        $this->assertIsArray($items, 'UrlChannel', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setEtag($etag) {
        $this->etag = $etag;
    }
    public function getEtag() {
        return $this->etag;
    }
}
