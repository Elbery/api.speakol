<?php
require_once 'service/apiModel.php';
require_once 'service/apiService.php';
require_once 'service/apiServiceRequest.php';
class WebfontsServiceResource extends apiServiceResource {
    public function listWebfonts($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new WebfontList($data);
        } else {
            return $data;
        }
    }
}
class apiWebfontsService extends apiService {
    public $webfonts;
    public function __construct(apiClient $apiClient) {
        $this->rpcPath = '/rpc';
        $this->restBasePath = '/webfonts/v1/';
        $this->version = 'v1';
        $this->serviceName = 'webfonts';
        $apiClient->addService($this->serviceName, $this->version);
        $this->webfonts = new WebfontsServiceResource($this, $this->serviceName, 'webfonts', json_decode('{"methods": {"list": {"parameters": {"sort": {"enum": ["alpha", "date", "popularity", "style", "trending"], "type": "string", "location": "query"}}, "id": "webfonts.webfonts.list", "httpMethod": "GET", "path": "webfonts", "response": {"$ref": "WebfontList"}}}}', true));
    }
}
class Webfont extends apiModel {
    public $kind;
    public $variants;
    public $subsets;
    public $family;
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setVariants($variants) {
        $this->variants = $variants;
    }
    public function getVariants() {
        return $this->variants;
    }
    public function setSubsets($subsets) {
        $this->subsets = $subsets;
    }
    public function getSubsets() {
        return $this->subsets;
    }
    public function setFamily($family) {
        $this->family = $family;
    }
    public function getFamily() {
        return $this->family;
    }
}
class WebfontList extends apiModel {
    protected $__itemsType = 'Webfont';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public function setItems($items) {
        $this->assertIsArray($items, 'Webfont', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
}
