<?php
require_once 'service/apiModel.php';
require_once 'service/apiService.php';
require_once 'service/apiServiceRequest.php';
class WebResourceServiceResource extends apiServiceResource {
    public function insert($verificationMethod, SiteVerificationWebResourceResource $postBody, $optParams = array()) {
        $params = array('verificationMethod' => $verificationMethod, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('insert', array($params));
        if ($this->useObjects()) {
            return new SiteVerificationWebResourceResource($data);
        } else {
            return $data;
        }
    }
    public function get($id, $optParams = array()) {
        $params = array('id' => $id);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new SiteVerificationWebResourceResource($data);
        } else {
            return $data;
        }
    }
    public function listWebResource($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SiteVerificationWebResourceListResponse($data);
        } else {
            return $data;
        }
    }
    public function update($id, SiteVerificationWebResourceResource $postBody, $optParams = array()) {
        $params = array('id' => $id, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('update', array($params));
        if ($this->useObjects()) {
            return new SiteVerificationWebResourceResource($data);
        } else {
            return $data;
        }
    }
    public function patch($id, SiteVerificationWebResourceResource $postBody, $optParams = array()) {
        $params = array('id' => $id, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('patch', array($params));
        if ($this->useObjects()) {
            return new SiteVerificationWebResourceResource($data);
        } else {
            return $data;
        }
    }
    public function getToken($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('getToken', array($params));
        if ($this->useObjects()) {
            return new SiteVerificationWebResourceGettokenResponse($data);
        } else {
            return $data;
        }
    }
    public function delete($id, $optParams = array()) {
        $params = array('id' => $id);
        $params = array_merge($params, $optParams);
        $data = $this->__call('delete', array($params));
        return $data;
    }
}
class apiSiteVerificationService extends apiService {
    public $webResource;
    public function __construct(apiClient $apiClient) {
        $this->rpcPath = '/rpc';
        $this->restBasePath = '/siteVerification/v1/';
        $this->version = 'v1';
        $this->serviceName = 'siteVerification';
        $apiClient->addService($this->serviceName, $this->version);
        $this->webResource = new WebResourceServiceResource($this, $this->serviceName, 'webResource', json_decode('{"methods": {"insert": {"scopes": ["https://www.googleapis.com/auth/siteverification"], "parameters": {"verificationMethod": {"required": true, "type": "string", "location": "query"}}, "request": {"$ref": "SiteVerificationWebResourceResource"}, "id": "siteVerification.webResource.insert", "httpMethod": "POST", "path": "webResource", "response": {"$ref": "SiteVerificationWebResourceResource"}}, "get": {"scopes": ["https://www.googleapis.com/auth/siteverification"], "parameters": {"id": {"required": true, "type": "string", "location": "path"}}, "id": "siteVerification.webResource.get", "httpMethod": "GET", "path": "webResource/{id}", "response": {"$ref": "SiteVerificationWebResourceResource"}}, "list": {"scopes": ["https://www.googleapis.com/auth/siteverification"], "id": "siteVerification.webResource.list", "httpMethod": "GET", "path": "webResource", "response": {"$ref": "SiteVerificationWebResourceListResponse"}}, "update": {"scopes": ["https://www.googleapis.com/auth/siteverification"], "parameters": {"id": {"required": true, "type": "string", "location": "path"}}, "request": {"$ref": "SiteVerificationWebResourceResource"}, "id": "siteVerification.webResource.update", "httpMethod": "PUT", "path": "webResource/{id}", "response": {"$ref": "SiteVerificationWebResourceResource"}}, "patch": {"scopes": ["https://www.googleapis.com/auth/siteverification"], "parameters": {"id": {"required": true, "type": "string", "location": "path"}}, "request": {"$ref": "SiteVerificationWebResourceResource"}, "id": "siteVerification.webResource.patch", "httpMethod": "PATCH", "path": "webResource/{id}", "response": {"$ref": "SiteVerificationWebResourceResource"}}, "getToken": {"scopes": ["https://www.googleapis.com/auth/siteverification"], "parameters": {"type": {"type": "string", "location": "query"}, "identifier": {"type": "string", "location": "query"}, "verificationMethod": {"type": "string", "location": "query"}}, "response": {"$ref": "SiteVerificationWebResourceGettokenResponse"}, "httpMethod": "GET", "path": "token", "id": "siteVerification.webResource.getToken"}, "delete": {"scopes": ["https://www.googleapis.com/auth/siteverification"], "parameters": {"id": {"required": true, "type": "string", "location": "path"}}, "httpMethod": "DELETE", "path": "webResource/{id}", "id": "siteVerification.webResource.delete"}}}', true));
    }
}
class SiteVerificationWebResourceGettokenRequest extends apiModel {
    public $verificationMethod;
    protected $__siteType = 'SiteVerificationWebResourceGettokenRequestSite';
    protected $__siteDataType = '';
    public $site;
    public function setVerificationMethod($verificationMethod) {
        $this->verificationMethod = $verificationMethod;
    }
    public function getVerificationMethod() {
        return $this->verificationMethod;
    }
    public function setSite(SiteVerificationWebResourceGettokenRequestSite $site) {
        $this->site = $site;
    }
    public function getSite() {
        return $this->site;
    }
}
class SiteVerificationWebResourceGettokenRequestSite extends apiModel {
    public $identifier;
    public $type;
    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }
    public function getIdentifier() {
        return $this->identifier;
    }
    public function setType($type) {
        $this->type = $type;
    }
    public function getType() {
        return $this->type;
    }
}
class SiteVerificationWebResourceGettokenResponse extends apiModel {
    public $token;
    public $method;
    public function setToken($token) {
        $this->token = $token;
    }
    public function getToken() {
        return $this->token;
    }
    public function setMethod($method) {
        $this->method = $method;
    }
    public function getMethod() {
        return $this->method;
    }
}
class SiteVerificationWebResourceListResponse extends apiModel {
    protected $__itemsType = 'SiteVerificationWebResourceResource';
    protected $__itemsDataType = 'array';
    public $items;
    public function setItems($items) {
        $this->assertIsArray($items, 'SiteVerificationWebResourceResource', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
}
class SiteVerificationWebResourceResource extends apiModel {
    public $owners;
    public $id;
    protected $__siteType = 'SiteVerificationWebResourceResourceSite';
    protected $__siteDataType = '';
    public $site;
    public function setOwners($owners) {
        $this->assertIsArray($owners, 'string', __METHOD__);
        $this->owners = $owners;
    }
    public function getOwners() {
        return $this->owners;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setSite(SiteVerificationWebResourceResourceSite $site) {
        $this->site = $site;
    }
    public function getSite() {
        return $this->site;
    }
}
class SiteVerificationWebResourceResourceSite extends apiModel {
    public $identifier;
    public $type;
    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }
    public function getIdentifier() {
        return $this->identifier;
    }
    public function setType($type) {
        $this->type = $type;
    }
    public function getType() {
        return $this->type;
    }
}
