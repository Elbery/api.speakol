<?php
require_once 'service/apiModel.php';
require_once 'service/apiService.php';
require_once 'service/apiServiceRequest.php';
class LanguagesServiceResource extends apiServiceResource {
    public function listLanguages($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new LanguagesListResponse($data);
        } else {
            return $data;
        }
    }
}
class DetectionsServiceResource extends apiServiceResource {
    public function listDetections($q, $optParams = array()) {
        $params = array('q' => $q);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new DetectionsListResponse($data);
        } else {
            return $data;
        }
    }
}
class TranslationsServiceResource extends apiServiceResource {
    public function listTranslations($q, $target, $optParams = array()) {
        $params = array('q' => $q, 'target' => $target);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new TranslationsListResponse($data);
        } else {
            return $data;
        }
    }
}
class apiTranslateService extends apiService {
    public $languages;
    public $detections;
    public $translations;
    public function __construct(apiClient $apiClient) {
        $this->rpcPath = '/rpc';
        $this->restBasePath = '/language/translate/';
        $this->version = 'v2';
        $this->serviceName = 'translate';
        $apiClient->addService($this->serviceName, $this->version);
        $this->languages = new LanguagesServiceResource($this, $this->serviceName, 'languages', json_decode('{"methods": {"list": {"parameters": {"target": {"type": "string", "location": "query"}}, "id": "language.languages.list", "httpMethod": "GET", "path": "v2/languages", "response": {"$ref": "LanguagesListResponse"}}}}', true));
        $this->detections = new DetectionsServiceResource($this, $this->serviceName, 'detections', json_decode('{"methods": {"list": {"parameters": {"q": {"repeated": true, "required": true, "type": "string", "location": "query"}}, "id": "language.detections.list", "httpMethod": "GET", "path": "v2/detect", "response": {"$ref": "DetectionsListResponse"}}}}', true));
        $this->translations = new TranslationsServiceResource($this, $this->serviceName, 'translations', json_decode('{"methods": {"list": {"parameters": {"q": {"repeated": true, "required": true, "type": "string", "location": "query"}, "source": {"type": "string", "location": "query"}, "cid": {"repeated": true, "type": "string", "location": "query"}, "target": {"required": true, "type": "string", "location": "query"}, "format": {"enum": ["html", "text"], "type": "string", "location": "query"}}, "id": "language.translations.list", "httpMethod": "GET", "path": "v2", "response": {"$ref": "TranslationsListResponse"}}}}', true));
    }
}
class DetectionsListResponse extends apiModel {
    protected $__detectionsType = 'DetectionsResourceItems';
    protected $__detectionsDataType = 'array';
    public $detections;
    public function setDetections($detections) {
        $this->assertIsArray($detections, 'DetectionsResourceItems', __METHOD__);
        $this->detections = $detections;
    }
    public function getDetections() {
        return $this->detections;
    }
}
class DetectionsResource extends apiModel {
}
class DetectionsResourceItems extends apiModel {
    public $isReliable;
    public $confidence;
    public $language;
    public function setIsReliable($isReliable) {
        $this->isReliable = $isReliable;
    }
    public function getIsReliable() {
        return $this->isReliable;
    }
    public function setConfidence($confidence) {
        $this->confidence = $confidence;
    }
    public function getConfidence() {
        return $this->confidence;
    }
    public function setLanguage($language) {
        $this->language = $language;
    }
    public function getLanguage() {
        return $this->language;
    }
}
class LanguagesListResponse extends apiModel {
    protected $__languagesType = 'LanguagesResource';
    protected $__languagesDataType = 'array';
    public $languages;
    public function setLanguages($languages) {
        $this->assertIsArray($languages, 'LanguagesResource', __METHOD__);
        $this->languages = $languages;
    }
    public function getLanguages() {
        return $this->languages;
    }
}
class LanguagesResource extends apiModel {
    public $name;
    public $language;
    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
    public function setLanguage($language) {
        $this->language = $language;
    }
    public function getLanguage() {
        return $this->language;
    }
}
class TranslationsListResponse extends apiModel {
    protected $__translationsType = 'TranslationsResource';
    protected $__translationsDataType = 'array';
    public $translations;
    public function setTranslations($translations) {
        $this->assertIsArray($translations, 'TranslationsResource', __METHOD__);
        $this->translations = $translations;
    }
    public function getTranslations() {
        return $this->translations;
    }
}
class TranslationsResource extends apiModel {
    public $detectedSourceLanguage;
    public $translatedText;
    public function setDetectedSourceLanguage($detectedSourceLanguage) {
        $this->detectedSourceLanguage = $detectedSourceLanguage;
    }
    public function getDetectedSourceLanguage() {
        return $this->detectedSourceLanguage;
    }
    public function setTranslatedText($translatedText) {
        $this->translatedText = $translatedText;
    }
    public function getTranslatedText() {
        return $this->translatedText;
    }
}
