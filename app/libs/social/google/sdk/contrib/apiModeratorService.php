<?php
require_once 'service/apiModel.php';
require_once 'service/apiService.php';
require_once 'service/apiServiceRequest.php';
class VotesServiceResource extends apiServiceResource {
    public function insert($seriesId, $submissionId, Vote $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'submissionId' => $submissionId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('insert', array($params));
        if ($this->useObjects()) {
            return new Vote($data);
        } else {
            return $data;
        }
    }
    public function patch($seriesId, $submissionId, Vote $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'submissionId' => $submissionId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('patch', array($params));
        if ($this->useObjects()) {
            return new Vote($data);
        } else {
            return $data;
        }
    }
    public function listVotes($seriesId, $optParams = array()) {
        $params = array('seriesId' => $seriesId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new VoteList($data);
        } else {
            return $data;
        }
    }
    public function update($seriesId, $submissionId, Vote $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'submissionId' => $submissionId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('update', array($params));
        if ($this->useObjects()) {
            return new Vote($data);
        } else {
            return $data;
        }
    }
    public function get($seriesId, $submissionId, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'submissionId' => $submissionId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new Vote($data);
        } else {
            return $data;
        }
    }
}
class ResponsesServiceResource extends apiServiceResource {
    public function insert($seriesId, $topicId, $parentSubmissionId, Submission $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'topicId' => $topicId, 'parentSubmissionId' => $parentSubmissionId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('insert', array($params));
        if ($this->useObjects()) {
            return new Submission($data);
        } else {
            return $data;
        }
    }
    public function listResponses($seriesId, $submissionId, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'submissionId' => $submissionId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SubmissionList($data);
        } else {
            return $data;
        }
    }
}
class TagsServiceResource extends apiServiceResource {
    public function insert($seriesId, $submissionId, Tag $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'submissionId' => $submissionId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('insert', array($params));
        if ($this->useObjects()) {
            return new Tag($data);
        } else {
            return $data;
        }
    }
    public function listTags($seriesId, $submissionId, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'submissionId' => $submissionId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new TagList($data);
        } else {
            return $data;
        }
    }
    public function delete($seriesId, $submissionId, $tagId, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'submissionId' => $submissionId, 'tagId' => $tagId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('delete', array($params));
        return $data;
    }
}
class SeriesServiceResource extends apiServiceResource {
    public function insert(Series $postBody, $optParams = array()) {
        $params = array('postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('insert', array($params));
        if ($this->useObjects()) {
            return new Series($data);
        } else {
            return $data;
        }
    }
    public function patch($seriesId, Series $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('patch', array($params));
        if ($this->useObjects()) {
            return new Series($data);
        } else {
            return $data;
        }
    }
    public function listSeries($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SeriesList($data);
        } else {
            return $data;
        }
    }
    public function update($seriesId, Series $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('update', array($params));
        if ($this->useObjects()) {
            return new Series($data);
        } else {
            return $data;
        }
    }
    public function get($seriesId, $optParams = array()) {
        $params = array('seriesId' => $seriesId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new Series($data);
        } else {
            return $data;
        }
    }
}
class SeriesSubmissionsServiceResource extends apiServiceResource {
    public function listSeriesSubmissions($seriesId, $optParams = array()) {
        $params = array('seriesId' => $seriesId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SubmissionList($data);
        } else {
            return $data;
        }
    }
}
class SeriesResponsesServiceResource extends apiServiceResource {
    public function listSeriesResponses($seriesId, $optParams = array()) {
        $params = array('seriesId' => $seriesId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SeriesList($data);
        } else {
            return $data;
        }
    }
}
class TopicsServiceResource extends apiServiceResource {
    public function insert($seriesId, Topic $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('insert', array($params));
        if ($this->useObjects()) {
            return new Topic($data);
        } else {
            return $data;
        }
    }
    public function listTopics($seriesId, $optParams = array()) {
        $params = array('seriesId' => $seriesId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new TopicList($data);
        } else {
            return $data;
        }
    }
    public function update($seriesId, $topicId, Topic $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'topicId' => $topicId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('update', array($params));
        if ($this->useObjects()) {
            return new Topic($data);
        } else {
            return $data;
        }
    }
    public function get($seriesId, $topicId, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'topicId' => $topicId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new Topic($data);
        } else {
            return $data;
        }
    }
}
class TopicsSubmissionsServiceResource extends apiServiceResource {
    public function listTopicsSubmissions($seriesId, $topicId, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'topicId' => $topicId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SubmissionList($data);
        } else {
            return $data;
        }
    }
}
class ModeratorGlobalServiceResource extends apiServiceResource {
}
class ModeratorGlobalSeriesServiceResource extends apiServiceResource {
    public function listModeratorGlobalSeries($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SeriesList($data);
        } else {
            return $data;
        }
    }
}
class ProfilesServiceResource extends apiServiceResource {
    public function patch(Profile $postBody, $optParams = array()) {
        $params = array('postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('patch', array($params));
        if ($this->useObjects()) {
            return new Profile($data);
        } else {
            return $data;
        }
    }
    public function update(Profile $postBody, $optParams = array()) {
        $params = array('postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('update', array($params));
        if ($this->useObjects()) {
            return new Profile($data);
        } else {
            return $data;
        }
    }
    public function get($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new Profile($data);
        } else {
            return $data;
        }
    }
}
class FeaturedServiceResource extends apiServiceResource {
}
class FeaturedSeriesServiceResource extends apiServiceResource {
    public function listFeaturedSeries($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SeriesList($data);
        } else {
            return $data;
        }
    }
}
class MyrecentServiceResource extends apiServiceResource {
}
class MyrecentSeriesServiceResource extends apiServiceResource {
    public function listMyrecentSeries($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SeriesList($data);
        } else {
            return $data;
        }
    }
}
class MyServiceResource extends apiServiceResource {
}
class MySeriesServiceResource extends apiServiceResource {
    public function listMySeries($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        $data = $this->__call('list', array($params));
        if ($this->useObjects()) {
            return new SeriesList($data);
        } else {
            return $data;
        }
    }
}
class SubmissionsServiceResource extends apiServiceResource {
    public function insert($seriesId, $topicId, Submission $postBody, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'topicId' => $topicId, 'postBody' => $postBody);
        $params = array_merge($params, $optParams);
        $data = $this->__call('insert', array($params));
        if ($this->useObjects()) {
            return new Submission($data);
        } else {
            return $data;
        }
    }
    public function get($seriesId, $submissionId, $optParams = array()) {
        $params = array('seriesId' => $seriesId, 'submissionId' => $submissionId);
        $params = array_merge($params, $optParams);
        $data = $this->__call('get', array($params));
        if ($this->useObjects()) {
            return new Submission($data);
        } else {
            return $data;
        }
    }
}
class apiModeratorService extends apiService {
    public $votes;
    public $responses;
    public $tags;
    public $series;
    public $series_submissions;
    public $series_submissions_submissions;
    public $series_submissions_responses;
    public $series_responses;
    public $series_responses_submissions;
    public $series_responses_responses;
    public $topics;
    public $topics_submissions;
    public $topics_submissions_submissions;
    public $global;
    public $global_series;
    public $global_series_series;
    public $profiles;
    public $featured;
    public $featured_series;
    public $featured_series_series;
    public $myrecent;
    public $myrecent_series;
    public $myrecent_series_series;
    public $my;
    public $my_series;
    public $my_series_series;
    public $submissions;
    public function __construct(apiClient $apiClient) {
        $this->rpcPath = '/rpc';
        $this->restBasePath = '/moderator/v1/';
        $this->version = 'v1';
        $this->serviceName = 'moderator';
        $apiClient->addService($this->serviceName, $this->version);
        $this->votes = new VotesServiceResource($this, $this->serviceName, 'votes', json_decode('{"methods": {"insert": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "unauthToken": {"type": "string", "location": "query"}, "submissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "request": {"$ref": "Vote"}, "id": "moderator.votes.insert", "httpMethod": "POST", "path": "series/{seriesId}/submissions/{submissionId}/votes/@me", "response": {"$ref": "Vote"}}, "get": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "userId": {"type": "string", "location": "query"}, "unauthToken": {"type": "string", "location": "query"}, "submissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "id": "moderator.votes.get", "httpMethod": "GET", "path": "series/{seriesId}/submissions/{submissionId}/votes/@me", "response": {"$ref": "Vote"}}, "list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"max-results": {"format": "uint32", "type": "integer", "location": "query"}, "seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "start-index": {"format": "uint32", "type": "integer", "location": "query"}}, "id": "moderator.votes.list", "httpMethod": "GET", "path": "series/{seriesId}/votes/@me", "response": {"$ref": "VoteList"}}, "update": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "userId": {"type": "string", "location": "query"}, "unauthToken": {"type": "string", "location": "query"}, "submissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "request": {"$ref": "Vote"}, "id": "moderator.votes.update", "httpMethod": "PUT", "path": "series/{seriesId}/submissions/{submissionId}/votes/@me", "response": {"$ref": "Vote"}}, "patch": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "userId": {"type": "string", "location": "query"}, "unauthToken": {"type": "string", "location": "query"}, "submissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "request": {"$ref": "Vote"}, "id": "moderator.votes.patch", "httpMethod": "PATCH", "path": "series/{seriesId}/submissions/{submissionId}/votes/@me", "response": {"$ref": "Vote"}}}}', true));
        $this->responses = new ResponsesServiceResource($this, $this->serviceName, 'responses', json_decode('{"methods": {"insert": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "parentSubmissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "unauthToken": {"type": "string", "location": "query"}, "anonymous": {"type": "boolean", "location": "query"}, "topicId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "request": {"$ref": "Submission"}, "id": "moderator.responses.insert", "httpMethod": "POST", "path": "series/{seriesId}/topics/{topicId}/submissions/{parentSubmissionId}/responses", "response": {"$ref": "Submission"}}, "list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"max-results": {"format": "uint32", "type": "integer", "location": "query"}, "sort": {"type": "string", "location": "query"}, "seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "author": {"type": "string", "location": "query"}, "start-index": {"format": "uint32", "type": "integer", "location": "query"}, "submissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "q": {"type": "string", "location": "query"}, "hasAttachedVideo": {"type": "boolean", "location": "query"}}, "id": "moderator.responses.list", "httpMethod": "GET", "path": "series/{seriesId}/submissions/{submissionId}/responses", "response": {"$ref": "SubmissionList"}}}}', true));
        $this->tags = new TagsServiceResource($this, $this->serviceName, 'tags', json_decode('{"methods": {"insert": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "submissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "request": {"$ref": "Tag"}, "id": "moderator.tags.insert", "httpMethod": "POST", "path": "series/{seriesId}/submissions/{submissionId}/tags", "response": {"$ref": "Tag"}}, "list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "submissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "id": "moderator.tags.list", "httpMethod": "GET", "path": "series/{seriesId}/submissions/{submissionId}/tags", "response": {"$ref": "TagList"}}, "delete": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "tagId": {"required": true, "type": "string", "location": "path"}, "submissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "httpMethod": "DELETE", "path": "series/{seriesId}/submissions/{submissionId}/tags/{tagId}", "id": "moderator.tags.delete"}}}', true));
        $this->series = new SeriesServiceResource($this, $this->serviceName, 'series', json_decode('{"methods": {"insert": {"scopes": ["https://www.googleapis.com/auth/moderator"], "request": {"$ref": "Series"}, "response": {"$ref": "Series"}, "httpMethod": "POST", "path": "series", "id": "moderator.series.insert"}, "get": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "id": "moderator.series.get", "httpMethod": "GET", "path": "series/{seriesId}", "response": {"$ref": "Series"}}, "list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"max-results": {"format": "uint32", "type": "integer", "location": "query"}, "q": {"type": "string", "location": "query"}, "start-index": {"format": "uint32", "type": "integer", "location": "query"}}, "response": {"$ref": "SeriesList"}, "httpMethod": "GET", "path": "series", "id": "moderator.series.list"}, "update": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "request": {"$ref": "Series"}, "id": "moderator.series.update", "httpMethod": "PUT", "path": "series/{seriesId}", "response": {"$ref": "Series"}}, "patch": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "request": {"$ref": "Series"}, "id": "moderator.series.patch", "httpMethod": "PATCH", "path": "series/{seriesId}", "response": {"$ref": "Series"}}}}', true));
        $this->series_submissions = new SeriesSubmissionsServiceResource($this, $this->serviceName, 'submissions', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"lang": {"type": "string", "location": "query"}, "max-results": {"format": "uint32", "type": "integer", "location": "query"}, "seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "author": {"type": "string", "location": "query"}, "start-index": {"format": "uint32", "type": "integer", "location": "query"}, "includeVotes": {"type": "boolean", "location": "query"}, "sort": {"type": "string", "location": "query"}, "q": {"type": "string", "location": "query"}, "hasAttachedVideo": {"type": "boolean", "location": "query"}}, "id": "moderator.series.submissions.list", "httpMethod": "GET", "path": "series/{seriesId}/submissions", "response": {"$ref": "SubmissionList"}}}}', true));
        $this->series_responses = new SeriesResponsesServiceResource($this, $this->serviceName, 'responses', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"max-results": {"format": "uint32", "type": "integer", "location": "query"}, "sort": {"type": "string", "location": "query"}, "seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "author": {"type": "string", "location": "query"}, "start-index": {"format": "uint32", "type": "integer", "location": "query"}, "q": {"type": "string", "location": "query"}, "hasAttachedVideo": {"type": "boolean", "location": "query"}}, "id": "moderator.series.responses.list", "httpMethod": "GET", "path": "series/{seriesId}/responses", "response": {"$ref": "SeriesList"}}}}', true));
        $this->topics = new TopicsServiceResource($this, $this->serviceName, 'topics', json_decode('{"methods": {"insert": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "request": {"$ref": "Topic"}, "id": "moderator.topics.insert", "httpMethod": "POST", "path": "series/{seriesId}/topics", "response": {"$ref": "Topic"}}, "list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"max-results": {"format": "uint32", "type": "integer", "location": "query"}, "q": {"type": "string", "location": "query"}, "start-index": {"format": "uint32", "type": "integer", "location": "query"}, "mode": {"type": "string", "location": "query"}, "seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "id": "moderator.topics.list", "httpMethod": "GET", "path": "series/{seriesId}/topics", "response": {"$ref": "TopicList"}}, "update": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "topicId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "request": {"$ref": "Topic"}, "id": "moderator.topics.update", "httpMethod": "PUT", "path": "series/{seriesId}/topics/{topicId}", "response": {"$ref": "Topic"}}, "get": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "topicId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "id": "moderator.topics.get", "httpMethod": "GET", "path": "series/{seriesId}/topics/{topicId}", "response": {"$ref": "Topic"}}}}', true));
        $this->topics_submissions = new TopicsSubmissionsServiceResource($this, $this->serviceName, 'submissions', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"max-results": {"format": "uint32", "type": "integer", "location": "query"}, "seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "includeVotes": {"type": "boolean", "location": "query"}, "topicId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "start-index": {"format": "uint32", "type": "integer", "location": "query"}, "author": {"type": "string", "location": "query"}, "sort": {"type": "string", "location": "query"}, "q": {"type": "string", "location": "query"}, "hasAttachedVideo": {"type": "boolean", "location": "query"}}, "id": "moderator.topics.submissions.list", "httpMethod": "GET", "path": "series/{seriesId}/topics/{topicId}/submissions", "response": {"$ref": "SubmissionList"}}}}', true));
        $this->global = new ModeratorGlobalServiceResource($this, $this->serviceName, 'global', json_decode('{}', true));
        $this->global_series = new ModeratorGlobalSeriesServiceResource($this, $this->serviceName, 'series', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"max-results": {"format": "uint32", "type": "integer", "location": "query"}, "q": {"type": "string", "location": "query"}, "start-index": {"format": "uint32", "type": "integer", "location": "query"}}, "response": {"$ref": "SeriesList"}, "httpMethod": "GET", "path": "search", "id": "moderator.global.series.list"}}}', true));
        $this->profiles = new ProfilesServiceResource($this, $this->serviceName, 'profiles', json_decode('{"methods": {"get": {"scopes": ["https://www.googleapis.com/auth/moderator"], "id": "moderator.profiles.get", "httpMethod": "GET", "path": "profiles/@me", "response": {"$ref": "Profile"}}, "update": {"scopes": ["https://www.googleapis.com/auth/moderator"], "request": {"$ref": "Profile"}, "response": {"$ref": "Profile"}, "httpMethod": "PUT", "path": "profiles/@me", "id": "moderator.profiles.update"}, "patch": {"scopes": ["https://www.googleapis.com/auth/moderator"], "request": {"$ref": "Profile"}, "response": {"$ref": "Profile"}, "httpMethod": "PATCH", "path": "profiles/@me", "id": "moderator.profiles.patch"}}}', true));
        $this->featured = new FeaturedServiceResource($this, $this->serviceName, 'featured', json_decode('{}', true));
        $this->featured_series = new FeaturedSeriesServiceResource($this, $this->serviceName, 'series', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "id": "moderator.featured.series.list", "httpMethod": "GET", "path": "series/featured", "response": {"$ref": "SeriesList"}}}}', true));
        $this->myrecent = new MyrecentServiceResource($this, $this->serviceName, 'myrecent', json_decode('{}', true));
        $this->myrecent_series = new MyrecentSeriesServiceResource($this, $this->serviceName, 'series', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "id": "moderator.myrecent.series.list", "httpMethod": "GET", "path": "series/@me/recent", "response": {"$ref": "SeriesList"}}}}', true));
        $this->my = new MyServiceResource($this, $this->serviceName, 'my', json_decode('{}', true));
        $this->my_series = new MySeriesServiceResource($this, $this->serviceName, 'series', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/moderator"], "id": "moderator.my.series.list", "httpMethod": "GET", "path": "series/@me/mine", "response": {"$ref": "SeriesList"}}}}', true));
        $this->submissions = new SubmissionsServiceResource($this, $this->serviceName, 'submissions', json_decode('{"methods": {"insert": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "topicId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "unauthToken": {"type": "string", "location": "query"}, "anonymous": {"type": "boolean", "location": "query"}}, "request": {"$ref": "Submission"}, "id": "moderator.submissions.insert", "httpMethod": "POST", "path": "series/{seriesId}/topics/{topicId}/submissions", "response": {"$ref": "Submission"}}, "get": {"scopes": ["https://www.googleapis.com/auth/moderator"], "parameters": {"lang": {"type": "string", "location": "query"}, "seriesId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}, "includeVotes": {"type": "boolean", "location": "query"}, "submissionId": {"format": "uint32", "required": true, "type": "integer", "location": "path"}}, "id": "moderator.submissions.get", "httpMethod": "GET", "path": "series/{seriesId}/submissions/{submissionId}", "response": {"$ref": "Submission"}}}}', true));
    }
}
class ModeratorTopicsResourcePartial extends apiModel {
    protected $__idType = 'ModeratorTopicsResourcePartialId';
    protected $__idDataType = '';
    public $id;
    public function setId(ModeratorTopicsResourcePartialId $id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
}
class ModeratorTopicsResourcePartialId extends apiModel {
    public $seriesId;
    public $topicId;
    public function setSeriesId($seriesId) {
        $this->seriesId = $seriesId;
    }
    public function getSeriesId() {
        return $this->seriesId;
    }
    public function setTopicId($topicId) {
        $this->topicId = $topicId;
    }
    public function getTopicId() {
        return $this->topicId;
    }
}
class ModeratorVotesResourcePartial extends apiModel {
    public $vote;
    public $flag;
    public function setVote($vote) {
        $this->vote = $vote;
    }
    public function getVote() {
        return $this->vote;
    }
    public function setFlag($flag) {
        $this->flag = $flag;
    }
    public function getFlag() {
        return $this->flag;
    }
}
class Profile extends apiModel {
    public $kind;
    protected $__attributionType = 'ProfileAttribution';
    protected $__attributionDataType = '';
    public $attribution;
    protected $__idType = 'ProfileId';
    protected $__idDataType = '';
    public $id;
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setAttribution(ProfileAttribution $attribution) {
        $this->attribution = $attribution;
    }
    public function getAttribution() {
        return $this->attribution;
    }
    public function setId(ProfileId $id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
}
class ProfileAttribution extends apiModel {
    protected $__geoType = 'ProfileAttributionGeo';
    protected $__geoDataType = '';
    public $geo;
    public $displayName;
    public $location;
    public $avatarUrl;
    public function setGeo(ProfileAttributionGeo $geo) {
        $this->geo = $geo;
    }
    public function getGeo() {
        return $this->geo;
    }
    public function setDisplayName($displayName) {
        $this->displayName = $displayName;
    }
    public function getDisplayName() {
        return $this->displayName;
    }
    public function setLocation($location) {
        $this->location = $location;
    }
    public function getLocation() {
        return $this->location;
    }
    public function setAvatarUrl($avatarUrl) {
        $this->avatarUrl = $avatarUrl;
    }
    public function getAvatarUrl() {
        return $this->avatarUrl;
    }
}
class ProfileAttributionGeo extends apiModel {
    public $latitude;
    public $location;
    public $longitude;
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }
    public function getLatitude() {
        return $this->latitude;
    }
    public function setLocation($location) {
        $this->location = $location;
    }
    public function getLocation() {
        return $this->location;
    }
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }
    public function getLongitude() {
        return $this->longitude;
    }
}
class ProfileId extends apiModel {
    public $user;
    public function setUser($user) {
        $this->user = $user;
    }
    public function getUser() {
        return $this->user;
    }
}
class Series extends apiModel {
    public $kind;
    public $description;
    protected $__rulesType = 'SeriesRules';
    protected $__rulesDataType = '';
    public $rules;
    public $unauthVotingAllowed;
    public $videoSubmissionAllowed;
    public $name;
    public $numTopics;
    public $anonymousSubmissionAllowed;
    public $unauthSubmissionAllowed;
    protected $__idType = 'SeriesId';
    protected $__idDataType = '';
    public $id;
    protected $__countersType = 'SeriesCounters';
    protected $__countersDataType = '';
    public $counters;
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setDescription($description) {
        $this->description = $description;
    }
    public function getDescription() {
        return $this->description;
    }
    public function setRules(SeriesRules $rules) {
        $this->rules = $rules;
    }
    public function getRules() {
        return $this->rules;
    }
    public function setUnauthVotingAllowed($unauthVotingAllowed) {
        $this->unauthVotingAllowed = $unauthVotingAllowed;
    }
    public function getUnauthVotingAllowed() {
        return $this->unauthVotingAllowed;
    }
    public function setVideoSubmissionAllowed($videoSubmissionAllowed) {
        $this->videoSubmissionAllowed = $videoSubmissionAllowed;
    }
    public function getVideoSubmissionAllowed() {
        return $this->videoSubmissionAllowed;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
    public function setNumTopics($numTopics) {
        $this->numTopics = $numTopics;
    }
    public function getNumTopics() {
        return $this->numTopics;
    }
    public function setAnonymousSubmissionAllowed($anonymousSubmissionAllowed) {
        $this->anonymousSubmissionAllowed = $anonymousSubmissionAllowed;
    }
    public function getAnonymousSubmissionAllowed() {
        return $this->anonymousSubmissionAllowed;
    }
    public function setUnauthSubmissionAllowed($unauthSubmissionAllowed) {
        $this->unauthSubmissionAllowed = $unauthSubmissionAllowed;
    }
    public function getUnauthSubmissionAllowed() {
        return $this->unauthSubmissionAllowed;
    }
    public function setId(SeriesId $id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setCounters(SeriesCounters $counters) {
        $this->counters = $counters;
    }
    public function getCounters() {
        return $this->counters;
    }
}
class SeriesCounters extends apiModel {
    public $users;
    public $noneVotes;
    public $videoSubmissions;
    public $minusVotes;
    public $anonymousSubmissions;
    public $submissions;
    public $plusVotes;
    public function setUsers($users) {
        $this->users = $users;
    }
    public function getUsers() {
        return $this->users;
    }
    public function setNoneVotes($noneVotes) {
        $this->noneVotes = $noneVotes;
    }
    public function getNoneVotes() {
        return $this->noneVotes;
    }
    public function setVideoSubmissions($videoSubmissions) {
        $this->videoSubmissions = $videoSubmissions;
    }
    public function getVideoSubmissions() {
        return $this->videoSubmissions;
    }
    public function setMinusVotes($minusVotes) {
        $this->minusVotes = $minusVotes;
    }
    public function getMinusVotes() {
        return $this->minusVotes;
    }
    public function setAnonymousSubmissions($anonymousSubmissions) {
        $this->anonymousSubmissions = $anonymousSubmissions;
    }
    public function getAnonymousSubmissions() {
        return $this->anonymousSubmissions;
    }
    public function setSubmissions($submissions) {
        $this->submissions = $submissions;
    }
    public function getSubmissions() {
        return $this->submissions;
    }
    public function setPlusVotes($plusVotes) {
        $this->plusVotes = $plusVotes;
    }
    public function getPlusVotes() {
        return $this->plusVotes;
    }
}
class SeriesId extends apiModel {
    public $seriesId;
    public function setSeriesId($seriesId) {
        $this->seriesId = $seriesId;
    }
    public function getSeriesId() {
        return $this->seriesId;
    }
}
class SeriesList extends apiModel {
    protected $__itemsType = 'Series';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public function setItems($items) {
        $this->assertIsArray($items, 'Series', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
}
class SeriesRules extends apiModel {
    protected $__votesType = 'SeriesRulesVotes';
    protected $__votesDataType = '';
    public $votes;
    protected $__submissionsType = 'SeriesRulesSubmissions';
    protected $__submissionsDataType = '';
    public $submissions;
    public function setVotes(SeriesRulesVotes $votes) {
        $this->votes = $votes;
    }
    public function getVotes() {
        return $this->votes;
    }
    public function setSubmissions(SeriesRulesSubmissions $submissions) {
        $this->submissions = $submissions;
    }
    public function getSubmissions() {
        return $this->submissions;
    }
}
class SeriesRulesSubmissions extends apiModel {
    public $close;
    public $open;
    public function setClose($close) {
        $this->close = $close;
    }
    public function getClose() {
        return $this->close;
    }
    public function setOpen($open) {
        $this->open = $open;
    }
    public function getOpen() {
        return $this->open;
    }
}
class SeriesRulesVotes extends apiModel {
    public $close;
    public $open;
    public function setClose($close) {
        $this->close = $close;
    }
    public function getClose() {
        return $this->close;
    }
    public function setOpen($open) {
        $this->open = $open;
    }
    public function getOpen() {
        return $this->open;
    }
}
class Submission extends apiModel {
    public $kind;
    protected $__attributionType = 'SubmissionAttribution';
    protected $__attributionDataType = '';
    public $attribution;
    public $created;
    public $text;
    protected $__topicsType = 'ModeratorTopicsResourcePartial';
    protected $__topicsDataType = 'array';
    public $topics;
    public $author;
    protected $__translationsType = 'SubmissionTranslations';
    protected $__translationsDataType = 'array';
    public $translations;
    protected $__parentSubmissionIdType = 'SubmissionParentSubmissionId';
    protected $__parentSubmissionIdDataType = '';
    public $parentSubmissionId;
    protected $__voteType = 'ModeratorVotesResourcePartial';
    protected $__voteDataType = '';
    public $vote;
    public $attachmentUrl;
    protected $__geoType = 'SubmissionGeo';
    protected $__geoDataType = '';
    public $geo;
    protected $__idType = 'SubmissionId';
    protected $__idDataType = '';
    public $id;
    protected $__countersType = 'SubmissionCounters';
    protected $__countersDataType = '';
    public $counters;
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setAttribution(SubmissionAttribution $attribution) {
        $this->attribution = $attribution;
    }
    public function getAttribution() {
        return $this->attribution;
    }
    public function setCreated($created) {
        $this->created = $created;
    }
    public function getCreated() {
        return $this->created;
    }
    public function setText($text) {
        $this->text = $text;
    }
    public function getText() {
        return $this->text;
    }
    public function setTopics($topics) {
        $this->assertIsArray($topics, 'ModeratorTopicsResourcePartial', __METHOD__);
        $this->topics = $topics;
    }
    public function getTopics() {
        return $this->topics;
    }
    public function setAuthor($author) {
        $this->author = $author;
    }
    public function getAuthor() {
        return $this->author;
    }
    public function setTranslations($translations) {
        $this->assertIsArray($translations, 'SubmissionTranslations', __METHOD__);
        $this->translations = $translations;
    }
    public function getTranslations() {
        return $this->translations;
    }
    public function setParentSubmissionId(SubmissionParentSubmissionId $parentSubmissionId) {
        $this->parentSubmissionId = $parentSubmissionId;
    }
    public function getParentSubmissionId() {
        return $this->parentSubmissionId;
    }
    public function setVote(ModeratorVotesResourcePartial $vote) {
        $this->vote = $vote;
    }
    public function getVote() {
        return $this->vote;
    }
    public function setAttachmentUrl($attachmentUrl) {
        $this->attachmentUrl = $attachmentUrl;
    }
    public function getAttachmentUrl() {
        return $this->attachmentUrl;
    }
    public function setGeo(SubmissionGeo $geo) {
        $this->geo = $geo;
    }
    public function getGeo() {
        return $this->geo;
    }
    public function setId(SubmissionId $id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setCounters(SubmissionCounters $counters) {
        $this->counters = $counters;
    }
    public function getCounters() {
        return $this->counters;
    }
}
class SubmissionAttribution extends apiModel {
    public $displayName;
    public $location;
    public $avatarUrl;
    public function setDisplayName($displayName) {
        $this->displayName = $displayName;
    }
    public function getDisplayName() {
        return $this->displayName;
    }
    public function setLocation($location) {
        $this->location = $location;
    }
    public function getLocation() {
        return $this->location;
    }
    public function setAvatarUrl($avatarUrl) {
        $this->avatarUrl = $avatarUrl;
    }
    public function getAvatarUrl() {
        return $this->avatarUrl;
    }
}
class SubmissionCounters extends apiModel {
    public $noneVotes;
    public $minusVotes;
    public $plusVotes;
    public function setNoneVotes($noneVotes) {
        $this->noneVotes = $noneVotes;
    }
    public function getNoneVotes() {
        return $this->noneVotes;
    }
    public function setMinusVotes($minusVotes) {
        $this->minusVotes = $minusVotes;
    }
    public function getMinusVotes() {
        return $this->minusVotes;
    }
    public function setPlusVotes($plusVotes) {
        $this->plusVotes = $plusVotes;
    }
    public function getPlusVotes() {
        return $this->plusVotes;
    }
}
class SubmissionGeo extends apiModel {
    public $latitude;
    public $location;
    public $longitude;
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }
    public function getLatitude() {
        return $this->latitude;
    }
    public function setLocation($location) {
        $this->location = $location;
    }
    public function getLocation() {
        return $this->location;
    }
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }
    public function getLongitude() {
        return $this->longitude;
    }
}
class SubmissionId extends apiModel {
    public $seriesId;
    public $submissionId;
    public function setSeriesId($seriesId) {
        $this->seriesId = $seriesId;
    }
    public function getSeriesId() {
        return $this->seriesId;
    }
    public function setSubmissionId($submissionId) {
        $this->submissionId = $submissionId;
    }
    public function getSubmissionId() {
        return $this->submissionId;
    }
}
class SubmissionList extends apiModel {
    protected $__itemsType = 'Submission';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public function setItems($items) {
        $this->assertIsArray($items, 'Submission', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
}
class SubmissionParentSubmissionId extends apiModel {
    public $seriesId;
    public $submissionId;
    public function setSeriesId($seriesId) {
        $this->seriesId = $seriesId;
    }
    public function getSeriesId() {
        return $this->seriesId;
    }
    public function setSubmissionId($submissionId) {
        $this->submissionId = $submissionId;
    }
    public function getSubmissionId() {
        return $this->submissionId;
    }
}
class SubmissionTranslations extends apiModel {
    public $lang;
    public $text;
    public function setLang($lang) {
        $this->lang = $lang;
    }
    public function getLang() {
        return $this->lang;
    }
    public function setText($text) {
        $this->text = $text;
    }
    public function getText() {
        return $this->text;
    }
}
class Tag extends apiModel {
    public $text;
    public $kind;
    protected $__idType = 'TagId';
    protected $__idDataType = '';
    public $id;
    public function setText($text) {
        $this->text = $text;
    }
    public function getText() {
        return $this->text;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setId(TagId $id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
}
class TagId extends apiModel {
    public $seriesId;
    public $tagId;
    public $submissionId;
    public function setSeriesId($seriesId) {
        $this->seriesId = $seriesId;
    }
    public function getSeriesId() {
        return $this->seriesId;
    }
    public function setTagId($tagId) {
        $this->tagId = $tagId;
    }
    public function getTagId() {
        return $this->tagId;
    }
    public function setSubmissionId($submissionId) {
        $this->submissionId = $submissionId;
    }
    public function getSubmissionId() {
        return $this->submissionId;
    }
}
class TagList extends apiModel {
    protected $__itemsType = 'Tag';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public function setItems($items) {
        $this->assertIsArray($items, 'Tag', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
}
class Topic extends apiModel {
    public $kind;
    public $description;
    protected $__rulesType = 'TopicRules';
    protected $__rulesDataType = '';
    public $rules;
    protected $__featuredSubmissionType = 'Submission';
    protected $__featuredSubmissionDataType = '';
    public $featuredSubmission;
    public $presenter;
    protected $__countersType = 'TopicCounters';
    protected $__countersDataType = '';
    public $counters;
    protected $__idType = 'TopicId';
    protected $__idDataType = '';
    public $id;
    public $name;
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
    public function setDescription($description) {
        $this->description = $description;
    }
    public function getDescription() {
        return $this->description;
    }
    public function setRules(TopicRules $rules) {
        $this->rules = $rules;
    }
    public function getRules() {
        return $this->rules;
    }
    public function setFeaturedSubmission(Submission $featuredSubmission) {
        $this->featuredSubmission = $featuredSubmission;
    }
    public function getFeaturedSubmission() {
        return $this->featuredSubmission;
    }
    public function setPresenter($presenter) {
        $this->presenter = $presenter;
    }
    public function getPresenter() {
        return $this->presenter;
    }
    public function setCounters(TopicCounters $counters) {
        $this->counters = $counters;
    }
    public function getCounters() {
        return $this->counters;
    }
    public function setId(TopicId $id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
}
class TopicCounters extends apiModel {
    public $users;
    public $noneVotes;
    public $videoSubmissions;
    public $minusVotes;
    public $submissions;
    public $plusVotes;
    public function setUsers($users) {
        $this->users = $users;
    }
    public function getUsers() {
        return $this->users;
    }
    public function setNoneVotes($noneVotes) {
        $this->noneVotes = $noneVotes;
    }
    public function getNoneVotes() {
        return $this->noneVotes;
    }
    public function setVideoSubmissions($videoSubmissions) {
        $this->videoSubmissions = $videoSubmissions;
    }
    public function getVideoSubmissions() {
        return $this->videoSubmissions;
    }
    public function setMinusVotes($minusVotes) {
        $this->minusVotes = $minusVotes;
    }
    public function getMinusVotes() {
        return $this->minusVotes;
    }
    public function setSubmissions($submissions) {
        $this->submissions = $submissions;
    }
    public function getSubmissions() {
        return $this->submissions;
    }
    public function setPlusVotes($plusVotes) {
        $this->plusVotes = $plusVotes;
    }
    public function getPlusVotes() {
        return $this->plusVotes;
    }
}
class TopicId extends apiModel {
    public $seriesId;
    public $topicId;
    public function setSeriesId($seriesId) {
        $this->seriesId = $seriesId;
    }
    public function getSeriesId() {
        return $this->seriesId;
    }
    public function setTopicId($topicId) {
        $this->topicId = $topicId;
    }
    public function getTopicId() {
        return $this->topicId;
    }
}
class TopicList extends apiModel {
    protected $__itemsType = 'Topic';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public function setItems($items) {
        $this->assertIsArray($items, 'Topic', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
}
class TopicRules extends apiModel {
    protected $__votesType = 'TopicRulesVotes';
    protected $__votesDataType = '';
    public $votes;
    protected $__submissionsType = 'TopicRulesSubmissions';
    protected $__submissionsDataType = '';
    public $submissions;
    public function setVotes(TopicRulesVotes $votes) {
        $this->votes = $votes;
    }
    public function getVotes() {
        return $this->votes;
    }
    public function setSubmissions(TopicRulesSubmissions $submissions) {
        $this->submissions = $submissions;
    }
    public function getSubmissions() {
        return $this->submissions;
    }
}
class TopicRulesSubmissions extends apiModel {
    public $close;
    public $open;
    public function setClose($close) {
        $this->close = $close;
    }
    public function getClose() {
        return $this->close;
    }
    public function setOpen($open) {
        $this->open = $open;
    }
    public function getOpen() {
        return $this->open;
    }
}
class TopicRulesVotes extends apiModel {
    public $close;
    public $open;
    public function setClose($close) {
        $this->close = $close;
    }
    public function getClose() {
        return $this->close;
    }
    public function setOpen($open) {
        $this->open = $open;
    }
    public function getOpen() {
        return $this->open;
    }
}
class Vote extends apiModel {
    public $vote;
    public $flag;
    protected $__idType = 'VoteId';
    protected $__idDataType = '';
    public $id;
    public $kind;
    public function setVote($vote) {
        $this->vote = $vote;
    }
    public function getVote() {
        return $this->vote;
    }
    public function setFlag($flag) {
        $this->flag = $flag;
    }
    public function getFlag() {
        return $this->flag;
    }
    public function setId(VoteId $id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
}
class VoteId extends apiModel {
    public $seriesId;
    public $submissionId;
    public function setSeriesId($seriesId) {
        $this->seriesId = $seriesId;
    }
    public function getSeriesId() {
        return $this->seriesId;
    }
    public function setSubmissionId($submissionId) {
        $this->submissionId = $submissionId;
    }
    public function getSubmissionId() {
        return $this->submissionId;
    }
}
class VoteList extends apiModel {
    protected $__itemsType = 'Vote';
    protected $__itemsDataType = 'array';
    public $items;
    public $kind;
    public function setItems($items) {
        $this->assertIsArray($items, 'Vote', __METHOD__);
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
    public function setKind($kind) {
        $this->kind = $kind;
    }
    public function getKind() {
        return $this->kind;
    }
}
