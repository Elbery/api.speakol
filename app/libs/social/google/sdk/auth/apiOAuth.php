<?php
require_once "external/OAuth.php";
class apiOAuth extends apiAuth {
    public $cacheKey;
    protected $consumerToken;
    protected $accessToken;
    protected $privateKeyFile;
    protected $developerKey;
    public $service;
    public function __construct() {
        global $apiConfig;
        if (!empty($apiConfig['developer_key'])) {
            $this->setDeveloperKey($apiConfig['developer_key']);
        }
        $this->consumerToken = new apiClientOAuthConsumer($apiConfig['oauth_consumer_key'], $apiConfig['oauth_consumer_secret'], NULL);
        $this->signatureMethod = new apiClientOAuthSignatureMethod_HMAC_SHA1();
        $this->cacheKey = 'OAuth:' . $apiConfig['oauth_consumer_key'];
    }
    public function authenticate($service) {
        global $apiConfig;
        $this->service = $service;
        $this->service['authorization_token_url'].= '?scope=' . apiClientOAuthUtil::urlencodeRFC3986($service['scope']) . '&domain=' . apiClientOAuthUtil::urlencodeRFC3986($apiConfig['site_name']) . '&oauth_token=';
        if (isset($_GET['oauth_verifier']) && isset($_GET['oauth_token']) && isset($_GET['uid'])) {
            $uid = $_GET['uid'];
            $secret = apiClient::$cache->get($this->cacheKey . ":nonce:" . $uid);
            apiClient::$cache->delete($this->cacheKey . ":nonce:" . $uid);
            $token = $this->upgradeRequestToken($_GET['oauth_token'], $secret, $_GET['oauth_verifier']);
            return json_encode($token);
        } else {
            $callbackUrl = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $uid = uniqid();
            $token = $this->obtainRequestToken($callbackUrl, $uid);
            apiClient::$cache->set($this->cacheKey . ":nonce:" . $uid, $token->secret);
            $this->redirectToAuthorization($token);
        }
    }
    public function setAccessToken($accessToken) {
        $accessToken = json_decode($accessToken, true);
        if ($accessToken == null) {
            throw new apiAuthException("Could not json decode the access token");
        }
        if (!isset($accessToken['key']) || !isset($accessToken['secret'])) {
            throw new apiAuthException("Invalid OAuth token, missing key and/or secret");
        }
        $this->accessToken = new apiClientOAuthConsumer($accessToken['key'], $accessToken['secret']);
    }
    public function getAccessToken() {
        return $this->accessToken;
    }
    public function setDeveloperKey($developerKey) {
        $this->developerKey = $developerKey;
    }
    public function upgradeRequestToken($requestToken, $requestTokenSecret, $oauthVerifier) {
        $ret = $this->requestAccessToken($requestToken, $requestTokenSecret, $oauthVerifier);
        $matches = array();
        @parse_str($ret, $matches);
        if (!isset($matches['oauth_token']) || !isset($matches['oauth_token_secret'])) {
            throw new apiAuthException("Error authorizing access key (result was: {$ret})");
        }
        $this->accessToken = new apiClientOAuthConsumer(apiClientOAuthUtil::urldecodeRFC3986($matches['oauth_token']), apiClientOAuthUtil::urldecodeRFC3986($matches['oauth_token_secret']));
        return $this->accessToken;
    }
    protected function requestAccessToken($requestToken, $requestTokenSecret, $oauthVerifier) {
        $accessToken = new apiClientOAuthConsumer($requestToken, $requestTokenSecret);
        $accessRequest = apiClientOAuthRequest::from_consumer_and_token($this->consumerToken, $accessToken, "GET", $this->service['access_token_url'], array('oauth_verifier' => $oauthVerifier));
        $accessRequest->sign_request($this->signatureMethod, $this->consumerToken, $accessToken);
        $request = apiClient::$io->makeRequest(new apiHttpRequest($accessRequest));
        if ($request->getResponseHttpCode() != 200) {
            throw new apiAuthException("Could not fetch access token, http code: " . $request->getResponseHttpCode() . ', response body: ' . $request->getResponseBody());
        }
        return $request->getResponseBody();
    }
    public function obtainRequestToken($callbackUrl, $uid) {
        $callbackParams = (strpos($_SERVER['REQUEST_URI'], '?') !== false ? '&' : '?') . 'uid=' . urlencode($uid);
        $ret = $this->requestRequestToken($callbackUrl . $callbackParams);
        $matches = array();
        preg_match('/oauth_token=(.*)&oauth_token_secret=(.*)&oauth_callback_confirmed=(.*)/', $ret, $matches);
        if (!is_array($matches) || count($matches) != 4) {
            throw new apiAuthException("Error retrieving request key ({$ret})");
        }
        return new apiClientOAuthToken(apiClientOAuthUtil::urldecodeRFC3986($matches[1]), apiClientOAuthUtil::urldecodeRFC3986($matches[2]));
    }
    protected function requestRequestToken($callbackUrl) {
        $requestTokenRequest = apiClientOAuthRequest::from_consumer_and_token($this->consumerToken, NULL, "GET", $this->service['request_token_url'], array());
        $requestTokenRequest->set_parameter('scope', $this->service['scope']);
        $requestTokenRequest->set_parameter('oauth_callback', $callbackUrl);
        $requestTokenRequest->sign_request($this->signatureMethod, $this->consumerToken, NULL);
        $request = apiClient::$io->makeRequest(new apiHttpRequest($requestTokenRequest));
        if ($request->getResponseHttpCode() != 200) {
            throw new apiAuthException("Couldn't fetch request token, http code: " . $request->getResponseHttpCode() . ', response body: ' . $request->getResponseBody());
        }
        return $request->getResponseBody();
    }
    public function redirectToAuthorization($token) {
        $authorizeRedirect = $this->service['authorization_token_url'] . $token->key;
        header("Location: $authorizeRedirect");
    }
    public function sign(apiHttpRequest $request) {
        if ($this->developerKey) {
            $request->setUrl($request->getUrl() . ((strpos($request->getUrl(), '?') === false) ? '?' : '&') . 'key=' . urlencode($this->developerKey));
        }
        $oauthRequest = apiClientOAuthRequest::from_request($request->getMethod(), $request->getBaseUrl(), $request->getQueryParams());
        $params = $this->mergeParameters($request->getQueryParams());
        foreach ($params as $key => $val) {
            if (is_array($val)) {
                $val = implode(',', $val);
            }
            $oauthRequest->set_parameter($key, $val);
        }
        $oauthRequest->sign_request($this->signatureMethod, $this->consumerToken, $this->accessToken);
        $authHeaders = $oauthRequest->to_header();
        $headers = $request->getHeaders();
        $headers[] = $authHeaders;
        $request->setHeaders($headers);
        $request->accessKey = $this->accessToken->key;
        return $request;
    }
    protected function mergeParameters($params) {
        $defaults = array('oauth_nonce' => md5(microtime() . mt_rand()), 'oauth_version' => apiClientOAuthRequest::$version, 'oauth_timestamp' => time(), 'oauth_consumer_key' => $this->consumerToken->key);
        if ($this->accessToken != null) {
            $params['oauth_token'] = $this->accessToken->key;
        }
        return array_merge($defaults, $params);
    }
    public function createAuthUrl($scope) {
        return null;
    }
    public function refreshToken($refreshToken) {
    }
    public function revokeToken() {
    }
}
