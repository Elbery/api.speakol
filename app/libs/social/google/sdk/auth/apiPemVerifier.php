<?php
class apiPemVerifier extends apiVerifier {
    private $publicKey;
    function __construct($pem) {
        if (!function_exists('openssl_x509_read')) {
            throw new Exception('The Google PHP API library needs the openssl PHP extension');
        }
        $this->publicKey = openssl_x509_read($pem);
        if (!$this->publicKey) {
            throw new apiAuthException("Unable to parse PEM: $pem");
        }
    }
    function __destruct() {
        if ($this->publicKey) {
            openssl_x509_free($this->publicKey);
        }
    }
    function verify($data, $signature) {
        $status = openssl_verify($data, $signature, $this->publicKey, "sha256");
        if ($status === - 1) {
            throw new apiAuthException("Signature verification error: " . openssl_error_string());
        }
        return $status === 1;
    }
}
