<?php
class apiP12Signer extends apiSigner {
    private $privateKey;
    function __construct($p12file, $password) {
        if (!function_exists('openssl_x509_read')) {
            throw new Exception('The Google PHP API library needs the openssl PHP extension');
        }
        $p12 = file_get_contents($p12file);
        $certs = array();
        if (!openssl_pkcs12_read($p12, $certs, $password)) {
            throw new apiAuthException("Unable to parse $p12file.  " . "Is this a .p12 file?  Is the password correct?  OpenSSL error: " . openssl_error_string());
        }
        if (!array_key_exists("pkey", $certs) || !$certs["pkey"]) {
            throw new apiAuthException("No private key found in p12 file $p12file");
        }
        $this->privateKey = openssl_pkey_get_private($certs["pkey"]);
        if (!$this->privateKey) {
            throw new apiAuthException("Unable to load private key in $p12file");
        }
    }
    function __destruct() {
        if ($this->privateKey) {
            openssl_pkey_free($this->privateKey);
        }
    }
    function sign($data) {
        if (!openssl_sign($data, $signature, $this->privateKey, "sha256")) {
            throw new apiAuthException("Unable to sign data");
        }
        return $signature;
    }
}
