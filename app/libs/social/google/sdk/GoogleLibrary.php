<?php
class GoogleLibrary extends Phalcon\Mvc\User\Component {
    protected $_google;
    protected $scope;
    public function postToMyWall($data) {
        $accessToken = SocialProviders::findFirst(array('owner_id = :owner_id: AND provider_type = :provider_type:', 'bind' => array('owner_id' => $data['user_id'], 'provider_type' => 'google')));
        if (!$accessToken) {
            throw new Exception('User not found!');
        }
        $access_token = $this->getDI()->get('google_client')->getAccessToken();
        if ($user_id) {
            $ret_obj = $this->_facebook->api('/me/feed', 'POST', array('link' => $data['link'], 'message' => $data['message']));
        } else {
            $redirect_url = 'http://ggo.com';
            $login_url = $this->facebookLoginURL($redirect_url);
            $login_url = $login_url . '&a=b';
            return $login_url;
        }
    }
    public function googleLogin($data) {
        $di = $this->getDI();
        $redirectUrl = $data['redirect_url'];
        $di->get('google_client')->setRedirectUri($redirectUrl);
        $di->get('google_client')->authenticate();
        $access_token = $di->get('google_client')->getAccessToken();
        if ($access_token) {
            $user = $di->get('google_oauth')->userinfo->get();
            $result['user'] = array('id' => $user['id'], 'first_name' => $user['given_name'], 'last_name' => $user['family_name'], 'email' => $user['email'], 'image' => $user['picture'], 'birthday' => !empty($user['birthday']) ? $user['birthday'] : false);
        }
        $result['access_token'] = $access_token;
        return $result;
    }
}
