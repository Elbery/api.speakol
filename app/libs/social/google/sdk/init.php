<?php
if (!defined('NEW_GOOGLE_LOGIN')) {
    return;
}
require_once dirname(__FILE__) . '/apiClient.php';
require_once dirname(__FILE__) . '/contrib/apiOauth2Service.php';
