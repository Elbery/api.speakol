<?php
require_once 'apiCacheParser.php';
class apiCurlIO implements apiIO {
    const CONNECTION_ESTABLISHED = "HTTP/1.0 200 Connection established\r\n\r\n";
    const FORM_URLENCODED = 'application/x-www-form-urlencoded';
    private static $ENTITY_HTTP_METHODS = array("POST" => null, "PUT" => null);
    private static $HOP_BY_HOP = array('connection', 'keep-alive', 'proxy-authenticate', 'proxy-authorization', 'te', 'trailers', 'transfer-encoding', 'upgrade');
    private static $DEFAULT_CURL_PARAMS = array(CURLOPT_RETURNTRANSFER => true, CURLOPT_FOLLOWLOCATION => 0, CURLOPT_FAILONERROR => false, CURLOPT_SSL_VERIFYPEER => true, CURLOPT_HEADER => true,);
    public function authenticatedRequest(apiHttpRequest $request) {
        $request = apiClient::$auth->sign($request);
        return $this->makeRequest($request);
    }
    public function makeRequest(apiHttpRequest $request) {
        $cached = $this->getCachedRequest($request);
        if ($cached !== false) {
            if (apiCacheParser::mustRevalidate($cached)) {
                $addHeaders = array();
                if ($cached->getResponseHeader('etag')) {
                    $addHeaders['If-None-Match'] = $cached->getResponseHeader('etag');
                } elseif ($cached->getResponseHeader('date')) {
                    $addHeaders['If-Modified-Since'] = $cached->getResponseHeader('date');
                }
                $request->setRequestHeaders($addHeaders);
            } else {
                return $cached;
            }
        }
        if (array_key_exists($request->getRequestMethod(), self::$ENTITY_HTTP_METHODS)) {
            $request = $this->processEntityRequest($request);
        }
        $ch = curl_init();
        curl_setopt_array($ch, self::$DEFAULT_CURL_PARAMS);
        curl_setopt($ch, CURLOPT_URL, $request->getUrl());
        if ($request->getPostBody()) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request->getPostBody());
        }
        $requestHeaders = $request->getRequestHeaders();
        if ($requestHeaders && is_array($requestHeaders)) {
            $parsed = array();
            foreach ($requestHeaders as $k => $v) {
                $parsed[] = "$k: $v";
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, $parsed);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request->getRequestMethod());
        curl_setopt($ch, CURLOPT_USERAGENT, $request->getUserAgent());
        $respData = curl_exec($ch);
        if (curl_errno($ch) == CURLE_SSL_CACERT) {
            error_log('SSL certificate problem, verify that the CA cert is OK.' . ' Retrying with the CA cert bundle from google-api-php-client.');
            curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacerts.pem');
            $respData = curl_exec($ch);
        }
        $respHeaderSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $respHttpCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curlErrorNum = curl_errno($ch);
        $curlError = curl_error($ch);
        curl_close($ch);
        if ($curlErrorNum != CURLE_OK) {
            throw new apiIOException("HTTP Error: ($respHttpCode) $curlError");
        }
        list($responseHeaders, $responseBody) = $this->parseHttpResponseBody($respData, $respHeaderSize);
        if ($respHttpCode == 304 && $cached) {
            if (isset($responseHeaders['connection'])) {
                $hopByHop = array_merge(self::$HOP_BY_HOP, explode(',', $responseHeaders['connection']));
                $endToEnd = array();
                foreach ($hopByHop as $key) {
                    if (isset($responseHeaders[$key])) {
                        $endToEnd[$key] = $responseHeaders[$key];
                    }
                }
                $cached->setResponseHeaders($endToEnd);
            }
            return $cached;
        }
        $request->setResponseHttpCode($respHttpCode);
        $request->setResponseHeaders($responseHeaders);
        $request->setResponseBody($responseBody);
        $this->setCachedRequest($request);
        return $request;
    }
    public function setCachedRequest(apiHttpRequest $request) {
        if (apiCacheParser::isResponseCacheable($request)) {
            apiClient::$cache->set($request->getCacheKey(), $request);
            return true;
        }
        return false;
    }
    public function getCachedRequest(apiHttpRequest $request) {
        if (false == apiCacheParser::isRequestCacheable($request)) {
            false;
        }
        return apiClient::$cache->get($request->getCacheKey());
    }
    public function parseHttpResponseBody($respData, $headerSize) {
        if (stripos($respData, self::CONNECTION_ESTABLISHED) !== false) {
            $respData = str_ireplace(self::CONNECTION_ESTABLISHED, '', $respData);
        }
        $responseBody = substr($respData, $headerSize);
        $responseHeaderLines = explode("\r\n", substr($respData, 0, $headerSize));
        $responseHeaders = array();
        foreach ($responseHeaderLines as $headerLine) {
            if ($headerLine && strpos($headerLine, ':') !== false) {
                list($header, $value) = explode(': ', $headerLine, 2);
                $header = strtolower($header);
                if (isset($responseHeaders[$header])) {
                    $responseHeaders[$header].= "\n" . $value;
                } else {
                    $responseHeaders[$header] = $value;
                }
            }
        }
        return array($responseHeaders, $responseBody);
    }
    public function processEntityRequest(apiHttpRequest $request) {
        $postBody = $request->getPostBody();
        $contentType = $request->getRequestHeader("content-type");
        if (false == $contentType) {
            $contentType = self::FORM_URLENCODED;
            $request->setRequestHeaders(array('content-type' => $contentType));
        }
        if ($contentType == self::FORM_URLENCODED && is_array($postBody)) {
            $postBody = http_build_query($postBody, '', '&');
            $request->setPostBody($postBody);
        }
        if (!$postBody || is_string($postBody)) {
            $postsLength = strlen($postBody);
            $request->setRequestHeaders(array('content-length' => $postsLength));
        }
        return $request;
    }
}
