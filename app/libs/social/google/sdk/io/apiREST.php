<?php
require_once "external/URITemplateParser.php";
require_once "service/apiUtils.php";
class apiREST {
    static public function execute(apiServiceRequest $req) {
        $result = null;
        $postBody = $req->getPostBody();
        $url = self::createRequestUri($req->getRestBasePath(), $req->getRestPath(), $req->getParameters());
        $httpRequest = new apiHttpRequest($url, $req->getHttpMethod(), null, $postBody);
        if ($postBody) {
            $contentTypeHeader = array();
            if (isset($req->contentType) && $req->contentType) {
                $contentTypeHeader['content-type'] = $req->contentType;
            } else {
                $contentTypeHeader['content-type'] = 'application/json; charset=UTF-8';
                $contentTypeHeader['content-length'] = apiUtils::getStrLen($postBody);
            }
            $httpRequest->setRequestHeaders($contentTypeHeader);
        }
        $httpRequest = apiClient::$io->authenticatedRequest($httpRequest);
        $decodedResponse = self::decodeHttpResponse($httpRequest);
        $ret = isset($decodedResponse['data']) ? $decodedResponse['data'] : $decodedResponse;
        return $ret;
    }
    static function decodeHttpResponse($response) {
        $code = $response->getResponseHttpCode();
        $body = $response->getResponseBody();
        $decoded = null;
        if ($code != '200' && $code != '201' && $code != '204') {
            $decoded = json_decode($body, true);
            $err = 'Error calling ' . $response->getRequestMethod() . ' ' . $response->getUrl();
            if ($decoded != null && isset($decoded['error']['message']) && isset($decoded['error']['code'])) {
                $err.= ": ({$decoded['error']['code']}) {$decoded['error']['message']}";
            } else {
                $err.= ": ($code) $body";
            }
            throw new apiServiceException($err, $code);
        }
        if ($code != '204') {
            $decoded = json_decode($body, true);
            if ($decoded == null) {
                throw new apiServiceException("Invalid json in service response: $body");
            }
        }
        return $decoded;
    }
    static function createRequestUri($basePath, $restPath, $params) {
        $requestUrl = $basePath . $restPath;
        $uriTemplateVars = array();
        $queryVars = array();
        foreach ($params as $paramName => $paramSpec) {
            if (!isset($paramSpec['location'])) {
                $paramSpec['location'] = $paramSpec['restParameterType'];
            }
            if ($paramSpec['type'] == 'boolean') {
                $paramSpec['value'] = ($paramSpec['value']) ? 'true' : 'false';
            }
            if ($paramSpec['location'] == 'path') {
                $uriTemplateVars[$paramName] = $paramSpec['value'];
            } else {
                if (isset($paramSpec['repeated']) && is_array($paramSpec['value'])) {
                    foreach ($paramSpec['value'] as $value) {
                        $queryVars[] = $paramName . '=' . rawurlencode($value);
                    }
                } else {
                    $queryVars[] = $paramName . '=' . rawurlencode($paramSpec['value']);
                }
            }
        }
        if (count($uriTemplateVars)) {
            $uriTemplateParser = new URI_Template_Parser($requestUrl);
            $requestUrl = $uriTemplateParser->expand($uriTemplateVars);
        }
        $requestUrl = str_replace('%40', '@', $requestUrl);
        if (count($queryVars)) {
            $requestUrl.= '?' . implode($queryVars, '&');
        }
        return $requestUrl;
    }
}
