<?php
require_once 'io/apiHttpRequest.php';
require_once 'io/apiCurlIO.php';
require_once 'io/apiREST.php';
require_once 'io/apiRPC.php';
interface apiIO {
    public function authenticatedRequest(apiHttpRequest $request);
    public function makeRequest(apiHttpRequest $request);
}
