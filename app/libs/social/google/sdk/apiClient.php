<?php
if (!function_exists('curl_init')) {
    throw new Exception('Google PHP API Client requires the CURL PHP extension');
}
if (!function_exists('json_decode')) {
    throw new Exception('Google PHP API Client requires the JSON PHP extension');
}
if (!function_exists('http_build_query')) {
    throw new Exception('Google PHP API Client requires http_build_query()');
}
if (!ini_get('date.timezone') && function_exists('date_default_timezone_set')) {
    date_default_timezone_set('UTC');
}
$cwd = dirname(__FILE__);
set_include_path("$cwd" . PATH_SEPARATOR . get_include_path());
require_once "config.php";
if (file_exists($cwd . '/local_config.php')) {
    $defaultConfig = $apiConfig;
    require_once ($cwd . '/local_config.php');
    $apiConfig = array_merge($defaultConfig, $apiConfig);
}
require_once 'service/apiModel.php';
require_once 'service/apiService.php';
require_once 'service/apiServiceRequest.php';
require_once 'auth/apiAuth.php';
require_once 'cache/apiCache.php';
require_once 'io/apiIO.php';
require_once ('service/apiMediaFileUpload.php');
class apiClient {
    const discoveryVersion = 'v0.3';
    static $auth;
    static $io;
    static $cache;
    protected $scopes = array();
    protected $useObjects = false;
    protected $services = array();
    private $authenticated = false;
    private $defaultService = array('authorization_token_url' => 'https://www.google.com/accounts/OAuthAuthorizeToken', 'request_token_url' => 'https://www.google.com/accounts/OAuthGetRequestToken', 'access_token_url' => 'https://www.google.com/accounts/OAuthGetAccessToken');
    public function __construct($config = array()) {
        global $apiConfig;
        $apiConfig = array_merge($apiConfig, $config);
        self::$cache = new $apiConfig['cacheClass']();
        self::$auth = new $apiConfig['authClass']();
        self::$io = new $apiConfig['ioClass']();
    }
    public function discover($service, $version = 'v1') {
        $this->addService($service, $version);
        $this->$service = $this->discoverService($service, $this->services[$service]['discoveryURI']);
        return $this->$service;
    }
    public function getAuthen() {
        $this->authenticated = FALSE;
        return $this->authenticated;
    }
    public function addService($service, $version) {
        global $apiConfig;
        if ($this->authenticated) {
            throw new apiException('Cant add services after having authenticated');
        }
        $this->services[$service] = $this->defaultService;
        if (isset($apiConfig['services'][$service])) {
            $this->services[$service] = array_merge($this->services[$service], $apiConfig['services'][$service]);
        }
        $this->services[$service]['discoveryURI'] = $apiConfig['basePath'] . '/discovery/' . self::discoveryVersion . '/describe/' . urlencode($service) . '/' . urlencode($version);
    }
    public function setAuthClass($authClassName) {
        self::$auth = new $authClassName();
    }
    public function authenticate() {
        $service = $this->prepareService();
        $this->authenticated = true;
        return self::$auth->authenticate($service);
    }
    public function createAuthUrl() {
        $service = $this->prepareService();
        return self::$auth->createAuthUrl($service['scope']);
    }
    private function prepareService() {
        $service = $this->defaultService;
        $scopes = array();
        if ($this->scopes) {
            $scopes = $this->scopes;
        } else {
            foreach ($this->services as $key => $val) {
                if (isset($val['scope'])) {
                    if (is_array($val['scope'])) {
                        $scopes = array_merge($val['scope'], $scopes);
                    } else {
                        $scopes[] = $val['scope'];
                    }
                } else {
                    $scopes[] = 'https://www.googleapis.com/auth/' . $key;
                }
                unset($val['discoveryURI']);
                unset($val['scope']);
                $service = array_merge($service, $val);
            }
        }
        $service['scope'] = implode(' ', $scopes);
        return $service;
    }
    public function setAccessToken($accessToken) {
        if ($accessToken == null || 'null' == $accessToken) {
            $accessToken = null;
        }
        self::$auth->setAccessToken($accessToken);
    }
    public function getAccessToken() {
        $token = self::$auth->getAccessToken();
        return (null == $token || 'null' == $token) ? null : $token;
    }
    public function setDeveloperKey($developerKey) {
        self::$auth->setDeveloperKey($developerKey);
    }
    public function setState($state) {
        self::$auth->setState($state);
    }
    public function setAccessType($accessType) {
        self::$auth->setAccessType($accessType);
    }
    public function setApprovalPrompt($approvalPrompt) {
        self::$auth->setApprovalPrompt($approvalPrompt);
    }
    public function setApplicationName($applicationName) {
        global $apiConfig;
        $apiConfig['application_name'] = $applicationName;
    }
    public function setClientId($clientId) {
        global $apiConfig;
        $apiConfig['oauth2_client_id'] = $clientId;
        self::$auth->clientId = $clientId;
    }
    public function setClientSecret($clientSecret) {
        global $apiConfig;
        $apiConfig['oauth2_client_secret'] = $clientSecret;
        self::$auth->clientSecret = $clientSecret;
    }
    public function setRedirectUri($redirectUri) {
        global $apiConfig;
        $apiConfig['oauth2_redirect_uri'] = $redirectUri;
        self::$auth->redirectUri = $redirectUri;
    }
    public function refreshToken($refreshToken) {
        self::$auth->refreshToken($refreshToken);
    }
    public function revokeToken($token = null) {
        self::$auth->revokeToken($token);
    }
    public function verifyIdToken($token = null) {
        return self::$auth->verifyIdToken($token);
    }
    public function setScopes($scopes) {
        $this->scopes = is_string($scopes) ? explode(" ", $scopes) : $scopes;
    }
    public function setUseObjects($useObjects) {
        global $apiConfig;
        $apiConfig['use_objects'] = $useObjects;
    }
    private function discoverService($serviceName, $serviceURI) {
        $request = self::$io->makeRequest(new apiHttpRequest($serviceURI));
        if ($request->getResponseHttpCode() != 200) {
            throw new apiException("Could not fetch discovery document for $serviceName, code: " . $request->getResponseHttpCode() . ", response: " . $request->getResponseBody());
        }
        $discoveryResponse = $request->getResponseBody();
        $discoveryDocument = json_decode($discoveryResponse, true);
        if ($discoveryDocument == NULL) {
            throw new apiException("Invalid json returned for $serviceName");
        }
        return new apiService($serviceName, $discoveryDocument, apiClient::getIo());
    }
    public static function getAuth() {
        return apiClient::$auth;
    }
    public static function getIo() {
        return apiClient::$io;
    }
    public function getCache() {
        return apiClient::$cache;
    }
}
class apiException extends Exception {
}
class apiAuthException extends apiException {
}
class apiCacheException extends apiException {
}
class apiIOException extends apiException {
}
class apiServiceException extends apiException {
}
