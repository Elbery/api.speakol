<?php
class apiClientOAuthException extends Exception {
}
class apiClientOAuthConsumer {
    public $key;
    public $secret;
    public function __construct($key, $secret, $callback_url = NULL) {
        $this->key = $key;
        $this->secret = $secret;
        $this->callback_url = $callback_url;
    }
}
class apiClientOAuthToken {
    public $key;
    public $secret;
    function __construct($key, $secret) {
        $this->key = $key;
        $this->secret = $secret;
    }
    function to_string() {
        return "oauth_token=" . apiClientOAuthUtil::urlencodeRFC3986($this->key) . "&oauth_token_secret=" . apiClientOAuthUtil::urlencodeRFC3986($this->secret);
    }
    function __toString() {
        return $this->to_string();
    }
}
class apiClientOAuthSignatureMethod {
    public function check_signature(&$request, $consumer, $token, $signature) {
        $built = $this->build_signature($request, $consumer, $token);
        return $built == $signature;
    }
}
class apiClientOAuthSignatureMethod_HMAC_SHA1 extends apiClientOAuthSignatureMethod {
    function get_name() {
        return "HMAC-SHA1";
    }
    public function build_signature($request, $consumer, $token, $privKey = NULL) {
        $base_string = $request->get_signature_base_string();
        $request->base_string = $base_string;
        $key_parts = array($consumer->secret, ($token) ? $token->secret : "");
        $key_parts = array_map(array('apiClientOAuthUtil', 'urlencodeRFC3986'), $key_parts);
        $key = implode('&', $key_parts);
        return base64_encode(hash_hmac('sha1', $base_string, $key, true));
    }
}
class apiClientOAuthSignatureMethod_RSA_SHA1 extends apiClientOAuthSignatureMethod {
    public function get_name() {
        return "RSA-SHA1";
    }
    protected function fetch_public_cert(&$request) {
        throw Exception("fetch_public_cert not implemented");
    }
    protected function fetch_private_cert($privKey) {
        throw Exception("fetch_private_cert not implemented");
    }
    public function build_signature(&$request, $consumer, $token, $privKey) {
        $base_string = $request->get_signature_base_string();
        if ($privKey == '') {
            $fp = fopen($GLOBALS['PRIV_KEY_FILE'], "r");
            $privKey = fread($fp, 8192);
            fclose($fp);
        }
        $privatekeyid = openssl_get_privatekey($privKey);
        $ok = openssl_sign($base_string, $signature, $privatekeyid);
        openssl_free_key($privatekeyid);
        return base64_encode($signature);
    }
    public function check_signature(&$request, $consumer, $token, $signature) {
        $decoded_sig = base64_decode($signature);
        $base_string = $request->get_signature_base_string();
        $cert = $this->fetch_public_cert($request);
        $publickeyid = openssl_get_publickey($cert);
        $ok = openssl_verify($base_string, $decoded_sig, $publickeyid);
        openssl_free_key($publickeyid);
        return $ok == 1;
    }
}
class apiClientOAuthRequest {
    private $parameters;
    private $http_method;
    private $http_url;
    public $base_string;
    public static $version = '1.0';
    function __construct($http_method, $http_url, $parameters = NULL) {
        @$parameters or $parameters = array();
        $this->parameters = $parameters;
        $this->http_method = $http_method;
        $this->http_url = $http_url;
    }
    public static function from_request($http_method = NULL, $http_url = NULL, $parameters = NULL) {
        $scheme = (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on") ? 'http' : 'https';
        @$http_url or $http_url = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        @$http_method or $http_method = $_SERVER['REQUEST_METHOD'];
        $request_headers = apiClientOAuthRequest::get_headers();
        if ($parameters) {
            $req = new apiClientOAuthRequest($http_method, $http_url, $parameters);
        } else if (@substr($request_headers['Authorization'], 0, 5) == "OAuth") {
            $header_parameters = apiClientOAuthRequest::split_header($request_headers['Authorization']);
            if ($http_method == "GET") {
                $req_parameters = $_GET;
            } else if ($http_method = "POST") {
                $req_parameters = $_POST;
            }
            $parameters = array_merge($header_parameters, $req_parameters);
            $req = new apiClientOAuthRequest($http_method, $http_url, $parameters);
        } else if ($http_method == "GET") {
            $req = new apiClientOAuthRequest($http_method, $http_url, $_GET);
        } else if ($http_method == "POST") {
            $req = new apiClientOAuthRequest($http_method, $http_url, $_POST);
        }
        return $req;
    }
    public static function from_consumer_and_token($consumer, $token, $http_method, $http_url, $parameters = NULL) {
        @$parameters or $parameters = array();
        $defaults = array("oauth_version" => apiClientOAuthRequest::$version, "oauth_nonce" => apiClientOAuthRequest::generate_nonce(), "oauth_timestamp" => apiClientOAuthRequest::generate_timestamp(), "oauth_consumer_key" => $consumer->key);
        $parameters = array_merge($defaults, $parameters);
        if ($token) {
            $parameters['oauth_token'] = $token->key;
        }
        return new apiClientOAuthRequest($http_method, $http_url, $parameters);
    }
    public function set_parameter($name, $value) {
        $this->parameters[$name] = $value;
    }
    public function get_parameter($name) {
        return $this->parameters[$name];
    }
    public function get_parameters() {
        return $this->parameters;
    }
    public function get_signable_parameters() {
        $params = $this->parameters;
        if (isset($params['oauth_signature'])) {
            unset($params['oauth_signature']);
        }
        $keys = array_map(array('apiClientOAuthUtil', 'urlencodeRFC3986'), array_keys($params));
        $values = array_map(array('apiClientOAuthUtil', 'urlencodeRFC3986'), array_values($params));
        $params = array_combine($keys, $values);
        uksort($params, 'strnatcmp');
        if (isset($params['title']) && isset($params['title-exact'])) {
            $temp = $params['title-exact'];
            $title = $params['title'];
            unset($params['title']);
            unset($params['title-exact']);
            $params['title-exact'] = $temp;
            $params['title'] = $title;
        }
        $pairs = array();
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                natsort($value);
                foreach ($value as $v2) {
                    $pairs[] = $key . '=' . $v2;
                }
            } else {
                $pairs[] = $key . '=' . $value;
            }
        }
        return implode('&', $pairs);
    }
    public function get_signature_base_string() {
        $parts = array($this->get_normalized_http_method(), $this->get_normalized_http_url(), $this->get_signable_parameters());
        $parts = array_map(array('apiClientOAuthUtil', 'urlencodeRFC3986'), $parts);
        return implode('&', $parts);
    }
    public function get_normalized_http_method() {
        return strtoupper($this->http_method);
    }
    public function get_normalized_http_url() {
        $parts = parse_url($this->http_url);
        $port = (isset($parts['port']) && $parts['port'] != '80') ? ':' . $parts['port'] : '';
        $path = (isset($parts['path'])) ? $parts['path'] : '';
        return $parts['scheme'] . '://' . $parts['host'] . $port . $path;
    }
    public function to_url() {
        $out = $this->get_normalized_http_url() . "?";
        $out.= $this->to_postdata();
        return $out;
    }
    public function to_postdata() {
        $total = array();
        foreach ($this->parameters as $k => $v) {
            $total[] = apiClientOAuthUtil::urlencodeRFC3986($k) . "=" . apiClientOAuthUtil::urlencodeRFC3986($v);
        }
        $out = implode("&", $total);
        return $out;
    }
    public function to_header() {
        $out = 'Authorization: OAuth ';
        $total = array();
        foreach ($this->parameters as $k => $v) {
            if (substr($k, 0, 5) != "oauth") continue;
            $out.= apiClientOAuthUtil::urlencodeRFC3986($k) . '="' . apiClientOAuthUtil::urlencodeRFC3986($v) . '", ';
        }
        $out = substr_replace($out, '', strlen($out) - 2);
        return $out;
    }
    public function __toString() {
        return $this->to_url();
    }
    public function sign_request($signature_method, $consumer, $token, $privKey = NULL) {
        $this->set_parameter("oauth_signature_method", $signature_method->get_name());
        $signature = $this->build_signature($signature_method, $consumer, $token, $privKey);
        $this->set_parameter("oauth_signature", $signature);
    }
    public function build_signature($signature_method, $consumer, $token, $privKey = NULL) {
        $signature = $signature_method->build_signature($this, $consumer, $token, $privKey);
        return $signature;
    }
    private static function generate_timestamp() {
        return time();
    }
    private static function generate_nonce() {
        $mt = microtime();
        $rand = mt_rand();
        return md5($mt . $rand);
    }
    private static function split_header($header) {
        $parts = explode(",", $header);
        $out = array();
        foreach ($parts as $param) {
            $param = ltrim($param);
            if (substr($param, 0, 5) != "oauth") continue;
            $param_parts = explode("=", $param);
            $out[$param_parts[0]] = rawurldecode(substr($param_parts[1], 1, -1));
        }
        return $out;
    }
    private static function get_headers() {
        if (function_exists('apache_request_headers')) {
            return apache_request_headers();
        }
        $out = array();
        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) == "HTTP_") {
                $key = str_replace(" ", "-", ucwords(strtolower(str_replace("_", " ", substr($key, 5)))));
                $out[$key] = $value;
            }
        }
        return $out;
    }
}
class apiClientOAuthDataStore {
    function lookup_consumer($consumer_key) {
    }
    function lookup_token($consumer, $token_type, $token) {
    }
    function lookup_nonce($consumer, $token, $nonce, $timestamp) {
    }
    function fetch_request_token($consumer) {
    }
    function fetch_access_token($token, $consumer) {
    }
}
class apiClientOAuthUtil {
    public static function urlencodeRFC3986($string) {
        return str_replace('%7E', '~', rawurlencode($string));
    }
    public static function urldecodeRFC3986($string) {
        return rawurldecode($string);
    }
}
