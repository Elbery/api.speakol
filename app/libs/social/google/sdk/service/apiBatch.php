<?php
class apiBatch {
    static public function execute() {
        $requests = func_get_args();
        return apiRPC::execute($requests);
    }
}
