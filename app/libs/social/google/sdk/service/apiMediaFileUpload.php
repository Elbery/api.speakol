<?php
class apiMediaFileUpload {
    public $mimeType;
    public $fileName;
    public $chunkSize;
    public static function process($metadata, $method, &$params) {
        $payload = array();
        $data = isset($params['data']) ? $params['data']['value'] : false;
        $mimeType = isset($params['mimeType']) ? $params['mimeType']['value'] : false;
        $file = isset($params['file']) ? $params['file']['value'] : false;
        $uploadPath = $method['mediaUpload']['protocols']['simple']['path'];
        unset($params['data']);
        unset($params['mimeType']);
        unset($params['file']);
        if ($file) {
            if (substr($file, 0, 1) != '@') {
                $file = '@' . $file;
            }
            $payload['file'] = $file;
            $payload['content-type'] = 'multipart/form-data';
            $payload['restBasePath'] = $uploadPath;
            return $payload;
        }
        $parsedMeta = is_string($metadata) ? json_decode($metadata, true) : $metadata;
        if ($metadata && false == $data) {
            return false;
        }
        $params['uploadType'] = array('type' => 'string', 'location' => 'query', 'value' => 'media',);
        $payload['restBasePath'] = $uploadPath;
        if (false == $metadata || false == $parsedMeta) {
            $payload['content-type'] = $mimeType;
            $payload['data'] = $data;
        } else {
            $boundary = isset($params['boundary']) ? $params['boundary'] : mt_rand();
            $boundary = str_replace('"', '', $boundary);
            $payload['content-type'] = 'multipart/related; boundary=' . $boundary;
            $related = "--$boundary\r\n";
            $related.= "Content-Type: application/json; charset=UTF-8\r\n";
            $related.= "\r\n" . $metadata . "\r\n";
            $related.= "--$boundary\r\n";
            $related.= "Content-Type: $mimeType\r\n";
            $related.= "Content-Transfer-Encoding: base64\r\n";
            $related.= "\r\n" . base64_encode($data) . "\r\n";
            $related.= "--$boundary--";
            $payload['data'] = $related;
        }
        return $payload;
    }
}
