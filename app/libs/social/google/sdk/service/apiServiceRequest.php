<?php
class apiServiceRequest {
    public $restBasePath;
    public $restPath;
    public $rpcPath;
    public $rpcName;
    public $httpMethod;
    public $parameters;
    public $postBody;
    public $batchKey;
    public $contentType;
    public function __construct($restBasePath, $rpcPath, $restPath, $rpcName, $httpMethod, $parameters, $postBody = null) {
        if (substr($restBasePath, 0, 4) == 'http') {
            $this->restBasePath = $restBasePath;
        } else {
            global $apiConfig;
            $this->restBasePath = $apiConfig['basePath'] . $restBasePath;
        }
        $this->restPath = $restPath;
        $this->rpcPath = $rpcPath;
        $this->rpcName = $rpcName;
        $this->httpMethod = $httpMethod;
        $this->parameters = $parameters;
        $this->postBody = $postBody;
    }
    public function getPostBody() {
        return $this->postBody;
    }
    public function setPostBody($postBody) {
        $this->postBody = $postBody;
    }
    public function getRestBasePath() {
        return $this->restBasePath;
    }
    public function getRestPath() {
        return $this->restPath;
    }
    public function getRpcPath() {
        return $this->rpcPath;
    }
    public function getRpcName() {
        return $this->rpcName;
    }
    public function getHttpMethod() {
        return $this->httpMethod;
    }
    public function getParameters() {
        return $this->parameters;
    }
    public function getBatchKey() {
        return $this->batchKey;
    }
    public function setBatchKey($batchKey) {
        $this->batchKey = $batchKey;
    }
    public function setContentType($type) {
        $this->contentType = $type;
    }
}
