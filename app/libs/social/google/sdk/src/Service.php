<?php
class Google_Service {
    public $version;
    public $servicePath;
    public $availableScopes;
    public $resource;
    private $client;
    public function __construct(Google_Client $client) {
        $this->client = $client;
    }
    public function getClient() {
        return $this->client;
    }
}
