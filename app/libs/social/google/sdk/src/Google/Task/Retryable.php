<?php
require_once realpath(dirname(__FILE__) . '/../../../autoload.php');
interface Google_Task_Retryable {
    public function allowedRetries();
}
