<?php
require_once "apiFileCache.php";
require_once "apiApcCache.php";
require_once "apiMemcacheCache.php";
abstract class apiCache {
    abstract function get($key, $expiration = false);
    abstract function set($key, $value);
    abstract function delete($key);
}
