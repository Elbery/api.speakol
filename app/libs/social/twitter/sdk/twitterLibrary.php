<?php
class twitterLibrary extends Phalcon\Mvc\User\Component {
    protected $_twitter;
    protected $scope;
    public function twitterLogin($data) {
        $di = $this->getDI();
        $access_token = json_decode($di->get('request')->get('twitter_oauth'), 1);
        $di->get('twitter')->config['user_token'] = $access_token['oauth_token'];
        $di->get('twitter')->config['user_secret'] = $access_token['oauth_token_secret'];
        $params = array('oauth_verifier' => $di->get('request')->get('oauth_verifier'));
        $code = $di->get('twitter')->request('POST', $di->get('twitter')->url('oauth/access_token', ''), $params);
        if ($code == 401) {
            $code = tmhUtilities::auto_fix_time_request($di->get('twitter'), 'POST', $di->get('twitter')->url('oauth/access_token', ''), $params);
        }
        if ($code == 200) {
            $access_token = $di->get('twitter')->extract_params($di->get('twitter')->response['response']);
        }
        $di->twitter->config['user_token'] = $access_token['oauth_token'];
        $di->twitter->config['user_secret'] = $access_token['oauth_token_secret'];
        if ($code == 401) {
            $code = tmhUtilities::auto_fix_time_request($di->get('twitter'), 'GET', $di->get('twitter')->url('1.1/account/verify_credentials'));
        }
        parse_str($di->get('twitter')->response['response'], $resp);
        if (!empty($resp->errors)) {
            return array('status' => 'ERROR', 'messages' => $resp->errors['0']->message);
        }
        $results['user'] = array('id' => $resp['user_id'], 'email' => '', 'birthday' => false,);
        $results['access_token'] = json_encode($access_token);
        return array('status' => 'OK', 'data' => $results);
    }
    public function redirectUrl($redirectUrl) {
        $params = array('oauth_callback' => $redirectUrl);
        $code = $this->twitter->request('POST', $this->twitter->url('oauth/request_token', ''), $params);
        $oauth = $this->twitter->extract_params($this->twitter->response['response']);
        $method = 'authenticate';
        $force = isset($_REQUEST['force']) ? '&force_login=1' : '';
        $authurl = $this->twitter->url("oauth/{$method}", '') . "?oauth_token={$oauth['oauth_token']}{$force}";
        $return = array('auth_url' => $authurl, 'oauth' => $oauth);
        return array('status' => 'OK', 'data' => $return);
    }
    public function postToMyWall($data) {
        $accessToken = SocialProviders::findFirst(array('owner_id = :owner_id: AND provider_type = :provider_type:', 'bind' => array('owner_id' => $data['user_id'], 'provider_type' => 'twitter')));
        if (!$accessToken) {
            throw new Exception('User not found!');
        }
        $di = $this->getDI();
        $access_token = $accessToken->getToken();
        $token = json_decode($access_token, 1);
        $di->getShared('twitter')->config['user_token'] = $token['oauth_token'];
        $di->getShared('twitter')->config['user_secret'] = $token['oauth_token_secret'];
        $params = array('status' => 'ssssss');
        $code = $di->getShared('twitter')->request('POST', $di->getShared('twitter')->url('1.1/statuses/update', 'json'), $params);
        $return = json_decode($di->getShared('twitter')->response['response']);
        return $return;
    }
}
