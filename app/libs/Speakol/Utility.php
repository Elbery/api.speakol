<?php
namespace Speakol;
use Phalcon\Mvc\User\Component;
class Utility extends Component {
    private $imageUpload;
    public $t;
    public function __construct($_di) {
        $this->t = new \Phalcon\DI();
        $this->setImageUploader($_di);
    }
    public static function time_elapsed_string($ptime) {
        $t = \Phalcon\DI::getDefault();
        $lang = $t->get('request')->get('lang');
        $t = $t->get('t');
        $etime = time() - $ptime;
        if ($etime < 1) {
            return '0 ' . $t->_('second');
        }
        $a = array(12 * 30 * 24 * 60 * 60 => array($t->_('year'), $t->_('two-year'), $t->_('years')), 30 * 24 * 60 * 60 => array($t->_('month'), $t->_('two-month'), $t->_('months')), 24 * 60 * 60 => array($t->_('day'), $t->_('two-day'), $t->_('days')), 60 * 60 => array($t->_('hour'), $t->_('two-hour'), $t->_('hours')), 60 => array($t->_('minute'), $t->_('two-minute'), $t->_('minutes')), 1 => array($t->_('second'), $t->_('two-second'), $t->_('seconds')));
        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                switch ($r) {
                    case 1:
                        $format = $r . ' ' . $str[0];
                        $format = ($lang == 'ar') ? $t->_('ago') . ' ' . $format : $format . ' ' . $t->_('ago');
                        return $format;
                    break;
                    case 2:
                        $format = $str[1];
                        $format = ($lang == 'ar') ? $t->_('ago') . ' ' . $format : $format . ' ' . $t->_('ago');
                        return $format;
                    break;
                    default:
                        $format = $r . ' ' . $str[2];
                        $format = ($lang == 'ar') ? $t->_('ago') . ' ' . $format : $format . ' ' . $t->_('ago');
                        return $format;
                    break;
                }
            }
        }
    }
    public function splitWords($string, $nb_caracs = '15', $separator = '..') {
        $string = strip_tags(html_entity_decode($string));
        if (mb_strlen($string) <= $nb_caracs) {
            $final_string = $string;
        } else {
            $final_string = "";
            $words = explode(" ", $string);
            $count = 0;
            foreach ($words as $value) {
                if ($count < $nb_caracs) {
                    if (!empty($final_string)) {
                        $final_string.= " ";
                    }
                    $final_string.= $value;
                } else {
                    break;
                }
                $count++;
            }
            $final_string.= $separator;
        }
        return $final_string;
    }
    public function getImageUploader() {
        return $this->imageUpload;
    }
    public function setImageUploader($_di) {
        $this->imageUpload = new ImageUploader($_di);
    }
    public static function generateURLFromDirPath($_dir, $_file) {
        return ((empty($_SERVER['HTTPS'])) ? 'http' : 'https') . '://' . $_SERVER['SERVER_NAME'] . $_dir . $_file;
    }
    public static function formatDateTime($_dateTime) {
        return self::time_elapsed_string(strtotime($_dateTime));
    }
    public static function handleDataBaseSaveErros($_objModel) {
        $strErrors = "";
        foreach ($_objModel->getMessages() as $message) {
            $strErrors.= $message->getMessage();
            $strErrors.= ". ";
        }
        throw new \Exception("Conflict:{$strErrors}", 409);
    }
    public static function handleException($response, $ex) {
        $response->setStatusCode($ex->getCode(), substr($ex->getMessage(), 0, strpos($ex->getMessage(), ":")));
        $errors[] = $ex->getMessage();
        $response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
        return $response;
    }
    public function getTemplate($name, $params) {
        $parameters = array_merge(array('publicUrl' => $this->config->application->publicUrl), $params);
        return $this->view->getRender('emailTemplates', $name, $parameters, function ($view) {
            $view->setRenderLevel(View::LEVEL_LAYOUT);
        });
        return $view->getContent();
    }
}
