<?php
namespace Speakol;
class ACL implements \Phalcon\Mvc\Micro\MiddlewareInterface {
    public function call($application) {
        try {
            $router = $application['router'];
            $activity = $router->getMatchedRoute()->getPattern();
            if ($this->checkGuestAccess($activity) != FALSE) {
                return true;
            }
            $signedInUserID = \Tokens::checkAccess();
            if (FALSE == $signedInUserID) {
                throw new \Exception('Unauthorized', 401);
            }
            $signedInUserRoleID = \Users::getRoleIDByUserID($signedInUserID);
            if (FALSE == $this->checkAuthorization($signedInUserRoleID, $activity)) {
                throw new \Exception('Forbidden', 403);
            }
        }
        catch(\Exception $ex) {
            $application->response->setHeader("Content-Type", "application/json");
            $application->response->setStatusCode($ex->getCode(), $ex->getMessage());
            $errors[] = $ex->getMessage();
            $application->response->setJsonContent(array('status' => 'ERROR', 'messages' => $errors));
            $application->response->send();
            return false;
        }
    }
    protected function checkAuthorization($_roleID, $_activity) {
        $configs = json_decode(json_encode(\API::instance()->getConfig('routesConfiguration')), 1);
        $activityID = \Activities::getActivityIdByAction($_activity);
        if (FALSE === $activityID) {
            $exist = self::searchForRoute($configs, $_activity);
            $alloweRolls = array(1, 2, 3, 4, 5);
            if (!isset($exist['role_id'])) {
                $message = 'Role id is missing. Please Add role_id value to the route "<b>' . $_activity . '</b>".</br>';
                throw new \Exception($message, '403');
            }
            if (!empty($exist['role_id']) && !is_array($exist['role_id'])) {
                $exist['role_id'] = array($exist['role_id']);
            }
            $activityInserted = false;
            foreach ($exist['role_id'] as $role) {
                if (!empty($role) && in_array($role, $alloweRolls)) {
                    if (!$activityInserted) {
                        $activity = new \Activities();
                        $activity->setAction($_activity);
                        $activity->setCreatedAt(date('Y-m-d H:i:s'));
                        $activity->setUpdatedAt(date('Y-m-d H:i:s'));
                        $activity->save();
                        $activityInserted = $activity->getId();
                    }
                    $roleActivity = new \RolesActivities();
                    $roleActivity->setRoleId($role);
                    $roleActivity->setActivityId($activityInserted);
                    $roleActivity->save();
                }
            }
        }
        return \RolesActivities::isRoleIdAuthorizedToQueryURI($_roleID, $activityID);
    }
    public static function searchForRoute($arr, $key, $controller = false) {
        foreach ($arr as $k => $row) {
            $exist = array_key_exists($key, $row);
            if ($exist) {
                if ($controller) {
                    return array('controller' => $k, 'route' => $row[$key]);
                } else {
                    return $row[$key];
                }
                break;
            }
        }
        return false;
    }
    protected function checkGuestAccess($_activity) {
        $guestRoleID = \Roles::getGuestRoleID();
        return $this->checkAuthorization($guestRoleID, $_activity);
    }
    protected function setCORS($app) {
        $response = $app->response;
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        $response->sendHeaders();
    }
}
