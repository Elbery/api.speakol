<?php
namespace Speakol;
class ImageUploader {
    const FILE_PERMS = 0644;
    const DIR_PERMS = 0777;
    private $uploadErrorStrings = array();
    private $allowedExtensions = array('bmp', 'gif', 'jpg', 'jpeg', 'png', 'psd', 'pspimage', 'thm', 'tif', 'yuv',);
    private $images_type = array('image/png', 'image/bmp', 'image/jpg', 'image/jpeg', 'image/gif', 'image/psd', 'image/pspimage', 'image/tif', 'image/yuv');
    private $audio_files_type = array('application/ogg', 'audio/ogg');
    const DIR_ARGUMENTS = 'arguments';
    const DIR_OPINIONS = 'opinions';
    const DIR_STORIES = 'stories';
    const DIR_DEBATES = 'debates';
    const DIR_ARGUMENTBOX = 'argumentboxes';
    const DIR_COMPARISONS = 'comparisons';
    const DIR_REPLIES = 'replies';
    const DIR_APPS = 'apps';
    const DIR_USERS = 'users';
    const DIR_PROFILE = 'profiles';
    private $dirModule;
    private $_di;
    const DIRUPLOADSWPUB = 'uploads';
    const DIRUPLOADS = 'public/uploads';
    const DIRPUBLIC = '/public';
    const MAX_FILE_SIZE = 15728640;
    private $_php_max_file_size;
    private $_upload_dir;
    public function __construct($_di = NULL) {
        $this->setDependencyInjection($_di);
        $t = \Phalcon\DI::getDefault();
        $this->t = $t->get('t');
        $this->uploadErrorStrings = array(0 => $this->t->_('upload-file-exceed-the-max'), 1 => $this->t->_('upload-file-exceed-the-file-size'), 2 => $this->t->_('upload-file-only-part'), 3 => $this->t->_('upload-file-no-file'), 4 => $this->t->_('upload-file-missing-tempory-folder'), 5 => $this->t->_('upload-file-failed-to-write'), 6 => $this->t->_('upload-file-stopped-by-extension'), 7 => $this->t->_('upload-file-not-allowed-file'),);
    }
    public function getDI() {
        return $this->_di;
    }
    public function setDependencyInjection($_di) {
        $this->_di = $_di;
    }
    public function getPHPMaxFileSize() {
        return (intval(ini_get("upload_max_filesize")) * 1024 * 1024);
    }
    public function splitDate() {
        return (array(date('Y'), date('m'), date('d')));
    }
    public function getDirModule() {
        return $this->dirModule;
    }
    public function setDirModule($_dirModule) {
        if ($_dirModule != self::DIR_USERS && $_dirModule != self::DIR_ARGUMENTS && $_dirModule != self::DIR_DEBATES && $_dirModule != self::DIR_COMPARISONS && $_dirModule != self::DIR_OPINIONS && $_dirModule != self::DIR_REPLIES && $_dirModule != self::DIR_APPS) {
            return FALSE;
        } else {
            $this->dirModule = $_dirModule;
            return TRUE;
        }
    }
    public function setUploadDir($dir = null) {
        $this->_upload_dir = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR;
        if (stripos($_SERVER['DOCUMENT_ROOT'], 'public') == FALSE) {
            $this->_upload_dir.= self::DIRUPLOADS;
        } else {
            $this->_upload_dir.= self::DIRUPLOADSWPUB;
        }
        $this->_upload_dir.= DIRECTORY_SEPARATOR . $this->getDirModule() . DIRECTORY_SEPARATOR;
        if (is_null($dir)) {
            list($year, $month, $day) = $this->splitDate();
            $this->_upload_dir.= $year . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR . $day . DIRECTORY_SEPARATOR;
        } else {
            $this->_upload_dir.= $dir . DIRECTORY_SEPARATOR;
        }
    }
    public function getUploadDir() {
        return $this->_upload_dir;
    }
    public function getUploadRootDir() {
        return $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . self::DIRUPLOADS;
    }
    function createUploadDir($path, $mode = self::DIR_PERMS) {
        $dirs = explode(DIRECTORY_SEPARATOR, $path);
        $count = count($dirs);
        $path = '';
        for ($i = 0;$i < $count;++$i) {
            if ($dirs[$i] == "") {
                continue;
            }
            $path.= DIRECTORY_SEPARATOR . $dirs[$i];
            if (!is_dir($path) && !mkdir($path, $mode)) {
                return FALSE;
            }
        }
        return TRUE;
    }
    public function handleErrors($code, $message) {
    }
    public function uploadImage($_dirModule, $image) {
        if ($this->setDirModule($_dirModule) == FALSE) {
            return FALSE;
        }
        $this->setUploadDir($dir);
        $dir = $this->getUploadDir();
        if ($this->createUploadDir($dir) == FALSE) {
            return FALSE;
        }
        $arrReturn = array();
        $data = explode(',', $image);
        $image_extension = "";
        $index = 11;
        while ($data[0][$index] != ";") {
            $image_extension.= $data[0][$index];
            $index++;
        }
        $strNewFileName = $this->generateFileName(rand(10, 1000) . "." . $image_extension);
        $tmp = '/tmp/' . $strNewFileName;
        $ifp = fopen($tmp, "wb");
        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);
        if (rename($tmp, $dir . $strNewFileName)) {
            $arrReturn[] = array('status' => TRUE, 'file' => $strNewFileName, 'path' => $this->extractRelativePath($dir), 'new' => $strNewFileName);
        } else {
            $arrReturn[] = array('status' => FALSE, 'file' => "Image", 'error' => $this->uploadErrorStrings[5],);
        }
        return $arrReturn;
    }
    public function upload($_dirModule, $_arrFiles, $dir = null, $options = array(), $audio = false) {
        if (!$this->getDi()->getShared('request')->hasFiles()) {
            return FALSE;
        }
        if ($this->setDirModule($_dirModule) == FALSE) {
            return FALSE;
        }
        $this->setUploadDir($dir);
        $dir = $this->getUploadDir();
        if ($this->createUploadDir($dir) == FALSE) {
            return FALSE;
        }
        $arrReturn = array();
        foreach ($_arrFiles as $file) {
            if (!$audio) {
                if (!in_array(strtolower($this->getExtension($file->getName())), $this->allowedExtensions)) {
                    $arrReturn[] = array('status' => FALSE, 'file' => $file->getName(), 'error' => $this->uploadErrorStrings[7],);
                    continue;
                }
                if (!in_array($file->getRealType(), $this->images_type)) {
                    $arrReturn[] = array('status' => FALSE, 'file' => $file->getName(), 'error' => $this->uploadErrorStrings[7],);
                    continue;
                }
            } else {
                if (!in_array($file->getRealType(), $this->audio_files_type)) {
                    $arrReturn[] = array('status' => FALSE, 'file' => $file->getName(), 'error' => "Invalid extension of audio file",);
                    continue;
                }
            }
            if (self::MAX_FILE_SIZE <= $file->getSize()) {
                $arrReturn[] = array('status' => FALSE, 'file' => $file->getName(), 'error' => $this->uploadErrorStrings[0],);
                continue;
            }
            if ($this->getPHPMaxFileSize() <= $file->getSize()) {
                $arrReturn[] = array('status' => FALSE, 'file' => $file->getName(), 'error' => $this->uploadErrorStrings[1],);
                continue;
            }
            $strNewFileName = $this->generateFileName($file->getName());
            $tmp = '/tmp/' . $strNewFileName;
            $perm = $dir . $strNewFileName;
            if (!$file->moveTo($tmp)) {
                $arrReturn[] = array('status' => FALSE, 'file' => $file->getName(), 'error' => $this->uploadErrorStrings[5],);
                continue;
            } else {
                rename($tmp, $perm);
            }
            $arrReturn[] = array('status' => TRUE, 'file' => $file->getName(), 'path' => $this->extractRelativePath($dir), 'new' => $strNewFileName);
        }
        return $arrReturn;
    }
    public function saveRemoteFile($fileUrl, $dir = null) {
        list($year, $month, $day) = $this->splitDate();
        $dir = $dir . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR . $day . DIRECTORY_SEPARATOR;
        $this->setUploadDir($dir);
        $dir = $this->getUploadDir();
        if ($this->createUploadDir($dir) == FALSE) {
            return FALSE;
        }
        $extension = $this->getExtension($fileUrl);
        $dir = $dir . md5($fileUrl . rand('9999', '99999999')) . '.' . $extension;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fileUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);
        $res = @file_put_contents($dir, $body);
        return array('dir' => str_replace('//', '/', str_replace($_SERVER['DOCUMENT_ROOT'], '', $dir)), 'res' => $res);
    }
    function httpParseHeaders($raw_headers) {
        $headers = array();
        $key = '';
        foreach (explode("\n", $raw_headers) as $i => $h) {
            $h = explode(':', $h, 2);
            if (isset($h[1])) {
                if (!isset($headers[$h[0]])) $headers[$h[0]] = trim($h[1]);
                elseif (is_array($headers[$h[0]])) {
                    $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1])));
                } else {
                    $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1])));
                }
                $key = $h[0];
            } else {
                if (substr($h[0], 0, 1) == "\t") $headers[$key].= "\r\n\t" . trim($h[0]);
                elseif (!$key) $headers[0] = trim($h[0]);
                trim($h[0]);
            }
        }
        return $headers;
    }
    public function generateFileName($_fileName) {
        $randText = \Phalcon\Text::random(\Phalcon\Text::RANDOM_ALNUM);
        return (time() . '_' . $randText . '_' . $this->getNameWithoutExtension($_fileName) . '.' . $this->getExtension($_fileName));
    }
    public function setUploadsDirPerms() {
        $apache = exec("whoami");
        $dir = $this->getUploadRootDir();
        exec("chown $apache $dir");
    }
    public function getExtension($_fileName) {
        $return = array();
        preg_match("/^.*\.(jpg|jpeg|png|gif|ogg)/i", $_fileName, $return);
        return $return['1'];
        return (substr($_fileName, strrpos($_fileName, '.') + 1));
    }
    public function getNameWithoutExtension($_fileName) {
        return (substr($_fileName, 0, stripos($_fileName, '.')));
    }
    public function extractRelativePath($path) {
        $absolutoToExclude = $_SERVER['DOCUMENT_ROOT'];
        $from = strpos($path, $absolutoToExclude);
        $length = strlen($absolutoToExclude);
        $part = substr($path, $from, $length);
        return substr($path, strlen($part));
    }
    public function delete($_filename) {
        return (unlink($_filename));
    }
}
