<?php
namespace Speakol;
use Phalcon\Mvc\User\Component;
interface SocialPluginFactory {
    public function make($_type);
}
class concreteSocialPluginFactory extends Component implements SocialPluginFactory {
    public function make($_type) {
        switch ($_type) {
            case \BaseModel::ARGUMENTSBOX:
                return new \Argumentboxes();
            case \BaseModel::DEBATE:
                return new \Debates();
            case \BaseModel::COMPARISON:
                return new \Comparisons();
            case \BaseModel::ARGUMENT:
                return new \Arguments();
            case \BaseModel::REPLY:
                return new \Replies();
            default:
                return new \BaseModel();
        }
    }
}
