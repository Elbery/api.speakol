<?php
use Phalcon\Mvc\User\Component, Phalcon\Mvc\View;
require_once 'Swift/swift_required.php';
class Mail extends Component {
    protected $_transport;
    public function send($mail) {
        $mailSettings = $this->config->mail;
        $message = Swift_Message::newInstance()->setSubject($mail['subject'])->setTo($mail['to'])->setFrom(array($mailSettings->fromEmail => $mailSettings->fromName))->setBody($mail['html'], 'text/html');
        if ($mail['file']) {
            $message->attach(Swift_Attachment::newInstance($mail['file'], 'Speakol.pdf', 'application/pdf'));
        }
        if (!$this->_transport) {
            $this->_transport = Swift_SmtpTransport::newInstance($mailSettings->smtp->server, $mailSettings->smtp->port, $mailSettings->smtp->security)->setUsername($mailSettings->smtp->username)->setPassword($mailSettings->smtp->password);
        }
        $mailer = Swift_Mailer::newInstance($this->_transport);
        $re = $mailer->send($message);
        ob_clean();
        return $re;
    }
}
