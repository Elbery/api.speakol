<?php
class Swift_SignedMessage extends Swift_Message {
    private $headerSigners = array();
    private $bodySigners = array();
    private $savedMessage = array();
    public static function newInstance($subject = null, $body = null, $contentType = null, $charset = null) {
        return new self($subject, $body, $contentType, $charset);
    }
    public function attachSigner(Swift_Signer $signer) {
        if ($signer instanceof Swift_Signers_HeaderSigner) {
            $this->headerSigners[] = $signer;
        } elseif ($signer instanceof Swift_Signers_BodySigner) {
            $this->bodySigners[] = $signer;
        }
        return $this;
    }
    public function toString() {
        $this->saveMessage();
        $this->doSign();
        $string = parent::toString();
        $this->restoreMessage();
        return $string;
    }
    public function toByteStream(Swift_InputByteStream $is) {
        $this->saveMessage();
        $this->doSign();
        parent::toByteStream($is);
        $this->restoreMessage();
    }
    protected function doSign() {
        foreach ($this->bodySigners as $signer) {
            $altered = $signer->getAlteredHeaders();
            $this->saveHeaders($altered);
            $signer->signMessage($this);
        }
        foreach ($this->headerSigners as $signer) {
            $altered = $signer->getAlteredHeaders();
            $this->saveHeaders($altered);
            $signer->reset();
            $signer->setHeaders($this->getHeaders());
            $signer->startBody();
            $this->_bodyToByteStream($signer);
            $signer->endBody();
            $signer->addSignature($this->getHeaders());
        }
    }
    protected function saveMessage() {
        $this->savedMessage = array('headers' => array());
        $this->savedMessage['body'] = $this->getBody();
        $this->savedMessage['children'] = $this->getChildren();
        if (count($this->savedMessage['children']) > 0 && $this->getBody() != '') {
            $this->setChildren(array_merge(array($this->_becomeMimePart()), $this->savedMessage['children']));
            $this->setBody('');
        }
    }
    protected function saveHeaders(array $altered) {
        foreach ($altered as $head) {
            $lc = strtolower($head);
            if (!isset($this->savedMessage['headers'][$lc])) {
                $this->savedMessage['headers'][$lc] = $this->getHeaders()->getAll($head);
            }
        }
    }
    protected function restoreHeaders() {
        foreach ($this->savedMessage['headers'] as $name => $savedValue) {
            $headers = $this->getHeaders()->getAll($name);
            foreach ($headers as $key => $value) {
                if (!isset($savedValue[$key])) {
                    $this->getHeaders()->remove($name, $key);
                }
            }
        }
    }
    protected function restoreMessage() {
        $this->setBody($this->savedMessage['body']);
        $this->setChildren($this->savedMessage['children']);
        $this->restoreHeaders();
        $this->savedMessage = array();
    }
}
