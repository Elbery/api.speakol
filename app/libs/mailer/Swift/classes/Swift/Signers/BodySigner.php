<?php
interface Swift_Signers_BodySigner extends Swift_Signer {
    public function signMessage(Swift_SignedMessage $message);
    public function getAlteredHeaders();
}
