<?php
class Swift_Encoder_Rfc2231Encoder implements Swift_Encoder {
    private $_charStream;
    public function __construct(Swift_CharacterStream $charStream) {
        $this->_charStream = $charStream;
    }
    public function encodeString($string, $firstLineOffset = 0, $maxLineLength = 0) {
        $lines = array();
        $lineCount = 0;
        $lines[] = '';
        $currentLine = & $lines[$lineCount++];
        if (0 >= $maxLineLength) {
            $maxLineLength = 75;
        }
        $this->_charStream->flushContents();
        $this->_charStream->importString($string);
        $thisLineLength = $maxLineLength - $firstLineOffset;
        while (false !== $char = $this->_charStream->read(4)) {
            $encodedChar = rawurlencode($char);
            if (0 != strlen($currentLine) && strlen($currentLine . $encodedChar) > $thisLineLength) {
                $lines[] = '';
                $currentLine = & $lines[$lineCount++];
                $thisLineLength = $maxLineLength;
            }
            $currentLine.= $encodedChar;
        }
        return implode("\r\n", $lines);
    }
    public function charsetChanged($charset) {
        $this->_charStream->setCharacterSet($charset);
    }
}
