<?php
class Swift_Message extends Swift_Mime_SimpleMessage {
    public function __construct($subject = null, $body = null, $contentType = null, $charset = null) {
        call_user_func_array(array($this, 'Swift_Mime_SimpleMessage::__construct'), Swift_DependencyContainer::getInstance()->createDependenciesFor('mime.message'));
        if (!isset($charset)) {
            $charset = Swift_DependencyContainer::getInstance()->lookup('properties.charset');
        }
        $this->setSubject($subject);
        $this->setBody($body);
        $this->setCharset($charset);
        if ($contentType) {
            $this->setContentType($contentType);
        }
    }
    public static function newInstance($subject = null, $body = null, $contentType = null, $charset = null) {
        return new self($subject, $body, $contentType, $charset);
    }
    public function addPart($body, $contentType = null, $charset = null) {
        return $this->attach(Swift_MimePart::newInstance($body, $contentType, $charset));
    }
    public function __wakeup() {
        Swift_DependencyContainer::getInstance()->createDependenciesFor('mime.message');
    }
}
