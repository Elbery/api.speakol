<?php
namespace Stripe;
class Stripe {
    public static $apiKey;
    public static $apiBase = 'https://api.stripe.com';
    public static $apiUploadBase = 'https://uploads.stripe.com';
    public static $apiVersion = null;
    public static $verifySslCerts = true;
    const VERSION = '2.1.2';
    public static function getApiKey() {
        return self::$apiKey;
    }
    public static function setApiKey($apiKey) {
        self::$apiKey = $apiKey;
    }
    public static function getApiVersion() {
        return self::$apiVersion;
    }
    public static function setApiVersion($apiVersion) {
        self::$apiVersion = $apiVersion;
    }
    public static function getVerifySslCerts() {
        return self::$verifySslCerts;
    }
    public static function setVerifySslCerts($verify) {
        self::$verifySslCerts = $verify;
    }
}
