<?php
namespace Stripe;
class Transfer extends ApiResource {
    public static function retrieve($id, $opts = null) {
        return self::_retrieve($id, $opts);
    }
    public static function all($params = null, $opts = null) {
        return self::_all($params, $opts);
    }
    public static function create($params = null, $opts = null) {
        return self::_create($params, $opts);
    }
    public function reverse($params = null, $opts = null) {
        $url = $this->instanceUrl() . '/reversals';
        list($response, $opts) = $this->request('post', $url, $params, $options);
        $this->refreshFrom($response, $opts);
        return $this;
    }
    public function cancel() {
        $url = $this->instanceUrl() . '/cancel';
        list($response, $opts) = $this->_request('post', $url);
        $this->refreshFrom($response, $opts);
        return $this;
    }
    public function save($opts = null) {
        return $this->_save($opts);
    }
}
