<?php
namespace Stripe;
class Card extends ApiResource {
    public function instanceUrl() {
        $id = $this['id'];
        if (!$id) {
            $class = get_class($this);
            $msg = "Could not determine which URL to request: $class instance " . "has invalid ID: $id";
            throw new Error\InvalidRequest($msg, null);
        }
        if (isset($this['customer'])) {
            $parent = $this['customer'];
            $base = Customer::classUrl();
        } elseif (isset($this['recipient'])) {
            $parent = $this['recipient'];
            $base = Recipient::classUrl();
        } else {
            return null;
        }
        $parent = ApiRequestor::utf8($parent);
        $id = ApiRequestor::utf8($id);
        $parentExtn = urlencode($parent);
        $extn = urlencode($id);
        return "$base/$parentExtn/cards/$extn";
    }
    public function delete($params = null, $opts = null) {
        return $this->_delete($params, $opts);
    }
    public function save($opts = null) {
        return $this->_save($opts);
    }
}
