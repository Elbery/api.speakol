<?php
namespace Stripe;
class Subscription extends ApiResource {
    public function instanceUrl() {
        $id = $this['id'];
        $customer = $this['customer'];
        if (!$id) {
            throw new Error\InvalidRequest("Could not determine which URL to request: " . "class instance has invalid ID: $id", null);
        }
        $id = ApiRequestor::utf8($id);
        $customer = ApiRequestor::utf8($customer);
        $base = Customer::classUrl();
        $customerExtn = urlencode($customer);
        $extn = urlencode($id);
        return "$base/$customerExtn/subscriptions/$extn";
    }
    public function cancel($params = null, $opts = null) {
        return $this->_delete($params, $opts);
    }
    public function save($opts = null) {
        return $this->_save($opts);
    }
    public function deleteDiscount() {
        $url = $this->instanceUrl() . '/discount';
        list($response, $opts) = $this->_request('delete', $url);
        $this->refreshFrom(array('discount' => null), $opts, true);
    }
}
