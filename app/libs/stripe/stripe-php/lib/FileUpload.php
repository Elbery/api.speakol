<?php
namespace Stripe;
class FileUpload extends ApiResource {
    public static function baseUrl() {
        return Stripe::$apiUploadBase;
    }
    public static function className() {
        return 'file';
    }
    public static function retrieve($id, $opts = null) {
        return self::_retrieve($id, $opts);
    }
    public static function create($params = null, $opts = null) {
        return self::_create($params, $opts);
    }
    public static function all($params = null, $opts = null) {
        return self::_all($params, $opts);
    }
}
