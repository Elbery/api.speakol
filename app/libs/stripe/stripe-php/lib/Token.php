<?php
namespace Stripe;
class Token extends ApiResource {
    public static function retrieve($id, $opts = null) {
        return self::_retrieve($id, $opts);
    }
    public static function create($params = null, $opts = null) {
        return self::_create($params, $opts);
    }
}
