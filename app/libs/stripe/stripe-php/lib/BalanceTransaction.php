<?php
namespace Stripe;
class BalanceTransaction extends ApiResource {
    public static function classUrl() {
        return "/v1/balance/history";
    }
    public static function retrieve($id, $opts = null) {
        return self::_retrieve($id, $opts);
    }
    public static function all($params = null, $opts = null) {
        return self::_all($params, $opts);
    }
}
