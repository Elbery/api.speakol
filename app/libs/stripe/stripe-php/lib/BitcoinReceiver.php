<?php
namespace Stripe;
class BitcoinReceiver extends ApiResource {
    public static function classUrl() {
        return "/v1/bitcoin/receivers";
    }
    public function instanceUrl() {
        $id = $this['id'];
        if (!$id) {
            $class = get_class($this);
            $msg = "Could not determine which URL to request: $class instance " . "has invalid ID: $id";
            throw new Error\InvalidRequest($msg, null);
        }
        $id = ApiRequestor::utf8($id);
        $extn = urlencode($id);
        if (!$this['customer']) {
            $base = BitcoinReceiver::classUrl();
            return "$base/$extn";
        } else {
            $base = Customer::classUrl();
            $parent = ApiRequestor::utf8($this['customer']);
            $parentExtn = urlencode($parent);
            return "$base/$parentExtn/sources/$extn";
        }
    }
    public static function retrieve($id, $opts = null) {
        return self::_retrieve($id, $opts);
    }
    public static function all($params = null, $opts = null) {
        return self::_all($params, $opts);
    }
    public static function create($params = null, $opts = null) {
        return self::_create($params, $opts);
    }
    public function delete($params = null, $opts = null) {
        return $this->_delete($params, $opts);
    }
    public function save($opts = null) {
        return $this->_save($opts);
    }
}
