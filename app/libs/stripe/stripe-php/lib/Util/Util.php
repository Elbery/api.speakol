<?php
namespace Stripe\Util;
use Stripe\Object;
abstract class Util {
    public static function isList($array) {
        if (!is_array($array)) {
            return false;
        }
        foreach (array_keys($array) as $k) {
            if (!is_numeric($k)) {
                return false;
            }
        }
        return true;
    }
    public static function convertStripeObjectToArray($values) {
        $results = array();
        foreach ($values as $k => $v) {
            if ($k[0] == '_') {
                continue;
            }
            if ($v instanceof Object) {
                $results[$k] = $v->__toArray(true);
            } elseif (is_array($v)) {
                $results[$k] = self::convertStripeObjectToArray($v);
            } else {
                $results[$k] = $v;
            }
        }
        return $results;
    }
    public static function convertToStripeObject($resp, $opts) {
        $types = array('account' => 'Stripe\\Account', 'card' => 'Stripe\\Card', 'charge' => 'Stripe\\Charge', 'coupon' => 'Stripe\\Coupon', 'customer' => 'Stripe\\Customer', 'list' => 'Stripe\\Collection', 'invoice' => 'Stripe\\Invoice', 'invoiceitem' => 'Stripe\\InvoiceItem', 'event' => 'Stripe\\Event', 'file' => 'Stripe\\FileUpload', 'token' => 'Stripe\\Token', 'transfer' => 'Stripe\\Transfer', 'plan' => 'Stripe\\Plan', 'recipient' => 'Stripe\\Recipient', 'refund' => 'Stripe\\Refund', 'subscription' => 'Stripe\\Subscription', 'fee_refund' => 'Stripe\\ApplicationFeeRefund', 'bitcoin_receiver' => 'Stripe\\BitcoinReceiver', 'bitcoin_transaction' => 'Stripe\\BitcoinTransaction',);
        if (self::isList($resp)) {
            $mapped = array();
            foreach ($resp as $i) {
                array_push($mapped, self::convertToStripeObject($i, $opts));
            }
            return $mapped;
        } elseif (is_array($resp)) {
            if (isset($resp['object']) && is_string($resp['object']) && isset($types[$resp['object']])) {
                $class = $types[$resp['object']];
            } else {
                $class = 'Stripe\\Object';
            }
            return $class::constructFrom($resp, $opts);
        } else {
            return $resp;
        }
    }
}
