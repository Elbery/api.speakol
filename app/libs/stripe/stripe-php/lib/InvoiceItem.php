<?php
namespace Stripe;
class InvoiceItem extends ApiResource {
    public static function retrieve($id, $opts = null) {
        return self::_retrieve($id, $opts);
    }
    public static function all($params = null, $opts = null) {
        return self::_all($params, $opts);
    }
    public static function create($params = null, $opts = null) {
        return self::_create($params, $opts);
    }
    public function save($opts = null) {
        return $this->_save($opts);
    }
    public function delete($params = null, $opts = null) {
        return $this->_delete($params, $opts);
    }
}
