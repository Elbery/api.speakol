<?php
namespace Stripe;
class Invoice extends ApiResource {
    public static function create($params = null, $opts = null) {
        return self::_create($params, $opts);
    }
    public static function retrieve($id, $opts = null) {
        return self::_retrieve($id, $opts);
    }
    public static function all($params = null, $opts = null) {
        return self::_all($params, $opts);
    }
    public static function upcoming($params = null, $opts = null) {
        $url = static ::classUrl() . '/upcoming';
        list($response, $opts) = static ::_staticRequest('get', $url, $params, $opts);
        return Util\Util::convertToStripeObject($response, $opts);
    }
    public function save($opts = null) {
        return $this->_save($opts);
    }
    public function pay($opts = null) {
        $url = $this->instanceUrl() . '/pay';
        list($response, $opts) = $this->_request('post', $url, null, $opts);
        $this->refreshFrom($response, $opts);
        return $this;
    }
}
