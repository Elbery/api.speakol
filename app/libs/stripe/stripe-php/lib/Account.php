<?php
namespace Stripe;
class Account extends ApiResource {
    public function instanceUrl() {
        if ($this['id'] === null) {
            return '/v1/account';
        } else {
            return parent::instanceUrl();
        }
    }
    public static function retrieve($id = null, $opts = null) {
        if (!$opts && is_string($id) && substr($id, 0, 3) === 'sk_') {
            $opts = $id;
            $id = null;
        }
        return self::_retrieve($id, $opts);
    }
    public static function create($params = null, $opts = null) {
        return self::_create($params, $opts);
    }
    public function save($opts = null) {
        return $this->_save();
    }
    public static function all($params = null, $opts = null) {
        return self::_all($params, $opts);
    }
}
