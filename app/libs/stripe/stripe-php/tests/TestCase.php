<?php
namespace Stripe;
class TestCase extends \PHPUnit_Framework_TestCase {
    const API_KEY = 'tGN0bIwXnHdwOa85VABjPdSn8nWY7G7I';
    protected static function authorizeFromEnv() {
        $apiKey = getenv('STRIPE_API_KEY');
        if (!$apiKey) {
            $apiKey = self::API_KEY;
        }
        Stripe::setApiKey($apiKey);
    }
    protected static function createTestCharge(array $attributes = array()) {
        self::authorizeFromEnv();
        return Charge::create($attributes + array('amount' => 2000, 'currency' => 'usd', 'description' => 'Charge for test@example.com', 'card' => array('number' => '4242424242424242', 'exp_month' => 5, 'exp_year' => date('Y') + 3,),));
    }
    protected static function createTestTransfer(array $attributes = array()) {
        self::authorizeFromEnv();
        $recipient = self::createTestRecipient();
        return Transfer::create($attributes + array('amount' => 2000, 'currency' => 'usd', 'description' => 'Transfer to test@example.com', 'recipient' => $recipient->id));
    }
    protected static function createTestCustomer(array $attributes = array()) {
        self::authorizeFromEnv();
        return Customer::create($attributes + array('card' => array('number' => '4242424242424242', 'exp_month' => 5, 'exp_year' => date('Y') + 3,),));
    }
    protected static function createTestRecipient(array $attributes = array()) {
        self::authorizeFromEnv();
        return Recipient::create($attributes + array('name' => 'PHP Test', 'type' => 'individual', 'tax_id' => '000000000', 'bank_account' => array('country' => 'US', 'routing_number' => '110000000', 'account_number' => '000123456789'),));
    }
    protected static function randomString() {
        $chars = 'abcdefghijklmnopqrstuvwxyz';
        $str = '';
        for ($i = 0;$i < 10;$i++) {
            $str.= $chars[rand(0, strlen($chars) - 1) ];
        }
        return $str;
    }
    protected static function retrieveOrCreatePlan($id) {
        self::authorizeFromEnv();
        try {
            $plan = Plan::retrieve($id);
        }
        catch(Error\InvalidRequest $exception) {
            $plan = Plan::create(array('id' => $id, 'amount' => 0, 'currency' => 'usd', 'interval' => 'month', 'name' => 'Gold Test Plan',));
        }
    }
    protected static function retrieveOrCreateCoupon($id) {
        self::authorizeFromEnv();
        try {
            $coupon = Coupon::retrieve($id);
        }
        catch(Error\InvalidRequest $exception) {
            $coupon = Coupon::create(array('id' => $id, 'duration' => 'forever', 'percent_off' => 25,));
        }
    }
    protected static function generateRandomString($length = 24) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTU';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0;$i < $length;$i++) {
            $randomString.= $characters[rand(0, $charactersLength - 1) ];
        }
        return $randomString;
    }
    protected static function createTestBitcoinReceiver($email) {
        $receiver = BitcoinReceiver::create(array('amount' => 100, 'currency' => 'usd', 'description' => 'some details', 'email' => $email));
        return $receiver;
    }
}
