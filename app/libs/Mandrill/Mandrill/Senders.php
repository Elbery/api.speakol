<?php
class Mandrill_Senders {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function getList() {
        $_params = array();
        return $this->master->call('senders/list', $_params);
    }
    public function domains() {
        $_params = array();
        return $this->master->call('senders/domains', $_params);
    }
    public function addDomain($domain) {
        $_params = array("domain" => $domain);
        return $this->master->call('senders/add-domain', $_params);
    }
    public function checkDomain($domain) {
        $_params = array("domain" => $domain);
        return $this->master->call('senders/check-domain', $_params);
    }
    public function verifyDomain($domain, $mailbox) {
        $_params = array("domain" => $domain, "mailbox" => $mailbox);
        return $this->master->call('senders/verify-domain', $_params);
    }
    public function info($address) {
        $_params = array("address" => $address);
        return $this->master->call('senders/info', $_params);
    }
    public function timeSeries($address) {
        $_params = array("address" => $address);
        return $this->master->call('senders/time-series', $_params);
    }
}
