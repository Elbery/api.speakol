<?php
class Mandrill_Subaccounts {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function getList($q = null) {
        $_params = array("q" => $q);
        return $this->master->call('subaccounts/list', $_params);
    }
    public function add($id, $name = null, $notes = null, $custom_quota = null) {
        $_params = array("id" => $id, "name" => $name, "notes" => $notes, "custom_quota" => $custom_quota);
        return $this->master->call('subaccounts/add', $_params);
    }
    public function info($id) {
        $_params = array("id" => $id);
        return $this->master->call('subaccounts/info', $_params);
    }
    public function update($id, $name = null, $notes = null, $custom_quota = null) {
        $_params = array("id" => $id, "name" => $name, "notes" => $notes, "custom_quota" => $custom_quota);
        return $this->master->call('subaccounts/update', $_params);
    }
    public function delete($id) {
        $_params = array("id" => $id);
        return $this->master->call('subaccounts/delete', $_params);
    }
    public function pause($id) {
        $_params = array("id" => $id);
        return $this->master->call('subaccounts/pause', $_params);
    }
    public function resume($id) {
        $_params = array("id" => $id);
        return $this->master->call('subaccounts/resume', $_params);
    }
}
