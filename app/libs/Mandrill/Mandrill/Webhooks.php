<?php
class Mandrill_Webhooks {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function getList() {
        $_params = array();
        return $this->master->call('webhooks/list', $_params);
    }
    public function add($url, $description = null, $events = array()) {
        $_params = array("url" => $url, "description" => $description, "events" => $events);
        return $this->master->call('webhooks/add', $_params);
    }
    public function info($id) {
        $_params = array("id" => $id);
        return $this->master->call('webhooks/info', $_params);
    }
    public function update($id, $url, $description = null, $events = array()) {
        $_params = array("id" => $id, "url" => $url, "description" => $description, "events" => $events);
        return $this->master->call('webhooks/update', $_params);
    }
    public function delete($id) {
        $_params = array("id" => $id);
        return $this->master->call('webhooks/delete', $_params);
    }
}
