<?php
class Mandrill_Exports {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function info($id) {
        $_params = array("id" => $id);
        return $this->master->call('exports/info', $_params);
    }
    public function getList() {
        $_params = array();
        return $this->master->call('exports/list', $_params);
    }
    public function rejects($notify_email = null) {
        $_params = array("notify_email" => $notify_email);
        return $this->master->call('exports/rejects', $_params);
    }
    public function whitelist($notify_email = null) {
        $_params = array("notify_email" => $notify_email);
        return $this->master->call('exports/whitelist', $_params);
    }
    public function activity($notify_email = null, $date_from = null, $date_to = null, $tags = null, $senders = null, $states = null, $api_keys = null) {
        $_params = array("notify_email" => $notify_email, "date_from" => $date_from, "date_to" => $date_to, "tags" => $tags, "senders" => $senders, "states" => $states, "api_keys" => $api_keys);
        return $this->master->call('exports/activity', $_params);
    }
}
