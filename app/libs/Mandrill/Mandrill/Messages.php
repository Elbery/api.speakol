<?php
class Mandrill_Messages {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function send($message, $async = false, $ip_pool = null, $send_at = null) {
        $_params = array("message" => $message, "async" => $async, "ip_pool" => $ip_pool, "send_at" => $send_at);
        return $this->master->call('messages/send', $_params);
    }
    public function sendTemplate($template_name, $template_content, $message, $async = false, $ip_pool = null, $send_at = null) {
        $_params = array("template_name" => $template_name, "template_content" => $template_content, "message" => $message, "async" => $async, "ip_pool" => $ip_pool, "send_at" => $send_at);
        return $this->master->call('messages/send-template', $_params);
    }
    public function search($query = '*', $date_from = null, $date_to = null, $tags = null, $senders = null, $api_keys = null, $limit = 100) {
        $_params = array("query" => $query, "date_from" => $date_from, "date_to" => $date_to, "tags" => $tags, "senders" => $senders, "api_keys" => $api_keys, "limit" => $limit);
        return $this->master->call('messages/search', $_params);
    }
    public function searchTimeSeries($query = '*', $date_from = null, $date_to = null, $tags = null, $senders = null) {
        $_params = array("query" => $query, "date_from" => $date_from, "date_to" => $date_to, "tags" => $tags, "senders" => $senders);
        return $this->master->call('messages/search-time-series', $_params);
    }
    public function info($id) {
        $_params = array("id" => $id);
        return $this->master->call('messages/info', $_params);
    }
    public function content($id) {
        $_params = array("id" => $id);
        return $this->master->call('messages/content', $_params);
    }
    public function parse($raw_message) {
        $_params = array("raw_message" => $raw_message);
        return $this->master->call('messages/parse', $_params);
    }
    public function sendRaw($raw_message, $from_email = null, $from_name = null, $to = null, $async = false, $ip_pool = null, $send_at = null, $return_path_domain = null) {
        $_params = array("raw_message" => $raw_message, "from_email" => $from_email, "from_name" => $from_name, "to" => $to, "async" => $async, "ip_pool" => $ip_pool, "send_at" => $send_at, "return_path_domain" => $return_path_domain);
        return $this->master->call('messages/send-raw', $_params);
    }
    public function listScheduled($to = null) {
        $_params = array("to" => $to);
        return $this->master->call('messages/list-scheduled', $_params);
    }
    public function cancelScheduled($id) {
        $_params = array("id" => $id);
        return $this->master->call('messages/cancel-scheduled', $_params);
    }
    public function reschedule($id, $send_at) {
        $_params = array("id" => $id, "send_at" => $send_at);
        return $this->master->call('messages/reschedule', $_params);
    }
}
