<?php
class Mandrill_Inbound {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function domains() {
        $_params = array();
        return $this->master->call('inbound/domains', $_params);
    }
    public function addDomain($domain) {
        $_params = array("domain" => $domain);
        return $this->master->call('inbound/add-domain', $_params);
    }
    public function checkDomain($domain) {
        $_params = array("domain" => $domain);
        return $this->master->call('inbound/check-domain', $_params);
    }
    public function deleteDomain($domain) {
        $_params = array("domain" => $domain);
        return $this->master->call('inbound/delete-domain', $_params);
    }
    public function routes($domain) {
        $_params = array("domain" => $domain);
        return $this->master->call('inbound/routes', $_params);
    }
    public function addRoute($domain, $pattern, $url) {
        $_params = array("domain" => $domain, "pattern" => $pattern, "url" => $url);
        return $this->master->call('inbound/add-route', $_params);
    }
    public function updateRoute($id, $pattern = null, $url = null) {
        $_params = array("id" => $id, "pattern" => $pattern, "url" => $url);
        return $this->master->call('inbound/update-route', $_params);
    }
    public function deleteRoute($id) {
        $_params = array("id" => $id);
        return $this->master->call('inbound/delete-route', $_params);
    }
    public function sendRaw($raw_message, $to = null, $mail_from = null, $helo = null, $client_address = null) {
        $_params = array("raw_message" => $raw_message, "to" => $to, "mail_from" => $mail_from, "helo" => $helo, "client_address" => $client_address);
        return $this->master->call('inbound/send-raw', $_params);
    }
}
