<?php
class Mandrill_Metadata {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function getList() {
        $_params = array();
        return $this->master->call('metadata/list', $_params);
    }
    public function add($name, $view_template = null) {
        $_params = array("name" => $name, "view_template" => $view_template);
        return $this->master->call('metadata/add', $_params);
    }
    public function update($name, $view_template) {
        $_params = array("name" => $name, "view_template" => $view_template);
        return $this->master->call('metadata/update', $_params);
    }
    public function delete($name) {
        $_params = array("name" => $name);
        return $this->master->call('metadata/delete', $_params);
    }
}
