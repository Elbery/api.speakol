<?php
class Mandrill_Whitelists {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function add($email, $comment = null) {
        $_params = array("email" => $email, "comment" => $comment);
        return $this->master->call('whitelists/add', $_params);
    }
    public function getList($email = null) {
        $_params = array("email" => $email);
        return $this->master->call('whitelists/list', $_params);
    }
    public function delete($email) {
        $_params = array("email" => $email);
        return $this->master->call('whitelists/delete', $_params);
    }
}
