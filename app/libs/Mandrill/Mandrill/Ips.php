<?php
class Mandrill_Ips {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function getList() {
        $_params = array();
        return $this->master->call('ips/list', $_params);
    }
    public function info($ip) {
        $_params = array("ip" => $ip);
        return $this->master->call('ips/info', $_params);
    }
    public function provision($warmup = false, $pool = null) {
        $_params = array("warmup" => $warmup, "pool" => $pool);
        return $this->master->call('ips/provision', $_params);
    }
    public function startWarmup($ip) {
        $_params = array("ip" => $ip);
        return $this->master->call('ips/start-warmup', $_params);
    }
    public function cancelWarmup($ip) {
        $_params = array("ip" => $ip);
        return $this->master->call('ips/cancel-warmup', $_params);
    }
    public function setPool($ip, $pool, $create_pool = false) {
        $_params = array("ip" => $ip, "pool" => $pool, "create_pool" => $create_pool);
        return $this->master->call('ips/set-pool', $_params);
    }
    public function delete($ip) {
        $_params = array("ip" => $ip);
        return $this->master->call('ips/delete', $_params);
    }
    public function listPools() {
        $_params = array();
        return $this->master->call('ips/list-pools', $_params);
    }
    public function poolInfo($pool) {
        $_params = array("pool" => $pool);
        return $this->master->call('ips/pool-info', $_params);
    }
    public function createPool($pool) {
        $_params = array("pool" => $pool);
        return $this->master->call('ips/create-pool', $_params);
    }
    public function deletePool($pool) {
        $_params = array("pool" => $pool);
        return $this->master->call('ips/delete-pool', $_params);
    }
    public function checkCustomDns($ip, $domain) {
        $_params = array("ip" => $ip, "domain" => $domain);
        return $this->master->call('ips/check-custom-dns', $_params);
    }
    public function setCustomDns($ip, $domain) {
        $_params = array("ip" => $ip, "domain" => $domain);
        return $this->master->call('ips/set-custom-dns', $_params);
    }
}
