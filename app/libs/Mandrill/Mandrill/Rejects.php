<?php
class Mandrill_Rejects {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function add($email, $comment = null, $subaccount = null) {
        $_params = array("email" => $email, "comment" => $comment, "subaccount" => $subaccount);
        return $this->master->call('rejects/add', $_params);
    }
    public function getList($email = null, $include_expired = false, $subaccount = null) {
        $_params = array("email" => $email, "include_expired" => $include_expired, "subaccount" => $subaccount);
        return $this->master->call('rejects/list', $_params);
    }
    public function delete($email, $subaccount = null) {
        $_params = array("email" => $email, "subaccount" => $subaccount);
        return $this->master->call('rejects/delete', $_params);
    }
}
