<?php
class Mandrill_Templates {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function add($name, $from_email = null, $from_name = null, $subject = null, $code = null, $text = null, $publish = true, $labels = array()) {
        $_params = array("name" => $name, "from_email" => $from_email, "from_name" => $from_name, "subject" => $subject, "code" => $code, "text" => $text, "publish" => $publish, "labels" => $labels);
        return $this->master->call('templates/add', $_params);
    }
    public function info($name) {
        $_params = array("name" => $name);
        return $this->master->call('templates/info', $_params);
    }
    public function update($name, $from_email = null, $from_name = null, $subject = null, $code = null, $text = null, $publish = true, $labels = null) {
        $_params = array("name" => $name, "from_email" => $from_email, "from_name" => $from_name, "subject" => $subject, "code" => $code, "text" => $text, "publish" => $publish, "labels" => $labels);
        return $this->master->call('templates/update', $_params);
    }
    public function publish($name) {
        $_params = array("name" => $name);
        return $this->master->call('templates/publish', $_params);
    }
    public function delete($name) {
        $_params = array("name" => $name);
        return $this->master->call('templates/delete', $_params);
    }
    public function getList($label = null) {
        $_params = array("label" => $label);
        return $this->master->call('templates/list', $_params);
    }
    public function timeSeries($name) {
        $_params = array("name" => $name);
        return $this->master->call('templates/time-series', $_params);
    }
    public function render($template_name, $template_content, $merge_vars = null) {
        $_params = array("template_name" => $template_name, "template_content" => $template_content, "merge_vars" => $merge_vars);
        return $this->master->call('templates/render', $_params);
    }
}
