<?php
class Mandrill_Tags {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function getList() {
        $_params = array();
        return $this->master->call('tags/list', $_params);
    }
    public function delete($tag) {
        $_params = array("tag" => $tag);
        return $this->master->call('tags/delete', $_params);
    }
    public function info($tag) {
        $_params = array("tag" => $tag);
        return $this->master->call('tags/info', $_params);
    }
    public function timeSeries($tag) {
        $_params = array("tag" => $tag);
        return $this->master->call('tags/time-series', $_params);
    }
    public function allTimeSeries() {
        $_params = array();
        return $this->master->call('tags/all-time-series', $_params);
    }
}
