<?php
class Mandrill_Users {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function info() {
        $_params = array();
        return $this->master->call('users/info', $_params);
    }
    public function ping() {
        $_params = array();
        return $this->master->call('users/ping', $_params);
    }
    public function ping2() {
        $_params = array();
        return $this->master->call('users/ping2', $_params);
    }
    public function senders() {
        $_params = array();
        return $this->master->call('users/senders', $_params);
    }
}
