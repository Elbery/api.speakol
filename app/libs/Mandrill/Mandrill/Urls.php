<?php
class Mandrill_Urls {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }
    public function getList() {
        $_params = array();
        return $this->master->call('urls/list', $_params);
    }
    public function search($q) {
        $_params = array("q" => $q);
        return $this->master->call('urls/search', $_params);
    }
    public function timeSeries($url) {
        $_params = array("url" => $url);
        return $this->master->call('urls/time-series', $_params);
    }
    public function trackingDomains() {
        $_params = array();
        return $this->master->call('urls/tracking-domains', $_params);
    }
    public function addTrackingDomain($domain) {
        $_params = array("domain" => $domain);
        return $this->master->call('urls/add-tracking-domain', $_params);
    }
    public function checkTrackingDomain($domain) {
        $_params = array("domain" => $domain);
        return $this->master->call('urls/check-tracking-domain', $_params);
    }
}
