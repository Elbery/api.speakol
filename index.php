<?php
try {
    $config = include __DIR__ . "../app/config/config.php";
    include __DIR__ . '../app/config/services.php';
    include __DIR__ . '../app/config/loader.php';
    $oApplication = new \Phalcon\Mvc\Micro();
    API::instance()->setConfig('mainConfiguration', $config)->setConfig('routesConfiguration', new Phalcon\Config\Adapter\Php(__DIR__ . '/../app/config/api-routes.php'))->run($oApplication);
    $oApplication->handle();
}
catch(Phalcon\Exception $e) {
    echo $e->getMessage();
}
catch(PDOException $e) {
    echo $e->getMessage();
}
