<?php

include '../app/plugins/API.php';

try {
    defined('PHALCONDEBUG') || define('PHALCONDEBUG', false);
    $config = include "../app/config/config.php";
    include '../app/config/services.php';
    include '../app/config/loader.php';
    $oApplication = new \Phalcon\Mvc\Micro();
    API::instance()->setConfig('mainConfiguration', $config)->setConfig('routesConfiguration', new Phalcon\Config\Adapter\Php(__DIR__ . '/../app/config/api-routes.php'))->run($oApplication);
    $oApplication->handle();
}
catch(Phalcon\Exception $e) {
    echo $e->getMessage();
}
catch(PDOException $e) {
    echo $e->getMessage();
}
